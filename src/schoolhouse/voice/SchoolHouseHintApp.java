/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package schoolhouse.voice;

import schoolhouse.common.AppCommands;
import schoolhouse.voice.HintWindow;

/**
 *
 * @author jwilliams
 */
public class SchoolHouseHintApp {

    /**
     * @param args the command line arguments
     */
    private VRMessaging mVRThread;
    public static void main(String[] args) {
        // TODO code application logic here
        
        // --- Setup the window
        SchoolHouseHintApp sha = new SchoolHouseHintApp();
        sha.run();
                
        
    }
    
    public void run(){
        HintWindow hw = new HintWindow(null, true);

                
        
        // --- Set up the voice VR Messaging Object
        // --- Set the type for the Dash Thread and VR Thread respectively
         mVRThread = new VRMessaging<AppCommands,String>();
        
        // ---  Initiate the voice recognition engine and thread reading hint messages
         mVRThread.start();
         
         
         hw.setVRMessgingObject(mVRThread);
         
         
//        VRMessaging <String> vr= new VRMessaging<>();              
     //   vr.SendMessage("Test");
        

        //SchoolCmdHints s = (SchoolCmdHints)vrm.ReadMessage();
        hw.setVisible(true);        
    }
    
}
