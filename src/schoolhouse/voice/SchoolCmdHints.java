/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package schoolhouse.voice;

/**
 *
 * @author jwilliams
 */
import java.util.HashMap;
import java.util.Vector;

/**
 *
 * @author wpeets
 * 
 * This class serves as representation of a string based command structure
 * 
 * Ex.
 * Action Vector    Object Vector        (hints) for a fictious role
 * =============    =============        ==========================
 * Add              School               Add Person
 * Remove           Person               Add Grade
 * Change           Mark                 Remove Grade
 * View             Course               Change Grade
 * Import           Class                View School
 * Export           Grade                View Mark
 *                  Notification         View Course
 *                                       View Class
 *                                       View Grade
 *                                       View Notification
 *                                       Import Grade
 *                                       Export Grade
 * 
 * Additional vectors up to 4 
 * can be added or not supplied.  Hints can also not
 * be supplied.  The goal of the hints is to narrow the permutations of 
 * possible solutions.
 * 
 */
public class SchoolCmdHints {
    /**
     * list of explicit hints
     */
    Vector m_cmdHints;
    
    /**
     * list of hint components
     */
    HashMap m_cmdVectors;
    
    /**
     * event handler for events
     */
  //  HandleCmdAction m_eventHandler;
 
    /**
     * list of hint components
     */
    public  SchoolCmdHints( /*HandleCmdAction eventHandler*/ )
    // --- BEGIN
    {
        // --- INITIALIZE
     //   m_eventHandler = eventHandler;
        m_cmdHints = new Vector();
        m_cmdVectors = new HashMap();
        
    }
    // --- END
    
    /**
     * list of hint components
     */
    public void addHint( String newHint )
    // --- BEGIN
    {
        // --- ADD COMMAND
        m_cmdHints.add( newHint );
        
    }
    // --- END
    
    /**
     * list of hint components
     */
    public void addHintVector( String vectorName, Vector hintVector )
    // --- BEGIN
    {
        // --- ADD LIST
        m_cmdVectors.put( vectorName, hintVector );
        
    }
    // --- END

        /**
     * list of hint components
     */
    public Vector getList( String vectorName, Vector hintVector )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        Vector vectorToReturn = null;
        
        // --- IF REQUESTING HINTS
        if( vectorName.equals( "hints") ){
            // --- RETURN HINTS
            vectorToReturn = m_cmdHints;
        }
        // --- ELSE
        else {
            // --- GET LIST
            vectorToReturn = (Vector)m_cmdVectors.get( vectorName );
        }
        // --- ENDIF REQUESTING HINTS
        
        // --- RETURN
        return( vectorToReturn );
    }
    // --- END

}