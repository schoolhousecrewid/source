package schoolhouse.voice;

import java.awt.BorderLayout;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import schoolhouse.common.AppCommands;
import schoolhouse.common.HandleCmdAction;

/**
 *
 * @author J. R.  Williams  Copyright 2014
 * Class that provides Send / Receive interface.  Accepts VR hint
 * assignments and returns voice recognition hits based on assigned hints
 * 
 */
public class VRMessaging <T,U> extends Thread implements Runnable{
    private int                    mQueueSize;
    private ArrayBlockingQueue <T> mArrayBlockingQueue;
    private ArrayBlockingQueue <U> mArrayBlockingQueuevr;
    private boolean                mVoiceAlreadyEnabled;    
            Semaphore              mProcessVoiceSem;    
    
    //private static VRMessaging mInstance = null;
    private VREngineMessageIntfc <U> mVRMIntfc;
    
    private U mHint;
    
    private String mAppIDFocus;
    // --- For Utility function
    private   AppCommands mDashCommand;
    Thread mThread;
    VoiceRecognitionThread<U> mVREngine;
    ConcurrentHashMap<String,AppCommands> mCommandHashMap;
        
    // --- VIEW COMPONENTS
                         JFrame       mVoiceFrame;
                         JPanel       mVoicePanel;
                         JLabel       mStateLabel;
                         JLabel       mTitleLabel;
                         JLabel       mCheckLabel;
                         JList        mListView;
                         JScrollPane  mScrollContainer;
                         boolean      mViewVisible;
                         boolean      mProcessEvents;
                         
                         
    private static final String       mEnableString = "Voice Processing: Enabled";
    private static final String       mDisableString = "Voice Processing: Disabled";
    private static final String       mEmptyString = "Empty List";
    private static final String       mTitleString = "Voice Messaging View";
    private static final String       mCheckString = "Checking: ";
                         DefaultListModel mEmptyList;
                         DefaultListModel mCurrentList;

    public VRMessaging()
    // --- BEGIN
    {
        // --- Default size of one for now
        System.out.println( "VRMessaging::VRMessaging" );
        mCommandHashMap = new ConcurrentHashMap<>();
        this.mQueueSize = 1;
        InitializeQueue();
        InitializeView();
        mAppIDFocus     = "";
        mViewVisible    = false;
        mProcessEvents  = false;
    }
    // --- END
    
    public void InitializeQueue(){
        System.out.println( "VRMessaging::InitializeQueue" );
       mArrayBlockingQueue = new ArrayBlockingQueue<T>(mQueueSize); 
           mArrayBlockingQueuevr = new ArrayBlockingQueue<U>(mQueueSize); 
           
           mVoiceAlreadyEnabled = false;
                      
           mProcessVoiceSem = new Semaphore(1);
           try{
               mProcessVoiceSem.acquire();
           }
           catch(InterruptedException ie){
               System.out.println("Interrupted Exception:"+ ie.getMessage());
           }
    }
    
    /**
     * @brief create view for state information 
     *
     */
    public void InitializeView()
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES            
        System.out.println( "VRMessaging::InitializeView" );
        JComponent newContentPane;
        mVoiceFrame = new JFrame("Voice View");
        mVoiceFrame.setDefaultCloseOperation( DISPOSE_ON_CLOSE );
        mArrayBlockingQueue = new ArrayBlockingQueue<T>(mQueueSize); 
 
        //Create and set up the content pane.
        mVoicePanel = new JPanel( new BorderLayout() );
 
        newContentPane = mVoicePanel;
        newContentPane.setOpaque(true); //content panes must be opaque
        mVoiceFrame.setContentPane(newContentPane);
        mVoiceFrame.setBounds( 750, 25, 200, 900 );
        
        // --- CREATE LIST VIEW
        mEmptyList = new DefaultListModel();
        mEmptyList.addElement( mEmptyString );
        mCurrentList = new DefaultListModel();
        mCurrentList.addElement( mEmptyString );
        mListView = new JList( mCurrentList );
        //mListView.setSelectionMode( ListSelectionModel.SINGLE_SELECTION );
        mScrollContainer = new JScrollPane( mListView );
        mVoicePanel.add( mScrollContainer, BorderLayout.CENTER );
        
        mTitleLabel = new JLabel();
        mTitleLabel.setText( mTitleString );
        mVoicePanel.add( mTitleLabel,BorderLayout.PAGE_START );
        mStateLabel = new JLabel();
        mStateLabel.setText( mDisableString );
        mVoicePanel.add( mStateLabel,BorderLayout.PAGE_END );
        mCheckLabel = new JLabel();
        mCheckLabel.setText( mCheckString );
        mVoicePanel.add( mCheckLabel, BorderLayout.EAST );
        
        //Display the window.
        mVoiceFrame.pack();
        mVoiceFrame.setVisible(false);
    }
    // --- END
    
    /**
     * @brief update list of hints in view     *
     */
    public void UpdateHintsView()
    // --- BEGIN
    {           
        //  --- DECLARE LOCAL VARIABLES
        System.out.println( "VRMessaging::UpdateHintsView" );
        AppCommands mapp;
        HashMap     hintVec;
        Iterator    litr;
        int         cmdKey  = -1;
        String      hintString;
        String      currString;

        // --- IF THE VIEW IS ENABLED
        if( (mViewVisible == true) && (mAppIDFocus != null) ){
            // --- GET CURRENT HINTS            
            mapp    = mCommandHashMap.get( mAppIDFocus );
            mTitleLabel.setText( mTitleString + ": " + mAppIDFocus );
            
            // --- IF THE WINDOW SENT HINTS
            if( mapp != null ){
                // --- GET HINT LIST
                hintVec = mapp.getHints();
                
                // --- IF HINTS ARE IN THE COMMAND
                if( hintVec != null ){
                    // --- DISPLAY HINTS 
                    litr    = hintVec.entrySet().iterator();
                    mCurrentList.clear();

                    // --- DO WHILE MORE HINTS OR COMMAND NOT FOUND
                    while( litr.hasNext() ){
                        Map.Entry mapEntry = (Map.Entry) litr.next();
                        hintString = (String)mapEntry.getValue();
                        cmdKey     = (int)mapEntry.getKey();
                        mCurrentList.addElement( hintString+"("+cmdKey+ ")" );
                    }                
                    // --- ENDDO WHILE MORE HINTS OR COMMAND NOT FOUND
                }
                // --- ENDIF HINTS ARE IN THE COMMAND
            }
            // --- ELSE
            else {
                // --- EMPTY LIST
                mCurrentList.clear();
                mCurrentList.addElement( mEmptyString );
            }
            // --- ENDIF THE WINDOW SENT HINTS
        }
        // --- ENDIF THE VIEW IS ENABLED
    }
    // --- END
    
    //  --- Place a Message in the Q
    public void SendMessage(T aMessage){
        try{
             mArrayBlockingQueue.put(aMessage);
        }
        catch (Exception e){
            System.out.println("Problem putting message:"+ e.getMessage());
        }        
    }
    
    //  --- Place a Message in the Q
    public void SendMessageVR(U aMessage){
        try{
             mArrayBlockingQueuevr.put(aMessage);
        }
        catch (Exception e){
            System.out.println("Problem putting message:"+ e.getMessage());
        }        
    }
    
    //  --- Retrieve Message from the Q    
    public T ReadMessage(){
        T lMessage;        
        lMessage = null;
        
        try{
            lMessage = mArrayBlockingQueue.take();
        }
        catch(Exception e){
            System.out.println("Problem getting message:"+ e.getMessage());
        }
        return lMessage;
    }
    
    
    //  --- Retrieve Message from the Q    
    public U ReadMessageVR(){
        U kMessage;        
        kMessage = null;
        
        try{
            System.out.println("#### taking...");
            kMessage = mArrayBlockingQueuevr.take();
            System.out.println("#### took...");            
        }
        catch(Exception e){
            System.out.println("Problem getting message:"+ e.getMessage());
        }
        return kMessage;
    }
    
    
    public void run(){
        // --- DECLARE LOCAL VARIABLES
        System.out.println( "VRMessaging::run" );
        boolean keepRunning = true;
        
        // --- Create VR Engine INterface
        mThread = new Thread(mVRMIntfc = new VREngineMessageIntfc<U>(){
                    int localTemp;
                    @Override 
                    public void run(){
                        while(true){
                            System.out.println("VRMessaging::run waiting ...");
                            // --- GET VOICE INPUT
                            mHint = ReadMessageVR();
                            
                            // --- DETERMINE IF VOICE INPUT MATCHES HINT VECTOR
                            localTemp = filter( mHint.toString() );
                            
                            // --- IF VOICE INPUT HAS MADE A HIT 
                            // --- ... and events are being processed
                            if( (localTemp > 0 ) && (mProcessEvents == true ) ){
                                // --- HANDLE HINT
                                // --- ... the app must exist at this point
                                AppCommands mapp = mCommandHashMap.get( mAppIDFocus );
                                
                                if( mapp != null ){
                                    HandleCmdAction appToCall = mapp.getEventHandler();
                                    appToCall.handleCmd("hint", null, localTemp);
                                }
                            } 
                            else {
                            } 
                            // --- ENDIF VOICE INPUT HAS MADE A HIT 

                        }                        
                    };
        });
        
        
        // --- Create the Voice Recognition Object
        mVREngine = new VoiceRecognitionThread<U>(mVRMIntfc, mProcessVoiceSem);
        
        // --- Start the VR Thread
        mVREngine.start();                
        
        // ---  Start the thread listening  to response from VR
        mThread.start();
                
        // --- Get the hint from the Hint window
        while( keepRunning == true ){
            try {
                System.out.println("VRMessaging::run Waiting for cmd ...");
                // --- Read Message from DASH
                mDashCommand  = (AppCommands)ReadMessage();
                System.out.println( "VRMessaging::run Command:" + mDashCommand.getCmd().toString() );
                                
                switch(mDashCommand.getCmd()){
                    case Hint:
                        System.out.println( "VRMessaging::run hints" );
                        ProcessHint();
                        //mProcessVoiceSem.release();
                        break;
                    case Enable:
                        System.out.println( "VRMessaging::run enable" );
                        if( false == mVoiceAlreadyEnabled ){
                            mProcessVoiceSem.release();     
                        }
                        mVoiceAlreadyEnabled= true;                        
                        mStateLabel.setText( mEnableString );
                        break;
                    case Disable:
                        System.out.println( "VRMessaging::run disable" );
                        if( true == mVoiceAlreadyEnabled){
                            mProcessVoiceSem.acquire();        
                        }
                        mVoiceAlreadyEnabled= false;
                        mStateLabel.setText( mDisableString );
                        break;
                    case Focus:
                        mAppIDFocus = mDashCommand.getAppId();
                        System.out.println( "VRMessaging::run Focus[" +  mAppIDFocus + "]" );
                        UpdateHintsView();
                        mDashCommand.getEventHandler().handleCmd( AppCommands.CommandValue.Focus.toString(),
                                                                  null, 10000 );
                        break;
                    case Show:
                        // --- SHOW VOICE MESSAGING FRAME
                        System.out.println( "VRMessaging::run show panel]" );
                        mVoiceFrame.setVisible( true );
                        mViewVisible = true;
                        mVoiceFrame.toBack();
                        //mVoiceFrame.setState( Frame.NORMAL );

                    break;
                    case Hide:
                        // --- SHOW VOICE MESSAGING FRAME
                        System.out.println( "VRMessaging::run hide panel" );
                        mVoiceFrame.setVisible( false );
                        mViewVisible = false;
                    break;
                    case Shutdown:
                        // --- TERMINATE THREAD
                        System.out.println( "VRMessaging::run shutdown" );
                        mVoiceFrame.dispose();
                        keepRunning = false;
                        break;
                    case Process:
                        // --- SHOW VOICE MESSAGING FRAME
                        System.out.println( "VRMessaging::run handle events" );
                        mProcessEvents = true;
                    break;
                    case Bypass:
                        // --- SHOW VOICE MESSAGING FRAME
                        System.out.println( "VRMessaging::run ignore events" );
                        mProcessEvents = false;
                    break;
                    case UNKNOWN:
                        break;
                    default:
                        break;
                }
                
                
                //System.out.println("Message from VR:"+s);            
            } catch (InterruptedException ex) {
                Logger.getLogger(VRMessaging.class.getName()).log(Level.SEVERE, null, ex);
            }
            mCheckLabel.setText( mCheckString );
        }
        
    }
    
    /**
     * @brief store incoming hints
     *
     */
    void ProcessHint(){
        //  --- VARIABLES
        String      currentApp;
        
        currentApp   = mDashCommand.getAppId();
        mCommandHashMap.put( currentApp, mDashCommand );    
        System.out.println( "VRMessaging::ProcessHint hints for:"+ currentApp );
        
        // --- IF THE APP MATCHES THE CURRENT VIEW
        if( currentApp.equals( mAppIDFocus ) ){
            // --- UPDATE VIEW
            UpdateHintsView();
        }
        // --- ENDIF THE APP MATCHES THE CURRENT VIEW      
    }
    
    int filter( String aString ){
        //  --- VARIABLES
        AppCommands mapp    = mCommandHashMap.get(mAppIDFocus);
        HashMap     hintVec;
        int         cmdKey  = -1;
        String      currString;
        System.out.println( "VRMessaging::ProcessHints App["+mAppIDFocus+"]" + aString );
        
        // --- SET VIEW
        mCheckLabel.setText( mCheckString + aString );

        // --- IF THE WINDOW THAT HAS FOCUS HAS SENT HINTS
        if( mapp != null ){
            // --- GET HINTS
            hintVec = mapp.getHints();
            
            // --- IF THE HINTS EXISTS
            if( hintVec != null ){
                // --- TRAVERSE List of HiNTS
                Iterator litr = hintVec.entrySet().iterator();
                
                // --- DO WHILE MORE HINTS OR COMMAND NOT FOUND
                while( litr.hasNext() ){
                    Map.Entry mapEntry = (Map.Entry) litr.next();
                    currString = (String)mapEntry.getValue();

                    // --- IF VOICE STRING MATCHES HINT
                    if( currString.compareToIgnoreCase(aString)==0 ){
                        cmdKey = (int)mapEntry.getKey();
                        return cmdKey;
                    } 
                    // --- ENDIF VOICE STRING MATCHES HINT
                }                
                // --- ENDDO WHILE MORE HINTS OR COMMAND NOT FOUND
            }
            // --- ENDIF THE HINTS EXISTS
        }
        // --- ENDIF THE WINDOW THAT HAS FOCUS HAS SENT HINTS
        
        // --- RETURN
        return( cmdKey );        
    }
}
