/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package schoolhouse.voice;

import java.util.concurrent.ArrayBlockingQueue;

/**
 *
 * @author jwilliams
 */
public class VREngineMessageIntfc <T> implements Runnable{
        private ArrayBlockingQueue <T> mArrayBlockingQueue;        

        public VREngineMessageIntfc() {
            // ---
            mArrayBlockingQueue = new ArrayBlockingQueue<>(1);
        }
        
        
        public void run(){    
            
        }
    public T ReadMessageVR(){
        T kMessage;        
        kMessage = null;
        
        try{
            System.out.println("#### H taking...");
            kMessage = mArrayBlockingQueue.take();
            System.out.println("#### H took...");            
        }
        catch(Exception e){
            System.out.println("Problem getting message:"+ e.getMessage());
        }
        return kMessage;
    }
        
        
        public void SendMessage(T aMessage){
            try{
                 mArrayBlockingQueue.put(aMessage);
            }
            catch (Exception e){
                System.out.println("Problem putting message:"+ e.getMessage());
            }        
        }
        
        public void SendMessageVR(T aMessage){
            try{
                 mArrayBlockingQueue.put(aMessage);
            }
            catch (Exception e){
                System.out.println("Problem putting message:"+ e.getMessage());
            }        
        }
        
    
        public T ReadMessage(){
            T lMessage;        
            lMessage = null;

            try{
                lMessage = mArrayBlockingQueue.take();
            }
            catch(Exception e){
                System.out.println("Problem getting message:"+ e.getMessage());
            }
            return lMessage;
        }
    
}
