/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package schoolhouse.voice;
//import edu.cmu.sphinx.frontend.util.Microphone;
import edu.cmu.sphinx.frontend.util.Microphone;
import edu.cmu.sphinx.recognizer.Recognizer;

import edu.cmu.sphinx.result.Result;
import edu.cmu.sphinx.util.props.ConfigurationManager;
import java.net.URL;
import java.util.concurrent.Semaphore;
import schoolhouse.common.AppCommands;

/**
 *
 * @author jwilliams
 * CSC         : Sphinx42.java
 * Description :This is the voice recognition thread.  It is kicked off by the
 *              VRMessaging object
 * 
 * 
 * 
 */
public class VoiceRecognitionThread <U> extends Thread implements Runnable{
    VREngineMessageIntfc<U> mVRMIntfc;

    Semaphore mProcessVoiceSemaphoreRef;

    
    Semaphore s1;
    public VoiceRecognitionThread(VREngineMessageIntfc<U> a, Semaphore aSem){
        mVRMIntfc = a;
        mProcessVoiceSemaphoreRef = aSem;
    }
    
    @Override
    public void run(){
        // --- DECLARE LOCAL VARIABLES
        ConfigurationManager cm;
        URL                  url;
        Recognizer           recognizer;
        Microphone           microphone;
        
        url = VoiceRecognitionThread.class.getResource( "voiceConfig.xml" );
        cm = new ConfigurationManager( url );
        
        recognizer = (Recognizer) cm.lookup( "recognizer" );
        
        // --- IF RECOGNIZER IS FOUND
        if( recognizer != null ){
            // --- ALLOCATE
            recognizer.allocate();    
            microphone = (Microphone) cm.lookup( "microphone" );

            /* the microphone will keep recording until the program exits */
	    if( microphone.startRecording() ) {
                System.out.println("Say: (Good morning | Hello) ( Bhiksha | Evandro | Paul | Philip | Rita | Will )");

                // loop the recognition until the programm exits.
                while (true) {

           	    try{
               		mProcessVoiceSemaphoreRef.acquire();
           	    }
           	    catch(InterruptedException ie){
               		 System.out.println("Interrupted Exception:"+ ie.getMessage());
           	    }

           	    mProcessVoiceSemaphoreRef.release();

                    System.out.println("Say: (Good morning | Hello) ( Bhiksha | Evandro | Paul | Philip | Rita | Will )");
                    System.out.println("Start speaking. Press Ctrl-C to quit.\n");

                    Result result = recognizer.recognize();

                    if (result != null) {
                        String resultText = result.getBestFinalResultNoFiller();
                        System.out.println("You said: " + resultText + '\n');
                        mVRMIntfc.SendMessage((U)resultText);
                    } else {
                        System.out.println("I can't hear what you said.\n");
                    }
                } 
            }
            // --- ELSE
            else{
                System.out.println( "Cannot start microphone." );
                recognizer.deallocate();
            }
        }
        // --- ELSE
        else {
            // --- HANDLE VOICE RECOGNITION ERROR
            System.out.println( "VoiceRecognitionThread::run voice error");
            // TODO code application logic here
            // start the microphone or exit if the programm if this is not possible
                System.out.println("Cannot start microphone.");
                recognizer.deallocate();
                System.exit(1);
            
        }
        // --- ENDIF RECOGNIZER IS FOUND
    }        
}