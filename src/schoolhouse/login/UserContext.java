/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package schoolhouse.login;

import java.util.ArrayList;
import schoolhouse.common.SchoolDbIntfc;

/**
 *
 * @author jwilliams
 */
public class UserContext {
    
    // --- 0 = Receive message
    // --- 1 = Send message    
    private      int mCtxConfiguration;
    
    private static String mCtxUsername = null;
    private static String mCtxRecipentName = null;
    private static String mCtxSubject = null;    
    private static String mCtxRole           ;
    private static String mCtxParticipantId  ;
    private static String mCtxParticipantMessagesTableName = null;
    private static String mCtxParticipantTableName = null;
    private static String mPfromField = null;
    private static String mPToField   = null;
    private static String mCtxDateSent   = null;
    private static String mCtxTimeSent   = null;    
    private static String mCtxMessage    =null;
    // --- Placeholder
    private Object mAuthority  ;
    private int    mSubAuthority;
    
    
    private ArrayList <Object> mClientstList;
    
    static private UserContext mUsrCtxInstance;
    
    static UserContext getInstance(){
        if(null== mUsrCtxInstance){
            mUsrCtxInstance = new UserContext();
        }
        return mUsrCtxInstance;
    }
    
    //public void getFrom
    
    public void setContextConfiguration(int aCtxConfig){
        mCtxConfiguration = aCtxConfig;
        
        switch(aCtxConfig){
            case 1:  //  Send Message
                mCtxParticipantMessagesTableName = FieldTableNamesDatabase.getInstance().getParticipantMessagesTableName();                
                mPToField = FieldTableNamesDatabase.getInstance().getToFieldName();
                mPfromField = FieldTableNamesDatabase.getInstance().getFromFieldName();
                mCtxParticipantTableName = FieldTableNamesDatabase.getInstance().getParticipantTableName();
                mCtxUsername = SchoolDbIntfc.getInstance().getUsername();
                
                
                        
           // --- Establish User Context            
           // UserContext.getInstance().setContextTableName("ParticipantMessagesTable");                                
                break;
            default:
                break;
        }
    }
    
    public void setContextTableName(String aContextTableName){
        mCtxParticipantMessagesTableName = aContextTableName;        
    }
    
    public int getContextConfiguration(){
        return mCtxConfiguration;
    }
    
    public void setUserName(String aUserName){
        mCtxUsername = aUserName;                
    }
    
    public void setSubject(String aSubject){
        mCtxSubject = aSubject;                
    }    

    
    public void setRecipientName(String aRecipientName){
        mCtxRecipentName = aRecipientName;                
    }    
    
    
   public String getParticipantTableName(){
       return mCtxParticipantTableName;
   }

    public String getUserName(){
        return mCtxUsername;                
    }    
    public String getRecipientName(){
        return mCtxRecipentName;                
    }   
    
    public String getmCtxSubject() {
        return mCtxSubject;
    }    

    public String getmDateSent() {
        return mCtxDateSent;
    }

    public void setmDateSent(String aDateSent) {
        mCtxDateSent = aDateSent;
    }

    public String getmTimeSent() {
        return mCtxTimeSent;
    }

    public void setmCtxTimeSent(String aCtxTimeSent) {
        mCtxTimeSent = aCtxTimeSent;
    }

    public String getmCtxMessage() {
        return mCtxMessage;
    }

    public void setmCtxMessage(String aCtxMessage) {
        mCtxMessage = aCtxMessage;
    }
    
}
