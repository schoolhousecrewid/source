/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package schoolhouse.login;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author jwilliams
 */
public class FrontContext extends JFrame implements ActionListener , WindowListener{

    /**
     * @param args the command line arguments
     */
    private GridLayout mGridLayoutMainJPanel;
    private JButton [] mJBArray;
    
    /*    public static void main(String[] args) {
    // TODO code application logic here
    
    FrontContext gw = new FrontContext();
    gw.startFrontContext();
    
    }*/
    
    FrontContext(){

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    
    
    public void startFrontContext(){
        mGridLayoutMainJPanel = new GridLayout(2,2);
        
        mJBArray = new JButton[4];
        mJBArray[0] = new JButton();
        mJBArray[1] = new JButton();
        mJBArray[2] = new JButton();
        mJBArray[3] = new JButton();
        
        
        try {
            Image img = ImageIO.read(getClass().getResource("Purple_001t.png"));
                mJBArray[0].setIcon(new ImageIcon(img));
            Image img_inv = ImageIO.read(getClass().getResource("Purple_001i.png"));                
                mJBArray[0].setPressedIcon(new ImageIcon(img_inv));
                
                
            Image img2 = ImageIO.read(getClass().getResource("Blue_001t.png"));
                mJBArray[1].setIcon(new ImageIcon(img2));
            Image img2i = ImageIO.read(getClass().getResource("Blue_001i.png"));
                mJBArray[1].setPressedIcon(new ImageIcon(img2i));
                
            Image img3 = ImageIO.read(getClass().getResource("Gold_001t.png"));
                mJBArray[2].setIcon(new ImageIcon(img3));
            Image img3i = ImageIO.read(getClass().getResource("Gold_001i.png"));
                mJBArray[2].setPressedIcon(new ImageIcon(img3i));

            Image img4 = ImageIO.read(getClass().getResource("Teal_001t.png"));
                mJBArray[3].setIcon(new ImageIcon(img4));
            Image img4i = ImageIO.read(getClass().getResource("Teal_001i.png"));
                mJBArray[3].setPressedIcon(new ImageIcon(img4i));
                
            } catch (IOException ex) {
        }        

        Dimension jjj = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        System.out.println("Width:"+jjj.getWidth());
        System.out.println("Height:"+jjj.getHeight());
        setSize((int)jjj.getWidth(),(int)jjj.getHeight());
        setLayout(mGridLayoutMainJPanel);
        
        for(int xx =0; xx < mJBArray.length; xx++){
               add(mJBArray[xx]);
               }
        setVisible(true);
        mJBArray[0].addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
               // System.exit(-1);
            }
        });
        mJBArray[1].addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                //System.exit(-1);
            }
        });
        // --- Messaging
        mJBArray[2].addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                /*               MessageFromForm m1 = new MessageFromForm();
                m1.setVisible(true);
                m1.setNameOfSender("Jeff Williams");
                m1.setDateSent("09/24/2014");
                m1.setTimeSent("04:00.00");*/ 
                MessageOptions m2 = new MessageOptions();
                m2.setVisible(true);
                //System.exit(-1);
            }
        });
        mJBArray[3].addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
               // System.exit(-1);
            }
        });
        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        //hrow new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        System.exit(-1);
    }

    @Override
    public void windowOpened(WindowEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowClosing(WindowEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowClosed(WindowEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        System.out.println("Hi Out");
    }

    @Override
    public void windowIconified(WindowEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowActivated(WindowEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
