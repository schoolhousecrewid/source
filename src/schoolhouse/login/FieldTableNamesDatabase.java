/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package schoolhouse.login;

/**
 *
 * @author jwilliams
 */
public class FieldTableNamesDatabase {
    
    // Update these fields if names / columns of the databases change
    // Database wide
    private final String ParticipantTableName          = "ParticipantTable";
    private final String ParticipantMessagesTableName  = "ParticipantMessagesTable";
    
    // --- ParticipcantMessagesTable
    private final String from_Field_ParticipantMessagesTableName            = "pfrom";       
    private final String to_Field_ParticipantMessagesTableName              = "pto";   
    private final String date_sent_Field_ParticipantMessagesTableName       = "date_sent";       
    private final String time_sent_Field_ParticipantMessagesTableName       = "time_sent";       
    private final String message_Field_ParticipantMessagesTableName         = "message";   
    private final String message_read_Field_ParticipantMessagesTableName    = "message_read";     
    private final String subject_Field_ParticipantMessagesTableName         = "subject";         
    
    // --- ParticipantTable
    private final String participant_id_FieldName                 = "participant_id";
    private final String participant_username_FieldName           = "username"     ;
    
    static public FieldTableNamesDatabase gFieldAndDbNamesInstance = null;
    
    FieldTableNamesDatabase(){
        
    }

    
    static public FieldTableNamesDatabase getInstance(){
        if( gFieldAndDbNamesInstance == null){
            gFieldAndDbNamesInstance = new FieldTableNamesDatabase();
        }        
        return gFieldAndDbNamesInstance;        
    }
    
    public String getParticipantTableName(){
        return ParticipantTableName;
    }
    
    public String getParticipantMessagesTableName(){
        return ParticipantMessagesTableName;
    }
    
    public String getParticipantIdFieldName(){
        return participant_id_FieldName;
    }
    
    
    public String getFromFieldName(){
       return from_Field_ParticipantMessagesTableName;
    }
    
    public String getToFieldName(){
       return to_Field_ParticipantMessagesTableName;
    }   
    
    public String getUsernameFieldName(){
       return participant_username_FieldName;
    }      

    public String getDate_sent_Field_ParticipantMessagesTableName() {
        return date_sent_Field_ParticipantMessagesTableName;
    }

    public String getTime_sent_Field_ParticipantMessagesTableName() {
        return time_sent_Field_ParticipantMessagesTableName;
    }

    public String getMessage_Field_ParticipantMessagesTableName() {
        return message_Field_ParticipantMessagesTableName;
    }

    public String getMessage_read_Field_ParticipantMessagesTableName() {
        return message_read_Field_ParticipantMessagesTableName;
    }    
    
    public String getSubject_Field_ParticipantMessagesTableName() {
        return subject_Field_ParticipantMessagesTableName;
    }
    
    
    
}
