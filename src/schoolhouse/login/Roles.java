/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package schoolhouse.login;

/**
 *
 * @author jwilliams
 */
public class Roles {
    static public final int ADMIN_ROLE   = 100;
    static public final int STUDENT_ROLE = 1  ;    
    static public final int PARENT_ROLE  = 2  ;    
    static public final int OTHER_ROLE   = 3  ;    
}
