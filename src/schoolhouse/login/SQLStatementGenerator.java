/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package schoolhouse.login;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import schoolhouse.common.SchoolDbIntfc;

/**
 *
 * @author jwilliams
 */
public class SQLStatementGenerator {
    private UserContext mUserContext;
    public SQLStatementGenerator(){
        
    }
    
    public SQLStatementGenerator(UserContext aUsrCtx){
        mUserContext = aUsrCtx;
    }
    
    public List <String> generateList(){
       List j = new ArrayList <> ();
       
       switch(mUserContext.getContextConfiguration()){
           
           case 0:  // Messaging config
               //  ----  Here the user context information dictates what queries to 
               //  ----  generate
               
               // ---  Generate a list of users you can send and receive messages
               
               
               break;
           case 1: /* Send Message */
               /* --- Need to return a list of queries that perform Send Message*/
               // ---  Establish pto and pfrom
               String lFromString  = UserContext.getInstance().getUserName();               
               String lToString    = UserContext.getInstance().getRecipientName();        
               
               String query1 ="select "+ FieldTableNamesDatabase.getInstance().getParticipantIdFieldName()+
                       " from "+FieldTableNamesDatabase.getInstance().getParticipantTableName()+ " where "+
                       FieldTableNamesDatabase.getInstance().getUsernameFieldName()+"= '"+lFromString+"';";

               String query2 ="select "+ FieldTableNamesDatabase.getInstance().getParticipantIdFieldName()+
                       " from "+FieldTableNamesDatabase.getInstance().getParticipantTableName()+ " where "+
                       FieldTableNamesDatabase.getInstance().getUsernameFieldName()+"= '"+lToString+"';";
               
               String fromId = null;
               String toId   = null;    
               
               // --- Get Intermediate results
               ResultSet r1  = SchoolDbIntfc.getInstance().runQuery(query1);
               try {
                    r1.first();
                    fromId = r1.getString(FieldTableNamesDatabase.getInstance().getParticipantIdFieldName());
               } catch (SQLException ex) {
                    Logger.getLogger(SQLStatementGenerator.class.getName()).log(Level.SEVERE, null, ex);
               }
               
               ResultSet r2  = SchoolDbIntfc.getInstance().runQuery(query2);               
               try {
                    r2.first();
                    toId = r2.getString(FieldTableNamesDatabase.getInstance().getParticipantIdFieldName());
               } catch (SQLException ex) {
                    Logger.getLogger(SQLStatementGenerator.class.getName()).log(Level.SEVERE, null, ex);
               }
               
               // --- Generate the insert query based on the result of the last two queries
               String query3 = "insert into "+ FieldTableNamesDatabase.getInstance().getParticipantMessagesTableName()+
                               " ("+FieldTableNamesDatabase.getInstance().getFromFieldName()+","+
                                    FieldTableNamesDatabase.getInstance().getToFieldName()  +","+
                                    FieldTableNamesDatabase.getInstance().getDate_sent_Field_ParticipantMessagesTableName()+","+
                                    FieldTableNamesDatabase.getInstance().getTime_sent_Field_ParticipantMessagesTableName()+","+                       
                                    FieldTableNamesDatabase.getInstance().getMessage_Field_ParticipantMessagesTableName()  +","+
                                    FieldTableNamesDatabase.getInstance().getMessage_read_Field_ParticipantMessagesTableName()+","+
                                    FieldTableNamesDatabase.getInstance().getSubject_Field_ParticipantMessagesTableName()+") values "+
                                    "( "+fromId+","+toId+",'"+ 
                                    UserContext.getInstance().getmDateSent()+"', '"+
                                    UserContext.getInstance().getmTimeSent()+"', '"+
                                    UserContext.getInstance().getmCtxMessage()+"', "+"'1'"+" ,'"+
                                    UserContext.getInstance().getmCtxSubject()+"');";
               
               SchoolDbIntfc.getInstance().runUpdate( query3, false );
               
              // String lNameToPartId  = null;
              // String lParticipantTable = UserContext.getInstance().getParticipantTableName();
               
               // --- 
               
               
               String s1 = "select pto from";
               
               break;
           default:
               break;
           
       }
       return j;
    }
    
    public String returnQueryString(UserContext aUsrCtx){
        String lQueryString;
        switch(aUsrCtx.getContextConfiguration()){
            case 0:
                break;
            case 1:  // Send Message
                lQueryString = "select pfrom from"; 
                break;
            default:
                break;
                
        }
        return null;
    }
}
