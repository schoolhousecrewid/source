/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package schoolhouse.login;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import schoolhouse.common.SchoolDbIntfc;

/**
 *
 * @author jwilliams
 */
public class CheckMessages extends javax.swing.JFrame {

    /**
     * Creates new form CheckMessages
     */
    public CheckMessages() {
        initComponents();
        mModel = new DefaultListModel();
        jList1.setModel(mModel);
        
        jList1.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                JList list = (JList)evt.getSource();
                if (evt.getClickCount() == 2) {
                    int index = list.locationToIndex(evt.getPoint());
                    showMessage(index);
                } else if (evt.getClickCount() == 3) {   // Triple-click
                    int index = list.locationToIndex(evt.getPoint());
                    showMessage(index);
                }
            }
        });        
    }
    static final String  myParticpantTable = "ParticipantTable";
    static final String  myMessagesTable = "ParticipantMessagesTable";
    String  myParticpantIdQuery; 
    private ResultSet mResultSetParticipantTable;
    private ResultSet mResultSet2;
    private ResultSet mResultSet3;
    private String mParticipantId;
    private String mUsername;
    private String  myMessageQuery;
    private int    mUgotMail;
    
    private String mDate;
    private int    mFromId;
    private String mFrom;
    private String mSubject;
    private String mMessage;
    private DefaultListModel mModel;
    private MessageFromForm mMessageFromForm;


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList();
        jLabel2 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Messages");

        jScrollPane1.setViewportView(jList1);

        jLabel2.setText("Date:            From:                     Subject:                       ");

        jButton1.setText("Check ...");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Exit");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dismissCheckMessages(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
            .addComponent(jScrollPane1)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 380, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 86, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        Object src = evt.getSource();
        if( src == jButton1){
            
            // --- Construct the query for the username
            myParticpantIdQuery = "select * from "+myParticpantTable+" where username = '"+
                                             SchoolDbIntfc.getInstance().getUsername()+"';";
            
            // --- Get the result set
            mResultSetParticipantTable = SchoolDbIntfc.getInstance().runQuery(myParticpantIdQuery);
            
            try {
                // --- Position the cursor
                mResultSetParticipantTable.first();
                
                // --- Get the participant id
                mParticipantId  = mResultSetParticipantTable.getString("participant_id");
                
                // --- Set up the message query for all unread messages to this user
                myMessageQuery = "select * from "+myMessagesTable+" where pto = "+ mParticipantId +
                        " and message_read > 0;"; 
                
                // ---- Query for unread messages
                mResultSetParticipantTable = SchoolDbIntfc.getInstance().runQuery(myMessageQuery);
                
                // --- Move to first column
                mResultSetParticipantTable.first();

                mDate = mResultSetParticipantTable.getString("date_sent");
                mSubject = mResultSetParticipantTable.getString("subject");
                
                // --- Get the from 
                mFromId  = mResultSetParticipantTable.getInt("pfrom");
                
                
                
                // --- Compound query
                String compoundQuery = "select * from ParticipantTable where participant_id="+mFromId+" ;";
                
                // ---- Query for username of sender (from)
                mResultSet2 = SchoolDbIntfc.getInstance().runQuery(compoundQuery);
                
                mResultSet2.first();
                mFrom = mResultSet2.getString("username");
                
                
                String temp = mDate + mFrom+"               " + mSubject;
                mModel.addElement(temp);
                // --- THis is incorrect!!    
                while(mResultSet2.next()){
                    mDate = mResultSet2.getString("date_sent");
                    mSubject = mResultSet2.getString("subject");
                    mMessage = mResultSet2.getString("message");
                    String temp1 = mDate + mFrom+"               " + mSubject;
                    mModel.addElement(temp1);                    
                }
                
               // mResultSet2.getInt("message_read");
                
                
                
                
            } catch (SQLException ex) {
                Logger.getLogger(CheckMessages.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println("SQL Error:"+ex.getMessage());
            }
                      
            
        }
        else if( src == jButton2){
            
        }
        else{
            System.out.println("WTF ??");
            System.exit(-2);            
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void dismissCheckMessages(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dismissCheckMessages
        // TODO add your handling code here:
        setVisible(false);
    }//GEN-LAST:event_dismissCheckMessages

    private void showMessage(int aIndex){
        
        
        try {
            mMessageFromForm = new MessageFromForm();
            mResultSetParticipantTable.first();
            int counter=0;
            while(counter<aIndex){
                mResultSetParticipantTable.next();
                counter++;
            }
            
            Integer lPfrom = mResultSetParticipantTable.getInt("pfrom");
            String temp2 = "select username from ParticipantTable where participant_id IN ( select pfrom from ParticipantMessagesTable where pfrom = "+ lPfrom.toString()+");";
            mResultSet3 = SchoolDbIntfc.getInstance().runQuery(temp2);
            mResultSet3.first();
            
            mFrom = mResultSet3.getString("username");
            mMessageFromForm.setNameOfSender(mFrom);
            mMessageFromForm.setDateSent(mResultSetParticipantTable.getString("date_sent"));
            mMessageFromForm.setTimeSent(mResultSetParticipantTable.getString("time_sent"));
            mMessageFromForm.setSubject(mResultSetParticipantTable.getString("subject"));
            mMessageFromForm.setMessage(mResultSetParticipantTable.getString("message"));
            mMessageFromForm.setVisible(true);

            
        } catch (SQLException ex) {
            Logger.getLogger(CheckMessages.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Show message err"+ex.getMessage());
        }
        
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CheckMessages.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CheckMessages.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CheckMessages.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CheckMessages.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CheckMessages().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JList jList1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
