/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package schoolhouse.common;

import schoolhouse.model.SchoolClass;
import schoolhouse.model.SchoolMessage;
import schoolhouse.model.SchoolMark;
import schoolhouse.model.SchoolHouse;
import schoolhouse.model.SchoolGrade;
import schoolhouse.model.SchoolCourse;
import schoolhouse.model.SchoolNotification;
import schoolhouse.model.SchoolStudent;
import schoolhouse.model.SchoolPerson;
import schoolhouse.model.SchoolPolicy.SchoolRoles;
import java.awt.event.ActionEvent;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import schoolhouse.model.SchoolGradeRank;
import schoolhouse.model.SchoolGradeScale;
import schoolhouse.model.SchoolGradeScale.ScalePoint;
import schoolhouse.model.SchoolNotification.SchoolContactType;
import schoolhouse.model.SchoolNotification.SchoolNotifyType;

/**
 *
 * @author wpeets
 * @brief adds data to module and updates the database
 */
public class SchoolModelUpdater implements HandleCmdAction {    

    /*
     * @brief used to interface with database
    */
    QueryChain             m_dbChain;     // --- USED FOR MASSIVE QUERIES
    QueryThread            m_dbCommand;   // --- USED TO INSERT INTO DATABASE
    SchoolApp              m_appHandle;
    SchoolModel            m_appModel;
    
    /**
     * @param appHandle
     * @param dataModel
     * @brief constructor
     * 
     * This initializes values
     */
    public SchoolModelUpdater( SchoolApp appHandle, SchoolModel dataModel )
    // --- BEGIN
    {
        // --- SET VALUE
        m_appHandle       = appHandle;
        m_appModel        = dataModel;
    }
    // --- END


    @Override
    public void handleCmd(String cmd, String cmdData, int cmdKey) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean handleAction(ActionEvent event) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean handleEvent(String currEvent) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @param currEvent
     * @param resultSet
     * @return 
     * @brief process data from the database
     * 
     */
    @Override
    public boolean handleQuery( String currEvent, ResultSet resultSet )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        boolean eventProcessed  = false;
        
        // --- RETURN
        return( eventProcessed );
    }
    // --- END

    
    /**
     * @brief add data to the model
     * 
     * @param newPerson person object
     * 
     */
    public boolean handlePerson( SchoolPerson newPerson )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        String   insertString;
        Integer  personId;
        Integer  personRole = newPerson.getRole().getValue();
        boolean  returnState = true;
        ArrayList<SchoolPerson> participantList = m_appModel.getPeopleList();
    
        // --- IF THE DATA IS IN THE DATABASE
        if( newPerson.getId() != 0 ){
            // --- ADD A NEW RECORD
            participantList.add( newPerson );
        }
        // --- ELSE
        else {
            // --- UPDATE DATABASE
            insertString = "INSERT INTO ParticipantTable "
                + "(org_id, last_name, first_name, middle_name, nick_name, role_id, title, email, phone, cell_phone, address, user_status, username) "
                + "VALUES ("
                + "'"+ newPerson.getOrgAssignedId()   + "',"
                + "'"+ newPerson.getLastName()        + "',"
                + "'"+ newPerson.getFirstName()       + "',"
                + "'"+ newPerson.getMiddleName()      + "',"
                + "'"+ newPerson.getNickName()        + "',"
                +      personRole.toString()          + ","
                + "'"+ newPerson.getTitle()           + "',"
                + "'"+ newPerson.getEmail()           + "',"
                + "'"+ newPerson.getPhone()           + "',"
                + "'"+ newPerson.getCellPhone()       + "',"
                + "'"+ newPerson.getAddress()         + "',"
                +      newPerson.getUserStatus()      + ","
                + "'"+ newPerson.getUserName()        + "')";
        
            // --- ADD DATA TO DATABASE
            m_dbChain = new QueryChain( m_appHandle, false );        
            m_dbChain.addInsert( insertString,  "newPerson"  );
            m_dbChain.executeChain();
            personId = m_dbChain.getLastId();
            newPerson.setId( personId );
        
            // --- ADD TO MODEL
            participantList.add( newPerson );
            
            // --- IF THE PERSON IS A STUDENT
            if( newPerson.getRole() == SchoolRoles.STUDENT ){
                // --- ADD STUDENT INFO
                updateStudentInfo( newPerson );
            }
            // --- ENDIF THE PERSON IS A STUDENT
        }
        // --- ENDIF THE DATA IS IN THE DATABASE
        
        // --- RETURN
        return( returnState );
    }
    // --- END
    
    
    /**
     * @param personInfo
     * @brief add student supplemental data to the database
     * 
     */
    public void updateStudentInfo( SchoolPerson personInfo )
    // --- BEGIN
    {
        Integer                 personId;
        SchoolPerson            parentInfo;
        SchoolStudent           studentInfo;
        SchoolPerson            watcherInfo;
        ArrayList<SchoolPerson> parentList;
        ArrayList<SchoolPerson> watcherList;
        Integer                 parentId;
        Integer                 watcherId;
        String                  insertString;

        // --- HANDLE DATABASE DATA
        try {
            // --- IF THE PERSON IS A STUDENT
            if( personInfo.getRole() == SchoolRoles.STUDENT ){                    
                // --- GET LIST OF PARENTS
                personId    = personInfo.getId();
                studentInfo = (SchoolStudent)personInfo;
                parentList  = studentInfo.getParentList();
                watcherList = studentInfo.getWatcherList();
                Iterator     parentIter   = parentList.iterator();
                Iterator     watcherIter  = watcherList.iterator();
                m_dbChain = new QueryChain( m_appHandle, true );
                m_dbChain.setHandler( this );
        
                // --- DO FOR EACH PARENT
                while( parentIter.hasNext( ) ) {
                    // --- GET DATA
                    parentInfo = (SchoolPerson)parentIter.next( );
                    parentId   = parentInfo.getId();
            
                    // --- AD UPDATE
                    insertString = "INSERT INTO ParentTable "
                                         + "(student_participant_id, parent_participant_id) "
                                         + "VALUES ("
                                         + personId.toString() 
                                         + "," + parentId.toString()
                                         + ")";
                    m_dbChain.addInsert( insertString, "" );
                }
                // --- ENDDO FOR EACH PARENT
                
                // --- DO FOR EACH WATCHER
                while( watcherIter.hasNext( ) ) {
                    // --- GET DATA
                    watcherInfo = (SchoolPerson)watcherIter.next( );
                    watcherId   = watcherInfo.getId();
            
                    // --- AD UPDATE
                    insertString = "INSERT INTO WatcherTable "
                                         + "(student_participant_id, watcher_participant_id) "
                                         + "VALUES ("
                                         + personId.toString() 
                                         + "," + watcherId.toString()
                                         + ")";
                    m_dbChain.addInsert( insertString, "" );
                }
                // --- ENDDO FOR EACH SCHOOL IN THE MODEL
            }
            // --- ENDDO FOR EACH MARK TYPE NAME
        }
        catch(Exception e){
            // --- IF THE DATA IS EMPTY
            if( e.getMessage() == "null" ){
                // --- LET IT PASS
            }
            // --- ELSE
            else {
                System.out.println( "SQL Problem..." + e.getMessage() );
            }
        }
        // --- ENDOF HANDLE DATABASE
                                    
        // --- ADD ADMIN
        m_dbChain.start();
    }
    // --- END
    
    
    
    public boolean handleSchool( SchoolHouse  newSchool )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        String       insertString;
        int          schoolId = 0;
        boolean      returnState  = true;
        ArrayList<SchoolHouse>  schoolList = m_appModel.getSchoolList();
              
        // --- IF THE DATA IS IN THE DATABASE
        if( newSchool.getId() != 0 ){
            // --- ADD A NEW RECORD
            schoolList.add( newSchool );
        }
        // --- ELSE
        else {
            // --- ADD A NEW RECORD
            insertString = "INSERT INTO SchoolTable "
                       + "(school_name, school_address) "
                       + "VALUES ("
                       + "'"+ newSchool.getName() +    "',"
                       + "'"+ newSchool.getAddress() + "'"
                       + ")";
        
            // --- ADD DATA TO DATABASE
            m_dbChain = new QueryChain( m_appHandle, false );        
            m_dbChain.addInsert( insertString,  "insertschool"  );
            m_dbChain.executeChain();
            schoolId = m_dbChain.getLastId();
            newSchool.setId( schoolId );
        
            // --- ADD TO MODEL
            schoolList.add( newSchool );
            addSchoolAdmin( newSchool );
        }
        // --- ENDIF THE DATA IS IN THE DATABASE
        
        // --- RETURN
        return( returnState );
    }
    // --- END
    
        
    /**
     * @brief process school data from the database
     * 
     * @param schoolInfo school object
     * 
     */
    public void addSchoolAdmin( SchoolHouse schoolInfo )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        Integer                 schoolId = schoolInfo.getId();
        SchoolPerson            adminInfo;
        ArrayList<SchoolPerson> adminList;
        Integer                 adminId;
        String                  insertString;

        // --- HANDLE DATABASE DATA
        try {
            // --- GET LIST OF ADMINS
            adminList = schoolInfo.getAdminList();
            Iterator     adminIter   = adminList.iterator();
            m_dbChain = new QueryChain( m_appHandle, true );
            m_dbChain.setHandler( this );
        
            // --- DO FOR EACH ADMIN IN THE SCHOOL
            while( adminIter.hasNext( ) ) {
                // --- GET DATA
                adminInfo = (SchoolPerson)adminIter.next( );
                adminId   = adminInfo.getId();
            
                // --- AD UPDATE
                insertString = "INSERT INTO AdminTable "
                                     + "(admin_participant_id, school_id) "
                                     + "VALUES ("
                                     + adminId.toString() 
                                     + ","+ schoolId.toString()
                                     + ")";
                m_dbChain.addInsert( insertString, "" );
            }
            // --- ENDDO FOR EACH ADMIN IN THE SCHOOL
        }
        catch(Exception e){
            // --- IF THE DATA IS EMPTY
            if( e.getMessage() == "null" ){
                // --- LET IT PASS
            }
            // --- ELSE
            else {
                System.out.println( "SQL Problem..." + e.getMessage() );
            }
        }
        // --- ENDOF HANDLE DATABASE
                                    
        // --- ADD ADMIN
        m_dbChain.start();
    }
    // --- END

    
    /**
     * @param newCourse
     * @brief add new course information to the database
     * 
     * 
     */
    public boolean handleCourse( SchoolCourse  newCourse )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        String       insertString;
        SchoolHouse  schoolForCourse;
        int          courseId = 0;
        boolean      returnState  = true;
    
        // --- GET SCHOOL FOR COURSE
        schoolForCourse = m_appModel.getSchool( newCourse.getSchool() );
        
        // --- IF THE DATA IS IN THE DATABASE
        if( newCourse.getId() != 0 ){
            // --- ADD A NEW RECORD
            schoolForCourse.addCourse( newCourse );
        }
        // --- ELSE
        else {
            // --- ADD A NEW RECORD
            insertString = "INSERT INTO CourseTable "
                       + "(org_course_id, school_id, course_name, course_credits) "
                       + "VALUES ("
                       + "'"+ newCourse.getOrgAssignedId() + "',"
                       + "'"+ newCourse.getSchool()        + "',"
                       + "'"+ newCourse.getName()          + "',"
                       + "'"+ newCourse.getCredits() + "'"
                       + ")";
        
            // --- ADD DATA TO DATABASE
            m_dbChain = new QueryChain( m_appHandle, false );        
            m_dbChain.addInsert( insertString,  "insertcourse"  );
            m_dbChain.executeChain();
            courseId = m_dbChain.getLastId();
            newCourse.setId( courseId );
        
            // --- ADD TO MODEL
            schoolForCourse.addCourse( newCourse );
        }
        // --- ENDIF THE DATA IS IN THE DATABASE
        
        // --- RETURN
        return( returnState );
    }
    // --- END
    
    /**
     * @param newClass
     * @brief add new class information to the database
     * 
     * 
     */
    public boolean handleClass( SchoolClass  newClass )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        String       insertString;
        SchoolCourse courseForClass;
        Integer      courseId;
        int          classId = 0;
        Integer      scaleId;
        boolean      returnState  = true;
    
        // --- GET SCHOOL FOR COURSE
        courseId = newClass.getCourseId();
        courseForClass = m_appModel.getCourse( courseId );
        
        // --- IF THE DATA IS IN THE DATABASE
        if( newClass.getId() != 0 ){
            // --- ADD A NEW RECORD
            courseForClass.addClass( newClass );
        }
        // --- ELSE
        else {
            // --- GET DATABASE DATA
            scaleId = newClass.getScale().getId();
            
            // --- ADD A NEW RECORD
            insertString = "INSERT INTO ClassTable "
                       + "(course_id, start_date, end_date, time, duration, class_name, scale_id) "
                       + "VALUES ("
                       + "'"+ courseId.toString()         + "',"
                       + "'"+ newClass.getStartDate()     + "',"
                       + "'"+ newClass.getEndDate()       + "',"
                       + "'"+ newClass.getTime()          + "',"
                       + "'"+ newClass.getDuration()      + "',"
                       + "'"+ newClass.getClassName()     + "',"
                       + "'"+ scaleId.toString()          + "'"
                       + ")";
        
            // --- ADD DATA TO DATABASE
            m_dbChain = new QueryChain( m_appHandle, false );        
            m_dbChain.addInsert( insertString,  "insertclass"  );
            m_dbChain.executeChain();
            classId = m_dbChain.getLastId();
            System.out.println( "SchoolModelUpdater::addClass update" + classId );
            newClass.setId( classId );
        
            // --- ADD TO MODEL
            courseForClass.addClass( newClass );
            addSchoolClassInfo( newClass );
        }
        // --- ENDIF THE DATA IS IN THE DATABASE
        
        // --- RETURN
        return( returnState );
    }
    // --- END
    
    
    /**
     * @param newMark
     * @brief add new class mark information to the database
     * 
     * 
     */
    public boolean handleMark( SchoolMark  newMark )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        String           insertString;
        int              markId        = 0;
        int              classId = newMark.getSchoolClass().getId();
        SchoolClass      classForMark  = m_appModel.getClass( classId );
        SchoolGradeScale markScale     = newMark.getScale();
        boolean          returnState   = true;
    
        // --- IF THE DATA IS IN THE DATABASE
        if( newMark.getId() != 0 ){
            // --- ADD A NEW RECORD
            classForMark.addMark( newMark );
        }
        // --- ELSE
        else {
            // --- ADD A NEW RECORD 
            insertString = "INSERT INTO ClassMarkerTable "
                       + "(class_id,  mark_name, date_assigned, date_due, marker_type_id, final_grade_percent, min_grade, max_grade, mark_modifier, scale_id ) "
                       + "VALUES ("
                       + "'"+ classId                     + "',"
                       + "'"+ newMark.getAssignmentName() + "',"
                       + "'"+ newMark.getAssignedDate()   + "',"
                       + "'"+ newMark.getDueDate()        + "',"
                       + "'"+ newMark.getType()           + "',"
                       + "'"+ newMark.getGradePercent()   + "',"
                       + "'"+ newMark.getMinGradeValue()  + "',"
                       + "'"+ newMark.getMaxGradeValue()  + "',"
                       + "'"+ newMark.getModifier()       + "',"
                       + "'"+ markScale.getId()           + "'"
                       + ")";
        
            // --- ADD DATA TO DATABASE
            m_dbChain = new QueryChain( m_appHandle, false );        
            m_dbChain.addInsert( insertString,  "insertmark"  );
            m_dbChain.executeChain();
            markId = m_dbChain.getLastId();
            newMark.setId( markId );
        
            // --- ADD TO MODEL
            classForMark.addMark( newMark );            
        }
        // --- ENDIF THE DATA IS IN THE DATABASE
        
        // --- RETURN
        return( returnState );
    }
    // --- END

    /**
     * @param newGrade
     * @brief add grade data to the database
     * 
     */
    public boolean handleGrade( SchoolGrade  newGrade )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        String          insertString;
        SchoolMark      markForGrade;
        Integer         markId          = 0;
        int             gradeId         = 0;
        Integer         classId;
        Float           newGradeVal;
        Float           newGradeChange;
        float           currentGrade;
        boolean         updateGrade;
        SchoolAppConfig cfg             = SchoolAppConfig.Instance();
        SchoolClass     currentClass;     
        SchoolStudent   currentStudent;     
        boolean         returnState  = true;
        
        // --- GET SCHOOL FOR COURSE
        updateGrade    = cfg.isImmediateUpdate();
        markForGrade   = newGrade.getMark();
        currentClass   = newGrade.getSchoolClass();
        currentStudent = newGrade.getStudent();
        classId        = currentClass.getId();
        markId         = markForGrade.getId();
        newGradeVal    = newGrade.getGrade();
        newGradeChange = newGrade.getGradeChange();
        
        // --- IF THE DATA IS IN THE DATABASE
        if( newGrade.getId() != 0 ){
            // --- ADD A NEW RECORD
            markForGrade.addGrade( newGrade );
        }
        // --- ELSE
        else {
            // --- ADD A NEW RECORD 
            insertString = "INSERT INTO MarkerGradeTable "
                       + "( mark_id, class_id, student_participant_id, date_handed_in, grade, grade_change ) "
                       + "VALUES ("
                       + "'"+ markId.toString()             + "',"
                       + "'"+ classId.toString()            + "',"
                       + "'"+ currentStudent.getId()        + "',"
                       + "'"+ newGrade.getDateReturned()    + "',"
                       + "'"+ newGradeVal.toString()        + "',"
                       + "'"+ newGradeChange.toString()     + "'"
                       + ")";
        
            // --- ADD DATA TO DATABASE
            m_dbChain = new QueryChain( m_appHandle, false );        
            m_dbChain.addInsert( insertString,  "insertgrade"  );
            m_dbChain.executeChain();
            gradeId = m_dbChain.getLastId();
            newGrade.setId( gradeId );
        
            // --- ADD TO MODEL
            markForGrade.addGrade( newGrade );
        }
        // --- ENDIF THE DATA IS IN THE DATABASE
        
        // --- HANDLE FINAL GRADES
        System.out.println( "SchoolModelUpdater::addGrade update" + updateGrade );
        
        // --- IF THE UPDATE IS OCCURRING AT INPUT TIME
        if( updateGrade == true ){
            // --- CALCULATE GRADE
            processFinalGrade( currentClass, currentStudent );

            // --- HANDLE NOTIFICATIONS
            processNotification( newGrade, currentStudent );
            
        }
        // --- ENDIF THE UPDATE IS OCCURRING AT INPUT TIME
        
        // --- RETURN
        return( returnState );
    }
    // --- END

    
    /**
     * @param newClass
     * @brief add class data to the database
     * 
     */
    public void addSchoolClassInfo( SchoolClass newClass )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        Integer      classId = newClass.getId();
        Integer      courseId = newClass.getCourseId();
        SchoolPerson classPersonInfo;
        ArrayList<SchoolPerson> educatorList;
        ArrayList<SchoolPerson> graderList;
        ArrayList<SchoolPerson> studentList;
        Integer      personId;
        String       insertString;

        // --- HANDLE DATABASE DATA
        try {
            // --- GET LIST OF ADMINS
            educatorList = newClass.getEducatorList();
            graderList = newClass.getGraderList();
            studentList = newClass.getStudentList();
            Iterator     educatorIter   = educatorList.iterator();
            Iterator     graderIter   = graderList.iterator();
            Iterator     studentIter   = studentList.iterator();
            m_dbChain = new QueryChain( m_appHandle, true );
            m_dbChain.setHandler( this );
        
            // --- DO FOR EACH EDUCATOR IN THE CLASS
            while( educatorIter.hasNext( ) ) {
                // --- GET DATA
                classPersonInfo = (SchoolPerson)educatorIter.next( );
                personId   = classPersonInfo.getId();
            
                // --- AD UPDATE
                insertString = "INSERT INTO ClassEducatorTable "
                                     + "(class_id, course_id, educator_participant_id) "
                                     + "VALUES ("
                                     + classId.toString()  + ","
                                     + courseId.toString() + ","
                                     + personId.toString()
                                     + ")";
                m_dbChain.addInsert( insertString, "" );
            }
            // --- ENDDO FOR EACH EDUCATOR IN THE CLASS
            
            // --- DO FOR EACH GRADER IN THE CLASS
            while( graderIter.hasNext( ) ) {
                // --- GET DATA
                classPersonInfo = (SchoolPerson)graderIter.next( );
                personId   = classPersonInfo.getId();
            
                // --- AD UPDATE
                insertString = "INSERT INTO ClassGraderTable "
                                     + "(class_id, course_id, grader_participant_id) "
                                     + "VALUES ("
                                     + classId.toString()  + ","
                                     + courseId.toString() + ","
                                     + personId.toString()
                                     + ")";
                m_dbChain.addInsert( insertString, "" );
            }
            // --- ENDDO FOR EACH GRADER IN THE CLASS
            
            // --- DO FOR EACH STUDENT IN THE CLASS
            while( studentIter.hasNext( ) ) {
                // --- GET DATA
                classPersonInfo = (SchoolPerson)studentIter.next( );
                personId   = classPersonInfo.getId();
            
                // --- AD UPDATE
                insertString = "INSERT INTO ClassStudentTable "
                                     + " (class_id, course_id, student_participant_id ,final_grade) "
                                     + "VALUES ("
                                     + classId.toString()  + ","
                                     + courseId.toString() + ","
                                     + personId.toString() + ","
                                     + "-1 "
                                     + ")";
                m_dbChain.addInsert( insertString, "" );
            }
            // --- ENDDO FOR EACH STUDENT IN THE CLASS
        }
        catch(Exception e){
            // --- IF THE DATA IS EMPTY
            if( e.getMessage() == "null" ){
                // --- LET IT PASS
            }
            // --- ELSE
            else {
                System.out.println( "SQL Problem..." + e.getMessage() );
            }
        }
        // --- ENDOF HANDLE DATABASE
                                    
        // --- ADD ADMIN
        m_dbChain.start();
    }
    // --- END
    
    /**
     * @param newNotification
     * @brief add grade data to the database
     * 
     */
    public boolean handleNotification( SchoolNotification  newNotification )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        String              insertString;
        Integer             notifyId  = 0;
        Integer             studentId = 0;
        Integer             classId;
        Integer             personId;
        String              criteria;
        boolean             returnState  = true;
        SchoolNotifyType    notifyType;
        SchoolContactType   contactType;
        String              newContact;
            
        // --- GET SCHOOL FOR COURSE
        studentId      = newNotification.getStudent().getId();
        classId        = newNotification.getSchoolClass().getId();
        notifyType     = newNotification.getType();
        personId       = newNotification.getWatcher().getId();
        criteria       = newNotification.getCriteria();
        contactType    = newNotification.getContactType();
        newContact     = newNotification.getContact();
        
        // --- IF A ALTERNATE CRITERIA IS NOT SET
        if( newContact == null ){
            // --- CONVERT TYPE TO STRING
            newContact = "";
        }
        // --- ENDIF A ALTERNATE CRITERIA IS NOT SET
        
        // --- IF A SPECIAL CRITERIA IS NOT SET
        if( (criteria==null) || (criteria.equals( "" )) ){
            // --- CONVERT TYPE TO STRING
            criteria = notifyType.toString();
        }
        // --- ENDIF A SPECIAL CRITERIA IS NOT SET
        
        // --- IF THE DATA IS IN THE DATABASE
        if( newNotification.getId() != 0 ){
            // --- ADD A NEW RECORD
            newNotification.getStudent().addNotification( newNotification );
        }
        // --- ELSE
        else {
            // --- ADD A NEW RECORD
            insertString = "INSERT INTO MarkerNotificationTable "
                       + "(student_participant_id, parent_watcher_participant_id, class_id, criteria, contact_method, special_contact ) "
                       + "VALUES ("
                       + "'"+ studentId.toString()          + "',"
                       + "'"+ personId.toString()           + "',"
                       + "'"+ classId.toString()            + "',"
                       + "'"+ criteria                      + "',"
                       + "'"+ contactType.toString()        + "',"
                       + "'"+ newContact                    + "'"
                       + ")";
        
            // --- ADD DATA TO DATABASE
            m_dbChain = new QueryChain( m_appHandle, false );        
            m_dbChain.addInsert( insertString,  "insertnotify"  );
            m_dbChain.executeChain();
            notifyId = m_dbChain.getLastId();
            newNotification.setId( notifyId );
        
            // --- ADD TO MODEL
            newNotification.getStudent().addNotification( newNotification );
        }
        // --- ENDIF THE DATA IS IN THE DATABASE
        
        // --- RETURN
        return( returnState );
    }
    // --- END

    /**
     * @brief add message to the model
     * 
     * @param newMessage message object
     * 
     */
    public boolean handleMessage( SchoolMessage newMessage )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        String   insertString;
        Integer  newMessageId;
        Integer  messageState = newMessage.getMsgState().getValue();
        boolean  returnState  = true;
        ArrayList<SchoolMessage> messageList = m_appModel.getMessageList();
    
        // --- IF THE DATA IS IN THE DATABASE
        if( newMessage.getId() != 0 ){
            // --- ADD A NEW RECORD
            messageList.add( newMessage );
        }
        // --- ELSE
        else {
            // --- UPDATE DATABASE
            insertString = "INSERT INTO ParticipantMessagesTable "
                + "(pfrom, pto, msg_type, msg_path, date_sent, time_sent, message, message_read, subject) "
                + "VALUES ("
                + "'"+ newMessage.getSource().getId()        + "',"
                + "'"+ newMessage.getDestination().getId()   + "',"
                + "'"+ newMessage.getNotifyType().toString() + "',"
                + "'"+ newMessage.getPath()                  + "',"
                + "'"+ newMessage.getDate()                  + "',"
                + "'"+ newMessage.getTime()                  + "',"
                + "'"+ newMessage.getMessage()               + "',"
                +      messageState.toString()               + ","
                + "'"+ newMessage.getSubject()        + "')";
        
            // --- ADD DATA TO DATABASE
            m_dbChain = new QueryChain( m_appHandle, false );        
            m_dbChain.addInsert( insertString,  "newMessage"  );
            m_dbChain.executeChain();
            newMessageId = m_dbChain.getLastId();
            newMessage.setId( newMessageId );
        
            // --- ADD TO MODEL
            messageList.add( newMessage ); 
            
            // --- DO CASE OF NOTIFY TYPE
            switch( newMessage.getNotifyType() ){
                // --- EMAIL
                case EMAIL:
                    // --- SEND EMAIL
                    MessageHandler messageHandler = new MessageHandler();
                    SchoolMessage  notifyMessage;
                    notifyMessage = new SchoolMessage( m_appHandle );
                    notifyMessage.setDestination( newMessage.getDestination() );
                    notifyMessage.setSource( newMessage.getSource() );
                    notifyMessage.setSubject( newMessage.getSubject() );
                    notifyMessage.setMessage( newMessage.getMessage() );
                    notifyMessage.setMsgType( SchoolMessage.SchoolMessageType.GENERAL );
                    notifyMessage.setNotifyType( SchoolNotification.SchoolContactType.EMAIL );
                    messageHandler.sendMessage( notifyMessage );
                break;
                // --- CELL
                case CELL:
                break;
                // --- OTHER
                default:
                break;
            }
            // --- ENDDO CASE OF NOTIFY TYPE
        }
        // --- ENDIF THE DATA IS IN THE DATABASE
        
        // --- RETURN
        return( returnState );
    }
    // --- END    
    
    /**
     * @param currClass
     * @param currStudent
     * @brief add grade data to the database
     * 
     */
    public void processFinalGrade( SchoolClass currClass, SchoolStudent currStudent )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        String       updateString;
        Integer      classId           = currClass.getId();
        Integer      studentId         = currStudent.getId();
        Float        newGradeChange;
        Float        average;
        float        currentGrade      = 0;
        float        pointTotal        = 0;
        float        weightTotal       = 0;
        float        gradeMin          = 0;
        float        gradeMax          = 0;
        int          totalGrades       = 0;
        SchoolGrade  gradeInfo;
        SchoolMark   markInfo;
        boolean      foundGrade        = false;
        ArrayList<SchoolMark> markList =  currClass.getMarkList();
        Iterator     markIter          = markList.iterator();
        
        // --- DO FOR EACH MARK
        while( markIter.hasNext() ){
            // --- GET GRADES
            markInfo = (SchoolMark)markIter.next();
            ArrayList<SchoolGrade> gradeList =  markInfo.getGradeList();
            Iterator gradeIter = gradeList.iterator();
            foundGrade = false;
            gradeMin = markInfo.getMinGradeValue();
            gradeMax = markInfo.getMaxGradeValue();
        
            // --- DO FOR EACH MARK IN THE CLASS
            while( (gradeIter.hasNext( )) && ( foundGrade == false) ){
                // --- GET DATA
                gradeInfo = (SchoolGrade)gradeIter.next( );

                // --- IF THERE IS A MATCH
                if( gradeInfo.getStudent() == currStudent ){
                    // --- GET GRADE
                    foundGrade = true;
                    newGradeChange = gradeInfo.getGradeChange();
                    totalGrades++;

                    // --- IF THERE IS NO GRADE CHANGE
                    // --- ...TBD not so hot; zero could be valid?-1
                    if( newGradeChange != -1 ){
                        // --- NEED TO FACTOR IN THE WEIGHT OF EACH GRADE
                        currentGrade = newGradeChange;
                    }
                    // --- ELSE
                    else {
                        // --- NEED TO FACTOR IN THE WEIGHT OF EACH GRADE
                        currentGrade = gradeInfo.getGrade();
                    }
                    // --- ENDIF THERE IS NO GRADE CHANGE
                    
                    // --- CALCULATE GRADE
                    weightTotal = (currentGrade/(gradeMax-gradeMin)) * markInfo.getGradePercent();
                    pointTotal = pointTotal + weightTotal;
                }
                // --- ENDIF THERE IS A MATCH
            }
            // --- ENDDO FOR EACH MARK IN THE CLASS
        }
        // --- ENDDO FOR EACH MARK IN THE CLASS
        
        // --- IF GRADES ARE FOUND
        if( totalGrades > 0 ){
            // --- CALCULATE AVERAGE
            // --- ...TBD should already ne done above
            average = pointTotal/totalGrades;
            currClass.setFinalGrade( currStudent, average );
            
            // --- UPDATE A NEW RECORD 
            updateString = "UPDATE ClassStudentTable SET final_grade="
                           + average.toString()
                           + " WHERE class_id="  + classId.toString()
                           + " and student_participant_id=" + studentId.toString()
                           + ";";
     
            // --- ADD DATA TO DATABASE
            m_dbChain = new QueryChain( m_appHandle, false );        
            m_dbChain.addUpdate( updateString,  "updategrade"  );
            m_dbChain.executeChain();
        }
        // --- ENDIF GRADES ARE FOUND        
    }
    // --- END   
    
    /**
     * @param currClass
     * @param currStudent
     * @brief add grade data to the database
     * 
     */
    public void processNotification( SchoolGrade currGrade, SchoolStudent currStudent )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        ArrayList<SchoolNotification> notificationList = currStudent.getNotificationList();
        Iterator           notifyIter   = notificationList.iterator();
        String             updateString;
        SchoolClass        classForGrade = currGrade.getSchoolClass();
        SchoolNotification notifyInfo;
        SchoolNotifyType   notifyType;
        SchoolContactType  contactType;
        SchoolGradeScale   gradeScale    = classForGrade.getScale();
        SchoolGradeRank    gradeRank = null;
        Float              currentGrade;
        float              newGradeChange = currGrade.getGradeChange();
        SchoolMessage      notifyMessage;
        ArrayList<SchoolPerson> edList = classForGrade.getEducatorList();
        
        // --- IF THERE IS NO GRADE CHANGE
        // --- ...TBD not so hot; zero could be valid?-1
        if( newGradeChange == -1 ){
            // --- NEED TO FACTOR IN THE WEIGHT OF EACH GRADE
            currentGrade = currGrade.getGrade();
        }
        // --- ELSE
        else {
            // --- NEED TO FACTOR IN THE WEIGHT OF EACH GRADE
            currentGrade = newGradeChange;                   
        }
        // --- ENDIF THERE IS NO GRADE CHANGE       
        
        // --- DO FOR EACH NOTIFICATION
        while( notifyIter.hasNext() ){
            // --- GET NOTIFICATION
            notifyInfo = (SchoolNotification)notifyIter.next();
            
            // --- IF NOTIFICATION APPLIES TO THIS CLASS
            if( classForGrade == notifyInfo.getSchoolClass() ){
                // --- GET NOTIFICATION CRITERIA
                notifyType = notifyInfo.getType();
                contactType = notifyInfo.getContactType();
                
                // --- DO CASE OF NOTIFY TYPE
                // --- ...TBD save scale point somewhere and call getRank in one place
                switch( notifyType ){
                    case FAILING: 
                    gradeRank = gradeScale.getRank( ScalePoint.FAIL );
                    break;
                    case BORDERLINE:
                    gradeRank = gradeScale.getRank( ScalePoint.D );
                    break;
                    case PASSING:
                    gradeRank = gradeScale.getRank( ScalePoint.C );
                    break;
                    case GOOD:   
                    gradeRank = gradeScale.getRank( ScalePoint.B );
                    break;
                    case EXCELLENT: 
                    gradeRank = gradeScale.getRank( ScalePoint.A );
                    break;
                    case OTHER:
                        // --- CHECK THE CRITERIA FIELD AND DO SOMETHING SPECIAL
                    break;
                    case UNKNOWN:
                    default:
                    gradeRank = gradeScale.getRank( ScalePoint.FAIL );
                    break;
                }
                // --- ENDDO CASE OF NOTIFY TYPE

                // --- IF GRADE REQUIRES NOTIFICATION
                if( gradeRank.getLevel() >= currentGrade ){
                    // --- CONSTRUCT MESSAGE
                    notifyMessage = new SchoolMessage( m_appHandle );
                    Calendar calendar = Calendar.getInstance();
                    java.util.Date currentDate = calendar.getTime();
                    java.sql.Date date = new java.sql.Date(currentDate.getTime());
                    java.sql.Time now = java.sql.Time.valueOf(
                                        calendar.get(Calendar.HOUR_OF_DAY) + ":" +
                                        calendar.get(Calendar.MINUTE) 
                                                + ":" +
                                        calendar.get(Calendar.SECOND));                    
                    notifyMessage.setDate(date);
                    notifyMessage.setTime(now);
                    notifyMessage.setDestination( notifyInfo.getWatcher() );
                    // --- USE FIRST EDUCATOR FOR SOURCE
                    notifyMessage.setSource( edList.get(0) );
                    String subjectString = "SchoolHouse Notification["+classForGrade.getClassName()+"]";
                    String msgString = "Student:" + currStudent.getLastName()
                                     + "Class:" + classForGrade.getClassName()
                                     + "Grade:" + currentGrade.toString()
                                     + "State:" + gradeRank.getName();
                    notifyMessage.setSubject( subjectString );
                    notifyMessage.setMessage( msgString );
                    notifyMessage.setNotifyType( contactType );
                    notifyMessage.setMsgState(SchoolMessage.SchoolMessageState.UNREAD);
                    handleMessage( notifyMessage );
                    
                }
                // --- ENDIF GRADE REQUIRES NOTIFICATION

                
            }
            // --- ENDIF NOTIFICATION APPLIES TO THIS CLASS
        }
        // --- ENDDO FOR EACH NOTIFICATION
   }
    // --- END
    
}

