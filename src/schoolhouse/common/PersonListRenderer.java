/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package schoolhouse.common;

import schoolhouse.model.SchoolPerson;
import schoolhouse.model.SchoolPolicy;
import static schoolhouse.common.DbEngine.createImageIcon;
import java.awt.Color;
import java.awt.Component;
import java.util.HashMap;
import java.util.Map;
import javax.swing.DefaultListCellRenderer;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import static schoolhouse.common.CmdView.createImageIcon;

/**
 *
 * @author wpeets
 */
public class PersonListRenderer extends DefaultListCellRenderer{
        private Map<Object, Icon> m_icons = null;
    
        public PersonListRenderer(Map<Object, Icon> icons )
        // --- BEGIN
        {
            // --- IF REPLACEMENT IMAGES ARE PASSED
            if( icons != null ){
                this.m_icons = icons;
            }
            // --- ELSE
            else {
                // --- ASSIGN DEFAULT
                ImageIcon  studentIcon = createImageIcon( "images/student.png" );
                ImageIcon  parentIcon  = createImageIcon( "images/parent.png" );
                ImageIcon  educatorIcon = createImageIcon( "images/educator.gif" );
                ImageIcon  graderIcon = createImageIcon( "images/grader.gif" );
                ImageIcon  watcherIcon = createImageIcon( "images/watcher.png" );
                ImageIcon  adminIcon = createImageIcon( "images/admin.gif" );
                ImageIcon  admin2Icon = createImageIcon( "images/manager.gif" );
                ImageIcon  otherIcon = createImageIcon( "images/memo.gif" );
                m_icons = new HashMap<Object, Icon>();
                m_icons.put(SchoolPolicy.SchoolRoles.STUDENT, studentIcon );
                m_icons.put(SchoolPolicy.SchoolRoles.PARENT, parentIcon );
                m_icons.put(SchoolPolicy.SchoolRoles.EDUCATOR, educatorIcon );
                m_icons.put(SchoolPolicy.SchoolRoles.GRADER, graderIcon );
                m_icons.put(SchoolPolicy.SchoolRoles.WATCHER, watcherIcon );
                m_icons.put(SchoolPolicy.SchoolRoles.ADMINISTRATOR, adminIcon );
                m_icons.put(SchoolPolicy.SchoolRoles.MANAGER, admin2Icon );
                m_icons.put(SchoolPolicy.SchoolRoles.OTHER, otherIcon );
            }
             // --- ENDIF REPLACEMENT IMAGES ARE PASSED
       }
        // --- END
 
        public Component getListCellRendererComponent( JList list,
                                                       Object value,
                                                       int index,
                                                       boolean isSelected,
                                                       boolean cellHasFocus ) 
        // --- BEGIN
        {
            // --- DECLARE LOCAL VARIABES
            // --- ... Get the renderer component from parent class
            SchoolPerson person;
            Integer      personId;
            String       personName;
            JLabel       label = null;
            Icon         icon;
 
            // --- IF A NON NULL VALUE IS PASSED
            if( value != null ){
                // --- SET CONTENT
                person     = (SchoolPerson)value;
                personId   = person.getId();
                personName = person.getLastName()  + ", " +  person.getFirstName() + "("  +
                             personId.toString() + ")";
                label      = (JLabel) super.getListCellRendererComponent( list, personName,
                                                                     index, isSelected, cellHasFocus);
 
                // Get icon to use for the list item value
                icon = m_icons.get( person.getRole() );
 
                // Set icon to display for value
                label.setIcon(icon);
        
                if( isSelected ) {
                    label.setBackground(Color.BLUE);
                    label.setForeground(Color.YELLOW);
                } else {
                    label.setForeground(Color.BLACK);
                    label.setBackground(Color.LIGHT_GRAY);
                }
            }
            // --- ENDIF A NON NULL VALUE IS PASSED

            return label;
        }
        // --- END

    /**
     * @return  *  @brief create image resource 
     * @param path name of image file
     * Returns an ImageIcon, 
     * or null if the path was invalid. 
     */
    protected static ImageIcon createImageIcon( String path )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        java.net.URL imgURL      = PersonListRenderer.class.getResource(path);
        ImageIcon    returnImage = null;
        
        if (imgURL != null) {
            returnImage = new ImageIcon(imgURL);
        } else {
            System.err.println( "Couldn't find file: " + path );
        }
        
        return( returnImage );
    }
    // --- END
}
