/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package schoolhouse.common;

import schoolhouse.common.SchoolApp;
import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.ImageIcon;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

/**
 *
 * @author wpeets
 */
public class CmdView extends JPanel implements ActionListener{
    public SchoolApp m_appHandle;

    public CmdView( SchoolApp appHandle)
    // --- BEGIN
    {
        m_appHandle = appHandle;
    }
    // --- END

    public void actionPerformed(ActionEvent e)
    // --- BEGIN
    {
        // --- TBD
        System.err.println( "How did we get here" );
    }
    // --- END
    
    public void showView()
    // --- BEGIN
    {
        // --- TBD
        System.err.println( "How did we get here" );
    }
    // --- END
    
    /** Returns an ImageIcon, or null if the path was invalid. */
    protected static ImageIcon createImageIcon( String path ) 
    // --- BEGIN
    {
        java.net.URL imgURL = CmdView.class.getResource( path );
        if (imgURL != null) {
            return new ImageIcon( imgURL );
        } else {
            System.err.println("Couldn't find file: " + path);
            return null;
        }
    }
    // ---END
    
}
