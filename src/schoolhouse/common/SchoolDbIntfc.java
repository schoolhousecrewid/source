package schoolhouse.common;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author wpeets
 */
public class SchoolDbIntfc extends DbIntfc {
    // --- CLASS MEMBERS
    
    /**
     * @brief this member turns this class into a singleton
     * for use by login
     * TBD does a class need to extend this class and move this variable
     */
    private static SchoolDbIntfc m_SchoolDbIntfc_Instance;

    public enum SchoolQuery {
        POLICY      ( "allpolicy",  "select * from PolicyTable" ),
        SCHOOL      ( "allschool",  "select * from SchoolTable"),
        PARTICIPANT ( "allpeople",  "select * from ParticipantTable" ),
        ROLE        ( "allrole",    "select * from RoleTable" ),
        COURSE      ( "allcourse",  "select * from CourseTable" ),
        CLASS       ( "allclass",   "select * from ClassTable" ),
        MARK_TYPE   ( "alltype",    "select * from MarkerTypeTable"),
        MARKER      ( "allmark",    "select * from ClassMarkerTable" ),
        GRADE       ( "allgrade",   "select * from MarkerGradeTable" ),
        EDUCATOR    ( "teacherlist","select * from ClassEducatorTable"  ),
        GRADER      ( "graderlist", "select * from ClassGraderTable"  ),
        STUDENT     ( "studentlist","select * from ClassStudentTable"  ),
        PARENT      ( "parentlist", "select * from ParentTable"  ),
        WATCHER     ( "watcherlist","select * from WatcherTable"  ),
        NOTIFICATION( "notifylist", "select * from MarkerNotificationTable"  ),
        RANK        ( "ranklist",   "select * from GradeRankTable"  ),
        SCALE       ( "scalelist",  "select * from GradeScaleTable"  ),
        MESSAGE     ( "messagelist","select * from ParticipantMessagesTable"  ),
        USERROLES   ( "userrole",   "select * from ParticipantTable where username='<tag0>';" ),
        ADMINLIST   ( "adlist",     "select * from ParticipantTable where role_id=6 or role_id=7;" ),
        ROLELIST    ( "rolelist",   "select * from RoleTable where role_id=<id0>;" ),
        SCHOOL_ID   ( "schoolid",   "select * from SchoolTable where school_name='<tag0>';" ),
        PERSON_ID   ( "personid",   "select * from ParticipantTable where last_name='<tag0>' and first_name='<tag1>' and role_id=<id0>;" ),
        USER_ID     ( "userid",     "select * from ParticipantTable where username='<tag0>';" ),
        USER_ROLE_ID( "userroleid", "select * from ParticipantTable where username='<tag0>' and role_id=<id0>;" ),
        COURSE_ID   ( "courseid",   "select * from CourseTable where course_name='<tag0>';" ),
        UPDATE_ID   ( "lastid",     "select LAST_INSERT_ID;" ),
        ADMININFO   ( "adminmap",   "select * from AdminTable" ),
        SCHOOLCLASS ( "schoolclass","select a.school_id, a.course_id, " +
                                    " b.class_id, b.start_date, b.end_date, b.time, b.duration, b.class_name" +
                                    " FROM CourseTable a LEFT JOIN ClassTable b " +
                                    " ON a.course_id = b.course_id and a.school_id=<id0>;" ),
        STUDENT_STD ( "stdlist",    "select * from ClassStudentTable where username='<id0>';"  ),
        PARENT_STD  ( "parentstdlist","select student_participant_id from ParentTable where parent_participant_id='<id0>';"  ),
        WATCHER_STD ( "watcherstdlist","select * from ClassStudentTable"  ),
        EDUCATOR_STD( "educatorstdlist","select * from ClassStudentTable"  ),
        GRADER_STD  ( "graderstdlist","select * from ClassStudentTable"  ),
        ADMIN_STD   ( "adminstdlist","select * from ClassStudentTable"  );

        /*
         * @brief sql_query
         */
        private StringBuffer m_query;
        
        /*
         * @brief sql query name
        */
        private final String m_name;
        
        SchoolQuery( String name, String query )
        // --- BEGIN
        {
            this.m_query = new StringBuffer( query );
            this.m_name = name;
        }
        // --- END
        
        public String dbQuery() 
        // --- BGIN
        { 
            return m_query.substring( 0 );
        }   
        // --- END
        
        public String dbQueryId( Integer replaceId, int tagVal ) 
        // --- BEGIN
        {
            // --- DECLARE LOCAL VARIABLES
            Integer       tagValue = tagVal;
            String        searchQuery = "<id" + tagValue.toString() + ">";
            String        replaceWord = replaceId.toString();
            int           startIndexVal = 0;
            int           endIndexVal = 0;
            //StringBuffer  str;

            // --- GET START OF TAG
            //str           = m_query;
            startIndexVal = m_query.indexOf(searchQuery);

            // --- IF THE TAG IS FOUND
            if( startIndexVal > 0 ){
                // --- GET END OF TAG
                endIndexVal = startIndexVal + searchQuery.length();

                // --- REPLACE TAG
                m_query.replace( startIndexVal, endIndexVal, replaceWord );
            }
            // --- ENDIF THE TAG IS FOUND
                        
            // --- RETURN
            return( m_query.substring(0) );
        }
        // --- END
        
        public String dbQueryString( String replaceString, int tagVal ) 
        // --- BEGIN
        {
            // --- DECLARE LOCAL VARIABLES
            Integer      tagValue    = tagVal;
            String       searchQuery = "<tag" + tagValue.toString() + ">";
            String       replaceWord = replaceString;
            int          startIndexVal;
            int          endIndexVal;
            //StringBuffer str;

            // --- GET START OF TAG
            //str           = m_query;
            startIndexVal = m_query.indexOf(searchQuery);

            // --- IF THE TAG IS FOUND
            if( startIndexVal > 0 ){
                // --- GET END OF TAG
                endIndexVal = startIndexVal + searchQuery.length();

                // --- REPLACE TAG
                m_query.replace( startIndexVal, endIndexVal, replaceWord );
            }
            // --- ENDIF THE TAG IS FOUND
            
            // --- RETURN
            return( m_query.substring(0) );
        }
        // --- END
        
        public String dbName() 
        // --- BGIN
        { 
            return m_name;
        }   
        // --- END
    };


    public enum SchoolInsert {
        SCHOOL_INSERT      ( "allschool",   "INSERT INTO schooltable (school_name,school_address) VALUES ('<tag0>','<tag1>')"),
        SCHOOL_ADMIN_INSERT( "schooladmin", "INSERT INTO admintable  (admin_participant_id, school_id) VALUES ('<tag0>',<tag1>)" ),
        COURSE_INSERT      ( "addcourse",   "INSERT INTO coursetable (school_id, course_name) VALUES (<tag0>,'<tag1>')" );

        
        private static final int enumSize = SchoolInsert.values().length;

        /*
         * @brief sql_query
         */
        private StringBuffer m_query;
        
        /*
         * @brief sql query name
        */
        private final String m_name;
        
        SchoolInsert( String name, String query )
        // --- BEGIN
        {
            this.m_query = new StringBuffer( query );
            this.m_name = name;
        }
        // --- END
        
        public String dbQuery() 
        // --- BGIN
        { 
            return m_query.substring( 0 );
        }   
        // --- END
        
        /**
         * @brief allow conversion from ordinal value to enum object
         * 
         * This takes a value and returns an enumeration representation.
         */
        static public SchoolInsert fromString( String value )
        // --- BEGIN
        {
            // --- DECLARE LOCAL VARIABLES
            SchoolInsert returnQuery = null;
            
            // --- DO FOR LIST
            for( SchoolInsert dbQuery: SchoolInsert.values() ){
                // --- IF THE VALUE IS FOUND
                if( dbQuery.m_name.equals( value ) ){
                    returnQuery = dbQuery;
                    break;
                }
                // --- ENDIF THE VALUE IS FOUND
            }
            // --- ENDDO FOR LIST

            // --- RETURN
            return( returnQuery );  
        }  
        // --- END
        
        public String dbQueryId( Integer replaceId, int tagVal ) 
        // --- BEGIN
        {
            // --- DECLARE LOCAL VARIABLES
            Integer       tagValue = tagVal;
            String        searchQuery = "<id" + tagValue.toString() + ">";
            String        replaceWord = replaceId.toString();
            int           startIndexVal = 0;
            int           endIndexVal = 0;
            //StringBuffer  str;

            // --- GET START OF TAG
            //str           = m_query;
            startIndexVal = m_query.indexOf(searchQuery);

            // --- IF THE TAG IS FOUND
            if( startIndexVal > 0 ){
                // --- GET END OF TAG
                endIndexVal = startIndexVal + searchQuery.length();

                // --- REPLACE TAG
                m_query.replace( startIndexVal, endIndexVal, replaceWord );
            }
            // --- ENDIF THE TAG IS FOUND
                        
            // --- RETURN
            return( m_query.substring(0) );
        }
        // --- END
        
        
        public String dbQueryString( String replaceString, int tagVal ) 
        // --- BEGIN
        {
            // --- DECLARE LOCAL VARIABLES
            Integer      tagValue    = tagVal;
            String       searchQuery = "<tag" + tagValue.toString() + ">";
            String       replaceWord = replaceString;
            int          startIndexVal;
            int          endIndexVal;
            //StringBuffer str;

            // --- GET START OF TAG
            //str           = m_query;
            startIndexVal = m_query.indexOf(searchQuery);

            // --- IF THE TAG IS FOUND
            if( startIndexVal > 0 ){
                // --- GET END OF TAG
                endIndexVal = startIndexVal + searchQuery.length();

                // --- REPLACE TAG
                m_query.replace( startIndexVal, endIndexVal, replaceWord );
            }
            // --- ENDIF THE TAG IS FOUND
            
            // --- RETURN
            return( m_query.substring(0) );
        }
        // --- END
        
        
        public String dbName() 
        // --- BGIN
        { 
            return m_name;
        }   
        // --- END
    };

    public enum SchoolDelete {
        PERSON         ( "deleteperson",      "DELETE FROM ParticipantTable WHERE participant_id=<id0>" ),
        PERSON_STUDENT ( "deletestudent",     "DELETE FROM ClassStudentTable WHERE student_participant_id=<id0>" ),
        PERSON_PARENT  ( "deleteparent",      "DELETE FROM ParentTable WHERE parent_participant_id=<id0>" ),
        PERSON_WATCHER ( "deletewatcher",     "DELETE FROM WatcherTable WHERE watcher_participant_id=<id0>" ),
        PERSON_GRADER  ( "deletegrader",      "DELETE FROM ClassGraderTable WHERE grader_participant_id=<id0>" ),
        PERSON_TEACH   ( "deleteeducator",    "DELETE FROM ClassEducatorTable WHERE educator_participant_id=<id0>" ),
        SCHOOL_ADMIN   ( "deleteschooladmin", "DELETE FROM AdminTable  WHERE school_id=<id0>" ),
        ADMIN          ( "deleteadmin",       "DELETE FROM AdminTable  WHERE admin_participant_id=<id0>" ),
        SCHOOL         ( "deleteschool",      "DELETE FROM SchoolTable WHERE school_id=<id0>" ),
        COURSE         ( "deletecourse",      "DELETE FROM CourseTable WHERE course_id=<id0>" ),
        CLASS          ( "deleteclass",       "DELETE FROM ClassTable WHERE class_id=<id0>" ),
        MARK           ( "deletemark",        "DELETE FROM ClassMarkerTable WHERE mark_id=<id0>" ),
        GRADE          ( "deletegrade",       "DELETE FROM MarkerGradeTable WHERE grade_id=<id0>" ),
        NOTIFY         ( "deletenotify",      "DELETE FROM MarkerNotificationTable WHERE notify_id=<id0>" ),
        MESSAGE        ( "deletemessage",     "DELETE FROM ParticipantMessagesTable WHERE id=<id0>" ),
        CLASS_TEACH    ( "deleteclassed",     "DELETE FROM ClassEducatorTable where class_id='<id0>';" ),
        CLASS_GRADER   ( "deleteclasswatch",  "DELETE FROM ClassGraderTable where class_id='<id0>';" ),
        CLASS_STUDENT  ( "deleteclassstud",   "DELETE FROM ClassStudentTable where class_id='<id0>';" ),
        STUDENT_PARENT ( "deleteparent",      "DELETE FROM ParentTable where student_participant_id='<id0>' and parent_participant_id='<id1>';" ),
        STUDENT_WATCHER( "deletewatcher",     "DELETE FROM WatcherTable where student_participant_id='<id0>' and watcher_participant_id='<id1>';" );

        
        private static final int enumSize = SchoolDelete.values().length;

        /*
         * @brief sql_query
         */
        private StringBuffer m_query;
        
        /*
         * @brief sql query name
        */
        private final String m_name;
        
        SchoolDelete( String name, String query )
        // --- BEGIN
        {
            this.m_query = new StringBuffer( query );
            this.m_name = name;
        }
        // --- END
        
        public String dbQuery() 
        // --- BGIN
        { 
            return m_query.substring( 0 );
        }   
        // --- END
        
        /**
         * @brief allow conversion from ordinal value to enum object
         * 
         * This takes a value and returns an enumeration representation.
         */
        static SchoolDelete fromString( String value )
        // --- BEGIN
        {
            // --- DECLARE LOCAL VARIABLES
            SchoolDelete returnQuery = null;
            
            // --- DO FOR LIST
            for( SchoolDelete dbQuery: SchoolDelete.values() ){
                // --- IF THE VALUE IS FOUND
                if( dbQuery.m_name.equals( value ) ){
                    returnQuery = dbQuery;
                    break;
                }
                // --- ENDIF THE VALUE IS FOUND
            }
            // --- ENDDO FOR LIST

            // --- RETURN
            return( returnQuery );  
        }  
        // --- END
        
        public String dbQueryId( Integer replaceId, int tagVal ) 
        // --- BEGIN
        {
            // --- DECLARE LOCAL VARIABLES
            Integer       tagValue = tagVal;
            String        searchQuery = "<id" + tagValue.toString() + ">";
            String        replaceWord = replaceId.toString();
            int           startIndexVal = 0;
            int           endIndexVal = 0;
            //StringBuffer  str;

            // --- GET START OF TAG
            //str           = m_query;
            startIndexVal = m_query.indexOf(searchQuery);

            // --- IF THE TAG IS FOUND
            if( startIndexVal > 0 ){
                // --- GET END OF TAG
                endIndexVal = startIndexVal + searchQuery.length();

                // --- REPLACE TAG
                m_query.replace( startIndexVal, endIndexVal, replaceWord );
            }
            // --- ENDIF THE TAG IS FOUND
                        
            // --- RETURN
            return( m_query.substring(0) );
        }
        // --- END
        
        
        public String dbQueryString( String replaceString, int tagVal ) 
        // --- BEGIN
        {
            // --- DECLARE LOCAL VARIABLES
            Integer      tagValue    = tagVal;
            String       searchQuery = "<tag" + tagValue.toString() + ">";
            String       replaceWord = replaceString;
            int          startIndexVal;
            int          endIndexVal;
            //StringBuffer str;

            // --- GET START OF TAG
            //str           = m_query;
            startIndexVal = m_query.indexOf(searchQuery);

            // --- IF THE TAG IS FOUND
            if( startIndexVal > 0 ){
                // --- GET END OF TAG
                endIndexVal = startIndexVal + searchQuery.length();

                // --- REPLACE TAG
                m_query.replace( startIndexVal, endIndexVal, replaceWord );
            }
            // --- ENDIF THE TAG IS FOUND
            
            // --- RETURN
            return( m_query.substring(0) );
        }
        // --- END
        
        public String dbName() 
        // --- BGIN
        { 
            return m_name;
        }   
        // --- END        
    };

    /**
     * @brief return common instance to interface
     * @return SchoolDbIntfc 
     */
     public static SchoolDbIntfc getInstance(){
        if(m_SchoolDbIntfc_Instance == null){
            m_SchoolDbIntfc_Instance = new SchoolDbIntfc();
        }
        return m_SchoolDbIntfc_Instance;
    }

};

