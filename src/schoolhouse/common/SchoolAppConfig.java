/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package schoolhouse.common;

/**
 *
 * @author wpeets
 * 
 * Pattern: Singleton
 * keep a list of configurable items
 */
public final class SchoolAppConfig extends SchoolHouseConfig {
    private static SchoolAppConfig m_instance  = null;
    
    private SchoolAppConfig()
    // --- BEGIN
    {
    
    }
    // --- END
    
    /**
     *
     * @author wpeets
     * @return static instance
     *     
     */
    public synchronized static SchoolAppConfig Instance()
    // --- BEGIN
    {
        // --- IF OBJECT IS NOT CREATED
        if( m_instance == null ){
            // --- CREATE OBJECT
            m_instance = new SchoolAppConfig();
        }
        // --- ENDIF OBJECT IS NOT CREATED
        
        // --- RETURN
        return( m_instance );
    }
    // --- END    
}
