/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package schoolhouse.common;

import schoolhouse.model.DataObject;
import schoolhouse.model.SchoolClass;
import schoolhouse.model.SchoolMessage;
import schoolhouse.model.SchoolMark;
import schoolhouse.model.SchoolHouse;
import schoolhouse.model.SchoolGradeScale;
import schoolhouse.model.SchoolGradeRank;
import schoolhouse.model.SchoolGrade;
import schoolhouse.model.SchoolCourse;
import schoolhouse.model.SchoolNotification;
import schoolhouse.model.SchoolStudent;
import schoolhouse.model.SchoolPerson;
import schoolhouse.model.SchoolPolicy;
import schoolhouse.model.SchoolPolicy.SchoolRoles;
import java.awt.event.ActionEvent;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Vector;
import schoolhouse.common.SchoolDbIntfc.SchoolQuery;

/**
 *
 * @author wpeets
 */
public class SchoolModel extends DataObject implements HandleCmdAction {    
    /*
     * @brief list of schools supported by the model
     */
    private ArrayList<SchoolHouse>  m_schoolList;
    
    /*
     * @brief list of persons supported by the model
     */
    private ArrayList<SchoolPerson> m_participantList;
    
    /*
     * @brief list of persons supported by the model
     */
    private ArrayList<SchoolGradeRank> m_rankList;
    private ArrayList<SchoolGradeScale> m_scaleList;
    
    /*
     * @brief list of messages
     */
    private ArrayList<SchoolMessage> m_messageList;
    
    /*
     * @brief data policy
     */
    private SchoolPolicy            m_schoolPolicy;

    /*
     * @brief used to interface with database
    */
    QueryChain             m_dbChain;     // --- USED FOR MASSIVE QUERIES
    QueryThread            m_dbCommand;   // --- USED TO INSERT INTO DATABASE
    SchoolApp              m_appHandle;
    DataObject             m_objectUnderTest;
    SchoolModelUpdater     m_updater;
    SchoolModelChanger     m_changer;
    SchoolModelPopulater   m_populater;
    SchoolModelRemover     m_remover;
    
    /**
     * @brief constructor
     * 
     * This initializes values
     */
    public SchoolModel( SchoolApp appHandle )
    // --- BEGIN
    {
        // --- SET VALUE
        m_appHandle       = appHandle;
        m_schoolPolicy    = new SchoolPolicy();
        m_updater         = new SchoolModelUpdater( m_appHandle, this );
        m_populater       = new SchoolModelPopulater( m_appHandle, this );
        m_remover         = new SchoolModelRemover( m_appHandle, this );
        m_changer         = new SchoolModelChanger( m_appHandle, this );
        m_schoolList      = new ArrayList<SchoolHouse>();
        m_participantList = new ArrayList<SchoolPerson>();
        m_messageList     = new ArrayList<SchoolMessage>();
        m_rankList        = new ArrayList<SchoolGradeRank>();
        m_scaleList       = new ArrayList<SchoolGradeScale>();
    }
    // --- END

    /**
     * @brief get access to application policy
     * 
     * The application has access to policy
     */
    public SchoolPolicy getPolicy( )
    // --- BEGIN
    {
        // --- RETURN
        return( m_schoolPolicy );
    }
    // --- END

    /**
     * @brief get access to list school scales
     * 
     * The application has access to policy
     */
    public ArrayList<SchoolGradeScale> getScaleList( )
    // --- BEGIN
    {
        // --- RETURN
        return( m_scaleList );
    }
    // --- END

    /**
     * @brief get access to list or ranks
     * 
     * The application has access to policy
     */
    public ArrayList<SchoolGradeRank> getRankList( )
    // --- BEGIN
    {
        // --- RETURN
        return( m_rankList );
    }
    // --- END

    @Override
    public boolean handleAction(ActionEvent event) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean handleEvent(String currEvent) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public void handleCmd(String cmd, String cmdData, int cmdKey) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    public void getData( Vector dataList )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        ListIterator iter          = dataList.listIterator();
        SchoolQuery  dataRequest;
        
        // --- CREATE THREAD TO GET DATA
        m_dbChain = new QueryChain( m_appHandle, false );
        m_dbChain.setHandler( m_populater );
        
        // --- DO FOR EACH DATA REQUEST
        while( iter.hasNext() ){
            // --- GET DATA TO BE REQUESTED
            dataRequest = (SchoolQuery)iter.next();
            m_dbChain.addQuery( dataRequest.dbQuery(), dataRequest.dbName() );
        }
        // --- ENDDO FOR EACH DATA REQUEST
        
        // --- MAKE DATABASE QUERIES NECESSARY
        m_dbChain.start(); 
    }
    // --- END
    
        

    /**
     * @brief process data from the database
     * 
     */
    @Override
    public boolean handleQuery( String currEvent, ResultSet resultSet )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        boolean eventProcessed = false;
        
        // --- RETURN
        return( eventProcessed );
    }
    // --- END

    
    /**
     * @brief add data to the model
     * 
     * @param newPerson person object
     * 
     */
    public void addPerson( SchoolPerson newPerson )
    // --- BEGIN
    {
        // --- ADD PERSON
        m_updater.handlePerson(newPerson);
        
    }
    // --- END
            
    /**
     * @brief add data to the model
     * 
     * @param newPerson person object
     * 
     */
    public void changePerson( SchoolPerson newPerson )
    // --- BEGIN
    {
        // --- ADD PERSON
        m_changer.handlePerson(newPerson);
        
    }
    // --- END
            
    /**
     * @brief remove data from the model
     * 
     * @param newPerson person object
     * 
     */
    public boolean removePerson( SchoolPerson newPerson )
    // --- BEGIN
    {
        // --- REMOVE PERSON
        return( m_remover.handlePerson(newPerson) );
        
    }
    // --- END
            
                
    public void addSchool( SchoolHouse  newSchool )
    // --- BEGIN
    {
        // --- ADD INFO
        m_updater.handleSchool( newSchool );
    }
    // --- END
    
    public void changeSchool( SchoolHouse  newSchool )
    // --- BEGIN
    {
        // --- ADD INFO
        m_changer.handleSchool( newSchool );
    }
    // --- END
    
    public void removeSchool( SchoolHouse  newSchool )
    // --- BEGIN
    {
        // --- ADD INFO
        m_remover.handleSchool( newSchool );
    }
    // --- END
    
            
    public void addCourse( SchoolCourse  newCourse )
    // --- BEGIN
    {
        // --- ADD INFO
        m_updater.handleCourse( newCourse );
    }
    // --- END
    
    public void changeCourse( SchoolCourse  newCourse )
    // --- BEGIN
    {
        // --- ADD INFO
        m_changer.handleCourse( newCourse );
    }
    // --- END
    
    public void removeCourse( SchoolCourse  newCourse )
    // --- BEGIN
    {
        // --- ADD INFO
        m_remover.handleCourse( newCourse );
    }
    // --- END
    
    public void addClass( SchoolClass  newClass )
    // --- BEGIN
    {
        // --- ADD INFO
        m_updater.handleClass( newClass );
    }
    // --- END
    
    
    public void changeClass( SchoolClass  newClass )
    // --- BEGIN
    {
        // --- ADD INFO
        m_changer.handleClass( newClass );
    }
    // --- END
    
    
    public void removeClass( SchoolClass  newClass )
    // --- BEGIN
    {
        // --- ADD INFO
        m_remover.handleClass( newClass );
    }
    // --- END
    
    
    public void addMark( SchoolMark  newMark )
    // --- BEGIN
    {
        // --- ADD INFO
        m_updater.handleMark( newMark );
    }
    // --- END

    public void changeMark( SchoolMark  newMark )
    // --- BEGIN
    {
        // --- ADD INFO
        m_changer.handleMark( newMark );
    }
    // --- END

    public void removeMark( SchoolMark  newMark )
    // --- BEGIN
    {
        // --- ADD INFO
        m_remover.handleMark( newMark );
    }
    // --- END

    /**
     * @brief add grade data to the database
     * 
     */
    public void addGrade( SchoolGrade  newGrade )
    // --- BEGIN
    {
        // --- ADD INFO
        m_updater.handleGrade( newGrade );
    }
    // --- END
   
    
    /**
     * @brief change grade data to the database
     * 
     */
    public void changeGrade( SchoolGrade  newGrade )
    // --- BEGIN
    {
        // --- ADD INFO
        m_changer.handleGrade( newGrade );
    }
    // --- END
   
    
    /**
     * @brief remove grade from the database
     * 
     */
    public void removeGrade( SchoolGrade  newGrade )
    // --- BEGIN
    {
        // --- ADD INFO
        m_remover.handleGrade( newGrade );
    }
    // --- END
   
    
    /**
     * @brief add notification data to the database
     * 
     */
    public void addNotification( SchoolNotification  newNotification )
    // --- BEGIN
    {
        // --- ADD INFO
        m_updater.handleNotification( newNotification );
    }
    // --- END

    /**
     * @brief add notification data to the database
     * 
     */
    public void changeNotification( SchoolNotification  newNotification )
    // --- BEGIN
    {
        // --- ADD INFO
        m_changer.handleNotification( newNotification );
    }
    // --- END

    /**
     * @brief remove notification data from the database
     * 
     */
    public void removeNotification( SchoolNotification  newNotification )
    // --- BEGIN
    {
        // --- ADD INFO
        m_remover.handleNotification( newNotification );
    }
    // --- END

    /**
     * @brief add message to the model
     * 
     * @param newMessage message object
     * 
     */
    public void addMessage( SchoolMessage newMessage )
    // --- BEGIN
    {
        // --- ADD INFO
        m_updater.handleMessage( newMessage );
    }
    // --- END
       
    /**
     * @brief change message to the model
     * 
     * @param newMessage message object
     * 
     */
    public void changeMessage( SchoolMessage newMessage )
    // --- BEGIN
    {
        // --- ADD INFO
        m_changer.handleMessage( newMessage );
    }
    // --- END
       
    /**
     * @brief remove message from the model
     * 
     * @param newMessage message object
     * 
     */
    public void removeMessage( SchoolMessage newMessage )
    // --- BEGIN
    {
        // --- ADD INFO
        m_remover.handleMessage( newMessage );
    }
    // --- END
       
    
    /**
     * 
     * @param schoolId
     * @return reference to school data; null if not in model
     */
    public SchoolHouse getSchool( Integer schoolId )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolHouse returnSchool = null;
        Iterator    schoolIter   = m_schoolList.iterator();
        SchoolHouse schoolInfo;
        
        // --- DO FOR EACH SCHOOL IN THE MODEL
        while( schoolIter.hasNext( ) ) {
            // --- GET DATA
            schoolInfo = (SchoolHouse)schoolIter.next( );
            
            // --- IF THERE IS A MATCH
            if( schoolInfo.getId() == schoolId ){
                // --- SET RETURN VALUE
                // --- ... TBD need to break out
                returnSchool = schoolInfo;
            }
            // --- ENDIF THERE IS A MATCH           
        }
        // --- ENDDO FOR EACH SCHOOL IN THE MODEL
        
        // --- RETURN
        return( returnSchool );
    }
    // --- END  
    
    /**
     * 
     * @param schoolName
     * @return reference to school data; null if not in model
     */
    public SchoolHouse getSchool( String schoolName )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolHouse returnSchool = null;
        Iterator    schoolIter   = m_schoolList.iterator();
        SchoolHouse schoolInfo;
        
        // --- DO FOR EACH SCHOOL IN THE MODEL
        while( schoolIter.hasNext( ) ) {
            // --- GET DATA
            schoolInfo = (SchoolHouse)schoolIter.next( );
            
            // --- IF THERE IS A MATCH
            if( schoolName.equals( schoolInfo.getName() ) ){
                // --- SET RETURN VALUE
                returnSchool = schoolInfo;
            }
            // --- ENDIF THERE IS A MATCH
            
        }
        // --- ENDDO FOR EACH SCHOOL IN THE MODEL
        
        // --- RETURN
        return( returnSchool );
    }
    // --- END
 
    
    /**
     * 
     * @param personlId
     * @return reference to person data; null if not in model
     */
    public SchoolPerson getPerson( Integer personId )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolPerson returnPerson = null;
        Iterator     personIter   = m_participantList.iterator();
        SchoolPerson personInfo;
        
        // --- DO FOR EACH SCHOOL IN THE MODEL
        while( personIter.hasNext( ) && (returnPerson == null) ){
            // --- GET DATA
            personInfo = (SchoolPerson)personIter.next( );
            
            // --- IF THERE IS A MATCH
            if( personInfo.getId() == personId ){
                // --- SET RETURN VALUE
                returnPerson = personInfo;
            }
            // --- ENDIF THERE IS A MATCH
        }
        // --- ENDDO FOR EACH SCHOOL IN THE MODEL
        
        // --- RETURN
        return( returnPerson );
    }
    // --- END

    /**
     * 
     * @param courseId
     * @return reference to school course data; null if not in model
     */
    public SchoolCourse getCourse( Integer courseId )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolCourse  returnCourse = null;
        Iterator      schoolIter   = m_schoolList.iterator();
        SchoolHouse   schoolInfo;
        SchoolCourse  courseInfo;
        boolean       foundCourse  = false;
        
        // --- DO FOR EACH SCHOOL IN THE MODEL
        while( schoolIter.hasNext( ) && (foundCourse == false) ){
            // --- GET DATA
            schoolInfo = (SchoolHouse)schoolIter.next( );
            returnCourse = schoolInfo.getCourse( courseId );
            
            // --- IF THERE IS A MATCH
            if( returnCourse != null ){
                // --- SET RETURN VALUE
                // --- ... TBD need to break out
                foundCourse = true;
            }
            // --- ENDIF THERE IS A MATCH           
        }
        // --- ENDDO FOR EACH SCHOOL IN THE MODEL
        
        // --- RETURN
        return( returnCourse );
    }
    // --- END  
    
    /**
     * 
     * @param classId
     * @return reference to school class data; null if not in model
     */
    public SchoolClass getClass( Integer classId )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolClass   returnClass = null;
        Iterator      schoolIter   = m_schoolList.iterator();
        SchoolHouse   schoolInfo;
        SchoolCourse  courseInfo;
        boolean       foundClass  = false;
        
        // --- DO FOR EACH SCHOOL IN THE MODEL
        while( schoolIter.hasNext( ) && (foundClass == false) ){
            // --- GET DATA
            schoolInfo = (SchoolHouse)schoolIter.next( );
            ArrayList<SchoolCourse> courseList = schoolInfo.getCourseList();
            Iterator      courseIter   = courseList.iterator();
            
            // --- DO FOR EACH COURSE IN THE SCHOOL
            while( courseIter.hasNext( ) && (foundClass == false) ){
                // --- GET DATA
                courseInfo = (SchoolCourse)courseIter.next( );
                returnClass = courseInfo.getClass( classId );

                // --- IF THERE IS A MATCH
                if( returnClass != null ){
                    // --- SET RETURN VALUE
                    // --- ... TBD need to break out
                    foundClass = true;
                }
                // --- ENDIF THERE IS A MATCH           
            }
            // --- ENDDO FOR EACH COURSE IN THE SCHOOL            
        }
        // --- ENDDO FOR EACH SCHOOL IN THE MODEL
        
        // --- RETURN
        return( returnClass );
    }
    // --- END  
    
    /**
     * 
     * @param markId
     * @return reference to class mark data; null if not in model
     */
    public SchoolMark getMark( Integer markId )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolMark    returnMark = null;
        Iterator      schoolIter   = m_schoolList.iterator();
        SchoolHouse   schoolInfo;
        SchoolCourse  courseInfo;
        SchoolClass   classInfo;
        boolean       foundMark  = false;
        
        // --- DO FOR EACH SCHOOL IN THE MODEL
        while( schoolIter.hasNext( ) && (foundMark == false) ){
            // --- GET DATA
            schoolInfo = (SchoolHouse)schoolIter.next( );
            ArrayList<SchoolCourse> courseList = schoolInfo.getCourseList();
            Iterator      courseIter   = courseList.iterator();
            
            // --- DO FOR EACH COURSE IN THE SCHOOL
            while( courseIter.hasNext( ) && (foundMark == false) ){
                // --- GET DATA
                courseInfo = (SchoolCourse)courseIter.next( );
                ArrayList<SchoolClass> classList = courseInfo.getClassList();
                Iterator      classIter   = classList.iterator();
                
                // --- DO FOR EACH CLASS IN THE COURSE
                while( classIter.hasNext( ) && (foundMark == false) ){
                    // --- GET DATA
                    classInfo = (SchoolClass)classIter.next( );
                    returnMark = classInfo.getMark( markId );

                    // --- IF THERE IS A MATCH
                    if( returnMark != null ){
                        // --- SET RETURN VALUE
                        // --- ... TBD need to break out
                        foundMark = true;
                    }
                    // --- ENDIF THERE IS A MATCH           
                }
                // --- ENDDO FOR EACH CLASS IN THE COURSE            
            }
            // --- ENDDO FOR EACH COURSE IN THE SCHOOL            
        }
        // --- ENDDO FOR EACH SCHOOL IN THE MODEL
        
        // --- RETURN
        return( returnMark );
    }
    // --- END  
    
    
    
    /**
     * 
     * @param gradeId
     * @return reference to class mark data; null if not in model
     */
    public SchoolGrade getGrade( Integer gradeId )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolGrade   returnGrade = null;
        Iterator      schoolIter  = m_schoolList.iterator();
        SchoolHouse   schoolInfo;
        SchoolCourse  courseInfo;
        SchoolClass   classInfo;
        SchoolMark    markInfo;
        boolean       foundGrade   = false;
        
        // --- DO FOR EACH SCHOOL IN THE MODEL
        while( schoolIter.hasNext( ) && (foundGrade == false) ){
            // --- GET DATA
            schoolInfo = (SchoolHouse)schoolIter.next( );
            ArrayList<SchoolCourse> courseList = schoolInfo.getCourseList();
            Iterator      courseIter   = courseList.iterator();
            
            // --- DO FOR EACH COURSE IN THE SCHOOL
            while( courseIter.hasNext( ) && (foundGrade == false) ){
                // --- GET DATA
                courseInfo = (SchoolCourse)courseIter.next( );
                ArrayList<SchoolClass> classList = courseInfo.getClassList();
                Iterator      classIter   = classList.iterator();
                
                // --- DO FOR EACH CLASS IN THE COURSE
                while( classIter.hasNext( ) && (foundGrade == false) ){
                    // --- GET DATA
                    classInfo = (SchoolClass)classIter.next( );
                    ArrayList<SchoolMark> markList = classInfo.getMarkList();
                    Iterator      markIter   = markList.iterator();

                    // --- DO FOR EACH CLASS IN THE COURSE
                    while( markIter.hasNext( ) && (foundGrade == false) ){
                        // --- GET DATA
                        markInfo = (SchoolMark)markIter.next( );
                        returnGrade = markInfo.getGrade( gradeId );

                        // --- IF THERE IS A MATCH
                        if( returnGrade != null ){
                            // --- SET RETURN VALUE
                            // --- ... TBD need to break out
                            foundGrade = true;
                        }
                        // --- ENDIF THERE IS A MATCH           
                    }
                    // --- ENDDO FOR EACH CLASS IN THE COURSE            
                }
                // --- ENDDO FOR EACH CLASS IN THE COURSE            
            }
            // --- ENDDO FOR EACH COURSE IN THE SCHOOL            
        }
        // --- ENDDO FOR EACH SCHOOL IN THE MODEL
        
        // --- RETURN
        return( returnGrade );
    }
    // --- END  
    /**
     * 
     * @param notifyId
     * @return reference to notification data; null if not in model
     */
    public SchoolNotification getNotification( Integer notifyId )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolNotification returnNotification = null;
        Iterator           personIter   = m_participantList.iterator();
        SchoolPerson       personInfo;
        SchoolStudent      schoolStudent;
        SchoolNotification notifyInfo = null;
        ArrayList<SchoolNotification> notificationList;
    
        // --- DO FOR EACH PERSON IN THE MODEL
        while( personIter.hasNext( ) && (returnNotification == null) ){
            // --- GET DATA
            personInfo = (SchoolPerson)personIter.next( );
            
            // --- IF PERSON IS A STUDENT
            if( personInfo.getRole() == SchoolRoles.STUDENT ){
                // --- GET NOTIFICATION LIST
                schoolStudent = (SchoolStudent)personInfo;
                notificationList = schoolStudent.getNotificationList();
                Iterator notifyIter   = notificationList.iterator();
                
                // --- DO FOR EACH NOTIFY OBJECT
                while( notifyIter.hasNext( ) && (returnNotification == null) ){
                    // --- GET DATA
                    notifyInfo = (SchoolNotification)notifyIter.next( );

                    // --- IF THERE IS A MATCH
                    if( notifyInfo.getId() == notifyId ){
                        // --- SET RETURN VALUE
                        returnNotification = notifyInfo;
                    }
                    // --- ENDIF THERE IS A MATCH
                }
                // --- ENDDO FOR EACH NOTIFY OBJECT
            }
            // --- ENDIF PERSON IS A STUDENT
        }
        // --- ENDDO FOR EACH PERSON IN THE MODEL
        
        // --- RETURN
        return( returnNotification );
    }
    // --- END

       
    /**
     * 
     * @param messageId
     * @return reference to person data; null if not in model
     */
    public SchoolMessage getMessage( int messageId )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolMessage returnMessage = null;
        Iterator      messageIter   = m_messageList.iterator();
        SchoolMessage messageInfo;
        
        // --- DO FOR EACH SCHOOL IN THE MODEL
        while( messageIter.hasNext( ) && (returnMessage == null) ){
            // --- GET DATA
            messageInfo = (SchoolMessage)messageIter.next( );
            
            // --- IF THERE IS A MATCH
            if( messageInfo.getId() == messageId ){
                // --- SET RETURN VALUE
                returnMessage = messageInfo;
            }
            // --- ENDIF THERE IS A MATCH
        }
        // --- ENDDO FOR EACH SCHOOL IN THE MODEL
        
        // --- RETURN
        return( returnMessage );
    }
    // --- END

    
    /**
     * 
     * @param rankId
     * @return reference to scale for school
     */
    public SchoolGradeRank getRank( int rankId )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolGradeRank returnRank = null;
        Iterator        rankIter   = m_rankList.iterator();
        SchoolGradeRank rankInfo;
        
        // --- DO FOR EACH SCHOOL IN THE MODEL
        while( rankIter.hasNext( ) && (returnRank == null) ){
            // --- GET DATA
            rankInfo = (SchoolGradeRank)rankIter.next( );
            
            // --- IF THERE IS A MATCH
            if( rankId == rankInfo.getId() ){
                // --- SET RETURN VALUE
                returnRank = rankInfo;
            }
            // --- ENDIF THERE IS A MATCH
        }
        // --- ENDDO FOR EACH SCHOOL IN THE MODEL
        
        // --- RETURN
        return( returnRank );
    }
    // --- END

    /**
     * 
     * @param scaleName
     * @return reference to scale for school
     */
    public SchoolGradeScale getScale( String scaleName )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolGradeScale returnScale = null;
        Iterator         scaleIter   = m_scaleList.iterator();
        SchoolGradeScale scaleInfo;
        
        // --- DO FOR EACH SCHOOL IN THE MODEL
        while( scaleIter.hasNext( ) && (returnScale == null) ){
            // --- GET DATA
            scaleInfo = (SchoolGradeScale)scaleIter.next( );
            
            // --- IF THERE IS A MATCH
            if( scaleInfo.getName().equals(scaleName) ){
                // --- SET RETURN VALUE
                returnScale = scaleInfo;
            }
            // --- ENDIF THERE IS A MATCH
        }
        // --- ENDDO FOR EACH SCHOOL IN THE MODEL
        
        // --- RETURN
        return( returnScale );
    }
    // --- END

    /**
     * 
     * @param scaleName
     * @return reference to scale for school
     */
    public SchoolGradeScale getScale( int scaleName )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolGradeScale returnScale = null;
        Iterator         scaleIter   = m_scaleList.iterator();
        SchoolGradeScale scaleInfo;
        
        // --- DO FOR EACH SCHOOL IN THE MODEL
        while( scaleIter.hasNext( ) && (returnScale == null) ){
            // --- GET DATA
            scaleInfo = (SchoolGradeScale)scaleIter.next( );
            
            // --- IF THERE IS A MATCH
            if( scaleName == scaleInfo.getId() ){
                // --- SET RETURN VALUE
                returnScale = scaleInfo;
            }
            // --- ENDIF THERE IS A MATCH
        }
        // --- ENDDO FOR EACH SCHOOL IN THE MODEL
        
        // --- RETURN
        return( returnScale );
    }
    // --- END

    
    /**
     * @brief get list of schools in the model
     * @return reference to school data
     */
    public ArrayList<SchoolHouse> getSchoolList()
    // --- BEGIN
    {
        return m_schoolList;
    }
    // --- END

    
    /**
     * @brief get list of people in the model
     * @return reference to participant data
     */
    public ArrayList<SchoolPerson> getPeopleList()
    // --- BEGIN
    {
        return m_participantList;
    }
    // --- END

    /**
     * @brief get list of messages in the model
     * @return reference to imported message data
     */
    public ArrayList<SchoolMessage> getMessageList()
    // --- BEGIN
    {
        return m_messageList;
    }
    // --- END

    
    /** 
     * #brief remove a school from the model 
     * @param schoolName
     * @param schoolAddress
     * @param schoolAdmin
     */
    public void removeSchool( Integer schoolId )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        String       insertString;
        String       schoolQuery;
        String       adminQuery;
        SchoolHouse  schoolInfo;
        SchoolPerson adminPerson;
  
        // --- GET SCHOOL TO REMOVE
        schoolInfo = getSchool( schoolId );
                
        // --- IF AN ADMIN IS PROVIDED
        if( schoolInfo != null ){
            
            // --- QUERY TO GET THE NEW ID
            m_dbChain = new QueryChain( m_appHandle, true );
            m_dbChain.setHandler( this );
            schoolQuery = SchoolDbIntfc.SchoolDelete.SCHOOL.dbQueryId( schoolId, 0 );
            adminQuery = SchoolDbIntfc.SchoolDelete.SCHOOL_ADMIN.dbQueryId( schoolId, 0 );
            m_dbChain.addQuery( schoolQuery,
                                SchoolDbIntfc.SchoolDelete.SCHOOL.dbName()  );
            m_dbChain.addQuery( adminQuery,
                                SchoolDbIntfc.SchoolDelete.SCHOOL_ADMIN.dbName()  );
            m_dbChain.start();
            
            // --- REMOVE PERSON
            m_schoolList.remove(schoolInfo);

        }
        // --- ENDIF AN ADMIN IS PROVIDED        
    }
    // --- END    
}

