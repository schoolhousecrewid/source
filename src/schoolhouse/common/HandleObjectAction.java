/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package schoolhouse.common;

import schoolhouse.model.DataObject;

/**
 * 
 * @author wpeets
 * @brief this interface provides an interface for communicating commands
 */
public interface HandleObjectAction {
    /**
     * communicates commands from the GUI interface 
     *
     * @param objectUnderTest object to process
     * @return 
     */
    public boolean handleObject( DataObject objectUnderTest );
    
}
