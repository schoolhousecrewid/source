/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package schoolhouse.common;

import java.awt.event.ActionEvent;
import java.sql.ResultSet;

/**
 * 
 * @author wpeets
 * @brief this interface provides an interface for communicating commands
 */
public interface HandleCmdAction {
    /**
     * communicates commands from the GUI interface 
     *
     */
    public boolean handleAction( ActionEvent event );
    
    /**
     * communicates commands from the schoolhouse application interface 
     *
     */
    public boolean handleEvent( String currEvent );
    
    /**
     * communicates commands from the DB interface 
     *
     */
    public boolean handleQuery( String currEvent, ResultSet m_resultSet ); 
    
    /**
     * communicates commands from the schoolhouse interface 
     *
     */
    public void handleCmd( String cmd, String cmdData, int cmdKey ); 
}
