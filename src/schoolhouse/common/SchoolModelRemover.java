/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package schoolhouse.common;

import schoolhouse.model.SchoolClass;
import schoolhouse.model.SchoolMessage;
import schoolhouse.model.SchoolMark;
import schoolhouse.model.SchoolHouse;
import schoolhouse.model.SchoolGrade;
import schoolhouse.model.SchoolCourse;
import schoolhouse.model.SchoolNotification;
import schoolhouse.model.SchoolStudent;
import schoolhouse.model.SchoolPerson;
import schoolhouse.model.SchoolPolicy.SchoolRoles;
import java.awt.event.ActionEvent;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import schoolhouse.common.SchoolDbIntfc.SchoolDelete;
import schoolhouse.model.SchoolGradeRank;
import schoolhouse.model.SchoolGradeScale;
import schoolhouse.model.SchoolGradeScale.ScalePoint;
import schoolhouse.model.SchoolNotification.SchoolNotifyType;

/**
 *
 * @author wpeets
 * @brief adds data to module and updates the database
 */
public class SchoolModelRemover implements HandleCmdAction {    

    /*
     * @brief used to interface with database
    */
    QueryChain             m_dbChain;     // --- USED FOR MASSIVE QUERIES
    QueryThread            m_dbCommand;   // --- USED TO INSERT INTO DATABASE
    SchoolApp              m_appHandle;
    SchoolModel            m_appModel;
    String                 m_DEL_MARKER = "deleteObject";
    
    /**
     * @param appHandle
     * @param dataModel
     * @brief constructor
     * 
     * This initializes values
     */
    public SchoolModelRemover( SchoolApp appHandle, SchoolModel dataModel )
    // --- BEGIN
    {
        // --- SET VALUE
        m_appHandle       = appHandle;
        m_appModel        = dataModel;
    }
    // --- END


    @Override
    public void handleCmd(String cmd, String cmdData, int cmdKey) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean handleAction(ActionEvent event) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean handleEvent(String currEvent) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @param currEvent
     * @param resultSet
     * @return 
     * @brief process data from the database
     * 
     */
    @Override
    public boolean handleQuery( String currEvent, ResultSet resultSet )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        boolean eventProcessed  = false;
        
        // --- RETURN
        return( eventProcessed );
    }
    // --- END

    
    /**
     * @brief remove data from the model and database
     * @return success state of remove option
     * @param newPerson person object
     * 
     */
    public boolean handlePerson( SchoolPerson newPerson )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolDelete  deleteString = SchoolDbIntfc.SchoolDelete.PERSON;
        SchoolDelete  roleString   = SchoolDbIntfc.SchoolDelete.PERSON;
        Integer       personId     = newPerson.getId();
        Integer       personRole   = newPerson.getRole().getValue();
        boolean       returnState  = true;
        boolean       removeState  = true;
        QueryChain    personChain;
        ArrayList<SchoolPerson> participantList = m_appModel.getPeopleList();
        System.out.println( "SchoolModelRemover::handlePerson" );

        // --- UPDATE DATABASE
        personChain = new QueryChain( m_appHandle, true );
        personChain.setHandler( this );
    
        // --- DO SOME CHECKS TO SEE WHAT IS GOING ON
        // --- IF THE PERSON IS A STUDENT
        if( newPerson.getRole() == SchoolRoles.STUDENT ){
            // --- DELETE STUDENT INFO
            System.out.println( "SchoolModelRemover::handlePerson remove watcher info" );
            removeStudentInfo( newPerson );
            roleString = SchoolDbIntfc.SchoolDelete.PERSON_STUDENT;
        }
        // --- ENDIF THE PERSON IS A STUDENT
        
        // --- IF THE PERSON IS A STUDENT
        if( newPerson.getRole() == SchoolRoles.PARENT ){
            // --- DELETE STUDENT INFO
            System.out.println( "SchoolModelRemover::handlePerson remove parent info" );
            roleString = SchoolDbIntfc.SchoolDelete.PERSON_PARENT;
        }
        // --- ENDIF THE PERSON IS A STUDENT
        
        // --- IF THE PERSON IS A STUDENT
        if( ( newPerson.getRole() == SchoolRoles.ADMINISTRATOR ) ||
            ( newPerson.getRole() == SchoolRoles.MANAGER       ) ){
            // --- DELETE ADMIN INFO
            System.out.println( "SchoolModelRemover::handlePerson remove admin info" );
            removeAdmin( newPerson );
        }
        // --- ENDIF THE PERSON IS A STUDENT
        
        // --- IF THE PERSON IS A STUDENT
        if( newPerson.getRole() == SchoolRoles.EDUCATOR ){
            // --- DELETE ADMIN INFO
            System.out.println( "SchoolModelRemover::handlePerson remove ed info" );
            roleString = SchoolDbIntfc.SchoolDelete.PERSON_TEACH;
        }
        // --- ENDIF THE PERSON IS A STUDENT
        
        // --- IF THE PERSON IS A STUDENT
        if( newPerson.getRole() == SchoolRoles.GRADER ){
            // --- DELETE ADMIN INFO
            System.out.println( "SchoolModelRemover::handlePerson remove ed info" );
            roleString = SchoolDbIntfc.SchoolDelete.PERSON_GRADER;
        }
        // --- ENDIF THE PERSON IS A STUDENT
        
        // --- IF THE DATA IS IN THE DATABASE
        if( personId != 0 ){
            // --- REMOVE FROM  MODEL
            // --- ... TBD what does it mean if not in model; do we delete
            // --- ... from the database anyway?
            removeState = participantList.remove( m_appModel.getPerson(personId) );
            System.out.println( "SchoolModelRemover::handlePerson model udpate " + removeState );
            
            // --- UPDATE DATABASE
            deleteString.dbQueryId( personId, 0 );
            personChain.addInsert( deleteString.dbQuery(), "" );
            roleString.dbQueryId( personId, 0 );
        
            // --- ADD DATA TO DATABASE
            m_dbCommand = new QueryThread( m_appHandle ); 
            m_dbCommand.setUpdate( deleteString.dbQuery(), m_DEL_MARKER );
            m_dbCommand.start();        
        }
        // --- ENDIF THE DATA IS IN THE DATABASE
        
        // --- RETURN
        return( returnState );
    }
    // --- END
    
    
    /**
     * @param personInfo
     * @brief remove student supplemental data from the database
     * 
     */
    public void removeStudentInfo( SchoolPerson personInfo )
    // --- BEGIN
    {
        Integer                 personId;
        SchoolPerson            parentInfo;
        SchoolStudent           studentInfo;
        SchoolPerson            watcherInfo;
        ArrayList<SchoolPerson> parentList;
        ArrayList<SchoolPerson> watcherList;
        Integer                 parentId;
        Integer                 watcherId;
        SchoolDelete            parentString = SchoolDbIntfc.SchoolDelete.STUDENT_PARENT;
        SchoolDelete            watcherString = SchoolDbIntfc.SchoolDelete.STUDENT_WATCHER;

        // --- HANDLE DATABASE DATA
        try {
            // --- IF THE PERSON IS A STUDENT
            if( personInfo.getRole() == SchoolRoles.STUDENT ){                    
                // --- GET LIST OF PARENTS
                personId    = personInfo.getId();
                studentInfo = (SchoolStudent)personInfo;
                parentList  = studentInfo.getParentList();
                watcherList = studentInfo.getWatcherList();
                Iterator     parentIter   = parentList.iterator();
                Iterator     watcherIter  = watcherList.iterator();
                m_dbChain = new QueryChain( m_appHandle, true );
                m_dbChain.setHandler( this );
        
                // --- DO FOR EACH PARENT
                while( parentIter.hasNext( ) ) {
                    // --- GET DATA
                    parentInfo = (SchoolPerson)parentIter.next( );
                    parentId   = parentInfo.getId();
            
                    // --- UPDATE DATABASE
                    parentString.dbQueryId( personId, 0 );
                    parentString.dbQueryId( parentId, 1 );
                    m_dbChain.addInsert( parentString.dbQuery(), "" );
                }
                // --- ENDDO FOR EACH PARENT
                
                // --- DO FOR EACH WATCHER
                while( watcherIter.hasNext( ) ) {
                    // --- GET DATA
                    watcherInfo = (SchoolPerson)watcherIter.next( );
                    watcherId   = watcherInfo.getId();
            
                    // --- UPDATE DATABASE
                    watcherString.dbQueryId( personId, 0 );
                    parentString.dbQueryId( watcherId, 1 );
                    m_dbChain.addInsert( watcherString.dbQuery(), "" );
                }
                // --- ENDDO FOR EACH SCHOOL IN THE MODEL
            }
            // --- ENDDO FOR EACH MARK TYPE NAME
        }
        catch(Exception e){
            // --- IF THE DATA IS EMPTY
            if( e.getMessage() == "null" ){
                // --- LET IT PASS
            }
            // --- ELSE
            else {
                System.out.println( "SQL Problem..." + e.getMessage() );
            }
        }
        // --- ENDOF HANDLE DATABASE
                                    
        // --- ADD ADMIN
        m_dbChain.start();
    }
    // --- END
    
    
    
    /**
     * @brief remove data from the model and database
     * @return success state of remove option
     * @param newSchool school object
     * 
     */
    public boolean handleSchool( SchoolHouse  newSchool )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolDelete deleteString = SchoolDbIntfc.SchoolDelete.SCHOOL;
        Integer      schoolId = newSchool.getId();
        boolean      returnState = true;
        boolean      removeState  = true;
        ArrayList<SchoolHouse>  schoolList = m_appModel.getSchoolList();
        ArrayList<SchoolPerson> adminList;
        ArrayList<SchoolCourse> courseList;
        SchoolPerson adminInfo;
        SchoolHouse  schoolHouse;
        SchoolCourse schoolCourse;
        
        // --- IF THE DATA IS IN THE DATABASE
        if( newSchool.getId() != 0 ){
            // --- GET SCHOOL INFO
            schoolHouse = m_appModel.getSchool( schoolId );
            adminList = schoolHouse.getAdminList();
            courseList = schoolHouse.getCourseList();
            Iterator     adminIter  = adminList.iterator();
            Iterator     courseIter  = courseList.iterator();
            
            // --- DO FOR EACH ADMIN
            while( adminIter.hasNext( ) ) {
                // --- GET DATA
                adminInfo = (SchoolPerson)adminIter.next( );

                // --- UPDATE DATABASE
                removeAdmin( adminInfo );
            }
            // --- ENDDO FOR EACH ADMIN             
        
            // --- DO FOR EACH COURSE
            while( courseIter.hasNext( ) ) {
                // --- GET DATA
                schoolCourse = (SchoolCourse)courseIter.next( );

                // --- UPDATE DATABASE
                handleCourse( schoolCourse );
            }
            // --- ENDDO FOR EACH COURSE             
        
            // --- REMOVE FROM  MODEL
            // --- ... TBD what does it mean if not in model; do we delete
            // --- ... from the database anyway? what about all courses
            removeState = schoolList.remove( m_appModel.getSchool(schoolId) );
            System.out.println( "SchoolModelRemover::handleSchool model udpate " + removeState );

            // --- UPDATE DATABASE
            deleteString.dbQueryId( schoolId, 0 );
        
            // --- ADD DATA TO DATABASE
            m_dbCommand = new QueryThread( m_appHandle ); 
            m_dbCommand.setUpdate( deleteString.dbQuery(), m_DEL_MARKER );
            m_dbCommand.start();             
        }
        // --- ENDIF THE DATA IS IN THE DATABASE
        
        // --- RETURN
        return( returnState );
    }
    // --- END
    
        
    /**
     * @brief process school data from the database
     * 
     * @param schoolInfo school object
     * 
     */
    public void removeAdmin( SchoolPerson personInfo )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolDelete            deleteString = SchoolDbIntfc.SchoolDelete.ADMIN;
        Integer                 adminId = personInfo.getId();

        // --- UPDATE DATABASE
        deleteString.dbQueryId( adminId, 0 );
            
        // --- ADD DATA TO DATABASE
        // --- ...TBD what about the school admin list if deleting a person and
        // --- ... not a school
        m_dbCommand = new QueryThread( m_appHandle ); 
        m_dbCommand.setUpdate( deleteString.dbQuery(), m_DEL_MARKER );
        m_dbCommand.start();             
    }
    // --- END

    
    /**
     * @param newCourse
     * @brief add new course information to the database
     * @return success state of remove option
     * 
     */
    public boolean handleCourse( SchoolCourse  newCourse )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolDelete deleteString = SchoolDbIntfc.SchoolDelete.COURSE;
        SchoolHouse  schoolForCourse;
        SchoolClass  schoolClass;
        int          courseId    = newCourse.getId();
        boolean      returnState = true;
        boolean      removeState;
        ArrayList<SchoolCourse>  courseList;    
        ArrayList<SchoolClass> classList;
        
        // --- IF THE DATA IS IN THE DATABASE
        if( newCourse.getId() != 0 ){
            // --- GET SCHOOL FOR COURSE
            schoolForCourse = m_appModel.getSchool( newCourse.getSchool() );
            courseList = schoolForCourse.getCourseList();
            classList       = newCourse.getClassList();
            Iterator classIter  = classList.iterator();
        
            // --- DO FOR EACH ADMIN
            while( classIter.hasNext( ) ) {
                // --- GET DATA
                schoolClass = (SchoolClass)classIter.next( );

                // --- UPDATE DATABASE
                handleClass( schoolClass );
            }
            // --- ENDDO FOR EACH ADMIN
    
            // --- REMOVE FROM  MODEL
            // --- ... TBD what does it mean if not in model; do we delete
            // --- ... from the database anyway? what about all courses
            removeState = courseList.remove( schoolForCourse.getCourse(courseId) );
            System.out.println( "SchoolModelRemover::handleCourse model udpate " + removeState );

            // --- UPDATE DATABASE
            deleteString.dbQueryId( courseId, 0 );
        
            // --- ADD DATA TO DATABASE
            m_dbCommand = new QueryThread( m_appHandle ); 
            m_dbCommand.setUpdate( deleteString.dbQuery(), m_DEL_MARKER );
            m_dbCommand.start();                     
        }
        // --- ENDIF THE DATA IS IN THE DATABASE
        
        // --- RETURN
        return( returnState );
    }
    // --- END
    
    /**
     * @param newClass
     * @return success state of remove option
     * @brief add new class information to the database
     * 
     * 
     */
    public boolean handleClass( SchoolClass  newClass )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolDelete deleteString = SchoolDbIntfc.SchoolDelete.CLASS;
        SchoolCourse courseForClass;
        SchoolClass  schoolClass;
        SchoolMark   schoolMark;
        Integer      courseId;
        int          classId = newClass.getId();
        boolean      returnState = true;
        boolean      removeState = false;
        ArrayList<SchoolClass> classList;
        ArrayList<SchoolMark>  markList;
    
        // --- IF THE DATA IS IN THE DATABASE
        if( classId != 0 ){
            // --- REMOVE CLASS DATA
            removeSchoolClassInfo( newClass );
            
            // --- GET COURSE FOR CLASS
            courseId       = newClass.getCourseId();
            courseForClass = m_appModel.getCourse( courseId );
            classList      = courseForClass.getClassList();
            schoolClass    = courseForClass.getClass(classId);
            markList       = newClass.getMarkList();
            Iterator markIter  = markList.iterator();
        
            // --- DO FOR EACH ADMIN
            while( markIter.hasNext( ) ) {
                // --- GET DATA
                schoolMark = (SchoolMark)markIter.next( );

                // --- UPDATE DATABASE
                handleMark( schoolMark );
            }
            // --- ENDDO FOR EACH ADMIN
            
            // --- REMOVE FROM  MODEL
            // --- ... TBD what does it mean if not in model; do we delete
            // --- ... from the database anyway? what about all courses
            removeState = classList.remove( schoolClass );
            System.out.println( "SchoolModelRemover::handleClass model udpate " + removeState );

            // --- UPDATE DATABASE
            deleteString.dbQueryId( classId, 0 );
        
            // --- ADD DATA TO DATABASE
            m_dbCommand = new QueryThread( m_appHandle ); 
            m_dbCommand.setUpdate( deleteString.dbQuery(), m_DEL_MARKER );
            m_dbCommand.start();            
        }
        // --- ENDIF THE DATA IS IN THE DATABASE
        
        // --- RETURN
        return( returnState );
    }
    // --- END
    
    /**
     * @param newClass
     * @brief add class data to the database
     * 
     */
    public void removeSchoolClassInfo( SchoolClass newClass )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolDelete educatorDelete = SchoolDbIntfc.SchoolDelete.CLASS_TEACH;
        SchoolDelete graderDelete = SchoolDbIntfc.SchoolDelete.CLASS_GRADER;
        SchoolDelete studentDelete = SchoolDbIntfc.SchoolDelete.CLASS_STUDENT;
        Integer      classId = newClass.getId();
        
        // --- REMOVE DATA
        m_dbChain = new QueryChain( m_appHandle, true );
        m_dbChain.setHandler( this );
        educatorDelete.dbQueryId( classId, 0 );
        graderDelete.dbQueryId( classId, 0 );
        studentDelete.dbQueryId( classId, 0 );
        m_dbChain.addUpdate( educatorDelete.dbQuery(), educatorDelete.dbName() );
        m_dbChain.addUpdate( graderDelete.dbQuery(), graderDelete.dbName() );
        m_dbChain.addUpdate( studentDelete.dbQuery(), studentDelete.dbName() );
                            
        // --- ADD ADMIN
        m_dbChain.start();
    }
    // --- END
    
    
    /**
     * @param newMark
     * @brief add new class mark information to the database
     * @return success state of remove option
     * 
     */
    public boolean handleMark( SchoolMark  newMark )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolDelete deleteString = SchoolDbIntfc.SchoolDelete.MARK;
        int          markId       = newMark.getId();
        int          classId;
        SchoolClass  classForMark;
        SchoolMark   schoolMark;
        SchoolGrade  schoolGrade;
        boolean      returnState  = true;
        boolean      removeState  = false;
        ArrayList<SchoolMark>  markList;
        ArrayList<SchoolGrade>  gradeList;
    
        // --- IF THE DATA IS IN THE DATABASE
        if( markId != 0 ){
            // --- GET CLASS FOR MARK
            classId = newMark.getSchoolClass().getId();
            classForMark  = m_appModel.getClass( classId );
            markList = classForMark.getMarkList();
            schoolMark = classForMark.getMark( markId );
            gradeList = schoolMark.getGradeList();
            Iterator gradeIter   = gradeList.iterator();
            
            // --- DO FOR EACH GRADE IN THE MARK
            // --- ... TBD IF THERE IS NO NEED TO GO ANY DEEPER MAYBE THIS CAN BE 
            // --- ... DONE WITH ONE DELETE QUERY
            while( gradeIter.hasNext( ) ) {
                // --- REMOVE GRADE FROM MARK
                schoolGrade = (SchoolGrade)gradeIter.next( );                
                handleGrade( schoolGrade );
            }
            // --- ENDDO FOR EACH GRADER IN THE CLASS

            // --- REMOVE FROM  MODEL
            // --- ... TBD what does it mean if not in model; do we delete
            // --- ... from the database anyway? what about all courses
            removeState = markList.remove( schoolMark );
            System.out.println( "SchoolModelRemover::handleMark model update " + removeState );

            // --- UPDATE DATABASE
            deleteString.dbQueryId( classId, 0 );
        
            // --- ADD DATA TO DATABASE
            m_dbCommand = new QueryThread( m_appHandle ); 
            m_dbCommand.setUpdate( deleteString.dbQuery(), m_DEL_MARKER );
            m_dbCommand.start();                    
        }
        // --- ENDIF THE DATA IS IN THE DATABASE
        
        // --- RETURN
        return( returnState );
    }
    // --- END

    /**
     * @param newGrade
     * @return success state of remove option
     * @brief remove grade data from the database
     * 
     */
    public boolean handleGrade( SchoolGrade  newGrade )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolDelete    deleteString    = SchoolDbIntfc.SchoolDelete.GRADE;
        SchoolMark      schoolMark;
        SchoolGrade     schoolGrade;
        Integer         markId;
        int             gradeId         = newGrade.getId();
        boolean         returnState     = true;
        boolean         removeState;
        ArrayList<SchoolGrade>  gradeList;
                
        // --- IF THE DATA IS IN THE DATABASE
        if( gradeId != 0 ){
            // --- GET MARK FOR GRADE
            markId = newGrade.getMark().getId();
            schoolMark  = m_appModel.getMark( markId );
            gradeList = schoolMark.getGradeList();
            schoolGrade = schoolMark.getGrade( gradeId );
            
            // --- REMOVE FROM  MODEL
            // --- ... TBD what does it mean if not in model; do we delete
            // --- ... from the database anyway? what about all courses
            removeState = gradeList.remove( schoolGrade );
            System.out.println( "SchoolModelRemover::handleGrade model update " + removeState );

            // --- UPDATE DATABASE
            deleteString.dbQueryId( gradeId, 0 );
        
            // --- ADD DATA TO DATABASE
            m_dbCommand = new QueryThread( m_appHandle ); 
            m_dbCommand.setUpdate( deleteString.dbQuery(), m_DEL_MARKER );
            m_dbCommand.start();                    
        }
        // --- ENDIF THE DATA IS IN THE DATABASE
        
        // --- RETURN
        return( returnState );
    }
    // --- END

    
    /**
     * @param newNotification
     * @brief remove grade data from the database
     * 
     */
    public boolean handleNotification( SchoolNotification  newNotification )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolNotification  schoolNotification;
        SchoolStudent       notifyPerson;
        SchoolDelete        deleteString = SchoolDbIntfc.SchoolDelete.NOTIFY;
        Integer             notifyId  = newNotification.getId();
        boolean             returnState = true;
        boolean             removeState = false;
        ArrayList<SchoolNotification>  notifyList;
                            
        // --- IF THE DATA IS IN THE DATABASE
        if( notifyId != 0 ){
            // --- GET DATA
            schoolNotification = m_appModel.getNotification( notifyId );
            notifyPerson = newNotification.getStudent();
            notifyList = notifyPerson.getNotificationList();
            
            // --- REMOVE FROM  MODEL
            // --- ... TBD what does it mean if not in model; do we delete
            // --- ... from the database anyway? what about all courses
            removeState = notifyList.remove( schoolNotification );
            System.out.println( "SchoolModelRemover::handleGrade model update " + removeState );

            // --- UPDATE DATABASE
            deleteString.dbQueryId( notifyId, 0 );
        
            // --- ADD DATA TO DATABASE
            m_dbCommand = new QueryThread( m_appHandle ); 
            m_dbCommand.setUpdate( deleteString.dbQuery(), m_DEL_MARKER );
            m_dbCommand.start(); 
        }
        // --- ENDIF THE DATA IS IN THE DATABASE
        
        // --- RETURN
        return( returnState );
    }
    // --- END

    /**
     * @brief remove message from the model
     * 
     * @param newMessage message object
     * 
     */
    public boolean handleMessage( SchoolMessage newMessage )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolDelete   deleteString = SchoolDbIntfc.SchoolDelete.MESSAGE;
        SchoolMessage  message;
        Integer        newMessageId = newMessage.getId();
        boolean        returnState = true;
        boolean        removeState = false;
        ArrayList<SchoolMessage> messageList;
    
        // --- IF THE DATA IS IN THE DATABASE
        if( newMessageId != 0 ){
            // --- GET DATA
            messageList = m_appModel.getMessageList();
            message = m_appModel.getMessage( newMessageId );
            
            // --- REMOVE FROM  MODEL
            // --- ... TBD what does it mean if not in model; do we delete
            // --- ... from the database anyway? what about all courses
            removeState = messageList.remove( message );
            System.out.println( "SchoolModelRemover::handleMessage model update " + removeState );

            // --- UPDATE DATABASE
            deleteString.dbQueryId( newMessageId, 0 );
        
            // --- ADD DATA TO DATABASE
            m_dbCommand = new QueryThread( m_appHandle ); 
            m_dbCommand.setUpdate( deleteString.dbQuery(), m_DEL_MARKER );
            m_dbCommand.start(); 
        }
        // --- ENDIF THE DATA IS IN THE DATABASE
        
        // --- RETURN
        return( returnState );
    }
    // --- END        
}

