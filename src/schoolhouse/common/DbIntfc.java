package schoolhouse.common;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author wpeets
 */
public class DbIntfc {
    protected Connection        m_connect           = null;
    protected Statement         m_statement         = null;
    protected PreparedStatement m_preparedStatement = null;
    protected ResultSet         m_resultSet         = null;
    protected Integer           m_lastId            = 0;  
    private   String            m_username;
    private   String            m_password;
    private   String            m_database;
    
    public DbIntfc()
    // --- BEGIN
    {
        
    }
    // ---END
    
    
  /*
   * @brief Connect to data repository and leave connecition
   * open for the application
   *
   * @param none, but probably some in the near future
   */
    public int connect( )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        boolean validConnection = false;
        int     returnVal       = 5;
        System.out.println( "DbIntfc::connect class connect:" );
        
        // --- IF DATA DOES NOT EXIST TO CONNECT
        if( (m_username == null ) || (m_username.equals("")) ||
            (m_database == null ) || (m_database.equals("")) ||
            (m_password == null ) || (m_password.equals("")) ){
            // --- CONNECT
            returnVal = 2;
            validConnection = connect( m_username, m_password, m_database, "" );
            
            // --- IF THE CONNECTION IS SUCCESSFUL
            if( validConnection == true ){
                // --- SET INDICATION
                returnVal = 4;
            }
            // --- ENDIF THE CONNECTION IS SUCCESSFUL
        }
        // --- ENDIF DATA DOES NOT EXIST TO CONNECT

        // --- RETURN CONNECTION STATE
        return( returnVal );
    }
    
    public boolean connect(String aUserName,  String aPassword,  String aDatabaseName, String aRole)
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        String dbVal   = "jdbc:mysql://localhost/" + aDatabaseName + "?";
        String userVal = "user=" + aUserName + "&";
        String pwVal   = "password=" + aPassword;
        
        // --- STORE USER
        setUsername( aUserName );
        setDatabase( aDatabaseName );
        
        try {
            // --- LOAD JDBC DRIVER FOR MYSQL    
            // ... this will load the MySQL driver, each DB has its own driver
            Class jdbDriver = Class.forName( "com.mysql.jdbc.Driver" );
           
            // --- CONNECT TO DATA REPOSITORY
            // ... setup the connection with the DB.
            System.out.println( "DbIntfc::connect data connect: " + dbVal );
            System.out.println( "DbIntfc::connect data connect: " + userVal );
            m_connect = DriverManager.getConnection( dbVal + userVal + pwVal );         
        }
        catch(Exception e){
                 System.out.println("Connect Problem..."+e.getMessage());
                 return false;
        }
          

        
        // --- RETURN
        return true;
    }    
    // --- END
    
    
    public ResultSet runQuery( String newQuery )
    // --- BEGIN
    {
        try {
            // --- QUERY DATA REPOSITORY
            System.out.println( "DbIntfc::runQuery SQL:"+newQuery );
            m_statement = m_connect.createStatement();
            m_resultSet = m_statement.executeQuery( newQuery );
        }
         catch(Exception e){
            System.out.println( "SQL Problem..."+e.getMessage() );
        }
        
        // --- RETURN
        return( m_resultSet );
    }
    // --- END
    
    /**
     *
     * @brief submit an SQL statement for database insert
     * @param newUpdate SQL string
     * @param getKeys   true if id should be attained
     * @return
     */
    public ResultSet runUpdate( String newUpdate, boolean getKeys )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        Integer status = 0;
        
        try {
            // --- QUERY DATA REPOSITORY
            System.out.println( "DbIntfc::runUpdate SQL:"+newUpdate );
            m_statement = m_connect.createStatement();
            status = m_statement.executeUpdate( newUpdate, Statement.RETURN_GENERATED_KEYS );
            System.out.println( "DbIntfc::runUpdate Records added: " + status );
            
            // --- IF THE UPDATE IS SUCCESSFUL
            if( (status != 0) && (getKeys==true) ){ 
                // --- GET ID DATA
                m_resultSet = m_statement.getGeneratedKeys();
                m_resultSet.next();
                m_lastId = m_resultSet.getInt( 1 );
                System.out.println( "DbIntfc::runUpdate Records id: " + m_lastId );
            }
            // --- ELSE
            else {
                // --- RESET ID
                m_lastId = 0;
            }
            // --- ENDIF THE UPDATE IS SUCCESSFUL
        }
        catch(Exception e){
            System.out.println( "SQL Problem..."+e.getMessage() );
        }
        
        // --- RETURN
        return( m_resultSet );
    }
    // --- END
    
    void disconnect()
    // --- BEGIN
    {
        try {
            // --- CLOSE CONNECTION
            m_connect.close();
        }
         catch(Exception e){
                 System.out.println("Shutdown Problem..."+e.getMessage());
        }
    }
    // --- END
    
    int getLastId()
    // --- BEGIN
    {
        // --- RETURN LAST UPDATE ID
        return( m_lastId );
    }
    // --- END
    
    /**
     * @return the m_userName
     */
    public String getUsername() {
        return m_username;
    }

    /**
     * @param username the m_userName to set
     */
    public void setUsername(String username) {
        this.m_username = username;
    }

    /**
     * @return the m_password
     */
    public String getPassword() {
        return m_password;
    }

    /**
     * @param password the m_password to set
     */
    public void setPassword(String password) {
        this.m_password = password;
    }

    /**
     * @return the database
     */
    public String getDatabase() {
        return m_database;
    }

    /**
     * @param db the m_password to set
     */
    public void setDatabase( String db ) {
        this.m_database = db;
    }

}

