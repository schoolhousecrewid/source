/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */   

package schoolhouse.common;

import schoolhouse.common.SchoolApp;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author wpeets
 */
public class QueryThread extends Thread {
    private   String           m_currentQuery;
    private   String           m_currentInsert;
    private   String           m_currentUpdate;
    private   String           m_currentAction;
    private   ResultSet        m_resultSet;
    private   SchoolApp        m_appHandle;
    private   DbIntfc          m_dbHandle;
    private   HandleCmdAction  m_resultHandler;
    protected Integer          m_lastId;  
    
    /**
     *
     * @param appHandle  SchoolHouse Application pointer
     */
    public QueryThread( SchoolApp appHandle )
    // --- BEGIN
    {
        // --- SAVE HANDLE TO DATABASE
        m_appHandle     = appHandle;
        m_dbHandle      = m_appHandle.getDbIntfc();
        m_currentQuery  = null;
        m_currentUpdate = null;
        m_currentInsert = null;
        m_resultHandler = null;
        m_lastId        = 0;  
    }
    // --- END
    
    public ResultSet getResults( )
    // --- BEGIN
    {
        // --- RETURN VALUE
        return( m_resultSet );  
    }
    // --- END
    
    /**
     *
     * @param resultHandler
     */
    public void setHandler( HandleCmdAction resultHandler )
    // --- BEGIN
    {
        // --- RETURN VALUE
        m_resultHandler = resultHandler; 
    }
    // --- END
    
    public void setQuery( String newQuery, String action )
    // --- BEGIN
    {
        // --- RETURN VALUE
        m_currentQuery = newQuery; 
        setCurrentAction( action );
    }
    // --- END
    
    public void setUpdate( String newUpdate, String action )
    // --- BEGIN
    {
        // --- RETURN VALUE
        m_currentUpdate = newUpdate; 
        setCurrentAction( action );
    }
    // --- END
    
    public void setInsert( String newInsert, String action )
    // --- BEGIN
    {
        // --- RETURN VALUE
        m_currentInsert = newInsert; 
        setCurrentAction( action );
    }
    // --- END
    
    public void run( )
    // --- BEGIN
    {
        // --- RETURN VALUE
        executeQuery();
    }
    // --- END
    
    public void executeQuery()
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        HandleCmdAction currentEngine;
        boolean         getInsertKey = true;
        
        // --- IF THERE IS AN UPDATE
        if( m_currentInsert != null ){
            // --- IF AN ACTION IS DEFINED
            if( m_currentAction.equals( "" ) ){
                // --- GET ID
                // --- ... actually I don't think you can gat an ID if there is
                // --- ... no primary key and that is the real limiting factor
                // --- ... otherwise there would be an error
                getInsertKey = false;
            }
            // --- PERFORM DATABASE UPDATE
            m_resultSet = m_dbHandle.runUpdate( m_currentInsert, getInsertKey ); 
            m_lastId = m_dbHandle.getLastId();
            
            // --- IF DATA IS RETURNED
            if( null == m_resultSet ){
                // --- HANDLE ERROR
                System.out.println( "QueryThread::run Insert No Data - " + m_currentAction  );
            }
            else{
                // --- INTEERRUPT MAIN TASK
                System.out.println( "QueryThread::run Insert Yes - " + m_currentAction );
                
                // --- UPDATER HANDLERS
                handleReturn();
            }
            // --- ENDIF DATA IS RETURNED
        }
        // --- ENDIF THERE IS AN UPDATE
        
        // --- IF THERE IS AN QUERY
        if( m_currentUpdate != null ){
            // --- QUERY DATA REPOSITORY
            getInsertKey = false;
            m_resultSet = m_dbHandle.runUpdate( m_currentUpdate, getInsertKey );
           
            // --- IF DATA IS RETURNED
            if( null == m_resultSet ){
                // --- HANDLE ERROR
                System.out.println( "QueryThread::run Update No Data - " + m_currentAction  );
            }
            else{
                // --- INTEERRUPT MAIN TASK
                System.out.println( "QueryThread::run Update Yes - " + m_currentAction );

                // --- UPDATER HANDLERS
                handleReturn();
            }
            // --- ENDIF DATA IS RETURNED
        }
        // --- ENDIF THERE IS AN QUERY
        
        // --- IF THERE IS AN QUERY
        if( m_currentQuery != null ){
            // --- QUERY DATA REPOSITORY
            m_resultSet = m_dbHandle.runQuery( m_currentQuery );
           
            // --- IF DATA IS RETURNED
            if( null == m_resultSet ){
                // --- HANDLE ERROR
                System.out.println( "QueryThread::run Query No Data - " + m_currentAction  );
            }
            else{
                // --- INTEERRUPT MAIN TASK
                System.out.println( "QueryThread::run Query Yes - " + m_currentAction );

                // --- UPDATER HANDLERS
                handleReturn();
            }
            // --- ENDIF DATA IS RETURNED
        }
        // --- ENDIF THERE IS AN QUERY
    }
    // --- END

    public void handleReturn()
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        HandleCmdAction currentEngine;
        boolean         queryStatus = false;
            
        // --- IF THE HANDLER HAS BEEN SET
        if( m_resultHandler != null ){
            // --- USE HANDLER TO PROCESS RESULTS
            currentEngine = m_resultHandler;

            // --- IF HANDLER IS FOUND
            if( currentEngine != null ){
                // --- PROCESS EVENT
                System.out.println( "QueryThread::notify Handler - " + m_resultHandler.toString() );
                queryStatus = currentEngine.handleQuery( m_currentAction, m_resultSet );
            }
            // --- ENDIF HANDLER IS FOUND
        }
        // --- ENDIF THE HANDLER HAS BEEN SET
            
        // --- ACQUIRE THE CURRENT HANDLER
        currentEngine = m_appHandle.getActiveEngine();                

        // --- IF HANDLER IS FOuND
        if( currentEngine != null ){
            // --- PROCESS EVENT
           System.out.println( "QueryThread::notify Engine - " + currentEngine.toString() );
           currentEngine.handleQuery( m_currentAction, m_resultSet );
        }
         // --- ENDIF HANDLER IS FOUND
    }
    // --- END

    /**
     * @return the m_currentAction
     */
    public String getCurrentAction() {
        return m_currentAction;
    }

    /**
     * @param currentAction the m_currentAction to set
     */
    public void setCurrentAction(String currentAction) {
        this.m_currentAction = currentAction;
    }
}
