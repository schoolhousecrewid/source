/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */   

package schoolhouse.common;

import schoolhouse.common.QueryThread;
import schoolhouse.common.SchoolApp;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author wpeets
 */
public class QueryChain extends Thread {
    private DbIntfc           m_dbHandle;
    private SchoolApp         m_appHandle;
    private HandleCmdAction   m_resultHandler;
    private boolean           m_continueChain;
    private boolean           m_useThreads;
    protected Integer         m_lastId;  
    
    /* 
     * list of queries to execute; implemented as a list to preserve order
    */
  //HashMap                   m_queryList;
    ArrayList<QueryThread>    m_queryList; 
            
    /**
     * @brief constructor
     * 
     * @author wpeets
     *
     * @param appHandle  SchoolHouse Application pointer
     * @param useThreads true if individual thread is used for each query
     * TBD class should define a boolean constant
     * NO_SEPARATE_THREAD as false
     * USE_SEPARATE_THREAD_FOR QUERIES as true
     * to make the code more readable or just don't
     * use separate threads all the time and get rid of the option
     */
    public QueryChain( SchoolApp appHandle, boolean useThreads )
    // --- BEGIN
    {
        // --- SAVE HANDLE TO DATABASE
        m_appHandle = appHandle;
        m_dbHandle = m_appHandle.getDbIntfc();
        m_continueChain = true;
        m_queryList = new ArrayList<QueryThread>();
        m_useThreads = useThreads;
        m_lastId     = 0;  
   }
    // --- END
    
    public void setHandler( HandleCmdAction resultHandler )
    // --- BEGIN
    {
        // --- SET HANDLER FOR RESULTS
        m_resultHandler = resultHandler;  
    }
    // --- END
    
    public void addQuery( String newQuery, String action )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        QueryThread listQuery = new QueryThread( m_appHandle );
        
        // --- ADD QUERY TO THE MAP
        listQuery.setQuery( newQuery, action );
        listQuery.setHandler( m_resultHandler );
        m_queryList.add( listQuery );
    }
    // --- END
    
    
    public void addUpdate( String newUpdate, String action )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        QueryThread listUpdate = new QueryThread( m_appHandle );
        
        // --- ADD QUERY TO THE MAP
        listUpdate.setUpdate( newUpdate, action );
        listUpdate.setHandler( m_resultHandler );
        m_queryList.add( listUpdate );
    }
    // --- END
    
    public void addInsert( String newUpdate, String action )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        QueryThread listUpdate = new QueryThread( m_appHandle );
        
        // --- ADD QUERY TO THE MAP
        listUpdate.setInsert( newUpdate, action );
        listUpdate.setHandler( m_resultHandler );
        m_queryList.add( listUpdate );
    }
    // --- END
    
    public void addQuery( QueryThread newQuery )
    // --- BEGIN
    {
        m_queryList.add( newQuery );
    }
    // --- END
    
    
    public void startChain( )
    // --- BEGIN
    {
        // --- START THREAD
        start();
         
    }
    // --- END
    
    public void stopChain( )
    // --- BEGIN
    {
        // --- START THREAD
        m_continueChain = false;
         
    }
    // --- END
    
    @Override
    public void run( )
    // --- BEGIN
    {
        // --- START THREAD
        executeChain();
         
    }
    // --- END
    
    public void executeChain()
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        Iterator    entries = m_queryList.iterator();
        String      queryKey;
        QueryThread newQuery;
        
        // --- DO FOR EACH QUERY
        while( (entries.hasNext()) && (m_continueChain == true) ){
            // --- GET QUERY
            newQuery = (QueryThread)entries.next();
            
            // --- IF THREADS ARE REQUESTED
            if( m_useThreads == true ){
                // --- START QUERY AS A THREAD
                newQuery.start();
            }
            // --- ELSE
            else {
                // --- RUN QUERY
                newQuery.executeQuery();
                m_lastId = m_dbHandle.getLastId();
            }
            // --- ENDIF THREADS ARE REQUESTED
            
            // --- TEST DELAY
            try {
                Thread.sleep( 500 );
            } 
            catch( InterruptedException ex ) {
                Thread.currentThread().interrupt();
            }
        }
        // --- ENDDO FOR EACH QUERY
    }
    // --- END
    
    // --- END
    int getLastId()
    // --- BEGIN
    {
        // --- RETURN LAST UPDATE ID
        return( m_lastId );
    }
    // --- END
    
}
