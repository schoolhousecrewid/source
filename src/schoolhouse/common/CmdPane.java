/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package schoolhouse.common;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import javax.swing.AbstractButton;
import javax.swing.JButton;

/**
 *
 * @author wpeets
 */
public class CmdPane extends CmdView {
    protected JButton  m_submitCmd;
    protected JButton  m_clearCmd;
    protected JButton  m_returnCmd;
    protected DbEngine m_activeEngine;

/**
 *
 * @param appHandle
 * @brief constructor for main view
 */
    public CmdPane( SchoolApp appHandle )
    // --- BEGIN
    {
        // --- SATISFY SUBCLASS
        super( appHandle );
        
        // --- DECLARE LOCAL VARIABLES
        GridBagConstraints c        = new GridBagConstraints();
        int                cmdCount = 0;
        
        // --- SET LAYOUT
        //setLayout( new BoxLayout(this, BoxLayout.X_AXIS) );
        //setLayout( new BoxLayout(this, BoxLayout.Y_AXIS) );
        setLayout( new GridBagLayout() );
        
        // --- CREATE TOP LEVEL ACTIONS
        c.gridx      = 0;
        c.gridy      = cmdCount;
        c.fill       = GridBagConstraints.BOTH;
        c.gridheight = 1;
        c.gridwidth  = 1;
        m_submitCmd = new JButton( "Submit" );
        m_submitCmd.setVerticalTextPosition( AbstractButton.CENTER );
        m_submitCmd.setHorizontalTextPosition( AbstractButton.LEADING );
        m_submitCmd.setMnemonic( KeyEvent.VK_S );
        m_submitCmd.setActionCommand( "submit" );
        m_submitCmd.setToolTipText( "Click this button to add data to the database" );
        m_submitCmd.addActionListener( this );
        add( m_submitCmd, c );
        cmdCount++;

        c.gridx      = 0;
        c.gridy      = cmdCount;
        c.fill       = GridBagConstraints.BOTH;
        c.gridheight = 1;
        c.gridwidth  = 1;
        m_clearCmd = new JButton( "Clear" );
        m_clearCmd.setVerticalTextPosition(AbstractButton.BOTTOM);
        m_clearCmd.setHorizontalTextPosition(AbstractButton.CENTER);
        m_clearCmd.setMnemonic(KeyEvent.VK_C);
        m_clearCmd.setActionCommand("clear");
        m_clearCmd.setEnabled(true);
        m_clearCmd.setToolTipText("Click this button to remove data from database");
        m_clearCmd.addActionListener(this);
        add( m_clearCmd, c );
        cmdCount++;
        
        c.gridx      = 0;
        c.gridy      = cmdCount;
        c.fill       = GridBagConstraints.BOTH;
        c.gridheight = 1;
        c.gridwidth  = 1;
        m_returnCmd = new JButton( "Return" );
        m_returnCmd.setVerticalTextPosition(AbstractButton.BOTTOM);
        m_returnCmd.setHorizontalTextPosition(AbstractButton.CENTER);
        m_returnCmd.setMnemonic(KeyEvent.VK_R);
        m_returnCmd.setActionCommand("return");
        m_returnCmd.setEnabled(true);
        m_returnCmd.setToolTipText("Click this button to return to previous pane");
        m_returnCmd.addActionListener( this );
        add( m_returnCmd, c );
    }
    // --- END
    
    /**
     *
     * @param act
     * @brief handle top level action
     */
    @Override
    public void actionPerformed( ActionEvent act ) 
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        DbEngine currentAction = m_appHandle.getActiveEngine();
        
        // --- PROCESS EVENT
        currentAction.actionPerformed( act );
    }
    // --- END
    
    /**
     *
     * @param newState
     * @brief set the enable state of the submit button
     */
    public void submitEnable( boolean newState ) 
    // --- BEGIN
    {        
        // --- SET STATE
        m_submitCmd.setEnabled( newState );
    }

    /**
     *
     * @param newState
     * @brief set the enable state of the clear button
     */
    public void clearEnable( boolean newState ) 
    // --- BEGIN
    {        
        // --- SET STATE
        m_clearCmd.setEnabled( newState );
    }

    
    /**
     *
     * @param newState
     * @brief set the enable state of the return button
     */
    public void returnEnable( boolean newState ) 
    // --- BEGIN
    {        
        // --- SET STATE
        m_returnCmd.setEnabled( newState );
    }

    
}
