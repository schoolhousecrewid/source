/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package schoolhouse.common;

import schoolhouse.model.SchoolClass;
import schoolhouse.model.SchoolMessage;
import schoolhouse.model.SchoolMark;
import schoolhouse.model.SchoolHouse;
import schoolhouse.model.SchoolGradeScale;
import schoolhouse.model.SchoolGradeRank;
import schoolhouse.model.SchoolGrade;
import schoolhouse.model.SchoolCourse;
import schoolhouse.model.SchoolNotification;
import schoolhouse.model.SchoolStudent;
import schoolhouse.model.SchoolPerson;
import static schoolhouse.common.DbEngine.ADMIN_INFO;
import static schoolhouse.common.DbEngine.COURSE_DATA;
import static schoolhouse.common.DbEngine.CLASS_DATA;
import static schoolhouse.common.DbEngine.PEOPLE_DATA;
import static schoolhouse.common.DbEngine.PARENT_DATA;
import static schoolhouse.common.DbEngine.WATCHER_DATA;
import static schoolhouse.common.DbEngine.SCHOOL_DATA;
import static schoolhouse.common.DbEngine.EDUCATOR_DATA;
import static schoolhouse.common.DbEngine.GRADER_DATA;
import static schoolhouse.common.DbEngine.STUDENT_DATA;
import static schoolhouse.common.DbEngine.MARK_DATA;
import static schoolhouse.common.DbEngine.GRADE_DATA;
import static schoolhouse.common.DbEngine.NOTIFICATION_DATA;
import static schoolhouse.common.DbEngine.MESSAGE_DATA;
import static schoolhouse.common.DbEngine.RANK_DATA;
import static schoolhouse.common.DbEngine.SCALE_DATA;
import static schoolhouse.common.DbEngine.ROLE_DATA;
import static schoolhouse.common.DbEngine.TYPE_DATA;
import schoolhouse.model.SchoolPolicy.SchoolRoles;
import java.awt.event.ActionEvent;
import java.sql.Date;
import java.sql.Time;
import java.sql.ResultSet;
import java.util.ArrayList;
import static schoolhouse.common.DbEngine.ADMIN_DATA;
import schoolhouse.model.DataObject.DataState;
import schoolhouse.model.SchoolAdmin;
import schoolhouse.model.SchoolEducator;
import schoolhouse.model.SchoolGradeScale.ScalePoint;
import schoolhouse.model.SchoolGrader;
import schoolhouse.model.SchoolMessage.SchoolMessageState;
import schoolhouse.model.SchoolMessage.SchoolMessageType;
import schoolhouse.model.SchoolNotification.SchoolContactType;
import schoolhouse.model.SchoolNotification.SchoolNotifyType;
import schoolhouse.model.SchoolParent;
import schoolhouse.model.SchoolWatcher;

/**
 *
 * @author wpeets
 * @brief takes data from the database and updates model
 */
public class SchoolModelPopulater implements HandleCmdAction {    
    /*
     * @brief used to interface with database
     */
    QueryChain   m_dbChain;     // --- USED TO INSERT INTO DATABASE
    SchoolApp    m_appHandle;
    SchoolModel  m_appModel;
    
    /**
     * @param appHandle
     * @param dataModel
     * @brief constructor
     * 
     * This initializes values
     */
    public SchoolModelPopulater( SchoolApp appHandle, SchoolModel dataModel )
    // --- BEGIN
    {
        // --- SET VALUE
        m_appHandle = appHandle;
        m_appModel  = dataModel;
    }
    // --- END


    @Override
    public boolean handleAction(ActionEvent event) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean handleEvent(String currEvent) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void handleCmd(String cmd, String cmdData, int cmdKey) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    /**
     * @param currEvent
     * @param resultSet
     * @return 
     * @brief process data from the database
     * 
     */
    @Override
    public boolean handleQuery( String currEvent, ResultSet resultSet )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        int     descriptorIndex = currEvent.indexOf( '_' );
        String  messageType     = null;
        String  descriptor      = null;
        Integer descriptorVal   = null;
        boolean eventProcessed  = false;

        // --- IF DATA IS SPECIFIC
        if( descriptorIndex != -1 ){
            // --- EXTRACT MESSAGE TYPE
            messageType = currEvent.substring(0, descriptorIndex );
            descriptor  = currEvent.substring(descriptorIndex+1, currEvent.length() );
            descriptorVal = new Integer( descriptor );
        }
        // --- ELSE
        else {
            // --- USE THE WHOLE STRING
            messageType = currEvent;
        }
        // --- ENDIF DATA IS SPECIFIC
        
        try {
            // --- DO CASE OF QUERY TYPE
            switch( messageType ){
                // --- CASE ALL OF THE PARTICIPANT DATA
                case PEOPLE_DATA:
                case ADMIN_DATA:
                    // --- STORE PEOPLE NAMES
                    addPeopleInfo( resultSet, descriptorVal );
                    eventProcessed = true;
                break;
                // --- CASE ALL OF THE SCHOOL DATA
                case SCHOOL_DATA:
                    // --- STORE SCHOOL DATA
                    handleSchool( resultSet, descriptorVal );
                    eventProcessed = true;
                break;
                // --- CASE ALL OF THE PARTICIPANT DATA
                case PARENT_DATA:
                    // --- STORE PEOPLE NAMES
                    addParentInfo( resultSet, descriptorVal );                    
                    eventProcessed = true;
                break;
                // --- CASE ALL OF THE PARTICIPANT DATA
                case WATCHER_DATA:
                    // --- STORE PEOPLE NAMES
                    addWatcherInfo( resultSet, descriptorVal );                    
                    eventProcessed = true;
                break;
                // --- CASE ALL OF THE PARTICIPANT DATA
                case ADMIN_INFO:
                    // --- STORE PEOPLE NAMES
                    addAdminInfo( resultSet, descriptorVal );                    
                    eventProcessed = true;
                break;
                // --- CASE ALL OF THE COURSE DATA
                case COURSE_DATA:
                    // --- STORE COURSE
                    addCourseInfo( resultSet, descriptorVal );
                    eventProcessed = true;
                break;
                // --- CASE ALL OF THE CLASS DATA
                case CLASS_DATA:
                    // --- STORE COURSE
                    addClassInfo( resultSet, descriptorVal );
                    eventProcessed = true;
                break;
                // --- CASE ALL OF THE EDUCATOR CLASS DATA
                case EDUCATOR_DATA:
                    addEducatorInfo( resultSet, descriptorVal );
                    eventProcessed = true;
                break;
                // --- CASE ALL OF THE GRADER CLASS DATA
                case GRADER_DATA:
                    addGraderInfo( resultSet, descriptorVal );
                    eventProcessed = true;
                break;
                // --- CASE ALL OF THE STUDENT CLASS DATA
                case STUDENT_DATA:
                    addClassStudentInfo( resultSet, descriptorVal );
                    eventProcessed = true;
                break;
                // --- CASE ALL OF THE MARK CLASS DATA
                case MARK_DATA:
                    addMarkInfo( resultSet, descriptorVal );
                    eventProcessed = true;
                break;
                // --- CASE ALL OF THE GRADE DATA
                case GRADE_DATA:
                    addGradeInfo( resultSet, descriptorVal );
                    eventProcessed = true;
                break;
                // --- CASE ALL OF THE GRADE DATA
                case NOTIFICATION_DATA:
                    addNotifyInfo( resultSet, descriptorVal );
                    eventProcessed = true;
                break;
                // --- CASE ALL OF THE GRADE DATA
                case MESSAGE_DATA:
                    addMessageInfo( resultSet, descriptorVal );
                    eventProcessed = true;
                break;
                // --- CASE ALL OF THE GRADE DATA
                case RANK_DATA:
                    addRankInfo( resultSet, descriptorVal );
                    eventProcessed = true;
                break;
                // --- CASE SCALE DATA
                case SCALE_DATA:
                    addScaleInfo( resultSet, descriptorVal );
                    eventProcessed = true;
                break;
                // --- CASE ALL OF THE ROLE DATA
                case ROLE_DATA:
                    // --- UPDATE ROLE INFO
                    addRoleInfo( resultSet, descriptorVal );
                    eventProcessed = false;
                break;
                // --- CASE ALL OF THE MARKER TYPES DATA
                case TYPE_DATA:
                    // --- STORE ROLE NAMES
                    addTypeInfo( resultSet, descriptorVal );
                    eventProcessed = false;
                break;
                default:
                    // --- DO NOTHING
                break;
            }
            // --- ENDDO CASE OF QUERY TYPE
        }
        catch(Exception e){
            // --- IF THE DATA IS EMPTY
            if( e.getMessage() == "null" ){
                // --- LET IT PASS
                eventProcessed = true;
            }
            // --- ELSE
            else {
                System.out.println( "SQL Problem..." + e.getMessage() );
                eventProcessed = false;
            }
        }
        
        // --- RETURN
        return( eventProcessed );
    }
    // --- END

        
    /**
     * @param peopleData
     * @param personIndex
     * @brief process people data from the database
     * 
     */
    public void addPeopleInfo( ResultSet peopleData, Integer personIndex )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        Integer      personId;
        Integer      dataVal;
        String       personString;
        SchoolPerson personInfo;
        SchoolRoles  personRole;
        ArrayList<SchoolPerson> participantList = m_appModel.getPeopleList();
        
        // --- HANDLE DATABASE DATA
        try {
            // --- DO FOR EACH PERSON NAME
            while( peopleData.next() ){
                // --- SAVE NAME IN SCHOOL LIST
                personId = peopleData.getInt( "participant_id" );
                personInfo = m_appModel.getPerson( personId );
                dataVal = peopleData.getInt( "role_id" );
                personRole = SchoolRoles.fromValue( dataVal );
                
                // --- IF THE PERSON DOES NOT EXIST
                if( personInfo == null ){
                    // --- DO CASE OF ROLE
                    switch( personRole ){
                        // --- STUDENT PARTICIPANT
                        case STUDENT:
                            personInfo = new SchoolStudent();
                        break;
                        case PARENT:
                            personInfo = new SchoolParent();
                        break;
                        case WATCHER:
                            personInfo = new SchoolWatcher();
                        break;
                        case EDUCATOR:
                            personInfo = new SchoolEducator();
                        break;
                        case GRADER:
                            personInfo = new SchoolGrader();
                        break;
                        case ADMINISTRATOR:
                        case MANAGER:
                            personInfo = new SchoolAdmin();
                        break;
                        // --- ALL OTHERS
                        default:
                            personInfo = new SchoolPerson();
                        break;
                    }
                    // --- ENDDO CASE OF ROLE
                    
                    // --- SET ID
                    personInfo.setId( personId );
                    participantList.add( personInfo );
                }
                // --- ENDIF THE PERSON DOES NOT EXIST
                
                // --- SET DATA
                personInfo.setRole( personRole );
                personString = peopleData.getString( "last_name" );
                personInfo.setLastName( personString );
                personString = peopleData.getString( "first_name" );
                personInfo.setFirstName( personString );
                personString = peopleData.getString( "middle_name" );
                personInfo.setMiddleName( personString );
                personString = peopleData.getString( "nick_name" );
                personInfo.setNickName( personString );
                personString = peopleData.getString( "org_id" );
                personInfo.setOrgAssigned(personString );
                personString = peopleData.getString( "title" );
                personInfo.setTitle( personString );
                personString = peopleData.getString( "email" );
                personInfo.setEmail( personString );
                personString = peopleData.getString( "phone" );
                personInfo.setPhone( personString );
                personString = peopleData.getString( "cell_phone" );
                personInfo.setCellPhone( personString );
                personString = peopleData.getString( "address" );
                personInfo.setAddress( personString );
                dataVal = peopleData.getInt( "user_status" );
                personInfo.setUserStatus( dataVal );
                personString = peopleData.getString( "username" );
                personInfo.setUserName( personString );
            }
            // --- ENDDO FOR EACH PERSON NAME
        }
        catch(Exception e){
            // --- IF THE DATA IS EMPTY
            if( e.getMessage() == "null" ){
                // --- LET IT PASS
            }
            // --- ELSE
            else {
                System.out.println( "SQL Problem..." + e.getMessage() );
            }
        }
        // --- ENDOF HANDLE DATABASE
    }
    // --- END

    
    /**
     * @param schoolData
     * @param schoolIndex
     * @brief process school data from the database
     * 
     */
    public void handleSchool( ResultSet schoolData, Integer schoolIndex )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        Integer     schoolId;
        String      schoolString;
        SchoolHouse schoolInfo;
        ArrayList<SchoolHouse>  schoolList = m_appModel.getSchoolList();
        
        // --- HANDLE DATABASE DATA
        try {
            // --- DO FOR EACH SCHOOL NAME
            while( schoolData.next() ){
                // --- SAVE NAME IN SCHOOL LIST
                schoolId = schoolData.getInt( "school_id" );
                schoolInfo = m_appModel.getSchool( schoolId );
                
                // --- IF THE SCHOOL DOES NOT EXIST
                if( schoolInfo == null ){
                    schoolInfo = new SchoolHouse( m_appHandle );
                    schoolInfo.setId( schoolId );
                    schoolList.add( schoolInfo );
                }
                // --- ENDIF THE SCHOOL DOES NOT EXIST
                
                // --- SET DATA
                schoolString = schoolData.getString( "school_name" );
                schoolInfo.setName( schoolString );
                schoolString = schoolData.getString( "school_address" );
                schoolInfo.setAddress( schoolString );
            }
            // --- ENDDO FOR EACH SCHOOL NAME
        }
        catch(Exception e){
            // --- IF THE DATA IS EMPTY
            if( e.getMessage() == "null" ){
                // --- LET IT PASS
            }
            // --- ELSE
            else {
                System.out.println( "SQL Problem..." + e.getMessage() );
            }
        }
        // --- ENDOF HANDLE DATABASE
    }
    // --- END    
    

    /**
     * @param educatorData
     * @param schoolIndex
     * @brief process educator data from the database
     * 
     */
    public void addEducatorInfo( ResultSet educatorData, Integer schoolIndex )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        Integer       courseId;
        Integer       classId;
        Integer       personId;
        SchoolHouse   schoolInfo;
        SchoolStudent studentInfo;
        SchoolPerson  educatorInfo;
        SchoolClass   classInfo;
        
        // --- HANDLE DATABASE DATA
        try {
            // --- DO FOR EACH EDUCATOR NAME
            while( educatorData.next() ){
                // --- SAVE NAME IN SCHOOL LIST
                courseId = educatorData.getInt( "course_id" );
                classId = educatorData.getInt( "class_id" );
                classInfo = m_appModel.getClass( classId );
                
                // --- IF THE CLASS DOES NOT EXIST
                if( classInfo == null ){
                }
                // --- ELSE
                else {
                    // --- SET DATA
                    personId = educatorData.getInt( "educator_participant_id" );
                    educatorInfo = m_appModel.getPerson( personId );
                    
                    // --- IF PERSON EXISTS
                    if( educatorInfo != null ){
                        // --- ADD ADMIN TO SCHOOL
                        classInfo.addEducator( educatorInfo );
                    }
                    // --- ENDIF PERSON EXISTS
                }
                // --- ENDIF THE CLASS DOES NOT EXIST
            }
            // --- ENDDO FOR EACH EDUCATOR NAME
        }
        catch(Exception e){
            // --- IF THE DATA IS EMPTY
            if( e.getMessage() == "null" ){
                // --- LET IT PASS
            }
            // --- ELSE
            else {
                System.out.println( "SQL Problem..." + e.getMessage() );
            }
        }
        // --- ENDOF HANDLE DATABASE
    }
    // --- END
    
    
    /**
     * @param graderData
     * @param schoolIndex
     * @brief process grader data from the database
     * 
     */
    public void addGraderInfo( ResultSet graderData, Integer schoolIndex )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        Integer       courseId;
        Integer       classId;
        Integer       personId;
        SchoolHouse   schoolInfo;
        SchoolStudent studentInfo;
        SchoolPerson  graderInfo;
        SchoolClass   classInfo;
        
        // --- HANDLE DATABASE DATA
        try {
            // --- DO FOR EACH GRADER NAME
            while( graderData.next() ){
                // --- SAVE NAME IN SCHOOL LIST
                courseId = graderData.getInt( "course_id" );
                classId = graderData.getInt( "class_id" );
                classInfo = m_appModel.getClass( classId );
                
                // --- IF THE CLASS DOES NOT EXIST
                if( classInfo == null ){
                }
                // --- ELSE
                else {
                    // --- SET DATA
                    personId = graderData.getInt( "grader_participant_id" );
                    graderInfo = m_appModel.getPerson( personId );
                    
                    // --- IF PERSON EXISTS
                    if( graderInfo != null ){
                        // --- ADD ADMIN TO SCHOOL
                        classInfo.addGrader( graderInfo );
                    }
                    // --- ENDIF PERSON EXISTS
                }
                // --- ENDIF THE CLASS DOES NOT EXIST
            }
            // --- ENDDO FOR EACH GRADER NAME
        }
        catch(Exception e){
            // --- IF THE DATA IS EMPTY
            if( e.getMessage() == "null" ){
                // --- LET IT PASS
            }
            // --- ELSE
            else {
                System.out.println( "SQL Problem..." + e.getMessage() );
            }
        }
        // --- ENDOF HANDLE DATABASE
    }
    // --- END
    
    /**
     * @param classData
     * @param classIndex
     * @brief process student data from the database
     * 
     */
    public void addClassStudentInfo( ResultSet classData, Integer classIndex )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        Integer       courseId;
        Integer       classId;
        Integer       personId;
        SchoolStudent studentInfo;
        SchoolClass   classInfo;
        Float         finalGrade;
        
        // --- HANDLE DATABASE DATA
        try {
            // --- DO FOR EACH STUDENT NAME
            while( classData.next() ){
                // --- SAVE NAME IN SCHOOL LIST
                courseId = classData.getInt( "course_id" );
                classId = classData.getInt( "class_id" );
                classInfo = m_appModel.getClass( classId );
                
                // --- IF THE CLASS DOES NOT EXIST
                if( classInfo == null ){
                }
                // --- ELSE
                else {
                    // --- SET DATA
                    personId = classData.getInt( "student_participant_id" );
                    studentInfo = (SchoolStudent)m_appModel.getPerson( personId );
                    finalGrade = classData.getFloat( "final_grade" );
                    
                    // --- IF PERSON EXISTS
                    if( studentInfo != null ){
                        // --- ADD ADMIN TO SCHOOL
                        classInfo.addStudent( studentInfo );
                        classInfo.setFinalGrade( studentInfo, finalGrade );
                    }
                    // --- ENDIF PERSON EXISTS
                }
                // --- ENDIF THE CLASS DOES NOT EXIST
            }
            // --- ENDDO FOR EACH STUDENT NAME
        }
        catch(Exception e){
            // --- IF THE DATA IS EMPTY
            if( e.getMessage() == "null" ){
                // --- LET IT PASS
            }
            // --- ELSE
            else {
                System.out.println( "SQL Problem..." + e.getMessage() );
            }
        }
        // --- ENDOF HANDLE DATABASE
    }
    // --- END
    
    
    /**
     * @param adminData
     * @param schoolIndex
     * @brief process admin data from the database
     * 
     */

    public void addAdminInfo( ResultSet adminData, Integer schoolIndex )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        Integer      schoolId;
        Integer      personId;
        SchoolHouse  schoolInfo;
        SchoolPerson adminInfo;
        
        // --- HANDLE DATABASE DATA
        try {
            // --- DO FOR EACH SCHOOL NAME
            while( adminData.next() ){
                // --- SAVE NAME IN SCHOOL LIST
                schoolId = adminData.getInt( "school_id" );
                schoolInfo = m_appModel.getSchool( schoolId );
                
                // --- IF THE SCHOOL DOES NOT EXIST
                if( schoolInfo == null ){
                }
                // --- ELSE
                else {
                    // --- SET DATA
                    personId = adminData.getInt( "admin_participant_id" );
                    adminInfo = m_appModel.getPerson( personId );
                    
                    // --- IF PERSON EXISTS
                    if( adminInfo != null ){
                        // --- ADD ADMIN TO SCHOOL
                        schoolInfo.addAdmin( adminInfo );
                    }
                    // --- ENDIF PERSON EXISTS
                }
                // --- ENDIF THE SCHOOL DOES NOT EXIST
            }
            // --- ENDDO FOR EACH SCHOOL NAME
        }
        catch(Exception e){
            // --- IF THE DATA IS EMPTY
            if( e.getMessage() == "null" ){
                // --- LET IT PASS
            }
            // --- ELSE
            else {
                System.out.println( "SQL Problem..." + e.getMessage() );
            }
        }
        // --- ENDOF HANDLE DATABASE
    }
    // --- END

    /**
     * @param parentData
     * @param schoolIndex
     * @brief process parent data from the database
     * 
     */

    public void addParentInfo( ResultSet parentData, Integer schoolIndex )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        Integer       studentId;
        Integer       personId;
        SchoolHouse   schoolInfo;
        SchoolStudent studentInfo;
        SchoolPerson  parentInfo;
        
        // --- HANDLE DATABASE DATA
        try {
            // --- DO FOR EACH SCHOOL NAME
            while( parentData.next() ){
                // --- SAVE NAME IN SCHOOL LIST
                studentId = parentData.getInt( "student_participant_id" );
                studentInfo = (SchoolStudent)m_appModel.getPerson( studentId );
                
                // --- IF THE SCHOOL DOES NOT EXIST
                if( studentInfo == null ){
                }
                // --- ELSE
                else {
                    // --- SET DATA
                    personId = parentData.getInt( "parent_participant_id" );
                    parentInfo = m_appModel.getPerson( personId );
                    
                    // --- IF PERSON EXISTS
                    if( parentInfo != null ){
                        // --- ADD ADMIN TO SCHOOL
                        studentInfo.addParent( parentInfo );
                    }
                    // --- ENDIF PERSON EXISTS
                }
                // --- ENDIF THE SCHOOL DOES NOT EXIST
            }
            // --- ENDDO FOR EACH SCHOOL NAME
        }
        catch(Exception e){
            // --- IF THE DATA IS EMPTY
            if( e.getMessage() == "null" ){
                // --- LET IT PASS
            }
            // --- ELSE
            else {
                System.out.println( "SQL Problem..." + e.getMessage() );
            }
        }
        // --- ENDOF HANDLE DATABASE
    }
    // --- END
    
    /**
     * @param watcherData database data of watchers
     * @param schoolIndex
     * @brief process parent data from the database
     * 
     */
    public void addWatcherInfo( ResultSet watcherData, Integer schoolIndex )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        Integer       studentId;
        Integer       personId;
        SchoolStudent studentInfo;
        SchoolPerson  watcherInfo;
        
        // --- HANDLE DATABASE DATA
        try {
            // --- DO FOR EACH SCHOOL NAME
            while( watcherData.next() ){
                // --- SAVE NAME IN SCHOOL LIST
                studentId = watcherData.getInt( "student_participant_id" );
                studentInfo = (SchoolStudent)m_appModel.getPerson( studentId );
                
                // --- IF THE SCHOOL DOES NOT EXIST
                if( studentInfo == null ){
                }
                // --- ELSE
                else {
                    // --- SET DATA
                    personId = watcherData.getInt( "watcher_participant_id" );
                    watcherInfo = m_appModel.getPerson( personId );
                    
                    // --- IF PERSON EXISTS
                    if( watcherInfo != null ){
                        // --- ADD ADMIN TO SCHOOL
                        studentInfo.addWatcher( watcherInfo );
                    }
                    // --- ENDIF PERSON EXISTS
                }
                // --- ENDIF THE SCHOOL DOES NOT EXIST
            }
            // --- ENDDO FOR EACH SCHOOL NAME
        }
        catch(Exception e){
            // --- IF THE DATA IS EMPTY
            if( e.getMessage() == "null" ){
                // --- LET IT PASS
            }
            // --- ELSE
            else {
                System.out.println( "SQL Problem..." + e.getMessage() );
            }
        }
        // --- ENDOF HANDLE DATABASE
    }
    // --- END
    
    
    /**
     * @param courseData
     * @param courseIndex
     * @brief process course data from the database
     * 
     */
    public void addCourseInfo( ResultSet courseData, Integer courseIndex )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        Integer      dbKey;
        Integer      personId;
        Integer      dataVal;
        Integer      courseCredits;
        Integer      schoolForCourse;
        String       courseString;
        String       courseId;
        SchoolPerson personInfo;
        SchoolHouse  schoolInfo;
        SchoolCourse courseInfo;
              
        // --- HANDLE DATABASE DATA
        try {
            // --- DO FOR EACH COURSE NAME
            while( courseData.next() ){
                // --- SAVE NAME IN SCHOOL LIST
                schoolForCourse = courseData.getInt("school_id");
                schoolInfo = m_appModel.getSchool( schoolForCourse );
                
                // --- IF THE SCHOOL IS IN THE MODEL
                if( schoolInfo != null ){
                    // --- GET COURSE ID
                    dbKey = courseData.getInt( "course_id" );
                    courseCredits = courseData.getInt( "course_credits" );
                    courseInfo = schoolInfo.getCourse( dbKey );
                    courseString = courseData.getString( "course_name" );
                    courseId = courseData.getString( "org_course_id" );
                                    
                    // --- IF THE COURSE DOES NOT EXIST
                    if( courseInfo == null ){
                        courseInfo = new SchoolCourse( m_appHandle );
                        courseInfo.setId( dbKey );
                        courseInfo.setCredits( courseCredits );
                        courseInfo.setName( courseString );
                        courseInfo.setOrgAssigned( courseId );
                        courseInfo.setSchool( schoolForCourse );
                        schoolInfo.addCourse( courseInfo );
                        
                    }
                    else {
                        // --- SET DATA
                        courseInfo.setName( courseString );
                        
                    }
                    // --- ENDIF THE COURSE DOES NOT EXIST
                
               }
                // --- ENDIF THE SCHOOL IS IN THE MODEL
            }
            // --- ENDDO FOR EACH COURSE NAME
        }
        catch(Exception e){
            // --- IF THE DATA IS EMPTY
            if( e.getMessage() == "null" ){
                // --- LET IT PASS
            }
            // --- ELSE
            else {
                System.out.println( "SQL Problem..." + e.getMessage() );
            }
        }
        // --- ENDOF HANDLE DATABASE
    }
    // --- END

    /**
     * @brief process class data from the database
     * 
     */
    public void addClassInfo( ResultSet classData, Integer classId )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        Integer          dbKey;
        Integer          courseId;
        Integer          personId;
        Integer          dataVal;
        String           classString;
        Integer          scaleId;
        SchoolPerson     personInfo;
        SchoolCourse     courseInfo;
        SchoolClass      classInfo;
        Date             classDate;              
        Time             classTime;  
        SchoolGradeScale classScale;
              
        // --- HANDLE DATABASE DATA
        try {
            // --- DO FOR EACH CLASS
            while( classData.next() ){
                // --- SAVE NAME IN SCHOOL LIST
                courseId = classData.getInt( "course_id" );
                courseInfo = m_appModel.getCourse( courseId );
                classId = classData.getInt( "class_id" );
                classInfo  = courseInfo.getClass( classId );
                courseInfo.setState( DataState.HASDATA );
                                                    
                // --- IF THE COURSE DOES NOT EXIST
                if( classInfo == null ){
                    // --- CREATE OBJECT
                    classInfo  = new SchoolClass( m_appHandle ); 
                    classInfo.setId( classId );
                    classInfo.setCourseId( courseId );
                    courseInfo.getClassList().add( classInfo );
                }
                // --- ENDIF THE COURSE DOES NOT EXIST
                
                // --- SET CLASS DATA
                classString = classData.getString( "class_name" );
                classInfo.setClassName( classString );
                classDate = classData.getDate( "start_date" );
                classInfo.setStartDate( classDate );
                classDate = classData.getDate( "end_date" );
                classInfo.setEndDate( classDate );
                classTime = classData.getTime( "time" );
                classInfo.setTime( classTime );
                classString = classData.getString( "duration" );
                classInfo.setDuration( classString );
                scaleId = classData.getInt( "scale_id" );
                classScale = m_appModel.getScale( scaleId );
                classInfo.setScale( classScale );
            }
            // --- ENDDO FOR EACH CLASS
        }
        catch(Exception e){
            // --- IF THE DATA IS EMPTY
            if( e.getMessage() == "null" ){
                // --- LET IT PASS
            }
            // --- ELSE
            else {
                System.out.println( "SQL Problem..." + e.getMessage() );
            }
        }
        // --- ENDOF HANDLE DATABASE
    }
    // --- END
    
    /**
     * @param markData
     * @param markerId
     * @brief process marks data from the database
     * 
     */
    public void addMarkInfo( ResultSet markData, Integer markerId )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        Integer          dbKey;
        Integer          dataVal;
        SchoolMark       markInfo;
        SchoolClass      classInfo;
        Integer          markId;
        Integer          classId;
        String           markName;
        String           markModifier;
        Date             assignDate;              
        Date             dueDate;              
        Integer          markType;              
        int              gradePercent;
        int              minGrade;
        int              maxGrade;
        SchoolGradeScale classScale;
        Integer          scaleId;
        
        // --- HANDLE DATABASE DATA
        try {
            // --- DO FOR EACH MARK
            while( markData.next() ){
                // --- SAVE NAME IN SCHOOL LIST
                markId = markData.getInt( "mark_id" );
                classId = markData.getInt( "class_id" );
                classInfo = m_appModel.getClass( classId );
                markName = markData.getString( "mark_name" );
                assignDate = markData.getDate( "date_assigned");
                dueDate = markData.getDate( "date_due");
                markType = markData.getInt( "marker_type_id");
                gradePercent = markData.getInt( "final_grade_percent" );
                minGrade = markData.getInt( "min_grade" );
                maxGrade = markData.getInt( "max_grade" );
                classInfo  = m_appModel.getClass( classId );
                markModifier = markData.getString( "mark_modifier" );                 
                scaleId = markData.getInt( "scale_id" );
                                                    
                // --- IF THE CLASS DOES NOT EXIST
                if( classInfo != null ){
                    // ---  GET MARK INFO
                    markInfo = classInfo.getMark( markId );
                    
                    // --- IF THE MARK DOES NOT EXIST
                    if( markInfo == null ){
                        // --- CREATE OBJECT
                        markInfo  = new SchoolMark( m_appHandle ); 
                        markInfo.setId( markId );
                        markInfo.setSchoolClass( classInfo );
                        classInfo.addMark( markInfo );
                    }
                    // --- ENDIF THE MARK DOES NOT EXIST
                
                    // --- SET CLASS DATA
                    markInfo.setAssignmentName( markName );
                    markInfo.setAssignedDate( assignDate );
                    markInfo.setDueDate( dueDate );
                    markInfo.setType( markType );
                    markInfo.setGradePercent( gradePercent );
                    markInfo.setMinGradeValue( minGrade );
                    markInfo.setMaxGradeValue( maxGrade );
                    markInfo.setModifier( markModifier );
                    classScale = m_appModel.getScale( scaleId );
                    classInfo.setScale( classScale );
                }
                // --- ENDIF THE CLASS DOES NOT EXIST
            }
            // --- ENDDO FOR EACH MARK
        }
        catch(Exception e){
            // --- IF THE DATA IS EMPTY
            if( e.getMessage() == "null" ){
                // --- LET IT PASS
            }
            // --- ELSE
            else {
                System.out.println( "SQL Problem..." + e.getMessage() );
            }
        }
        // --- ENDOF HANDLE DATABASE
    }
    // --- END 
    
    /**
     * @param gradeData
     * @param classGradeId
     * @brief process grade data from the database
     * 
     */
    public void addGradeInfo( ResultSet gradeData, Integer classGradeId )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        Integer       dbKey;
        Integer       dataVal;
      //SchoolHouse   schoolInfo = getSchool( schoolId );
        SchoolMark    markInfo;
        SchoolClass   classInfo;
        SchoolGrade   gradeInfo;
        Integer       gradeId;
        Integer       markId;
        Integer       classId;
        Integer       studentId;
        SchoolStudent studentInfo;
        Date          handInDate;              
        Integer       grade;              
        Integer       gradeChange;              
        
        // --- HANDLE DATABASE DATA
        try {
            // --- DO FOR EACH MARK
            while( gradeData.next() ){
                // --- SAVE GRADE DATA
                gradeId = gradeData.getInt( "grade_id" );
                markId = gradeData.getInt( "mark_id" );
                markInfo = m_appModel.getMark( markId );
                classId = gradeData.getInt( "class_id" );
                classInfo = m_appModel.getClass( classId );
                studentId = gradeData.getInt( "student_participant_id" );
                studentInfo = (SchoolStudent)m_appModel.getPerson( studentId );
                handInDate = gradeData.getDate( "date_handed_in");
                grade = gradeData.getInt( "grade");
                gradeChange = gradeData.getInt( "grade_change" );
                                                             
                // --- IF THE CLASS DOES NOT EXIST
                if( markInfo != null ){
                    // ---  GET MARK INFO
                    gradeInfo = markInfo.getGrade( gradeId );
                    
                    // --- IF THE MARK DOES NOT EXIST
                    if( gradeInfo == null ){
                        // --- CREATE OBJECT
                        gradeInfo  = new SchoolGrade( m_appHandle ); 
                        gradeInfo.setId( gradeId );
                        gradeInfo.setSchoolClass( classInfo );
                        gradeInfo.setMark( markInfo );
                        markInfo.addGrade( gradeInfo );
                    }
                    // --- ENDIF THE MARK DOES NOT EXIST
                
                    // --- SET GRADE DATA
                    gradeInfo.setStudent( studentInfo );
                    gradeInfo.setDateReturned( handInDate );
                    gradeInfo.setGrade( grade );
                    gradeInfo.setGradeChange( gradeChange );
                }
                // --- ENDIF THE CLASS DOES NOT EXIST
            }
            // --- ENDDO FOR EACH MARK
        }
        catch(Exception e){
            // --- IF THE DATA IS EMPTY
            if( e.getMessage() == "null" ){
                // --- LET IT PASS
            }
            // --- ELSE
            else {
                System.out.println( "SQL Problem..." + e.getMessage() );
            }
        }
        // --- ENDOF HANDLE DATABASE
    }
    // --- END  
    
    /**
     * @param notifyData database data of notifications
     * @param studentIndex TBD maybe restrict data to a student in future
     * @brief process notification data from the database
     * 
     */
    public void addNotifyInfo( ResultSet notifyData, Integer studentIndex )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        Integer            notifyId;
        Integer            studentId;
        Integer            watcherId;
        Integer            classId;
        String             criteria;
        SchoolStudent      studentInfo;
        SchoolPerson       watcherInfo;
        SchoolClass        classInfo;
        SchoolNotification notifyInfo;
        SchoolNotifyType   notifyType;
        SchoolContactType  contactType;
        String             contactString;
        String             newContact;
        
        // --- HANDLE DATABASE DATA
        try {
            // --- DO FOR EACH SCHOOL NAME
            while( notifyData.next() ){
                // --- ACQUIRE NOTIFY DATA
                notifyId = notifyData.getInt( "notify_id" );
                studentId = notifyData.getInt( "student_participant_id" );
                watcherId = notifyData.getInt( "parent_watcher_participant_id" );
                classId = notifyData.getInt( "class_id" );
                criteria = notifyData.getString( "criteria" );
                contactString = notifyData.getString( "contact_method" );
                newContact = notifyData.getString( "special_contact" );
                System.out.println( "SchoolModelPopulater::addNotifyInfo id - "+ notifyId  );
                
                // ---  CONVERT
                studentInfo = (SchoolStudent)m_appModel.getPerson( studentId );
                watcherInfo = m_appModel.getPerson( watcherId );
                classInfo = m_appModel.getClass( classId );
                notifyType = SchoolNotifyType.fromString( criteria );
                notifyInfo = m_appModel.getNotification( notifyId );
                contactType = SchoolContactType.fromString(contactString );
                
                // --- IF THE NOTIFY DOES NOT EXIST
                if( notifyInfo == null ){
                    // --- CREATE
                    notifyInfo = new SchoolNotification( m_appHandle );
                    notifyInfo.setId( notifyId );
                }
                // --- ELSE
                else {
                    // --- NO ACTION NEEDED                    
                }
                // --- ENDIF THE NOTIFY DOES NOT EXIST
                
                // --- SET DATA
                notifyInfo.setSchoolClass( classInfo );
                notifyInfo.setStudent( studentInfo );
                notifyInfo.setType( notifyType );
                notifyInfo.setWatcher( watcherInfo );
                notifyInfo.setContact( newContact );
                notifyInfo.setContactType( contactType );
                studentInfo.addNotification( notifyInfo );
                
                // --- IF THE CRITERIA IS NOT STANDARD
                if( notifyType == SchoolNotifyType.UNKNOWN ){
                    // --- COPY STRING
                    notifyInfo.setCriteria( criteria );
                }
                // --- ENDIF THE CRITERIA IS NOT STANDARD

            }
            // --- ENDDO FOR EACH SCHOOL NAME
        }
        catch(Exception e){
            // --- IF THE DATA IS EMPTY
            if( e.getMessage() == "null" ){
                // --- LET IT PASS
            }
            // --- ELSE
            else {
                System.out.println( "SQL Problem..." + e.getMessage() );
            }
        }
        // --- ENDOF HANDLE DATABASE
    }
    // --- END
    
    /**
     * @param messageData database data of messages
     * @param msgIndex message index
     * @brief process message data from the database
     * 
     */
    public void addMessageInfo( ResultSet messageData, Integer msgIndex )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        Integer            messageId;
        Integer            source;
        Integer            destination;
        Date               msgDate;
        Time               msgTime;
        String             msg;
        Integer            msgStateId;
        String             subject;
        String             typeString;
        String             msgPath;
        SchoolPerson       msgPersonSrc;        
        SchoolPerson       msgPersonDest;        
        SchoolMessage      messageInfo;
        SchoolMessageType  msgType = SchoolMessage.SchoolMessageType.GENERAL;
        SchoolContactType  contactType = SchoolContactType.EMAIL;
        SchoolMessageState msgState;
        ArrayList<SchoolMessage> messageList = m_appModel.getMessageList();
        
        // --- HANDLE DATABASE DATA
        try {
            // --- DO FOR EACH SCHOOL NAME
            while( messageData.next() ){
                // --- ACQUIRE NOTIFY DATA
                messageId = messageData.getInt( "id" );
                source = messageData.getInt( "pfrom" );
                destination = messageData.getInt( "pto" );
                msgDate = messageData.getDate( "date_sent" );
                msgTime = messageData.getTime( "time_sent" );
                msg = messageData.getString( "message" );
                msgStateId = messageData.getInt( "message_read" );
                subject = messageData.getString( "subject" );
                typeString = messageData.getString( "msg_type" );
                msgPath = messageData.getString( "msg_path" );
                
                // ---  CONVERT
                msgPersonSrc = m_appModel.getPerson( source );
                msgPersonDest = m_appModel.getPerson( destination );
                msgState = SchoolMessageState.fromValue( msgStateId );
                messageInfo = m_appModel.getMessage( messageId );
                contactType = SchoolContactType.fromString( typeString );
                
                // --- IF THE NOTIFY DOES NOT EXIST
                if( messageInfo == null ){
                    // --- CREATE
                    messageInfo = new SchoolMessage( m_appHandle );
                    messageInfo.setId( messageId );
                    messageList.add( messageInfo );
                }
                // --- ELSE
                else {
                    // --- NO ACTION NEEDED                    
                }
                // --- ENDIF THE NOTIFY DOES NOT EXIST
                
                // --- SET DATA
                messageInfo.setDate( msgDate );
                messageInfo.setDestination( msgPersonDest );
                messageInfo.setMessage( msg );
                messageInfo.setMsgState( msgState );
                messageInfo.setSource(msgPersonSrc);
                messageInfo.setSubject( subject );
                messageInfo.setTime( msgTime );
                messageInfo.setMsgType( msgType );
                messageInfo.setNotifyType( contactType );
                messageInfo.setPath( msgPath );
            }
            // --- ENDDO FOR EACH SCHOOL NAME
        }
        catch(Exception e){
            // --- IF THE DATA IS EMPTY
            if( e.getMessage() == "null" ){
                // --- LET IT PASS
            }
            // --- ELSE
            else {
                System.out.println( "SQL Problem..." + e.getMessage() );
            }
        }
        // --- ENDOF HANDLE DATABASE
    }
    // --- END
    
    
    /**
     * @param rankData database data of messages
     * @param msgIndex message index
     * @brief process message data from the database
     * 
     */
    public void addRankInfo( ResultSet rankData, Integer msgIndex )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        Integer           rankId;
        String            rankName;
        Integer           rankLevel;
        float             rankPoints;
        SchoolGradeRank   rankInfo;        
        ArrayList<SchoolGradeRank> rankList = m_appModel.getRankList();
        
        // --- HANDLE DATABASE DATA
        try {
            // --- DO FOR EACH SCHOOL NAME
            while( rankData.next() ){
                // --- ACQUIRE NOTIFY DATA
                rankId     = rankData.getInt( "rank_id" );
                rankName   = rankData.getString( "rank_name" );
                rankLevel  = rankData.getInt( "rank_level" );
                rankPoints = rankData.getFloat( "rank_points" );
                
                // ---  CONVERT
                rankInfo = m_appModel.getRank( rankId );
                
                // --- IF THE NOTIFY DOES NOT EXIST
                if( rankInfo == null ){
                    // --- CREATE
                    rankInfo = new SchoolGradeRank( m_appHandle );
                    rankInfo.setId( rankId );
                    rankList.add( rankInfo );
                }
                // --- ELSE
                else {
                    // --- NO ACTION NEEDED                    
                }
                // --- ENDIF THE NOTIFY DOES NOT EXIST
                
                // --- SET DATA
                rankInfo.setName( rankName );
                rankInfo.setLevel( rankLevel );
                rankInfo.setPoints( rankPoints );
            }
            // --- ENDDO FOR EACH SCHOOL NAME
        }
        catch(Exception e){
            // --- IF THE DATA IS EMPTY
            if( e.getMessage() == "null" ){
                // --- LET IT PASS
            }
            // --- ELSE
            else {
                System.out.println( "SQL Problem..." + e.getMessage() );
            }
        }
        // --- ENDOF HANDLE DATABASE
    }
    // --- END
    
    /**
     * @param rankData database data of messages
     * @param msgIndex message index
     * @brief process message data from the database
     * 
     */
    public void addScaleInfo( ResultSet scaleData, Integer msgIndex )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        Integer           scaleId;
        String            scaleName;
        Integer           rankLevelAp;
        Integer           rankLevelA;
        Integer           rankLevelAm;
        Integer           rankLevelBp;
        Integer           rankLevelB;
        Integer           rankLevelBm;
        Integer           rankLevelCp;
        Integer           rankLevelC;
        Integer           rankLevelCm;
        Integer           rankLevelDp;
        Integer           rankLevelD;
        Integer           rankLevelDm;
        Integer           rankLevelF;
        SchoolGradeScale  scaleInfo; 
        SchoolGradeRank   rankInfo;
        ArrayList<SchoolGradeScale> scaleList = m_appModel.getScaleList();
        
        // --- HANDLE DATABASE DATA
        try {
            // --- DO FOR EACH SCHOOL NAME
            while( scaleData.next() ){
                // --- ACQUIRE NOTIFY DATA
                // --- TBD... maybe change database or enum name so we can loop
                scaleId     = scaleData.getInt( "scale_id" );
                scaleName   = scaleData.getString( "scale_name" );
                rankLevelAp = scaleData.getInt( "ap_level" );
                rankLevelA  = scaleData.getInt( "a_level" );
                rankLevelAm = scaleData.getInt( "am_level" );
                rankLevelBp = scaleData.getInt( "bp_level" );
                rankLevelB  = scaleData.getInt( "b_level" );
                rankLevelBm = scaleData.getInt( "bm_level" );
                rankLevelCp = scaleData.getInt( "cp_level" );
                rankLevelC  = scaleData.getInt( "c_level" );
                rankLevelCm = scaleData.getInt( "cm_level" );
                rankLevelDp = scaleData.getInt( "dp_level" );
                rankLevelD  = scaleData.getInt( "d_level" );
                rankLevelDm = scaleData.getInt( "dm_level" );
                rankLevelF  = scaleData.getInt( "f_level" );
                
                // ---  CONVERT
                scaleInfo = m_appModel.getScale( scaleName );
                
                // --- IF THE NOTIFY DOES NOT EXIST
                if( scaleInfo == null ){
                    // --- CREATE
                    scaleInfo = new SchoolGradeScale( m_appHandle );
                    scaleInfo.setId( scaleId );
                    scaleList.add( scaleInfo );
                }
                // --- ELSE
                else {
                    // --- NO ACTION NEEDED                    
                }
                // --- ENDIF THE NOTIFY DOES NOT EXIST
                
                // --- SET DATA
                // --- ... TBD check ranks for validity 
                scaleInfo.setName( scaleName );
                rankInfo = m_appModel.getRank( rankLevelAp );
                scaleInfo.setRank( ScalePoint.APLUS, rankInfo );
                rankInfo = m_appModel.getRank( rankLevelA );
                scaleInfo.setRank( ScalePoint.A, rankInfo );
                rankInfo = m_appModel.getRank( rankLevelAm );
                scaleInfo.setRank( ScalePoint.AMINUS, rankInfo );
                rankInfo = m_appModel.getRank( rankLevelBp );
                scaleInfo.setRank( ScalePoint.BPLUS, rankInfo );
                rankInfo = m_appModel.getRank( rankLevelB );
                scaleInfo.setRank( ScalePoint.B, rankInfo );
                rankInfo = m_appModel.getRank( rankLevelBm );
                scaleInfo.setRank( ScalePoint.BMINUS, rankInfo );
                rankInfo = m_appModel.getRank( rankLevelCp );
                scaleInfo.setRank( ScalePoint.CPLUS, rankInfo );
                rankInfo = m_appModel.getRank( rankLevelC );
                scaleInfo.setRank( ScalePoint.C, rankInfo );
                rankInfo = m_appModel.getRank( rankLevelCm );
                scaleInfo.setRank( ScalePoint.CMINUS, rankInfo );
                rankInfo = m_appModel.getRank( rankLevelDp );
                scaleInfo.setRank( ScalePoint.DPLUS, rankInfo );
                rankInfo = m_appModel.getRank( rankLevelD );
                scaleInfo.setRank( ScalePoint.D, rankInfo );
                rankInfo = m_appModel.getRank( rankLevelDm );
                scaleInfo.setRank( ScalePoint.DMINUS, rankInfo );
                rankInfo = m_appModel.getRank( rankLevelF );
                scaleInfo.setRank( ScalePoint.FAIL, rankInfo );
            }
            // --- ENDDO FOR EACH SCHOOL NAME
        }
        catch(Exception e){
            // --- IF THE DATA IS EMPTY
            if( e.getMessage() == "null" ){
                // --- LET IT PASS
            }
            // --- ELSE
            else {
                System.out.println( "SQL Problem..." + e.getMessage() );
            }
        }
        // --- ENDOF HANDLE DATABASE
    }
    // --- END
    
    /**
     * @brief add role data
     * 
     */
    public void addRoleInfo( ResultSet roleData, Integer roleId )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES

        // --- TBD WHAT TO DO HERE
        // ---...not sure we can update an enum
        /*
        // --- CASE ALL OF THE ROLE DATA
        case ROLE_DATA:
            // --- STORE ROLE NAMES
            m_roleMap.clear();

            // --- DO FOR EACH ROLE NAME
            while( data.next() ){
                // --- SAVE NAME IN SCHOOL LIST
                dbKey = data.getInt("role_id");
                dbValue = data.getString("role_name");
                m_roleMap.put( dbKey, dbValue );                               
            }
            // --- ENDDO FOR EACH ROLE NAME

            // --- SAVE DATA
            // --- ... TBD need to remove current result set if exists
            m_dataMap.put( currEvent, data );
            eventProcessed = true;
        break;
        */
    }
    // --- END
         
    /**
     * @brief add role data
     * 
     */
    public void addTypeInfo( ResultSet typeData, Integer typeId )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES

        // --- TBD WHAT TO DO HERE
        // ---...not sure we can update an enum
        /*
                case TYPE_DATA:
                    // --- STORE ROLE NAMES
                    m_typeMap.clear();
                    
                    // --- DO FOR EACH MARK TYPE NAME
                    while( data.next() ){
                        // --- SAVE NAME IN SCHOOL LIST
                        dbKey = data.getInt("marker_type_id");
                        dbValue = data.getString("marker_type_name");
                        m_roleMap.put( dbKey, dbValue );                               
                    }
                    // --- ENDDO FOR EACH MARK TYPE NAME

                    // --- SAVE DATA
                    // --- ... TBD need to remove current result set if exists
                    m_dataMap.put( currEvent, data );
                    eventProcessed = true;
                break;
        */
    }
    // --- END
         
                        
}

