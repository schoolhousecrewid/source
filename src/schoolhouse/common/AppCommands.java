
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package schoolhouse.common;

import java.util.HashMap;
import java.util.Vector;
import schoolhouse.common.HandleCmdAction;

/**
 *
 * @author wpeets
 * 
 * This class serves as representation of a string based command structure
 * 
 * Ex.
 * Action Vector    Object Vector        (hints) for a fictitious role
 * =============    =============        ==========================
 * Add              School               Add Person
 * Remove           Person               Add Grade
 * Change           Mark                 Remove Grade
 * View             Course               Change Grade
 * Import           Class                View School
 * Export           Grade                View Mark
 *                  Notification         View Course
 *                                       View Class
 *                                       View Grade
 *                                       View Notification
 *                                       Import Grade
 *                                       Export Grade
 * 
 * Additional vectors up to 4 
 * can be added or not supplied.  Hints can also not
 * be supplied.  The goal of the hints is to narrow the permutations of 
 * possible solutions.
 * 
 */
public class AppCommands {


   
    public enum CommandValue { Hint(1),    Enable(2),   Disable(3),
                               Show(4),    Hide(5),     Focus(6),   
                               Ack(7),     Shutdown(8), Process(9),
                               Bypass(10), OTHER(11),   UNKNOWN(12);
                        
    private final int m_value;
    private static final int enumSize = CommandValue.values().length;

    /**
     * @brief constructor
     * 
     * This initializes values
     */
        private CommandValue( int value )
        // --- BEGIN
        {
            // --- SET VALUE
            this.m_value = value;
        }
        // --- END
        
        /**
         * @brief size of enum
         * 
         * returns the size of enum
         */
        static int getSize( ) {
            return( enumSize );
        } 
        
        /**
         * @brief get int cast of enum
         * 
         * returns the ordinal representation of enum
         */
        public int getValue()
        // --- BEGIN
        {
            // --- RETURN INTEGER REPRESENTATION
            return( m_value );
        }       
        // --- END
        
        
        /**
         * @brief allow conversion from ordinal value to enum object
         * 
         * This takes a value and returns an enumeration representation.
         */
        static CommandValue fromValue( int value )
        // --- BEGIN
                { 
            // --- DECLARE LOCAL VVARIABLES
            CommandValue enumRep = null;
            
            // --- DO WHILE REPRESENTATION NOT FOUND
            for( CommandValue enumVal: CommandValue.values() ) { 
                // --- IF ORDINAL VALUE MATCHES
                if( enumVal.m_value == value) { 
                    // --- RETURN ENUM
                    return enumVal;  
                }  
                // --- ENDIF ORDINAL VALUE MATCHES
            }  
            // --- ENDDO WHILE REPRESENTATION NOT FOUND
  
            // --- RETURN
            return( enumRep );  
        }  
        // --- END
    };
    
    
    
    /**
     * list of explicit hints
     */
    HashMap m_cmdHints;
    
    /**
     * list of hint components
     */
    HashMap m_cmdVectors;
    
    /**
     * event handler for events
     */
    private HandleCmdAction m_eventHandler;
 
    /**
     * application id
     */
    private String m_appId;
    
    /**
     * enumeration identifying command
     */
    private CommandValue m_cmd;
     
    /**
     * constructor
     */
    public  AppCommands( HandleCmdAction eventHandler )
    // --- BEGIN
    {
        // --- INITIALIZE
        m_eventHandler = eventHandler;
        m_cmdHints     = new HashMap();
        m_cmdVectors   = new HashMap();
        
    }
    // --- END
    
    /**
     * add to list of hint components
     */
    public void addHint( String newHint, int hintId )
    // --- BEGIN
    {
        // --- ADD COMMAND
        m_cmdHints.put( hintId, newHint );
        
    }
    // --- END
    
    /**
     * list of hint components
     */
    public void addHintVector( String vectorName, Vector hintVector )
    // --- BEGIN
    {
        // --- ADD LIST
        m_cmdVectors.put( vectorName, hintVector );
        
    }
    // --- END

        /**
     * list of hint components
     */
    public Vector getList( String vectorName, Vector hintVector )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        Vector vectorToReturn = null;
        
        // --- IF REQUESTING HINTS
        if( vectorName.equals( "hints") ){
            // --- RETURN HINTS
            //vectorToReturn = m_cmdHints;
        }
        // --- ELSE
        else {
            // --- GET LIST
            vectorToReturn = (Vector)m_cmdVectors.get( vectorName );
        }
        // --- ENDIF REQUESTING HINTS
        
        // --- RETURN
        return( vectorToReturn );
    }
    // --- END
    
    
    public HashMap getHints( )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        HashMap vectorToReturn = null;
        
        vectorToReturn = m_cmdHints;
        
        // --- RETURN
        return( vectorToReturn );
    }
    // --- END
    

    /**
     * @return the m_appId
     */
    public String getAppId() {
        return m_appId;
    }

    /**
     * @param appId the m_appId to set
     */
    public void setAppId( String appId ) {
        this.m_appId = appId;
    }
    
    
    /**
     * @return the m_cmd
     */
    public CommandValue getCmd() {
        return m_cmd;
    }

    /**
     * @param m_cmd the m_cmd to set
     */
    public void setCmd(CommandValue cmd) {
        this.m_cmd = cmd;
    }
    
    /**
     * @return the m_eventHandler
     */
    public HandleCmdAction getEventHandler() {
        return m_eventHandler;
    }

    /**
     * @param m_eventHandler the m_eventHandler to set
     */
    public void setEventHandler(HandleCmdAction m_eventHandler) {
        this.m_eventHandler = m_eventHandler;
    }

    
}
