/*!
 * \class DbEngine
 * \brief Base class for views that interact with the database for school data
 *
 * This class serves as the base class for any panel that will access the
 * schoolhouse database.
 *
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package schoolhouse.common;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import schoolhouse.voice.VRMessaging;


/**
 *
 * @author wpeets
 */
public class DbEngine extends JPanel implements ActionListener,
                                                HandleCmdAction
{ 
    /**
     * constants used to define data actions
     */
  //public static final String SCHOOL_DATA       = SchoolDbIntfc.SchoolQuery.SCHOOL.dbName();
    public static final String SCHOOL_DATA       = "allschool";
    public static final String PEOPLE_DATA       = "allpeople";
    public static final String COURSE_DATA       = "allcourse";
    public static final String CLASS_DATA        = "allclass";
    public static final String ROLE_DATA         = "allrole";
    public static final String TYPE_DATA         = "alltype";
    public static final String MARK_DATA         = "allmark";
    public static final String GRADE_DATA        = "allgrade";
    public static final String EDUCATOR_DATA     = "teacherlist";
    public static final String PARENT_DATA       = "parentlist";
    public static final String WATCHER_DATA      = "watcherlist";
    public static final String GRADER_DATA       = "graderlist";
    public static final String STUDENT_DATA      = "studentlist";
    public static final String NOTIFICATION_DATA = "notifylist";
    public static final String RANK_DATA         = "ranklist";
    public static final String SCALE_DATA        = "scalelist";
    public static final String MESSAGE_DATA      = "messagelist";
    public static final String SCHOOL_NAMES      = "school_names";
    public static final String PEOPLE_NAMES      = "people_names";
    public static final String COURSE_NAMES      = "course_names";
    public static final String CLASS_NAMES       = "class_names";
    public static final String ROLE_NAMES        = "role_names";
    public static final String USERROLES         = "userrole";
    public static final String ADMIN_DATA        = "adlist";
    public static final String SCHOOL_ID         = "schoolid";
    public static final String PERSON_ID         = "personid";
    public static final String USER_DATA         = "userid";
    public static final String USER_ROLE_DATA    = "userroleid";
    public static final String COURSE_ID         = "courseid";
    public static final String ADMIN_INFO        = "adminmap";
    public static final String SCHOOL_CLASS      = "schoolclass";
    public static final String STUDENTS_STD      = "stdlist";
    public static final String PARENTS_STD       = "parentstdlist";
    public static final String WATCHERS_STD      = "watcherstdlist";
    public static final String EDUCATORS_STD     = "educatorstdlist";
    public static final String GRADERS_STD       = "graderstdlist";
    public static final String ADMINS_STD        = "adminstdlist";
    public static final int    FORM_OFFSET       = 9000;
    public static final int    SCHOOL_OFFSET     = 8000;
    public static final int    PEOPLE_OFFSET     = 7000;
    public static final int    ACTION_OFFSET     = 1000;
    /*
     * TBD a thoussand offset is not enough for a large number of
     * persons or a large number of classes; a different
     * approach is required
     */

     /**
     * application data support
     */
    protected JFrame         m_parentFrame; // --- PARENT CONTAINER
    protected HashMap        m_dataMap;     // --- ALL DATA ACQUIRED FROM DATABASE
    protected HashMap        m_schoolMap;   // --- LIST OF SCHOOLS
    protected HashMap        m_peopleMap;   // --- LIST OF PEOPLE
    protected HashMap        m_adminMap;    // --- LIST OF PEOPLE
    protected HashMap        m_courseMap;   // --- LIST OF COURSES
    protected HashMap        m_classMap;    // --- LIST OF CLASSES
    protected HashMap        m_titleMap;    // --- LIST OF TITLE
    protected HashMap        m_roleMap;     // --- LIST OF ROLES
    protected HashMap        m_typeMap;     // --- LIST OF GRADE TYPES
    protected HashMap        m_markMap;     // --- LIST OF MARKS
    protected HashMap        m_gradeMap;    // --- LIST OF GRADE
    protected HashMap        m_educatorMap; // --- LIST OF GRADE
    protected HashMap        m_graderMap;   // --- LIST OF GRADE
    protected HashMap        m_notifyMap;   // --- LIST OF NOTIFICATIONS
    public    QueryChain     m_dbChain;     // --- USED FOR MASSIVE QUERIES
    public    QueryThread    m_dbCommand;   // --- USED TO INSERT INTO DATABASE
    protected ArrayList <JLabel> m_labelList;
    protected SchoolApp      m_appHandle;
    protected boolean        m_engineEnabled;
              String         m_nameId;
              DbEngine       m_subEngine;
              
              
    // --- DB QUERY SUPPORT
    // --- ... deprecated can be deleted at some point
    protected ResultSet   m_dataSet;    
    String                m_currentCommand; // set by engine; read by thread
    Boolean               m_cmdInProgress;
            
    // --- VOICE SUPPORT
    protected VRMessaging<AppCommands,String> m_VRThread;              
    protected AppCommands   m_engineHints;
    protected AppCommands   m_engineState;
    protected boolean       m_hasHints;

    // --- DISPLAY SUPPORT
    int                m_labelWidth;
    int                m_dataWidth;
    int                m_rowCount;
    GridBagConstraints m_gbc;
    
    public DbEngine( SchoolApp appHandle )
    // --- BEGIN
    {
        super( new BorderLayout() );
           
        // --- SAVE PARENT
        m_appHandle     = appHandle;
        m_parentFrame   = appHandle.getFrame();
        m_VRThread      = schoolhouse.login.LoginForm1.mVRThread;
        m_hasHints      = false;
        m_engineEnabled = true;
        m_subEngine     = null;
        
        // --- CREATE STORAGE FOR MEMORY
        m_dataMap     = new HashMap();
        m_schoolMap   = new HashMap();
        m_peopleMap   = new HashMap();
        m_courseMap   = new HashMap();
        m_classMap    = new HashMap();
        m_titleMap    = new HashMap();
        m_roleMap     = new HashMap();
        m_typeMap     = new HashMap();
        m_markMap     = new HashMap();
        m_gradeMap    = new HashMap();
        m_adminMap    = new HashMap();
        //m_educatorMap = new HashMap();
        //m_graderMap   = new HashMap();
                
        // --- SET LAYOUT
        setLayout( new GridBagLayout() );
        m_labelWidth  = 500;
        m_dataWidth   = 500;
        m_rowCount    = 0;
        m_gbc         = new GridBagConstraints();
        m_labelList   = new ArrayList<JLabel>();
        m_engineHints = new AppCommands( this );
        m_engineState = new AppCommands( this );
        UIManager.getDefaults().put( "Separator.foreground", Color.RED );    
        UIManager.getDefaults().put( "Separator.background", Color.RED );    
    }
    // --- END  
    
    /**
     * @brief initialize  
     * 
     */
    public void initialize()
    // --- BEGIN
    {
        
    }
    // --- END
    
    
    /**
     * @param componentLabel String label for row component
     * @param displayObject Object to add to form
     * @brief add row to work panel 
     * 
     */
    protected void addRow(String componentLabel, JComponent displayObject)
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLE
        JLabel currentLabel = new JLabel( componentLabel + ":");
        
        // --- ADD LABEL
        currentLabel.setSize( m_labelWidth, 0 );
        m_gbc.gridx = 0;
        m_gbc.gridy = m_rowCount;
        add( currentLabel, m_gbc );
        m_labelList.add( currentLabel );
        
        // --- ADD COMPONENT
        displayObject.setSize( m_dataWidth, 0 );
        m_gbc.gridx++;
        add( displayObject, m_gbc );
        m_rowCount++;
    }
    // --- END 
    
    /**
     * @brief add a separator in the work panel 
     * 
     */
    protected void addSeparator( )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLE
        JSeparator currentSep = new JSeparator( SwingConstants.HORIZONTAL );
        
        // --- ADD  HORIONTAL SEPARATOR
        currentSep.setPreferredSize( new Dimension(500,10));
        //currentSep.setBackground( Color.green );
        //currentSep.setForeground( Color.blue );
        currentSep.setForeground( (Color.yellow) );
        currentSep.setBackground( (Color.yellow) );
        m_gbc.gridx = 0;
        m_gbc.gridy = m_rowCount;
        m_gbc.gridwidth = 2;
        m_gbc.fill = GridBagConstraints.VERTICAL;
        m_gbc.fill = GridBagConstraints.HORIZONTAL;
        m_gbc.weighty = 1;
        add( currentSep, m_gbc );
        m_gbc.gridwidth = 1;
        m_rowCount++;
    }
    // --- END 
    
    /**
     * @param labelIndex row offset of label
     * @return object
     * @brief get component label 
     * 
     */
    protected JLabel getLabel( int labelIndex )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLE
        JLabel returnLabel = null;
        
        // --- IF THE PANEL HAS THE LABEL
        if( m_labelList.size() >= labelIndex ){
            // --- ADD  HORIONTAL SEPARATOR
            returnLabel = m_labelList.get( labelIndex );
        }
        // --- ENDIF THE PANEL HAS THE LABEL

        // --- RETURN
        return( returnLabel );
    }
    // --- END 
    
    
    /**
     * @return image representation 
     *  @brief create image resource 
     *  @param path name of image file
     * Returns an ImageIcon, 
     * or null if the path was invalid. 
     */
    protected static ImageIcon createImageIcon( String path ) {
        java.net.URL imgURL = DbEngine.class.getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL);
        } else {
            System.err.println( "Couldn't find file: " + path );
            return null;
        }
    }
    
    /**
     * show GUI.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
     public void showPanel() {
        //Display the window.
        setVisible( true );
     }
 
         /**
     * hide GUI.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
     public void hidePanel() {
        //Display the window.
        setVisible( false );
     }
     
    /** Listens to the combo box.
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {

    }
    /** Listens to the combo box.
     * @param e GUI action
     * @return true if the action is handled
     */
    @Override
    public boolean handleAction(ActionEvent e) {
        // --- DECLARE LOCAL VARIAABLES
        boolean eventHandled = false;
        
        // --- RETURN WHETHER THE EVENT WAS HANDLED
        return( eventHandled );
    }
    
    /**
     * @param currEvent application event
     * @return true if event is processed
     * @brief handle application events
     *
     */
    @Override
    public boolean handleEvent( String currEvent ) 
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIAABLES
        boolean   eventProcessed = false;
        
        // --- DO CASE OF EVENT
        switch( currEvent ){
            case "Focus":
                // --- SEND VOICE FOCUS
                updateState( AppCommands.CommandValue.Focus );
                eventProcessed = true;
            break;
            default:
            break;    
        }
        // --- ENDDO CASE OF EVENT
        // --- RETURN WHETHER THE EVENT WAS HANDLED
        return( eventProcessed );
    }
    // --- END
    
    /**
     * @param currEvent id of database data
     * @param data database data
     * @return true if any processing was done
     * @brief handle application events
     *
     */
    @Override
    public boolean handleQuery( String currEvent, ResultSet data )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIAABLES
        boolean eventProcessed = false;
        System.out.println( "DbEngine::handleQuery " + currEvent );
        
        try {
            // --- RESET DATA
            data.beforeFirst();
            
            // --- DO CASE OF QUERY TYPE
            switch( currEvent ){
                // --- CASE ALL OF THE SCHOOL DATA
                case SCHOOL_DATA:
                case PEOPLE_DATA:
                case ADMIN_DATA:
                case ROLE_DATA:
                case TYPE_DATA:
                case COURSE_DATA:
                case CLASS_DATA:
                case MARK_DATA:
                case GRADE_DATA:
                case EDUCATOR_DATA:
                case GRADER_DATA:
                case ADMIN_INFO:
                case STUDENT_DATA:
                case NOTIFICATION_DATA:
                case MESSAGE_DATA:
                case RANK_DATA:
                case SCALE_DATA:
                    // --- SAVE DATA
                    // --- ... TBD need to remove current result set if exists
                    m_dataMap.put( currEvent, data );
                    eventProcessed = true;
                break;
                case PARENTS_STD:
                case WATCHERS_STD:
                case EDUCATORS_STD:
                case GRADERS_STD:
                case ADMINS_STD:
                    eventProcessed = true;
                break;
                default:
                    // --- DO NOTHING
                break;
            }
            // --- ENDDO CASE OF QUERY TYPE
        }
        catch(Exception e){
            // --- IF THE DATA IS EMPTY
            if( e.getMessage() == "null" ){
                // --- LET IT PASS
                eventProcessed = true;
            }
            // --- ELSE
            else {
                System.out.println( "SQL Problem..." + e.getMessage() );
                eventProcessed = false;
            }
        }
        
        // --- RETURN WHETHER THE EVENT WAS HANDLED
        return( eventProcessed );
    }
    // --- END
    
    /**
     * communicates commands from the schoolhouse interface 
     *
     * @param cmd  application command
     * @param cmdData data associated with command
     * @param cmdKey
     */
    @Override
    public void handleCmd( String cmd, String cmdData, int cmdKey )
    // --- BEGIN
    {
 
    }
    // --- END
    
    /**
     * @return true if hints are set
     * @brief returns hint state 
     *
     */
    public boolean hasHints( )
    // --- BEGIN
    {
        // --- RETURN STATE
        return( m_hasHints );
    }
    // --- END
    
    /**
     * @brief disable user input
     */
    public void disableForm()
    // --- BEGIN
    {       
        // --- DISABLE FIELDS
        m_engineEnabled = false;
    }
    // --- END
   
    /**
     * @brief enable user input
     */
    public void enableForm()
    // --- BEGIN
    {       
        // --- DISABLE FIELDS
        m_engineEnabled = true;
    }
    // --- END
   
    /**
     * @param newCommand command to send
     * @brief update application state
     */
    public void updateState( AppCommands.CommandValue newCommand )
    // --- BEGIN
    {       
        // --- DISABLE FIELDS
        System.out.println( "DbEngine::updateState" );
        m_engineState.setCmd( newCommand );
//        m_engineState.setCmd( AppCommands.CommandValue.Focus );
        m_VRThread.SendMessage( m_engineState );
        m_appHandle.getActiveDash().resetTimer();
    }
    // --- END
    /**
     * @brief update application state
     */
    public void updateHints()
    // --- BEGIN
    {       
        // --- DISABLE FIELDS
        System.out.println( "DbEngine::Hints" );
        m_hasHints = true;
        m_engineHints.setCmd( AppCommands.CommandValue.Hint );
        m_VRThread.SendMessage( m_engineHints );
        m_appHandle.getActiveDash().resetTimer();
    }
    // --- END
   
    
    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    public void createWin() {
        //Create and set up the content pane.
        setOpaque(true); //content panes must be opaque
    }    

    /**
     * @return id of engine
     * @brief get engine name
     */
    public String getId()
    // --- BEGIN
    {       
        // --- RETURN NAME
        return( m_nameId );
    }
    // --- END
   
    /**
     * @param engineId id of engine
     * @brief get engine name
     */
    public void setId( String engineId )
    // --- BEGIN
    {       
        // --- SET NAME
        m_nameId = engineId;
        m_engineHints.setAppId( m_nameId );
        m_engineState.setAppId( m_nameId );
    }
    // --- END
   
    
    
}
