/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package schoolhouse.common;


import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;
import schoolhouse.model.SchoolMessage;
import schoolhouse.model.SchoolPerson;

/**
 *
 * @author wpeets
 */
public class MessageHandler
{
    /**
     * @brief constructor
     * 
     * This initializes values
     */
    public MessageHandler(  )
    // --- BEGIN
    {
        System.out.println( "MessageHandler::MessageHandler" );
    }
    // --- END


    
    /**
     * @param msgToSend schoolhouse message
     * @brief send email message external to schoolhouse
     * 
     * Send a schoolhouse message
     */
    public void sendMessage( SchoolMessage msgToSend )
    // --- BEGIN
    {    
        // --- DECLARE LOCAL VARIABLES
        SchoolPerson sender   = msgToSend.getSource();
        SchoolPerson receiver = msgToSend.getDestination();
        String       to       = receiver.getEmail();
        String       from     = sender.getUserName(); 
        String       fromMail = from + "@schoolhouse.email"; 
        String       host     = "localhost"; 
        String       server   = "mail.hmailserver.com";
      

        // Get system properties
        Properties properties = System.getProperties();

        // Setup mail server
        properties.setProperty( "mail.transport.protocol", "smtp" );
        properties.setProperty( "mail.host", server );
        properties.setProperty( "mail.smtp.host", host );
        //properties.setProperty( "outgoing.verizon.net", host );
        properties.setProperty( "mail.user", from );
        properties.setProperty( "mail.password", from );

        // Get the default Session object.
        Session session = Session.getDefaultInstance(properties);

        try{
            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress( fromMail ));

            // Set To: header field of the header.
            message.addRecipient( Message.RecipientType.TO,
                                                       new InternetAddress(to));

            // Set Subject: header field
            message.setSubject( msgToSend.getSubject() );

            // Now set the actual message
            message.setText( msgToSend.getMessage() );

            // Send message
            System.out.println( "MessageHandler::sendMessage send emil" );
            Transport.send(message);
        }
        catch (MessagingException mex) {
            System.out.println( "NessageHandler::sendMessage no can do" );
            mex.printStackTrace();
        }
    }
    // --- END    
    
    /**
     * @param msgToSend schoolhouse message
     * @brief send text message external to schoolhouse
     * 
     */
    public void sendText( SchoolMessage msgToSend )
    // --- BEGIN
    {      
        // --- DECLARE LOCAL VARIABLES
        SchoolPerson sender   = msgToSend.getSource();
        SchoolPerson receiver = msgToSend.getDestination();
        String       cellNum  = receiver.getCellPhone();
        String       to       = cellNum.replaceAll("[^0-9.]", "") + "@vtext.com";
        String       from     = sender.getUserName(); 
        String       fromMail = from + "@schoolhouse.email"; 
        String       host     = "localhost"; 
        String       server   = "mail.hmailserver.com";
      

        // Get system properties
        Properties properties = System.getProperties();

        // Setup mail server
        properties.setProperty( "mail.transport.protocol", "smtp" );
        properties.setProperty( "mail.host", server );
        properties.setProperty( "mail.smtp.host", host );
        //properties.setProperty( "outgoing.verizon.net", host );
        properties.setProperty( "mail.user", from );
        properties.setProperty( "mail.password", from );

        // Get the default Session object.
        Session session = Session.getDefaultInstance(properties);

        try{
            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress( fromMail ));

            // Set To: header field of the header.
            message.addRecipient( Message.RecipientType.TO,
                                                       new InternetAddress(to));

            // Set Subject: header field
            message.setSubject( msgToSend.getSubject() );

            // Now set the actual message
            message.setText( msgToSend.getMessage() );

            // Send message
            System.out.println( "MessageHandler::sendText send text: " + to );
            Transport.send(message);
        }
        catch (MessagingException mex) {
            System.out.println( "NessageHandler::sendText no can do" );
            mex.printStackTrace();
        }
    }
    // --- END
}
