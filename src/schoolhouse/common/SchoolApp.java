/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 +  -----------------------------------------------
 *  |               Title Label                   |
 *  -----------------------------------------------
 *  |         |                           |       |
 *  |  action |   work                    | Cmd   |
 *  |  Pane   |   Pane                    | Pane  |
 *  |         |                           |       |
 *  |         |                           |       |
 *  |         |                           |       |
 *  |         |                           |       |
 *  |         |                           |       |
 *  -----------------------------------------------
 *  |               Status Label                  |
 *  -----------------------------------------------
 */

package schoolhouse.common;

import java.awt.BorderLayout;
import schoolhouse.model.SchoolPerson;
import schoolhouse.model.SchoolPolicy;
import schoolhouse.model.SchoolPolicy.SchoolActions;
import schoolhouse.model.SchoolPolicy.SchoolObjects;
import schoolhouse.model.SchoolPolicy.SchoolRoles;
import java.awt.Color;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.sql.ResultSet;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import schoolhouse.dashboard.DashApp;
import schoolhouse.model.SchoolAdmin;
import schoolhouse.model.SchoolEducator;
import schoolhouse.model.SchoolGrader;
import schoolhouse.model.SchoolManager;
import schoolhouse.model.SchoolMessage;
import schoolhouse.model.SchoolNotification;
import schoolhouse.model.SchoolParent;
import schoolhouse.model.SchoolStudent;
import schoolhouse.model.SchoolWatcher;


/**
 *
 * @author wpeets
 */
public class SchoolApp implements HandleCmdAction {     
    /**
     * @brief
     * The SchoolApp currently uses BorderLayout to control
     * its main components
     */
    protected     SchoolDbIntfc      m_schoolIntfc;
    protected     DbEngine           m_activeEngine;
    protected     JFrame             m_frame;
    protected     DbEngine           m_mainView;
    protected     JLabel             m_titleLabel;
    protected     JPanel             m_titlePane;
    protected     JPanel             m_statusPane;
    protected     JPanel             m_actionPane;
    protected     JPanel             m_workPane;
    protected     JPanel             m_cmdPane;
    protected     JLabel             m_statusLabel;
                  GridBagConstraints m_titleConstraints;
                  GridBagConstraints m_actionConstraints;
                  GridBagConstraints m_workConstraints;
                  GridBagConstraints m_cmdConstraints;
                  GridBagConstraints m_statusConstraints;
                  SchoolPerson       m_appUser;
                  String             m_appName;
                  SchoolModel        m_schoolModel;
    static public DashApp            m_activeDash;
                  JOptionPane        m_questionDialog;
               
    /**
     * @brief
     * constructor
     */
    public SchoolApp( SchoolRoles userRole )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        boolean                   connectionSuccess = false;
        SchoolDbIntfc.SchoolQuery policy = SchoolDbIntfc.SchoolQuery.POLICY;
        QueryThread               policyQuery = null;
        SchoolPolicy              schoolPolicy;

        // --- INITIALIE CLASS VARIABES
        m_schoolModel  = new SchoolModel( this );
        schoolPolicy   = m_schoolModel.getPolicy();
        m_activeEngine = null;
                
        // --- CREATE USER
        m_appUser = createPerson( userRole );

        // --- CREATE DISPLAY
        createApp();
        
        // --- CONNECT TO REPOSITORY
        m_schoolIntfc = SchoolDbIntfc.getInstance();
        connectionSuccess = true;
        
        // --- IF CONNECTED
        if( connectionSuccess == true ){
            // --- GET POLICY DATA
            policyQuery = new QueryThread( this );
        
            // --- ADD QUERY TO THE MAP
            policyQuery.setQuery( policy.dbQuery(), policy.dbName() );
            policyQuery.setHandler( schoolPolicy );
            
            // --- RUN NOW; DO NOT IMPLEMENT IN A THREAD IN ORDER TO GET RESULTS NOW
            // --- ... is this bad tbd
            //policyQuery.start();
            policyQuery.executeQuery();
        }
        // --- ENDIF CONNECTED
    }
    // --- END
  
    
    void createApp()
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        Container framePane;
        
         // --- CREATE FRAME
        m_frame = new JFrame( "SchoolApp" );
        m_frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
        framePane = m_frame.getContentPane();
        //framePane.setLayout( new GridBagLayout() );
        framePane.setLayout( new BorderLayout() );
        
        // --- CREATE BASE COMPONENTS    
        // --- CREATE TITLE
        m_titleLabel = new JLabel();
        m_titleLabel.setBackground(Color.red);
        m_titleLabel.setText( "Empty" );
        m_titleLabel.setVerticalAlignment( SwingConstants.CENTER );
        m_titleLabel.setHorizontalAlignment( SwingConstants.CENTER );
        m_titlePane = new JPanel();
        m_titlePane.add( m_titleLabel );
        //m_titleConstraints = new GridBagConstraints();
        //m_titleConstraints.gridx = 0;
        //m_titleConstraints.gridy = 0;
        //m_titleConstraints.gridwidth = 2;
        //m_titleConstraints.fill = GridBagConstraints.HORIZONTAL;

        // --- CREATE STATUS LINE
        m_statusLabel = new JLabel();
        m_statusLabel.setBackground( Color.green );
        m_statusLabel.setText( "Empty" );
        m_statusLabel.setVerticalAlignment( SwingConstants.CENTER );
        m_statusLabel.setHorizontalAlignment( SwingConstants.CENTER );
        m_statusPane = new JPanel();
        m_statusPane.add( m_statusLabel );
        //m_statusConstraints = new GridBagConstraints();
        //m_statusConstraints.gridx = 0;
        //m_statusConstraints.gridy = 3;
        //m_statusConstraints.gridwidth = 2;
        //m_statusConstraints.fill = GridBagConstraints.HORIZONTAL;
   
        // --- CREATE ACTION PANEL
        m_actionPane = new JPanel();
        m_actionPane.setBackground( Color.yellow );
        //m_actionConstraints = new GridBagConstraints();
        //m_actionConstraints.gridx = 0;
        //m_actionConstraints.gridy = 1;
        //m_actionConstraints.gridheight = 2;
        //m_actionConstraints.fill = GridBagConstraints.VERTICAL;
   
        // --- CREATE WORK PANEL
        m_workPane = new JPanel();
        m_workPane.setBackground( Color.orange );
        //m_workConstraints = new GridBagConstraints();
        //m_workConstraints.gridx = 1;
        //m_workConstraints.gridy = 1;
        //m_workConstraints.fill = GridBagConstraints.HORIZONTAL;
       
        // --- CREATE COMMAND PANEL
        m_cmdPane = new JPanel();
        m_cmdPane.setBackground( Color.green );
        //m_cmdConstraints = new GridBagConstraints();
        //m_cmdConstraints.gridx = 1;
        //m_cmdConstraints.gridy = 2;
        //m_cmdConstraints.fill = GridBagConstraints.HORIZONTAL;
        
        // --- ADD ELEMENTS
        //framePane.add( m_titleLabel, m_titleConstraints );
        //framePane.add( m_statusLabel, m_statusConstraints );
        //framePane.add( m_actionPane, m_actionConstraints );
        //framePane.add( m_workPane, m_workConstraints );
        //framePane.add( m_cmdPane, m_cmdConstraints );
        framePane.add( m_titlePane, BorderLayout.NORTH );
        framePane.add( m_statusPane, BorderLayout.SOUTH );
        framePane.add( m_actionPane, BorderLayout.WEST );
        framePane.add( m_workPane, BorderLayout.CENTER );
        framePane.add( m_cmdPane, BorderLayout.EAST );
        
        // --- CREATE QUESTION DIALOG
        // --- ...TBD need to disable x
        m_questionDialog = new JOptionPane();
        
        // --- SETUP FOR EXIT
        m_frame.addWindowListener( new java.awt.event.WindowAdapter() {
                                         public void windowClosing(WindowEvent winEvt) {
                                                                closeMainPanel();
                                        }
            } );
    }
    // --- END
    
    
    public DbIntfc getDbIntfc()
    // --- BEGIN
    {
        // --- RETURN ACTION PANE
        return( m_schoolIntfc );
    }
    // --- END
            
/**
 *
 * @return 
 * @brief get handle to user info
 * 
 */
    public SchoolPerson getPerson()
    // --- BEGIN
    {
        // --- RETURN ACTION PANE
        return( m_appUser );
    }
    // --- END
            
    /**
     *
     * @return 
     * @brief store user info
     * 
     */
    public void setPerson( SchoolPerson personData )
    // --- BEGIN
    {
        // --- COPY OF DATA
        m_appUser.setLastName( personData.getLastName() );
        m_appUser.setFirstName( personData.getFirstName() );
        m_appUser.setMiddleName( personData.getMiddleName() );
        m_appUser.setNickName( personData.getNickName() );
        m_appUser.setOrgAssigned( personData.getOrgAssignedId() );
        m_appUser.setTitle( personData.getTitle() );
        m_appUser.setEmail( personData.getEmail() );
        m_appUser.setPhone( personData.getPhone() );
        m_appUser.setCellPhone( personData.getCellPhone() );
        m_appUser.setAddress( personData.getAddress() );
        m_appUser.setUserStatus( personData.getUserStatus() );
        //m_appUser.setRole( personData.getRole() );
    }
    // --- END
            
/**
 *
 * @return 
 * @brief get role of the user
 * 
 */
    public SchoolRoles getRole()
    // --- BEGIN
    {
        // --- RETURN ACTION PANE
        return( m_appUser.getRole() );
    }
    // --- END
            
/**
 *
 * @brief set role of the user
 * 
 */
    public void setRole( SchoolRoles userRole )
    // --- BEGIN
    {
        // --- SET ROLE
        m_appUser.setRole( userRole );
    }
    // --- END
            
/**
 *
 * @brief get username of the user
 * 
 */
    public String getUser()
    // --- BEGIN
    {
        // --- RETURN USER NAME
        return( m_appUser.getUserName() );
    }
    // --- END
            
    /**
     *
     * @param userName username of user
     * @brief set role of the user
     * 
     */
    public void setUser( String userName )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolDbIntfc.SchoolQuery personQuery = SchoolDbIntfc.SchoolQuery.USER_ID;
        QueryThread               userQuery = new QueryThread( this );
        System.out.println( "SchoolApp::setUser " + userName );
        QueryChain          queryChain;
                
        // --- SET USER
        m_appUser.setUserName( userName );

        // --- QUERY ROLES FOR THE USER OF THE APPLICATION
        queryChain = new QueryChain( this, false );
        queryChain.setHandler( this );
        
        // --- IF THE USER HAS A ROLE
        if( m_appUser.getRole() != SchoolRoles.UNKNOWN ){
            // --- GET USER DATA FOR A SPECIFIC ROLE
            personQuery = SchoolDbIntfc.SchoolQuery.USER_ROLE_ID;
            personQuery.dbQueryId( m_appUser.getRole().getValue(), 0 );       
        }
        // --- ENDIF THE USER HAS A ROLE
        
        // --- SET USER
        personQuery.dbQueryString( userName, 0 );
        queryChain.addQuery( personQuery.dbQuery(), personQuery.dbName() );
        
        // --- EXECUTE QUERY INLINE
        queryChain.executeChain();              
    }
    // --- END
            
/**
 *
 * @brief get name of the application
 * 
 */
    public String getAppName()
    // --- BEGIN
    {
        // --- RETURN NAME
        return( m_appName );
    }
    // --- END
            
/**
 *
 * @brief set name of the application
 * 
 */
    public void setAppName( String appName )
    // --- BEGIN
    {
        // --- SET NAME
        m_appName = appName;
        m_frame.setTitle( m_appName );
    }
    // --- END
    
    public Container getActionPane()
    // --- BEGIN
    {
        // --- RETURN ACTION PANE
        return( m_actionPane );
    }
    // --- END
            
    public Container getWorkPane()
    // --- BEGIN
    {
        // --- RETURN ACTION PANE
        return( m_workPane );
    }
    // --- END
            
    public Container getCmdPane()
    // --- BEGIN
    {
        // --- RETURN ACTION PANE
        return( m_cmdPane );
    }
    // --- END
            
    public DbEngine getActiveEngine()
    // --- BEGIN
    {
        // --- IF THERE IS AN ACTIVE ENGiNE
        if( m_activeEngine != null ){
            System.out.println( "SchoolApp::getActiveEngine[" + getAppName() +
                     "] has engine - " + m_activeEngine.getId() );
            
        }
        // --- ELSE
        else{
            System.out.println( "SchoolApp::getActiveEngine[" + getAppName() +
                     "] no engine" );
        }
        // --- ENDIF THERE IS AN ACTIVE ENGiNE
        
        // --- RETURN ACTION PANE
        return( m_activeEngine );
    }
    // --- END
            
    public void setActiveEngine( DbEngine latest )
    // --- BEGIN
    {
        // --- SET PANE
        m_activeEngine = latest;
        //m_activeEngine.handleEvent( AppCommands.CommandValue.Focus.toString() );    
    }
    // --- END
                        
    /**
     *
     * @return
     */
    public DashApp getActiveDash()
    // --- BEGIN
    {
        // --- IF THERE IS AN ACTIVE ENGiNE
        if( m_activeDash != null ){
            System.out.println( "DashView::getActiveDash has dash" );
            
        }
        // --- ELSE
        else{
            System.out.println( "DashView::getActiveDash no dash" );            
        }
        // --- ENDIF THERE IS AN ACTIVE ENGiNE
        
        // --- RETURN ACTION PANE
        return( m_activeDash );
    }
    // --- END
            
    /**
     *
     * @param current dashboard
     */
    public void setActiveDash( DashApp latest )
    // --- BEGIN
    {
        // --- SET PANE
        m_activeDash = latest;
    }
    // --- END
                        
    public void setHeader( String newString )
    // --- BEGIN
    {
        // --- RETURN ACTION PANE
        m_titleLabel.setText( newString );
    }
    // --- END
            
    public void setStatus( String newString )
    // --- BEGIN
    {
        // --- RETURN ACTION PANE
        m_statusLabel.setText( newString );
    }
    // --- END
            
            
    public void showMainPanel()
    // --- BEGIN
    {
        // --- REMOVE COMPONENTS
        m_actionPane.removeAll();
        m_workPane.removeAll();
        m_cmdPane.removeAll();
        setActiveEngine( null );

        //-- DISPLAY THE WINDOW
        m_actionPane.add( m_mainView );
        showApp();
    }
    // --- END
    
    
    public void showApp()
    // --- BEGIN
    {
        //-- DISPLAY THE WINDOW
        m_frame.pack();
        m_frame.setVisible( true );
        setFocus();
    }
    // --- END
 
    public void closeMainPanel()
    // --- BEGIN
    {
        // --- DECLARE LOcAL VARIABLES
        Thread currentThread;
        
        // --- KILL THREAD
        System.out.println( "SchoolApp::closeMainPanel" );            
        m_frame.setVisible( false );
        
        // --- STOP THREAD
        // --- ... TBD not sure why exit is happening
        //currentThread = Thread.currentThread();
        //currentThread.interrupt();
    }
    // --- END
 
    /**
     *
     * 
     * #brief inform infrastructure of focus
     */
    public void setFocus() 
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        DbEngine currentEngine = getActiveEngine( );
        boolean  handleEvent;
        
        // --- SEND VOICE FOCUS
        
        // --- IF AN ACTION IS SELECTED
        if( (currentEngine != null) && (currentEngine.hasHints()==true) ){
            // --- HAVE THE VIEW HANDLE ITS CURRENT HINTS
            System.out.println( "SchoolApp::setFocus - " + currentEngine.getId() );
            handleEvent = currentEngine.handleEvent( AppCommands.CommandValue.Focus.toString() );    
        }
        // --- ELSE
        else if( m_mainView.hasHints() == true ){
            // --- SEND TOP LEVEL HINTS
            System.out.println( "SchoolApp::windowGainedFocus  Top level" );
            handleEvent = m_mainView.handleEvent( AppCommands.CommandValue.Focus.toString() );           
        }
        // --- ENDIF AN ACTION IS SELECTED
    }
    // --- END

    /*
     * @brief get display handle
     */
    public JFrame getFrame()
    // --- BEGIN
    {
        // --- RETURN APPLICAITON FRAME
        return( m_frame );
    }
    // --- END
    
    /*
     * @brief get display handle
     */
    public SchoolModel getModel()
    // --- BEGIN
    {
        // --- RETURN APPLICAITON FRAME
        return( m_schoolModel );
    }
    // --- END
    
    /**
     * @brief returns true or false if role has any kind of policy access
     * 
     * This object checks all the policies for an action or a policy and
     * returns true or false if any access exits. action or object can be null.
     * 
     */
    public boolean hasAccessState( SchoolActions sAction, SchoolObjects sObject )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        boolean accessState = false;
        SchoolPolicy schoolPolicy = m_schoolModel.getPolicy();
        
        // --- DETERMINE ACCESS STATE
        accessState = schoolPolicy.hasAccessState( getRole(), sAction, sObject);
        
        // --- RETuRN
        return( accessState );
       
    }
    // --- END
    
    /**
     * communicates commands from the schoolhouse interface 
     *
     */
    public void handleCmd( String cmd, String cmdData, int cmdKey )
    // --- BEGIN
    {
 
    }
    // --- END
    
    /**
     * @param appQuestion question string
     * @return true for yes; false for no
     * @brief ask user a question 
     *
     */
    public boolean askQuestion( String appQuestion )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        boolean returnValue = false;
        int     response;
        
        // --- DISPLAY QUESTION
        response = JOptionPane.showConfirmDialog( null, appQuestion, 
                                     "Confirm",
                                     JOptionPane.YES_NO_OPTION, 
                                     JOptionPane.QUESTION_MESSAGE );
        
        if (response == JOptionPane.NO_OPTION) {
          System.out.println("No button clicked");
          returnValue = false;
        } else if (response == JOptionPane.YES_OPTION) {
          System.out.println("Yes button clicked");
          returnValue = true;
        } else if (response == JOptionPane.CLOSED_OPTION) {
          System.out.println("JOptionPane closed");
          returnValue = false;
        }

        // --- RETURN
        return( returnValue );
    }
    // --- END
  
    /**
     * @param appMessage message to display
     * @return always true
     * @brief display a message 
     *
     */
    public boolean showMessage( String appMessage )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        boolean returnValue = true;
        int     response;
        
        // --- DISPLAY QUESTION
        JOptionPane.showMessageDialog( m_frame, 
                                                  appMessage, 
                                                  "Message",
                                     JOptionPane.INFORMATION_MESSAGE );
        
        // --- RETURN
        return( returnValue );
    }
    // --- END

    @Override
    public boolean handleAction(ActionEvent event) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean handleEvent(String currEvent) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean handleQuery( String currEvent, ResultSet m_resultSet )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        boolean       eventProcessed = true;
        Integer       dbKey;
        String        personString;
        SchoolPerson  personInfo;
        SchoolStudent studentInfo;
        System.out.println( "SchoolApp::handleQuery - " + currEvent  );
        
        try {
            // --- DO CASE OF DATA TYPE
            switch( currEvent ){
                // --- CASE PARENT'S STUDENT LIST
                case DbEngine.PARENTS_STD:
                    // --- DO FOR EACH STUDENT:
                    while( m_resultSet.next() ){
                        // --- GET ID OF STUDENT
                        dbKey = m_resultSet.getInt( "student_participant_id" );
                        System.out.println( "SchoolApp::handleQuery - " + dbKey  );
                        studentInfo = (SchoolStudent)m_schoolModel.getPerson( dbKey );
                        m_appUser.addToStudentList( studentInfo );
                    }
                    // --- ENDDO FOR EACH STUDENT:                   
                break;
                // --- CASE USER DATA
                case DbEngine.USER_DATA:
                case DbEngine.USER_ROLE_DATA:
                    // --- IF USER DATA IS RETURNED
                    if( m_resultSet.next() == true ){
                        // --- SET DATA
                        personInfo = getPerson();
                        //personInfo.setRole( personRole );
                        personString = m_resultSet.getString( "username" );
                        personInfo.setUserName( personString );
                        System.out.println( "SchoolApp::handleQuery  save user- " + personString  );
                        personString = m_resultSet.getString( "last_name" );
                        personInfo.setLastName( personString );
                        personString = m_resultSet.getString( "first_name" );
                        personInfo.setFirstName( personString );
                        personString = m_resultSet.getString( "middle_name" );
                        personInfo.setMiddleName( personString );
                        personString = m_resultSet.getString( "nick_name" );
                        personInfo.setNickName( personString );
                        personString = m_resultSet.getString( "org_id" );
                        personInfo.setOrgAssigned(personString );
                        personString = m_resultSet.getString( "title" );
                        personInfo.setTitle( personString );
                        personString = m_resultSet.getString( "email" );
                        personInfo.setEmail( personString );
                        personString = m_resultSet.getString( "phone" );
                        personInfo.setPhone( personString );
                        personString = m_resultSet.getString( "cell_phone" );
                        personInfo.setCellPhone( personString );
                        personString = m_resultSet.getString( "address" );
                        personInfo.setAddress( personString );
                        dbKey = m_resultSet.getInt( "participant_id" );
                        personInfo.setId( dbKey );
                        System.out.println( "SchoolApp::handleQuery  id - " + dbKey  );
                        dbKey = m_resultSet.getInt( "user_status" );
                        personInfo.setUserStatus( dbKey );
                        
                        // --- SEND EMAIL
                        // --- ... probably not needed here.
                        MessageHandler messageHandler = new MessageHandler();
                        SchoolMessage  startMessage;
                        SchoolPerson   msgPerson = personInfo;
                        startMessage = new SchoolMessage( this );
                        startMessage.setDestination( msgPerson );
                        startMessage.setSource( msgPerson );
                        startMessage.setSubject( "Yo" );
                        startMessage.setMessage("Started from the bottom, now we here");
                        startMessage.setMsgType( SchoolMessage.SchoolMessageType.GENERAL );
                        startMessage.setNotifyType( SchoolNotification.SchoolContactType.EMAIL );
                        //messageHandler.sendMessage( startMessage );
                        //messageHandler.sendText( startMessage );
                    }
                    // --- ENDIF USER DATA IS RETURNED
                break;
                default:
                break;
            }
            // --- ENDDO CASE OF DATA TYPE
        }
        catch(Exception e){
             System.out.println( "SQL Problem..." + e.getMessage() );
             eventProcessed = false;
        }

        // --- RETURN
        return( eventProcessed );
    }
  
    /**
     * @param newRole
     * @param appMessage message to display
     * @return always true
     * @brief display a message 
     *
     */
    public SchoolPerson createPerson( SchoolRoles newRole )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolPerson newPerson;
        
        // --- DO CASE OF ROLE
        switch( newRole ){
            case STUDENT:       newPerson = (SchoolPerson)( new SchoolStudent( )); break;
            case PARENT:        newPerson = (SchoolPerson)( new SchoolParent( )); break;
            case EDUCATOR:      newPerson = (SchoolPerson)( new SchoolEducator( )); break;
            case GRADER:        newPerson = (SchoolPerson)( new SchoolGrader( )); break;  
            case WATCHER:       newPerson = (SchoolPerson)( new SchoolWatcher( )); break; 
            case ADMINISTRATOR: newPerson = (SchoolPerson)( new SchoolAdmin( )); break;
            case MANAGER:       newPerson = (SchoolPerson)( new SchoolManager( )); break;
            case OTHER:         newPerson = (SchoolPerson)( new SchoolPerson( )); break;   
            case UNKNOWN:       newPerson = (SchoolPerson)( new SchoolPerson( )); break;
            default:            newPerson = (SchoolPerson)( new SchoolPerson( )); break;   
        }
        
        // --- SET ROLE
        newPerson.setRole( newRole );
        
        // --- RETURN OBJECT
        return( newPerson );
    }
    // --- END
}
