/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package schoolhouse.common;

/**
 *
 * @author wpeets
 */
public class SchoolHouseConfig {
    private boolean m_voiceEnabled;
    private boolean m_voiceViewEnabled;
    private boolean m_voiceProcessingEnabled;
    private boolean m_immediateUpdate;

    /**
     * @return the m_voiceEnabled
     */
    public boolean isVoiceEnabled() {
        return m_voiceEnabled;
    }

    /**
     * @param voiceEnabled the m_voiceEnabled to set
     */
    public void setVoiceEnabled(boolean voiceEnabled) {
        this.m_voiceEnabled = voiceEnabled;
    }

    /**
     * @return the m_voiceViewEnabled
     */
    public boolean isVoiceViewEnabled() {
        return m_voiceViewEnabled;
    }

    /**
     * @param voiceViewEnabled the m_voiceViewEnabled to set
     */
    public void setVoiceViewEnabled(boolean voiceViewEnabled) {
        this.m_voiceViewEnabled = voiceViewEnabled;
    }

    /**
     * @return the m_voiceViewEnabled
     */
    public boolean isVoiceProcessingEnabled() {
        return m_voiceProcessingEnabled;
    }

    /**
     * @param voiceProcessingEnabled the m_voiceProcessingEnabled to set
     */
    public void setVoiceProcessingEnabled(boolean voiceProcessingEnabled) {
        this.m_voiceProcessingEnabled = voiceProcessingEnabled;
    }

    /**
     * @return the m_immediateUpdate
     */
    public boolean isImmediateUpdate() {
        return m_immediateUpdate;
    }

    /**
     * @param immediateUpdate the m_immediateUpdate to set
     */
    public void setImmediateUpdate(boolean immediateUpdate) {
        this.m_immediateUpdate = immediateUpdate;
    }
            
    
}
