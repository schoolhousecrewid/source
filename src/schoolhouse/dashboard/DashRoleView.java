/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package schoolhouse.dashboard;

import schoolhouse.common.AppCommands;
import schoolhouse.model.SchoolPolicy.SchoolRoles;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JToggleButton;
import schoolhouse.client.ClientApp;
import schoolhouse.common.AppCommands.CommandValue;
import schoolhouse.common.DbEngine;
import schoolhouse.common.HandleCmdAction;
import schoolhouse.common.QueryChain;
import schoolhouse.common.SchoolApp;
import schoolhouse.common.SchoolDbIntfc.SchoolQuery;
import schoolhouse.model.SchoolPolicy;
import java.awt.Color;
import schoolhouse.common.MessageHandler;
import schoolhouse.common.QueryThread;
import schoolhouse.model.SchoolMessage;
import schoolhouse.model.SchoolNotification;
import schoolhouse.model.SchoolPerson;

/**
 *
 * @author wpeets
 */
public class DashRoleView extends DbEngine implements HandleCmdAction, ItemListener {
    protected RoleButton[]        m_roleList;   
    protected int                 m_totalUserRoles;
              SchoolRoles         m_currentRole;
    private   Map<Object,Icon>    m_icons                 = null;
              Thread              m_appThread;
              ClientApp           m_clientApp;
     static   final int           m_ROLE_INDICATOR_WIDTH  = 150;
     static   final int           m_ROLE_INDICATOR_HEIGHT = 40;
     
                      
    DashRoleView( SchoolApp appHandle )
    // --- BEGIN
    {
        // --- SATISFY SUBCLASS
        super( appHandle );
        
        // --- DECLARE LOCAL VARIABLES
        int listSize = SchoolRoles.getSize();
        
        // --- INITIALIZE CLASS VARIABLE        
        m_roleList       = new RoleButton[ listSize ];
        m_totalUserRoles = 0;
        m_currentRole    = SchoolRoles.UNKNOWN;
        m_icons          = new HashMap<Object, Icon>();
        ImageIcon  studentIcon = createImageIcon( "images/student.png" );
        ImageIcon  parentIcon  = createImageIcon( "images/parent.png" );
        ImageIcon  educatorIcon = createImageIcon( "images/educator.gif" );
        ImageIcon  graderIcon = createImageIcon( "images/grader.gif" );
        ImageIcon  watcherIcon = createImageIcon( "images/watcher.png" );
        ImageIcon  adminIcon = createImageIcon( "images/admin.gif" );
        ImageIcon  admin2Icon = createImageIcon( "images/manager.gif" );
        ImageIcon  otherIcon = createImageIcon( "images/memo.gif" );
        
        // --- SET UP iCONS
        m_icons.put(SchoolPolicy.SchoolRoles.STUDENT, studentIcon );
        m_icons.put(SchoolPolicy.SchoolRoles.PARENT, parentIcon );
        m_icons.put(SchoolPolicy.SchoolRoles.EDUCATOR, educatorIcon );
        m_icons.put(SchoolPolicy.SchoolRoles.GRADER, graderIcon );
        m_icons.put(SchoolPolicy.SchoolRoles.WATCHER, watcherIcon );
        m_icons.put(SchoolPolicy.SchoolRoles.ADMINISTRATOR, adminIcon );
        m_icons.put(SchoolPolicy.SchoolRoles.MANAGER, admin2Icon );
        m_icons.put(SchoolPolicy.SchoolRoles.OTHER, otherIcon );
    }
    // --- END
    
    /**
     *
     * @brief add a role control to view
     */
    public void initialize( )
    // --- BEGIN
    {    
        // --- DECLARE LOCAL VARIABLES

        // --- INITIALIZE
        System.out.println( "DashRoleView::initialize"  );       
        m_appHandle.setHeader( "DASHBOARD" );
        m_appHandle.setStatus( "Select a role please" ); 
        setBackground( Color.PINK );
    }
    // --- END

   public void createView(  )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        QueryThread         roleThread;
        SchoolQuery         roleQuery = SchoolQuery.USERROLES;
                
        // --- CREATE TOP LEVEL ACTIONS
        setLayout( new GridBagLayout() );  
        
        // --- QUERY ROLES FOR THE USER OF THE APPLICATION
        roleThread = new QueryThread( m_appHandle );
        roleThread.setHandler( this );
        roleQuery.dbQueryString( m_appHandle.getUser(), 0 );
        roleThread.setQuery( roleQuery.dbQuery(), roleQuery.dbName()  );
        roleThread.start();  
   }
   // --- END
   
   
    /**
     *
     * @param userRole SchoolHouse role supported by user
     * @brief add a role control to view
     */
    public void addRole( SchoolRoles userRole )
    // --- BEGIN
    {  
        // --- DECLARE LOCAL VARIABLES
        GridBagConstraints c        = new GridBagConstraints();
        Icon               icon;
        String             roleName = userRole.name();
    
        // --- CREATE BUTTON FOR ROLE
        System.out.println( "DashView::addRole - " + roleName  );
        icon = m_icons.get( userRole );
        m_roleList[m_totalUserRoles] = new RoleButton( roleName, icon );
        m_roleList[m_totalUserRoles].setPreferredSize( new Dimension(m_ROLE_INDICATOR_WIDTH, 
                                               m_ROLE_INDICATOR_HEIGHT) );
        m_roleList[m_totalUserRoles].addItemListener( this );
        // --- ADD BUTTON FOR ROLE
        c.gridx      = 0;
        c.gridy      = m_totalUserRoles;
        c.fill       = GridBagConstraints.BOTH;
        c.gridheight = 1;
        c.gridwidth  = 1;
        add( m_roleList[m_totalUserRoles], c );
        m_totalUserRoles++;
        setSize( m_ROLE_INDICATOR_WIDTH, m_totalUserRoles*m_ROLE_INDICATOR_HEIGHT );
    }
    // --- END

    /**
     * @return true if event is processed 
     * @brief handle database generated events
     */
    @Override
    public boolean handleQuery( String currEvent, ResultSet data ) 
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        boolean      eventProcessed = true;
        SchoolRoles  userRole       = SchoolRoles.UNKNOWN;
        Integer      dbKey;
        int          roleCount      = 0;
        String       personString;
        SchoolPerson personInfo;
        System.out.println( "DashView::handleQuery - " + currEvent  );
        
        try {
            // --- DO CASE OF DATA TYPE
            switch( currEvent ){
                // --- CASE THE SCHOOL DATA IS RETRIEVED
                case DbEngine.USERROLES:
                    // --- UPDATE HINTS
                    //m_engineHints.setAppId( m_appHandle.getAppName() + "role" );
                    setId( m_appHandle.getAppName() );
                    m_engineHints.setCmd( CommandValue.Hint );
                    
                    // --- DO FOR EACH SCHOOL NAME
                    while( data.next() ){
                        // --- SAVE NAME IN SCHOOL LIST
                        dbKey = data.getInt( "role_id" );
                        userRole = SchoolRoles.fromValue( dbKey );
                        addRole( userRole );
                        m_engineHints.addHint( userRole.toString(), dbKey );                            
                        roleCount++;
                    }
                    // --- ENDDO FOR EACH SCHOOL NAME
                    
                    // --- SEND UPDATED HINTS
                    updateHints();
        
                    // --- IF THERE IS ONLY ONE ROLE
                    if( roleCount == 1 ){
                        // --- START THE CLIENT IMMEDIATELY
                        handleCmd( "INIT", userRole.toString(), userRole.getValue() );
                    }
                    // --- ENDIF THERE IS ONLY ONE ROLE
                    
                break;
                default:
                break;
            }
            // --- ENDDO CASE OF DATA TYPE
        }
        catch(Exception e){
             System.out.println( "SQL Problem..." + e.getMessage() );
             eventProcessed = false;
        }

        // --- RETURN
        return( eventProcessed );
    }
    // --- END

    
    @Override
    public boolean handleAction(ActionEvent event) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean handleEvent(String currEvent)
    // --- BEGIN
    {
        // --- DECLARE LocAL VARIABLES
        boolean returnState = true;
        
        // --- DO CASE OF EVENT
        switch( currEvent ){
            case "Focus":
                // --- SEND VOICE FOCUS
                updateState( AppCommands.CommandValue.Focus );
            break;
            default:
            break;    
        }
        // --- ENDDO CASE OF EVENT
        
        // --- RETURN
        return( returnState );    
    }
    // --- END

        
    /**
     *
     * @param ev
     */
    @Override
    public void itemStateChanged( ItemEvent ev ) 
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        Object        source      = ev.getItemSelectable(); 
        int           selectState = ev.getStateChange();
        JToggleButton btnHandle   = (JToggleButton)source;
        String        btnText     = btnHandle.getText();
        int           value;
        SchoolRoles   clientRole  = SchoolRoles.UNKNOWN;
        int           selectedRole= -1;
        
        // --- IF USER IS BEING ENABLED
        if( selectState == ItemEvent.SELECTED ) {
            // --- ENABLE VIEW BOX
            System.out.println( "DashRoleView::itemStateChanged selected " + btnText );
            System.out.println( "DashRoleView::itemStateChanged total " + m_totalUserRoles );
                
            // --- DO FOR EACH ROLE
            for( value=0; value < m_totalUserRoles; value++ ) { 
                // --- IF NOT CURRENT BUTTON
                System.out.println( "DashRoleView::itemStateChanged check " + m_roleList[value].getText() );
                
                // --- IF ROLE DOES NOT MATCH SELECTED ITEM
                if( source != m_roleList[value] ){
                    // --- IF THE ITEM IS SELECTED
                    if( m_roleList[value].isSelected() == true ){
                        // --- DESELECT IT
                        m_roleList[value].removeItemListener( this );
                        m_roleList[value].setSelected( false );
                        m_roleList[value].addItemListener( this );
                            
                        // --- IF THREAD IS RUNNING
                        if( m_clientApp != null ){
                            // --- KILL APP
                            m_clientApp.getFrame().dispose();
                            m_clientApp = null;
                        }
                        // --- ENDIF THREAD IS RUNNiNG
                    }
                    // --- ENDIF THE ITEM IS SELECTED
                }
                // --- ELSE
                else {
                    // --- MARK SELECTION
                    System.out.println( "DashRoleView::itemStateChanged selection " + m_roleList[value].getText() );
                    selectedRole = value;
                }
                // --- ENDIF ROLE DOES NOT MATCH SELECTED ITEM
            }  
            // --- ENDDO FOR EACH ROLE
                
            // --- START A CLIENT APP  
            try {
                clientRole = SchoolRoles.valueOf( btnText );
            }
            catch (Exception e){
                System.out.println( "SQL Problem..." + e.getMessage() );
            }
                
            // --- IF THE ITEM IS SELECTED
            if( selectedRole > -1 ){
                // --- CREATE APPLICATION
                System.out.println( "DashView::itemStateChanged Start " + btnText );
                startClient( clientRole );
            }
            // --- ENDIF THE ITEM IS SELECTED
        }
        // --- ENDIF USER IS BEING ENABLED
            
        // --- IF SOUND IS BEING DISABLED
        if( selectState == ItemEvent.DESELECTED ) {
            // --- DISABLE VIEW BOX
            System.out.println( "DashView::itemStateChanged deselected " + btnText );
                
            // --- KILL APP
            m_clientApp.getFrame().dispose();
            m_clientApp = null;
        }
        // --- ENDIF SOUND IS BEING DISABLED
    }
    // --- END
    
    @Override
    public void handleCmd( String cmd, String cmdData, int cmdKey )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        System.out.println("DashView::handleCmd[" + cmd + "]" );
        final int sVal = SchoolRoles.STUDENT.getValue();
        final int pVal = SchoolRoles.PARENT.getValue();
        final int eVal = SchoolRoles.EDUCATOR.getValue();
        final int gVal = SchoolRoles.GRADER.getValue();
        final int wVal = SchoolRoles.WATCHER.getValue();
        final int aVal = SchoolRoles.ADMINISTRATOR.getValue();
        final int tVal = SchoolRoles.MANAGER.getValue();
        final int oVal = SchoolRoles.OTHER.getValue();
        SchoolRoles clientRole;
        String      roleName;
        String      buttonName   = null;
        int         value        = 0;
        int         selectedRole = -1;
        
        // --- DO CASE OF EVENT
        switch( cmdKey ){
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
                // --- SET ROLE DATA
                clientRole   = SchoolRoles.fromValue( cmdKey );
                roleName     = clientRole.name();
                // --- DO FOR EACH ROLE CREATED FOR USER
                for( value=1; value <= m_totalUserRoles; value++ ) { 
                    // --- GET BUTTON NAME
                    buttonName = m_roleList[value].getText();
                    
                    // --- IF NOT CURRENT BUTTON
                    if( buttonName.equals(roleName) ){
                        // --- SAVE ROLE
                        selectedRole = value;
                    }
                    // --- ELSE
                    else {
                        // --- IF THE ITEM IS SELECTED
                        if( m_roleList[value].isSelected() == true ){
                            // --- DESELECT IT
                            System.out.println("DashView::handleCmd kill[" + clientRole.name() + "]" );
                            m_roleList[value].removeItemListener( this );
                            m_roleList[value].setSelected( false );
                            m_roleList[value].addItemListener( this );
                            
                            // --- IF THREAD IS RUNNiNG
                            if( m_clientApp != null ){
                                // --- KILL APP
                                m_clientApp.getFrame().dispose();
                                m_clientApp = null;
                            }
                            // --- ENDIF THREAD IS RUNNiNG
                        }
                        // --- ENDIF THE ITEM IS SELECTED
                    }
                }  
                // --- ENDDO FOR EACH ROLE
                
                // --- IF THE APP SHOULD BE STARTED
                if(  selectedRole > -1 ){
                    // --- START A CLIENT APP  
                    try {
                        clientRole = SchoolRoles.fromValue( cmdKey );
                    }
                    catch (Exception e){
                        System.out.println( "SQL Problem..." + e.getMessage() );
                    }
                
                    // --- CREATE APPLICATION
                    System.out.println("DashView::handleCmd start[" + clientRole.name() + "]" );
                    m_roleList[selectedRole].removeItemListener( this );
                    m_roleList[selectedRole].setSelected( true );
                    m_roleList[selectedRole].addItemListener( this );
                    startClient( clientRole );
                }
                // --- ENDIF THE APP SHOULD BE STARTED
            break;
            case 10000:
                 System.out.println( "DashView::handleCmd  Acknowledge received" );
            break;   
            default:
            break;    
        }
        // --- ENDDO CASE OF EVENT
    }
    // --- END
    
    /**
     *
     * @param clientRole current user role
     * @brief start client application
     * 
     */
    void startClient( SchoolRoles clientRole )
    // --- BEGIN
    {
        // --- CREATE APPLICATION
        System.out.println( "DashView::startClient" );
        m_clientApp = new ClientApp( clientRole, m_appHandle.getUser() );
        m_appThread = new Thread( m_clientApp );

        // --- DISPLAY MAIN MENU
        m_appThread.start();
    }
    // --- END

    /**
     *
     * @brief destroy the only running client
     * 
     */
    void killClient( )
    // --- BEGIN
    {                       
        // --- IF THREAD IS RUNNiNG
        if( m_clientApp != null ){
            // --- KILL APP
            m_clientApp.getFrame().dispose();
            m_clientApp = null;
        }
        // --- ENDIF THREAD IS RUNNiNG
    }
    // --- END

    /**
     *
     * @brief destroy the view
     * 
     */
    void killView( )
    // --- BEGIN
    {   
        // --- TERMINATE CLIENT
        killClient();
        
        // --- TERMINATE VOICE
        updateState( CommandValue.Shutdown );
    }
    // --- END

    /**
     *
     * @param clientRole current user role
     * @brief clear client state
     * 
     */
    public void clearRole( SchoolRoles clientRole )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        int value = clientRole.getValue() - 1;
        // --- CLEAR STATE
        System.out.println("DashView::clearRole[" + clientRole.name() + "]" );
        m_roleList[value].removeItemListener( this );
        m_roleList[value].setSelected( false );
        m_roleList[value].addItemListener( this );
    }
    // --- END

}
