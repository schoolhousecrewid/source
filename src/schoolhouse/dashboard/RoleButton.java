/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package schoolhouse.dashboard;

//import java.awt.*;

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.Icon;
import javax.swing.JToggleButton;

//import java.awt.event.*;
//import com.sun.java.swing.*;

/**
 *
 * @author wpeets
 * @brief A rectangle that has a fixed size. 
 */
public class RoleButton extends JToggleButton {

    private Dimension     m_preferredSize;
    private final String m_name;
    private boolean      m_restrictMaximumSize;

    public RoleButton( String name, Icon graphic )
    // --- BEGIN
    {
        super( name, graphic );
        m_name                = name;
        m_restrictMaximumSize = false;
    }
    // --- END

    /* 
     * Our BLDComponents are completely opaque, so we override this method 
     * to return true.  This lets the painting system know that it doesn't
     * need to paint any covered part of the components underneath this
     * component.  The end result is possibly improved painting performance.
     */
    @Override
    public boolean isOpaque() {
        return true;
    }
    
    @Override
    public Dimension getPreferredSize() {
        return m_preferredSize;
    }

    @Override
    public void setPreferredSize( Dimension newSize )
    // --- BEGIN
    {
        m_preferredSize = newSize;
    }
    // --- END
    
    @Override
    public Dimension getMinimumSize() {
        return m_preferredSize;
    }

    @Override
    public Dimension getMaximumSize() {
        if (m_restrictMaximumSize) {
            return m_preferredSize;
        } else {
            return super.getMaximumSize();
        }
    }

    public void setSizeRestriction(boolean restrictSize) {
	m_restrictMaximumSize = restrictSize;
    }

}
