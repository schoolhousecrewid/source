/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package schoolhouse.dashboard;

import schoolhouse.model.SchoolPolicy.SchoolRoles;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.Time;
import java.util.Calendar;
import java.util.Iterator;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.Timer;
import schoolhouse.common.HandleCmdAction;
import schoolhouse.common.SchoolApp;

/**
 *
 * @author wpeets
 * 
 */
public class DashTimer extends JTextField implements HandleCmdAction, ActionListener {
              Timer               m_dashTimer;
    static    final int           m_REFRESH_RATE = 500;
    static    final int           m_TIMER_RATE   = m_REFRESH_RATE*2;
              java.sql.Time       m_refVal;                    
              java.sql.Time       m_startTime;
              java.sql.Time       m_resetTime;
              java.sql.Time       m_lastTime;
              java.sql.Time       m_timeOut;                    
              java.sql.Time       m_timerVal;                    
              JPanel              m_timePane;
    protected JTextField          m_currentTimeField;
    protected JTextField          m_timerField;
              SchoolApp           m_appHandle;
     
                      
    DashTimer( SchoolApp appHandle )
    // --- BEGIN
    {
        // --- SATISFY SUBCLASS
        super( );
        
        // --- INITIALIZE CLASS VARIABLE
        m_appHandle = appHandle;
        m_dashTimer = null;
        m_refVal    = Time.valueOf( "00:00:00" );  
    }
    // --- END
    
    /**
     *
     * @param timeoutVal value for timer to expire
     * @brief set up timer
     */
    public void initialize( String timeoutVal )
    // --- BEGIN
    {            
        // --- SET INITIAL TIMER VALUE
        m_timeOut  = Time.valueOf( timeoutVal );    
        m_timerVal = Time.valueOf( timeoutVal );
        setText( m_timeOut.toString() );       
        Calendar calendar = Calendar.getInstance();
        m_lastTime = Time.valueOf(calendar.get( Calendar.HOUR_OF_DAY) + ":" +
                                        calendar.get(Calendar.MINUTE) + ":" +
                                        calendar.get(Calendar.SECOND));
        m_startTime = m_lastTime;
        m_resetTime = m_lastTime;
      
        // --- IF TIMER ALREADY SET
        if( m_dashTimer != null ){
            // --- STOP TIMER
            stop();
        }
        // --- END
        
        // --- START TIMER
        m_dashTimer = new Timer( m_REFRESH_RATE, this );
        m_dashTimer.setInitialDelay( 0 );
   }
   // --- END
    

    /**
     *
     * @brief start timer countdown
     */
    public void reset( )
    // --- BEGIN
    {                    
        m_startTime = m_lastTime;
        m_resetTime = m_lastTime;
    }
    // --- END
    

    /**
     *
     * @brief start timer countdown
     */
    public void start( )
    // --- BEGIN
    {                    
        m_dashTimer.start(); 
    }
    // --- END
    

    /**
     *
     * @brief stop timer countdown
     */
    public void stop( )
    // --- BEGIN
    {                    
        m_dashTimer.stop(); 
    }
    // --- END
    

    
    /**
     * @param currEvent
     * @param data
     * @return true if event is processed 
     * @brief handle database generated events
     */
    @Override
    public boolean handleQuery( String currEvent, ResultSet data ) 
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        Container   actionArea     = m_appHandle.getActionPane();
        Container   workArea       = m_appHandle.getWorkPane();
        Container   cmdArea        = m_appHandle.getCmdPane();
        JFrame      frame          = m_appHandle.getFrame();
        boolean     eventProcessed = true;
        Iterator    entries;
        String      dbValue;
        SchoolRoles userRole       = SchoolRoles.UNKNOWN;
        Integer     dbKey;
        int         roleCount  = 0;
        System.out.println( "DashTimer::handleQuery - " + currEvent  );
        
        try {
            // --- DO CASE OF DATA TYPE
            switch( currEvent ){
                default:
                break;
            }
            // --- ENDDO CASE OF DATA TYPE
        }
        catch(Exception e){
             System.out.println( "SQL Problem..." + e.getMessage() );
             eventProcessed = false;
        }

        // --- RETURN
        return( eventProcessed );
    }
    // --- END

    
    @Override
    public boolean handleAction(ActionEvent event) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean handleEvent(String currEvent)
    // --- BEGIN
    {
        // --- DECLARE LocAL VARIABLES
        boolean     returnState = true;
        
        // --- DO CASE OF EVENT
        switch( currEvent ){
            default:
            break;    
        }
        // --- ENDDO CASE OF EVENT
        
        // --- RETURN
        return( returnState );    
    }
    // --- END

    /**
     *
     * @brief handle top level action
     * @param currentAction gui event
     */
    @Override
    public void actionPerformed( ActionEvent currentAction )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        Object        actionSource = currentAction.getSource();
        java.sql.Time now;
        long          diffTime;
        DashApp       dashHandle;
        
        // --- IF TIMER HAS FIRED
        if( actionSource == m_dashTimer ){
            // --- GET THE CURRENT TIME
            Calendar calendar = Calendar.getInstance();
            now = java.sql.Time.valueOf(calendar.get(Calendar.HOUR_OF_DAY) + ":" +
                                        calendar.get(Calendar.MINUTE) + ":" +
                                        calendar.get(Calendar.SECOND));                    
            
            // --- GET TIME DELTA
            diffTime = (now.getTime() - m_resetTime.getTime());
            
            // --- IF TIMER NEED REFRESSING
            if( diffTime > m_TIMER_RATE ){
                // --- UPDATE VIEW
                long newTime = m_timeOut.getTime() - (diffTime);
                
                // --- IF THERE IS STILL TIME LEFT
                if( newTime > m_refVal.getTime() ){
                    // --- SET TIMER
                    m_timerVal.setTime( newTime );
                    setText( m_timerVal.toString() );             
                    m_lastTime = now;
                   
                }
                // --- ELSE
                else{
                    // --- HANDLE ELAPSED TiMER EVENT
                    // --- ... RESET TIME FOR NOW
                    stop();
                    //m_timerVal = now;
                    setText( m_timeOut.toString() );
                    m_appHandle.showMessage( "Exiting app." );
                                       
                    // --- LOGOUT
                    m_appHandle.closeMainPanel();                   
                }
                // --- ENDIF THERE IS STILL TIME LEFT                
            }
            // --- ENDIF TIMER NEED REFRESSING
        }
        // --- ENDIF TIMER HAS FIRED
    }
    // --- END
    
    
    
    @Override
    public void handleCmd( String cmd, String cmdData, int cmdKey )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        System.out.println("DashView::handleCmd[" + cmd + "]" );
        final int sVal = SchoolRoles.STUDENT.getValue();
        final int pVal = SchoolRoles.PARENT.getValue();
        final int eVal = SchoolRoles.EDUCATOR.getValue();
        final int gVal = SchoolRoles.GRADER.getValue();
        final int wVal = SchoolRoles.WATCHER.getValue();
        final int aVal = SchoolRoles.ADMINISTRATOR.getValue();
        final int tVal = SchoolRoles.MANAGER.getValue();
        final int oVal = SchoolRoles.OTHER.getValue();
        SchoolRoles clientRole = null;
        int         value;
        
        // --- DO CASE OF EVENT
        switch( cmdKey ){
            default:
            break;    
        }
        // --- ENDDO CASE OF EVENT
    }
    // --- END
}
