/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package schoolhouse.dashboard;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import javax.swing.JTextField;
import javax.swing.Timer;
import schoolhouse.common.SchoolApp;

/**
 *
 * @author wpeets
 * 
 */
public class DashClock extends JTextField implements ActionListener {
              Timer               m_internalTimer;              
    static    final int           m_REFRESH_RATE = 500;
              SchoolApp           m_appHandle;
              
    /**
     *
     * @brief constructor
     * 
     */
    DashClock( SchoolApp appHandle )
    // --- BEGIN
    {
        // --- SATISFY SUBCLASS
        super( );
        
        // --- INITIALIZE CLASS VARIABLE
        m_appHandle = appHandle;
        m_internalTimer = null;
    }
    // --- END
    
    /**
     *
     * @brief set up timer
     */
    public void initialize( )
    // --- BEGIN
    {                  
        // --- START TIMER
        m_internalTimer = new Timer( m_REFRESH_RATE, this );
        m_internalTimer.setInitialDelay( 0 );
        start();
    }
    // --- END
    

    /**
     *
     * @brief start timer countdown
     */
    public void start( )
    // --- BEGIN
    {                    
        m_internalTimer.start(); 
    }
    // --- END
    

    /**
     *
     * @brief stop timer countdown
     */
    public void stop( )
    // --- BEGIN
    {                    
        m_internalTimer.stop(); 
    }
    // --- END
   

    /**
     *
     * @brief handle timer action
     * @param currentAction event object
     */
    @Override
    public void actionPerformed( ActionEvent currentAction )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        Object        actionSource = currentAction.getSource();
        java.sql.Time now;
        
        // --- IF TIMER HAS FIRED
        if( actionSource == m_internalTimer ){
            // --- GET THE CURRENT TIME
            Calendar calendar = Calendar.getInstance();
            now = java.sql.Time.valueOf(calendar.get(Calendar.HOUR_OF_DAY) + ":" +
                                        calendar.get(Calendar.MINUTE) + ":" +
                                        calendar.get(Calendar.SECOND));                    
            setText( now.toString() );                
        }
        // --- ENDIF TIMER HAS FIRED
    }
    // --- END    
}
