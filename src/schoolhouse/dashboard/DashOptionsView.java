/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package schoolhouse.dashboard;

import schoolhouse.client.ClientApp;
import schoolhouse.common.SchoolApp;
import schoolhouse.model.SchoolPolicy.SchoolRoles;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import schoolhouse.common.AppCommands.CommandValue;
import schoolhouse.common.DbEngine;
import schoolhouse.common.SchoolAppConfig;

/**
 *
 * @author wpeets
 */
public class DashOptionsView extends DbEngine implements ItemListener {
    protected String          m_userName;   
    protected JLabel          m_userLabel;  
    protected JPanel          m_userPanel;
    protected FlowLayout      m_userLayout;
    protected JCheckBox       m_soundEnable;
    protected JCheckBox       m_soundViewEnable;
    protected JCheckBox       m_soundProcessEnable;
    protected JCheckBox       m_updateImmediateEnable;
    protected JTextField      m_userField;   
              JSeparator      m_sepLine;
              SchoolAppConfig m_settings;
                        
    /**
     *
     * @param appHandle framework pointer
     * @brief constructor for main view
     */
    public DashOptionsView( SchoolApp appHandle )
    {
        // --- SATISFY SUBCLASS
        super( appHandle );
        
        // --- INIT CLASS VARIABLES
        m_settings = SchoolAppConfig.Instance();
        m_settings.setImmediateUpdate( true );
    }
    // --- END
    
    /**
     *
     * @param userName
     * @brief add a role control to view
     */
    public void initialize( String userName )
    // --- BEGIN
    {    
        // --- CREATE VIEW
        System.out.println( "DashOptionsView::initialize"  );       
        m_userName = userName;
    }
    // --- END

   public void createView(  )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        System.out.println( "DashOptionsView::createView"  );       
        int cmdWidth = 500;
        GridBagConstraints gbc = new GridBagConstraints();
        
        // --- SET LAYOUT
        setLayout( new GridBagLayout() );
        
        m_userPanel = new JPanel();
        m_userLayout = new FlowLayout();
        m_userPanel.setLayout( m_userLayout );
        m_userField = new JTextField( "User Name");
        m_userField.setSize( cmdWidth, 0 );
        m_userField.setActionCommand( "schoollist" );
        m_userField.setToolTipText( "Click this button to add person");
        m_userField.setText( m_userName );
        m_userField.setEditable( false );
        m_userField.addActionListener( this );
        m_userLabel = new JLabel( "User Name" );
        gbc.gridx   = 0;
        gbc.gridy   = 0;
        gbc.gridx++;
        m_userPanel.add( m_userLabel );
        m_userPanel.add( m_userField );
        add( m_userPanel, gbc );
        
        // --- SEPARATOR
        m_sepLine     = new JSeparator( SwingConstants.HORIZONTAL );
        m_sepLine.setPreferredSize( new Dimension(500,1));
        gbc.gridx     = 0;
        gbc.gridy     = 1;
        gbc.gridwidth = 2;
        gbc.fill      = GridBagConstraints.VERTICAL;
        gbc.weighty   = 1;
        add( m_sepLine, gbc );
        gbc.gridwidth = 1;
       
        // --- CREATE SOUND CONTROLS
        m_soundEnable = new JCheckBox( "Enable Voice Process" );
        m_soundEnable.setMnemonic( KeyEvent.VK_V ); 
        m_soundEnable.setSelected( false );
        m_soundEnable.addItemListener( this );
        gbc.gridx = 0;
        gbc.gridy = 2;
        //add( m_userLabel, gbc );
        gbc.gridx++;
        add( m_soundEnable, gbc );

        m_soundViewEnable = new JCheckBox( "Enable Voice View" );
        m_soundViewEnable.setMnemonic( KeyEvent.VK_D ); 
        m_soundViewEnable.setSelected( false );
        m_soundViewEnable.setEnabled( false );
        m_soundViewEnable.addItemListener(this);
        gbc.gridx = 0;
        gbc.gridy = 3;
        //add( m_userLabel, gbc );
        gbc.gridx++;
        add( m_soundViewEnable, gbc );
        
        m_soundProcessEnable = new JCheckBox( "Enable Sound Processing" );
        m_soundProcessEnable.setMnemonic( KeyEvent.VK_P ); 
        m_soundProcessEnable.setSelected( true );
        m_soundProcessEnable.setEnabled( true );
        m_soundProcessEnable.addItemListener( this );
        gbc.gridx = 0;
        gbc.gridy = 4;
        //add( m_userLabel, gbc );
        gbc.gridx++;
        add( m_soundProcessEnable, gbc );
        
        // --- ADD DATABASE OPTIONS
        m_updateImmediateEnable = new JCheckBox( "Immediate Data Update" );
        m_updateImmediateEnable.setMnemonic( KeyEvent.VK_U ); 
        m_updateImmediateEnable.setSelected( true );
        m_updateImmediateEnable.setEnabled( true );
        m_updateImmediateEnable.addItemListener( this );
        gbc.gridx = 0;
        gbc.gridy = 5;
        //add( m_userLabel, gbc );
        gbc.gridx++;
        add( m_updateImmediateEnable, gbc );    
        
        // --- NOW WE CAN CREATE HINTS FOR THIS WINDOW
        // --- ... cannot add these hints yet; one set per window
        setId( m_appHandle.getAppName() );
        m_engineHints.setCmd( CommandValue.Hint );
        m_engineHints.addHint( "Voice",    10 );                            
        m_engineHints.addHint( "Disable",  20 );                            
        m_engineHints.addHint( "Show",     30 );                            
        m_engineHints.addHint( "Hide",     40 );                            
        m_engineHints.addHint( "Process",  50 );                            
        m_engineHints.addHint( "Bypass",   60 );                            
        //updateHints();        
   }
   // --- END


    /**
     *
     * @param userEvent
     * @brief handle top level action
     */
    @Override
    public void actionPerformed( ActionEvent userEvent ) {
        // --- DECLARE LOCAL VARIABLES
        Container actionArea = m_appHandle.getActionPane();
        Container workArea = m_appHandle.getWorkPane();
        Container cmdArea = m_appHandle.getCmdPane();
        JFrame    frame = m_appHandle.getFrame();
        String    userCmd = userEvent.getActionCommand();
        ClientApp clientApp;
        Thread    appThread;
        SchoolRoles clientRole = SchoolRoles.UNKNOWN;
        System.out.println( "RoleView::actionPerformed - " + userCmd  );       
    }
    // --- END
    
   /**
    * Listens to the check boxes.
     * @param e
    */
    @Override
    public void itemStateChanged( ItemEvent e ) 
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        System.out.println( "RoleView::itemStateChanged");
        Object      source      = e.getItemSelectable(); 
        int         selectState = e.getStateChange();
        
        // --- IF EVENT IS ENABLE BUTTON
        if( source == m_soundEnable ){
            // --- IF SOUND IS BEING ENABLED
            if( selectState == ItemEvent.SELECTED ) {
                // --- ENABLE VIEW BOX
                m_settings.setVoiceEnabled( true );
                setVoiceState( true );
            }
            // --- ENDIF SOUND IS BEING ENABLED
            
            // --- IF SOUND IS BEING DISABLED
            if( selectState == ItemEvent.DESELECTED ) {
                // --- DISABLE VIEW BOX
                m_settings.setVoiceEnabled( false );
                setVoiceState( false );
            }
        }
        // --- ENDIF EVENT IS ENABLE BUTTON
        
        // --- IF EVENT IS VIEW BUTTON
        if( source == m_soundViewEnable ){
            // --- IF SOUND IS BEING ENABLED
            if( selectState == ItemEvent.SELECTED ) {
                // --- ENABLE VIEW BOX
                m_settings.setVoiceViewEnabled( true );
                setVoiceView( true );
            }
            // --- ENDIF SOUND IS BEING ENABLED
            
            // --- IF SOUND IS BEING DISABLED
            if( selectState == ItemEvent.DESELECTED ) {
                // --- DISABLE VIEW BOX
                m_settings.setVoiceViewEnabled( false );
                setVoiceView( false );
            }
        }
        // --- ENDIF EVENT IS VIEW BUTTON

        // --- IF EVENT IS VIEW BUTTON
        if( source == m_soundProcessEnable ){
            // --- IF SOUND IS BEING ENABLED
            if( selectState == ItemEvent.SELECTED ) {
                // --- ENABLE VIEW BOX
                m_settings.setVoiceProcessingEnabled( true );
                setProcessState( true );
            }
            // --- ENDIF SOUND IS BEING ENABLED
            
            // --- IF SOUND IS BEING DISABLED
            if( selectState == ItemEvent.DESELECTED ) {
                // --- DISABLE VIEW BOX
                m_settings.setVoiceProcessingEnabled( false );
                setProcessState( false );
            }
        }
        // --- ENDIF EVENT IS VIEW BUTTON
        
        // --- IF UPDATE SELECTED
        if( source == m_updateImmediateEnable ){
            // --- IF SOUND IS BEING ENABLED
            if( selectState == ItemEvent.SELECTED ) {
                // --- ENABLE VIEW BOX
                m_settings.setVoiceViewEnabled( true );
            }
            // --- ENDIF SOUND IS BEING ENABLED
            
            // --- IF SOUND IS BEING DISABLED
            if( selectState == ItemEvent.DESELECTED ) {
                // --- DISABLE VIEW BOX
                m_settings.setImmediateUpdate( false );
            }
        }
        // --- ENDIF UPDATE SELECTED
    }
    // --- END

   /**
    * @param newState true or false
    * @brief set state voice processing
    */
    public void setVoiceState( boolean newState ) 
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        System.out.println( "RoleView::setVoiceState" );
        //AppCommands mac = new AppCommands( this );
        
        // --- IF SOUND IS BEING ENABLED
        if( newState == true ) {
            // --- ENABLE VIEW BOX
            m_soundViewEnable.setEnabled( true );
            updateState( CommandValue.Enable );
        }
        // --- ELSE IF SOUND IS BEING DISABLED
        else {
            // --- DISABLE VIEW BOX
            m_soundViewEnable.setEnabled( false );
            updateState( CommandValue.Disable );
        }
        // --- ENDIF SOUND IS BEING ENABLED
    }
    // --- END
    
   /**
    * @param voiceState true or false
    * @brief set state voice view
    */
    public void setVoiceView( boolean voiceState ) 
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        System.out.println( "RoleView::setVoiceView");
        
        // --- IF SOUND VIEW IS BEING ENABLED
        if( voiceState == true ) {
            // --- ENABLE VIEW BOX
            m_soundViewEnable.setEnabled( true );
            updateState( CommandValue.Show );
            
            // --- SET FOCUS BACK TO THE DASH
            System.out.println( "RoleView::setVoiceView refocus dash");
            m_appHandle.getFrame().toFront();
            m_appHandle.getFrame().setState( Frame.NORMAL );
        }
        // --- ELSE IF SOUND VIEW IS BEING DISABLED
        else {
            // --- DISABLE VIEW BOX
            m_soundViewEnable.setSelected( false );
            updateState( CommandValue.Hide );
        }
        // --- ENDIF SOUND VIEW IS BEING ENABLED
    }
    // --- END
    
   /**
    * @param processState true or false
    * @brief set state voice view
    */
    public void setProcessState( boolean processState ) 
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        System.out.println( "RoleView::setProcessState");
        
        // --- IF SOUND PROCESSING IS BEING ENABLED
        if( processState == true ) {
            // --- ENABLE VIEW BOX
            m_soundProcessEnable.setEnabled( true );
            m_engineHints.setCmd( CommandValue.Process );                
            updateState( CommandValue.Process );
        }
        // --- ELSE IF SOUND PROCESSING IS BEING DISABLED
        else {
            // --- DISABLE VIEW BOX
            m_soundProcessEnable.setSelected( false );
            updateState( CommandValue.Bypass );
        }
        // --- ENDIF SOUND VIEW IS BEING ENABLED
    }
    // --- END
    
    
    
    
    /**
     * communicates commands from the GUI interface 
     *
     * @param event
     * @return   
     */
    @Override
    public boolean handleAction(ActionEvent event) 
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        boolean eventState = true;
        
        // --- PASS IT ON
        actionPerformed( event );
        
        // --- RETURN
        return( eventState );
    }
    // --- END
    
    @Override
    public boolean handleEvent(String currEvent) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return 
     * @brief handle database generated events
     */
    @Override
    public boolean handleQuery( String currEvent, ResultSet data ) 
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        boolean     eventProcessed = true;
        System.out.println( "RoleView::handleQuery - " + currEvent  );

        // --- RETURN
        return( eventProcessed );
    }
    // --- END

    @Override
    public void handleCmd( String cmd, String cmdData, int cmdKey )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        System.out.println("DashView::handleCmd[" + cmd + "]" );
        
        // --- DO CASE OF EVENT
        switch( cmdKey ){
            case 10:
                 setVoiceState( true );
            break;
            case 20:
                 setVoiceState( false );
            break;
            case 30:
                 setVoiceView( true );
            break;
            case 40:
                 setVoiceView( false );
            break;
            case 50:
                 setProcessState( true );
            break;
            case 60:
                 setProcessState( false );
            break;
            case 10000:
                 System.out.println( "DashView::handleCmd  Acknowledge received" );
            break;   
            default:
            break;    
        }
        // --- ENDDO CASE OF EVENT
    }
    // --- END
}
