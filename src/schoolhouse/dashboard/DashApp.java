/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package schoolhouse.dashboard;

import java.awt.Container;
import schoolhouse.common.SchoolApp;
import schoolhouse.model.SchoolPolicy.SchoolRoles;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import schoolhouse.common.AppCommands;
import schoolhouse.common.DbEngine;

/**
 *
 * @author wpeets
 */
public class DashApp extends SchoolApp implements Runnable, WindowFocusListener {
    
    /**
     * @brief main view window display for application
     * 
     */
    static public DashRoleView    m_main;
    static public DashOptionsView m_options;
    static public DashStatusView  m_status;
    
    /**
     * @brief constructor
     * 
     * @param userName 
     */
    public DashApp( String userName )
    // --- BEGIN
    {
        // --- SATISFY SUPERCLASS
        // --- ... when the application starts the role taken is unknown
        // --- ... TBD unless maybe the user has one role
        super( SchoolRoles.UNKNOWN );
        
        // --- DECLARE LOCAL VARIABLES
        Container actionArea = getActionPane();
        Container workArea   = getWorkPane();
        Container cmdArea    = getCmdPane();
               
        // --- CREATE VIEW
        setUser( userName );
        setAppName( "DASHBOARD" );
        m_main    = null;
        m_options = null;
        m_status  = null;
        
        // --- CREATE ROLE BUTTONS
        m_main = new DashRoleView( this );
        m_main.initialize();
        m_main.createView();
        actionArea.add( m_main );
        m_main.setSize( 150, 500 );
        
        // --- INIT APPLICATION
        setActiveDash( this );
        m_mainView = m_main;

        // --- CREATE OPTIONS
        m_options = new DashOptionsView( this );
        m_options.initialize( getUser() );
        m_options.createView();
        workArea.add( m_options );
        m_options.setSize( 400, 500 );

        // --- CREATE STATUS
        m_status = new DashStatusView( this );
        m_status.initialize();
        m_status.createView();
        cmdArea.add( m_status ); 
        m_status.setSize( 150, 500 );        
    }
    // --- END
    
    /**
     *
     * @author wpeets
     * 
     * #brief display user interface
     */

    @Override
    public void run()
    // --- BEGIN
    {
        // --- MAKE MAIN WINDOW VISIBLE
        showApp();
        setActiveEngine( m_main );
        m_frame.setBounds( 25, 25, 700, 375 );
        m_frame.addWindowFocusListener( this );
    }
    // --- END
    
    /**
     * @brief terminate application
     */
    @Override
    public void closeMainPanel()
    // --- BEGIN
    {
        // --- DECLARE LOcAL VARIABLES
        Thread currentThread;
        
        // --- KILL CLIENT
        m_main.killView();
        
        // --- KILL THREAD
        System.out.println( "DashApp::closeMainPanel" );            
        m_frame.setVisible( false );        
    }
    // --- END

    /**
     * @brief reset timer
     */  
    public void resetTimer()
    // --- BEGIN
    {
        // --- IF THE APPLICATION HAS A TIMER
        if( m_status != null ){
            // --- RESET TIMER
            m_status.resetTimer();
        }
        // --- ENDIF THE APPLICATION HAS A TIMER
    }
    // --- END
   
    /**
     *
     * @param clientRole current user role
     * @brief clear client state
     * 
     */
    public void clearRole( SchoolRoles clientRole )
    // --- BEGIN
    {
        // --- IF THE APPLICATION HAS ROLES
        if( m_main != null ){
            // --- CLEAR ROLE
            m_main.clearRole( clientRole );
        }
        // --- ENDIF THE APPLICATION HAS ROLES
    }
    // --- END
    
    
    /**
     * @brief display client menu
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // --- DECLARE LOCAL VARIABLES
        DashApp  clientDash;
        Thread   dashThread;
        
        // --- CREATE APPLICATION
        System.out.println( "Create API thread" );
        //clientDash = new ClientDash( "dscott" );
        clientDash = new DashApp( "dadpeets" );
        dashThread = new Thread( clientDash );
        
        // --- DISPLAY MAIN MENU
        dashThread.start();
    }
    @Override
    public void windowGainedFocus(WindowEvent e) 
    // --- BEGIN
    {
        // --- SEND VOICE FOCUS
        setFocus();    
    }
    // --- END
    
    
    @Override
    public void windowLostFocus(WindowEvent e) {
        // --- DO NOTHING
    }
    
    
    /**
     *
     * 
     * #brief inform infrastructure of focus
     * 
     */
    public void setFocus() 
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        DbEngine currentEngine = getActiveEngine( );
        boolean  handleEvent;
        
        // --- SEND VOICE FOCUS
        
        // --- IF AN ACTION IS SELECTED
        if( (currentEngine != null) && (currentEngine.hasHints()==true) ){
            // --- HAVE THE VIEW HANDLE ITS CURRENT HINTS
            System.out.println( "SchoolApp::setFocus - " + currentEngine.getId() );
            handleEvent = currentEngine.handleEvent( AppCommands.CommandValue.Focus.toString() );    
        }
        // --- ELSE
        else if( m_mainView.hasHints() == true ){
            // --- SEND TOP LEVEL HINTS
            System.out.println( "SchoolApp::windowGainedFocus  Top level" );
            handleEvent = m_mainView.handleEvent( AppCommands.CommandValue.Focus.toString() );           
        }
        // --- ENDIF AN ACTION IS SELECTED
    }
    // --- END

    
}
