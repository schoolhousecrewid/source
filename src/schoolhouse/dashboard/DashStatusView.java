/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package schoolhouse.dashboard;

import java.awt.FlowLayout;
import schoolhouse.common.AppCommands;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import schoolhouse.common.DbEngine;
import schoolhouse.common.HandleCmdAction;
import schoolhouse.common.SchoolApp;

/**
 *
 * @author wpeets
 */
public class DashStatusView extends DbEngine implements HandleCmdAction {
                  java.sql.Time  m_startTime;
                  java.sql.Time  m_resetTime;
                  java.sql.Time  m_lastTime;
                  java.sql.Time  m_timeOut;                    
                  java.sql.Time  m_timerVal;                    
    private final DashTimer      m_dashTimer;
    protected     DashClock      m_dashClock;
    protected     JLabel         m_clockLabel;
    protected     JLabel         m_timerLabel;
    protected     JPanel         m_clockPanel;
    protected     JPanel         m_timerPanel;
     
                      
    DashStatusView( SchoolApp appHandle )
    // --- BEGIN
    {
        // --- SATISFY SUBCLASS
        super( appHandle );        
        m_dashClock = new DashClock( m_appHandle );
        m_dashTimer = new DashTimer( m_appHandle );
    }
    // --- END
    
    /**
     *
     * @brief add a role control to view
     */
    @Override
    public void initialize( )
    // --- BEGIN
    {    
        // --- START TIMER
        m_dashTimer.initialize( "00:10:00" );
        m_dashTimer.start(); 
        m_dashClock.initialize();
    }
    // --- END

   public void createView(  )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        GridBagConstraints  timeConstraints;
                        
        // --- CREATE CLOCK PANEL
        timeConstraints       = new GridBagConstraints();
        timeConstraints.gridx = 0;
        timeConstraints.gridy = 0;
        timeConstraints.fill  = GridBagConstraints.HORIZONTAL;
        m_clockPanel          = new JPanel();
        m_clockPanel.setLayout( new FlowLayout() );
        m_clockLabel = new JLabel( "Clock: " );
        m_clockPanel.add( m_clockLabel );
        m_clockPanel.add(m_dashClock );
        add( m_clockPanel, timeConstraints );
        
        
        // --- CREATE TIMER PANEL
        timeConstraints.gridx = 0;
        timeConstraints.gridy = 1;
        timeConstraints.fill  = GridBagConstraints.HORIZONTAL;
        m_timerPanel          = new JPanel();
        m_timerPanel.setLayout( new FlowLayout() );
        m_timerLabel = new JLabel( "Timer: " );
        m_timerPanel.add( m_timerLabel );
        m_timerPanel.add(m_dashTimer );
        add( m_timerPanel, timeConstraints );      
   }
   // --- END

    /**
     * @brief reset timer
     */  
   public void resetTimer()
   // --- BEGIN
   {
       // --- RESET TIMER
       m_dashTimer.reset();
   }
   // --- END
   
   
    
    @Override
    public boolean handleAction(ActionEvent event) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean handleEvent(String currEvent)
    // --- BEGIN
    {
        // --- DECLARE LocAL VARIABLES
        boolean returnState = true;
        
        // --- DO CASE OF EVENT
        switch( currEvent ){
            case "Focus":
                // --- SEND VOICE FOCUS
                updateState( AppCommands.CommandValue.Focus );
            break;
            default:
            break;    
        }
        // --- ENDDO CASE OF EVENT
        
        // --- RETURN
        return( returnState );    
    }
    // --- END

        
    
    @Override
    public void handleCmd( String cmd, String cmdData, int cmdKey )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        System.out.println("DashStatusView::handleCmd[" + cmd + "]" );
    }
    // --- END
}
