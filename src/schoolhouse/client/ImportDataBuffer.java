package schoolhouse.client;

import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author J. Williams <jwilliams@schoolhouse_infinity_scaling>
 */
public class ImportDataBuffer {
     
    private ArrayList <String> m_ArrayList;

    public ArrayList<String> getM_ArrayList() {
        return m_ArrayList;
    }
    
    public ImportDataBuffer() {
        m_ArrayList = new ArrayList<>();        
    }
    
    public  void addItem(String aString){
        m_ArrayList.add(aString);        
    }

    
    
}
