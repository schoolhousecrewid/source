/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package schoolhouse.client;

import com.toedter.calendar.JDateChooser;
import schoolhouse.common.SchoolDbIntfc;
import schoolhouse.common.QueryChain;
import schoolhouse.common.SchoolApp;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Vector;
import javax.swing.JComboBox;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import schoolhouse.common.AppCommands;
import schoolhouse.common.DbEngine;
import schoolhouse.common.HandleObjectAction;
import schoolhouse.common.SchoolModel;
import schoolhouse.model.SchoolClass;
import schoolhouse.model.SchoolMark;
import schoolhouse.model.SchoolMark.SchoolMarkType;
import schoolhouse.model.SchoolCourse;
import schoolhouse.model.SchoolGradeScale;
import schoolhouse.model.SchoolHouse;

/**
 *
 * @author wpeets
 */
public class MarkView extends DbEngine implements ActionListener {
    protected JComboBox               m_schoolList;
    protected JComboBox               m_courseList;
    protected JComboBox               m_classList;
    protected JTextField              m_className;
    protected JComboBox               m_courseName;
    protected JComboBox               m_markerList;
    protected JTextField              m_markerId;
    protected JTextField              m_markTypeId;
    protected JComboBox               m_markerTypeName;
    protected JTextField              m_markName;
    protected JDateChooser            m_markAssigned;
    protected JDateChooser            m_markDue;
    protected JTextField              m_markPercent;
    protected JTextField              m_markMin;
    protected JTextField              m_markMax;
    protected JComboBox               m_scaleList;
    protected JTextField              m_markModifier;
              ArrayList<SchoolCourse> m_currentCourseList;
              ArrayList<SchoolClass>  m_currentClassList;
              ArrayList<SchoolMark>   m_currentMarkList;
              SchoolHouse             m_currentSchool;
              SchoolCourse            m_currentCourse;
              SchoolClass             m_currentClass;
              SchoolMarkType          m_currentType;
              SchoolMark              m_currentMark;
              HandleObjectAction      m_objectHandler;
              ScaleDialog             m_scaleDialog;

    public MarkView( SchoolApp appHandle )
    // --- BEGIN
    {
        // --- INITIALIZE CLASS VARIABLES
        super( appHandle );
        m_objectHandler = null;
        m_currentMark   = null;
        m_scaleDialog   = new ScaleDialog( m_appHandle );
        
        // ---  CREATE VIEW INTERFACE
        createViews();
    }
    // --- END
              
    private void createViews( )
    // --- BEGIN
    {
        // --- CREATE ADD ACTIONS
        m_schoolList = new JComboBox( );
        m_schoolList.setActionCommand( "schoollist" );
        m_schoolList.addActionListener( this );
        addRow( "School List", m_schoolList );

        m_courseList = new JComboBox( );
        m_courseList.setActionCommand( "courselist" );
        m_courseList.addActionListener( this );
        addRow( "Course List", m_courseList );

        m_classList = new JComboBox( );
        m_classList.setActionCommand("classlist");
        m_classList.addActionListener(this);
        addRow( "Class List", m_classList );

        m_className = new JTextField( "Class Name");
        m_className.setActionCommand("classname");
        m_className.setToolTipText("Automatic field");
        m_className.setEditable( false );
        m_className.setBackground( Color.gray );
        m_className.setColumns( 20 );
        addRow( "Class Name", m_className );

        addSeparator();
        
        m_markerList = new JComboBox(  );
        m_markerList.setActionCommand("marklist");
        m_markerList.setToolTipText("Marker Id");
        m_markerList.addActionListener(this);
        addRow( "Marker", m_markerList );

        m_markerId = new JTextField(  );
        m_markerId.setBackground( Color.gray );
        m_markerId.setActionCommand("markerid");
        m_markerId.setToolTipText("Marker Id");
        m_markerId.addActionListener(this);
        addRow( "Marker Id", m_markerId );

        m_markTypeId = new JTextField(  );
        m_markTypeId.setActionCommand("mType");
        m_markTypeId.setToolTipText("Marker Type Id");
        m_markTypeId.addActionListener(this);
        m_markTypeId.setBackground( Color.gray );
        m_markTypeId.setEditable( false );
        m_markTypeId.setColumns( 20 );
        addRow( "Mark Type Id", m_markTypeId );

        m_markerTypeName = new JComboBox( );
        m_markerTypeName.setActionCommand("markertypename");
        m_markerTypeName.setToolTipText("Click this to enter type of mark");
        m_markerTypeName.addActionListener(this);
        addRow( "Mark Type", m_markerTypeName );

        m_markName = new JTextField( "Mark Name" );
        m_markName.setActionCommand("id");
        m_markName.setToolTipText("Click this to enter address");
        m_markName.addActionListener(this);
        addRow( "Mark Name", m_markName );

        m_markAssigned = new JDateChooser(  );
        m_markAssigned.setToolTipText("Click this set date assigned");
        addRow( "Assigned Date", m_markAssigned );
  
        m_markDue = new JDateChooser( );
        m_markDue.setToolTipText("Date assignment is due");
        addRow( "Date Due", m_markDue );
   
        m_markPercent = new JTextField( "Final Grade Percentage" );
        m_markPercent.setActionCommand("percent");
        m_markPercent.setToolTipText("Total cass percentage");
        m_markPercent.addActionListener(this);
        addRow( "Final Grade Percentage", m_markPercent );
   
        m_markMin = new JTextField( "Min" );
        m_markMin.setActionCommand("minGrade");
        m_markMin.setToolTipText("Date Due");
        m_markMin.addActionListener(this);
        addRow( "Min", m_markMin );
   
        m_markMax = new JTextField( "Max" );
        m_markMax.setActionCommand("maxGrade");
        m_markMax.setToolTipText("Max");
        m_markMax.addActionListener(this);
        addRow( "Max", m_markMax );
        
        // --- SCALE LIST
        m_scaleList = new JComboBox(  );
        m_scaleList.setActionCommand( "scalelist" );
        m_scaleList.setToolTipText( "Possible School Scales" );
        m_scaleList.addActionListener(this);
        addRow( "Grade Scale", m_scaleList );

        m_markModifier = new JTextField( "Modifier" );
        m_markModifier.setActionCommand("modifier");
        m_markModifier.setToolTipText("Modifier");
        m_markModifier.addActionListener(this);
        addRow( "Modifier", m_markModifier );
    }
    // --- END
    
    /**
     * @brief initial action on creating action 
     */
    public void initialize( HandleObjectAction objectHandler )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolModel appModel    = m_appHandle.getModel();
        Vector      dataRequest = new Vector();
        
        // --- MAKE DATABASE QUERIES NECESSARY FOR ADDING ADDING PARTICIPANT
        dataRequest.add( SchoolDbIntfc.SchoolQuery.SCHOOL );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.COURSE );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.RANK );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.SCALE );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.CLASS );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.MARK_TYPE );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.MARKER );
        appModel.getData( dataRequest );
        
        // --- STORE HANDLER
        m_objectHandler = objectHandler;
        setId( m_appHandle.getAppName() + "[MARK]" );
        
        // --- CREATE INITIAL HINTS
        m_engineHints.addHint( "School List",   1  + FORM_OFFSET );
        m_engineHints.addHint( "Mark List",     2  + FORM_OFFSET );
        m_engineHints.addHint( "Course List",   3  + FORM_OFFSET );
        m_engineHints.addHint( "Class List",    4  + FORM_OFFSET );
        m_engineHints.addHint( "Mark Type",     5  + FORM_OFFSET );
        m_engineHints.addHint( "Up",            6  + FORM_OFFSET );
        m_engineHints.addHint( "Down",          7  + FORM_OFFSET );
        m_engineHints.addHint( "Name",          8  + FORM_OFFSET );
        m_engineHints.addHint( "Assigned Date", 9  + FORM_OFFSET );
        m_engineHints.addHint( "Due Date",      10 + FORM_OFFSET );
        m_engineHints.addHint( "Final Percent", 11 + FORM_OFFSET );
        m_engineHints.addHint( "Min",           12 + FORM_OFFSET );
        m_engineHints.addHint( "Max",           13 + FORM_OFFSET );
        m_engineHints.addHint( "Clear",         14 + FORM_OFFSET );
        
        // --- SEND HINTS
        m_appHandle.setActiveEngine( this );
        updateHints();
        m_appHandle.setFocus();
    }
    // --- END
    

    /**
     * @brief handle actions from GUI events 
     */
    public void actionPerformed(ActionEvent e) {
        // --- DECLARE LOCAL VARIABLES
        int              typeValue;
        String           currentCourse;
        String           currentMark;
        String           currentClassString;
        Integer          currentClass;
        String           actionCmd = e.getActionCommand();
        String           currentSchool;
        String           markName;
        SchoolCourse     courseInfo;
        SchoolClass      classInfo;
        SchoolMark       markInfo;
        Integer          classId;
        Date             sqlDateAssigned;
        Date             sqlDateDue;
        boolean          markState = false;        
        boolean          markExists  = false;
        String           currentScale;
        SchoolGradeScale scaleInfo;
        System.out.println( "AddMark::actionPerformed Cmd: " + actionCmd );
        
        try {
            // --- IF THE RETURN BUTTON IS PRESSED
            if( "return".equals( actionCmd )) {
               m_appHandle.showMainPanel();
            }
            // --- IF THE RETURN BUTTON IS PRESSED
            else if( "clear".equals( actionCmd )) {
                // --- CLEAR FIELDS
                clearForm();
            }
            // --- IF THE RETURN BUTTON IS PRESSED
            else if( "submit".equals( actionCmd ) ){
                // --- MAKE SURE MARK FORM IS COMPLETE
                String newMarkName = m_markName.getText();
                markState  = validMark();
            
                // --- IF THE MARK DATA IN THE FORM IS COMPLETE
                if( markState == true ){
                    // --- CREATE OBJECT TO ADD
                    SchoolMark newMark = new SchoolMark( m_appHandle );
                    newMark.setSchoolClass( m_currentClass );
                    newMark.setAssignmentName( m_markName.getText() );
                    sqlDateAssigned = new java.sql.Date( m_markAssigned.getDate().getTime());                                    
                    newMark.setAssignedDate( sqlDateAssigned );
                    sqlDateDue = new java.sql.Date( m_markDue.getDate().getTime());                                    
                    newMark.setDueDate( sqlDateDue );
                    newMark.setType(m_markerTypeName.getSelectedIndex()+1 );
                    Integer gradePercent = new Integer( m_markPercent.getText() );
                    Integer gradeMin = new Integer( m_markMin.getText() );
                    Integer gradeMax = new Integer( m_markMax.getText() );
                    newMark.setGradePercent( gradePercent );
                    newMark.setMaxGradeValue( gradeMax );
                    newMark.setMinGradeValue( gradeMin );
                    
                    // --- SET SCALE
                    currentScale = (String)m_scaleList.getSelectedItem();
                    scaleInfo = m_appHandle.getModel().getScale( currentScale );
                    newMark.setScale( scaleInfo );

                    // --- IF MARK IS SET
                    if( m_currentMark != null ){
                        // --- SET ID
                        newMark.setId( m_currentMark.getId() );
                    }
                    // --- ENDIF MARK IS SET
                    
                    // --- HANDLE SUBMIT
                    m_objectHandler.handleObject( newMark );
                }
                // --- ELSE
                else {
                    // --- WARN USERT DATA ALREADY EXIST VIA STATUS BAR
                    m_appHandle.setStatus("Info: course already exist");
                }
                // --- ENDIF THE COURSE DOES NOT EXISTS
                
            }
            // --- IF THE RETURN BUTTON IS PRESSED
            else if( "schoollist".equals( actionCmd ) ){
                // --- FILL COURSE LIST FOR SCHOOL
                currentSchool = (String)m_schoolList.getSelectedItem();
                m_currentSchool = m_appHandle.getModel().getSchool( currentSchool );
                m_currentCourseList = m_currentSchool.getCourseList();
                Iterator courseIter = m_currentCourseList.iterator();
                m_courseList.removeActionListener( this );
                m_courseList.removeAllItems();
                System.out.println("Course List: " + currentSchool );

                // --- DO FOR EACH COURSE IN THE SCHOOL
                while( courseIter.hasNext( ) ) {
                    // --- GET DATA
                    courseInfo = (SchoolCourse)courseIter.next( );
                    m_courseList.addItem( courseInfo.getName() );                               
                    System.out.println("Course List: " + courseInfo.getName() );

                }   
                // --- ENDDO FOR EACH COURSE IN THE SCHOOL
                    
                m_courseList.addActionListener( this );
            }
            else if( "courselist".equals( actionCmd ) ){                   
                // --- CLEAR CLASS LIST
                currentCourse = (String)m_courseList.getSelectedItem();
                m_currentCourse = m_currentSchool.getCourse( currentCourse );
                m_currentClassList = m_currentCourse.getClassList();
                m_classList.removeAllItems();
                m_classList.removeActionListener( this );
                Iterator classIter = m_currentClassList.iterator();

                // --- DO FOR EACH COURSE IN THE MODEL
                while( classIter.hasNext( ) ) {
                    // --- GET DATA
                    classInfo = (SchoolClass)classIter.next( );
                    classId = classInfo.getId();
                    m_classList.addItem( classId.toString() );
                }
                // --- ENDDO FOR EACH COURSE IN THE MODEL
                
                m_classList.addActionListener( this );
            }
            // --- ELSE
            else if( "classlist".equals( actionCmd ) ){
                // --- POPULATE WITH INFO
                currentClassString = (String)m_classList.getSelectedItem();
                currentClass = new Integer( currentClassString );
                // --- TBD SHOULD PROBABLY GET FROM CURRENT COURSE
                m_currentClass = m_currentCourse.getClass( currentClass );
                m_className.setText( m_currentClass.getClassName() );
                
                // --- GET MARKS DATA
                m_currentMarkList = m_currentClass.getMarkList();
                m_markerList.removeAllItems();
                m_markerList.removeActionListener( this );
                Iterator markIter = m_currentMarkList.iterator();
            
                // --- DO FOR EACH COURSE IN THE MODEL
                while( markIter.hasNext( ) ) {
                    // --- GET DATA
                    markInfo = (SchoolMark)markIter.next( );
                    markName = markInfo.getAssignmentName();
                    m_markerList.addItem( markName );
                }
                // --- ENDDO FOR EACH COURSE IN THE MODEL
                m_markerList.addActionListener( this );
            }
            // --- ELSE
            else if( "marklist".equals( actionCmd ) ){
                // --- POPULATE WITH INFO
                currentMark = (String)m_markerList.getSelectedItem();
                m_currentMark = m_currentClass.getMark( currentMark );
                
                // --- SET FORM
                Integer markId = m_currentMark.getId();
                m_markerId.setText( markId.toString() );
                Integer markType = m_currentMark.getType();
                m_markTypeId.setText( markType.toString() );
                m_currentType = SchoolMarkType.fromValue( m_currentMark.getType() );
                m_markerTypeName.setSelectedIndex( m_currentMark.getType() - 1 );
                m_markName.setText(m_currentMark.getAssignmentName() );
                m_markAssigned.setDate( m_currentMark.getAssignedDate() );
                m_markDue.setDate( m_currentMark.getDueDate() );
                Integer percentVal = m_currentMark.getGradePercent();
                m_markPercent.setText( percentVal.toString() );
                Integer minVal = m_currentMark.getMinGradeValue();
                m_markMin.setText( minVal.toString() );
                Integer maxVal = m_currentMark.getMaxGradeValue();
                m_markMax.setText( maxVal.toString() );
                m_markModifier.setText( m_currentMark.getModifier() );           
            }
            // --- CASE ROLE OF TYPE IS SELECTED
            else if( "markertypename".equals( actionCmd ) ){
                // -- RESET ON SELECTION
                typeValue = m_markerTypeName.getSelectedIndex();
                typeValue++;
                Integer newType = typeValue;
                m_currentType = SchoolMarkType.fromValue( typeValue );
                m_markTypeId.setText( newType.toString() );
            }
            else if( "scalelist".equals( actionCmd ) ){
                    // --- SCHOW SCLE DATA FOR SELECTION
                    System.out.println( "MarkView::actionPerformed Scales" );
                    currentScale = (String)m_scaleList.getSelectedItem();
                    //schoolId = (Integer)m_schoolIndex.elementAt( schoolIndex );
                    scaleInfo = m_appHandle.getModel().getScale( currentScale );
                    m_scaleDialog.show( scaleInfo );
            }
            // --- ENDIF THE RETURN BUTTON IS PRESSED

        }
        catch( Exception err ){
            System.out.println("SQL Problem..."+err.getMessage());
        }        
    }
    // --- END
    
    
    /** 
     * @brief determine if the form has valid course data
     */
    public boolean validMark()
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        boolean dataValid = true;
    
        // --- IF ANY FIELD IS BLANK
        if( m_markName.equals("") ){
            // --- COURSE CANNOT BE ADDED
            dataValid = false;
        }    
        // --- ENDIF ANY FIELD IS BLANK
        
        // -- RETURN STATE 
        return( dataValid );
    }
    // --- END


    /** 
     * @brief determine if the course data exists
     */
    public boolean existsMark( String newName )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        boolean    dataFound = false;
        Iterator   markIter = m_currentMarkList.iterator();
        String     markName;
        SchoolMark markInfo;
                    
        // --- DO FOR EACH COURSE IN THE MODEL
        while( (markIter.hasNext()) && (dataFound==false) ) {
            // --- GET DATA
            markInfo = (SchoolMark)markIter.next( );
            markName = markInfo.getAssignmentName();
            
            // --- IF THE MARK ALREADY EXISTS
            if( markName.equals(newName) ){
                // --- RETURN TRUE
                dataFound = true;
            }
            // --- ENDIF THE MARK ALREADY EXISTS
        }
        // --- ENDDO FOR EACH COURSE IN THE MODEL
       
        // -- RETURN STATE 
        return( dataFound );
    }
    // --- END
 
    /** 
     * @brief handle non gui generated events
     */
    @Override
    public boolean handleEvent(String currEvent) 
    // --- BEGIN
    {
        // --- DECLARE LocAL VARIABLES
        boolean returnState = true;
        
        // --- DO CASE OF EVENT
        switch( currEvent ){
            case "Focus":
                // --- SEND VOICE FOCUS
                updateState( AppCommands.CommandValue.Focus );
            break;
            default:
            break;    
        }
        // --- ENDDO CASE OF EVENT
        
        // --- RETURN
        return( returnState );    
    }
    // --- END

    /**
     * @param currEvent
     * @param data
     * @return 
     * @brief handle database generated events
     */
    @Override
    public boolean handleQuery( String currEvent, ResultSet data ) 
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        boolean                     eventProcessed = super.handleQuery( currEvent, data );
        SchoolHouse                 schoolInfo;
        ArrayList<SchoolHouse>      schoolList;
        ArrayList<SchoolGradeScale> scaleList;
        SchoolGradeScale            scaleInfo;
        
        // --- IF DATA HAS BEEN RETRIEVED
        if( eventProcessed == true ){
            try {
                // --- DO CASE OF DATA TYPE
                switch( currEvent ){
                    // --- CASE THE SCHOOL DATA IS RETRIEVED
                    case DbEngine.SCHOOL_DATA:
                        // --- POPULATE SCHOOL COMBO BOX
                        m_schoolList.removeActionListener( this );
                        m_schoolList.removeAllItems();
                        schoolList = m_appHandle.getModel().getSchoolList();
                        Iterator schoolIter = schoolList.iterator();

                        // --- DO FOR EACH SCHOOL
                        while( schoolIter.hasNext() ){
                            // --- ADD NAME TO LIST
                            schoolInfo = (SchoolHouse)schoolIter.next( );
                            m_schoolList.addItem( schoolInfo.getName() );                               
                        }
                        // --- ENDDO FOR EACH SCHOOL
                        
                        // --- REINSTATE LISTENER
                        m_schoolList.addActionListener( this );
                    break;
                    // --- CASE COURSE DATA IS RETRIEVED
                    case DbEngine.SCALE_DATA:
                        // --- POPULATE SCHOOL COMBO BOX
                        m_scaleList.removeActionListener( this );
                        m_scaleList.removeAllItems();
                        scaleList = m_appHandle.getModel().getScaleList();
                        Iterator scaleIter = scaleList.iterator();

                        // --- DO FOR EACH SCHOOL
                        while( scaleIter.hasNext() ){
                            // --- ADD NAME TO LIST
                            scaleInfo = (SchoolGradeScale)scaleIter.next( );
                            m_scaleList.addItem( scaleInfo.getName() );                               
                        }
                        // --- ENDDO FOR EACH SCHOOL
                        m_scaleList.addActionListener( this );
                    break;
                    // --- CASE ROLE DATA
                    case DbEngine.TYPE_DATA:
                        // --- POPULATE ROLE COMBO BOX
                        m_markerTypeName.removeAllItems();
            
                        // --- DO WHILE REPRESENTATION NOT FOUND
                        for( SchoolMarkType enumVal: SchoolMarkType.values() ) {
                            // --- ADD ROLE TO GUI LIST
                            m_markerTypeName.addItem( enumVal.toString() );                               
                        }  
                        // --- ENDDO WHILE REPRESENTATION NOT FOUND 
                    break;
                    // --- CASE NO ACTION NEEDED BY FORM
                    default:
                        // --- NO ACTION NEEDED BY FORM
                    break;
                }
                // --- ENDDO CASE OF DATA TYPED       
            }
            catch(Exception e){
                 System.out.println( "SQL Problem..." + e.getMessage() );
                 eventProcessed = false;
            }
        }
        // --- ELSE 
        else {
             System.out.println( "AddSchool::handleQuery  data not handled" );
        }
        // --- ENDIF DATA HAS BEEN RETRIEVED
       
        // --- RETURN
        return( eventProcessed );
    }
    // --- END

    
  /**
     * @brief communicates commands from the schoolhouse interface 
     *
     */
    @Override
    public void handleCmd( String cmd, String cmdData, int cmdKey )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        int         totalItems;
        int         newItem;
        int         schoolId;
        JComboBox   currentList = null;
               
        // --- SET STATUS AREA
        m_appHandle.setStatus( "Command: " + cmd + ":" + cmdData );
        
        // --- IF THE COMMAND IS A FORM COMMAND
        if( cmdKey > FORM_OFFSET ){
            // --- DO CASE OF COMMAND
            switch( cmdKey ){
                case 9001:
                    m_schoolList.requestFocus();
                break;
                case 9002:
                    m_markerList.requestFocus();
                break;
                case 9003:
                    m_courseList.requestFocus();
                break;
                case 9004:
                    m_classList.requestFocus();
                break;
                case 9005:
                    m_markerTypeName.requestFocus();
                break;
                // --- CASE UP LIST
                case 9006:
                    if( m_schoolList.hasFocus() == true ){
                        currentList = m_schoolList;
                    }
                    if( m_markerList.hasFocus() == true ){
                        currentList = m_markerList;
                    }
                    if( m_courseList.hasFocus() == true ){
                        currentList = m_courseList;
                    }
                    if( m_classList.hasFocus() == true ){
                        currentList = m_classList;
                    }
                    if( m_markerTypeName.hasFocus() == true ){
                        currentList = m_markerTypeName;
                    }
                    totalItems = currentList.getItemCount();
                    newItem = currentList.getSelectedIndex();
                    newItem++;
                    if( newItem > totalItems){
                        newItem = 0;
                    }
                    currentList.setSelectedIndex(newItem);
                break;
                // --- CASE DOWN LIST
                case 9007:
                    if( m_schoolList.hasFocus() == true ){
                        currentList = m_schoolList;
                    }
                    if( m_markerList.hasFocus() == true ){
                        currentList = m_markerList;
                    }
                    if( m_courseList.hasFocus() == true ){
                        currentList = m_courseList;
                    }
                    if( m_classList.hasFocus() == true ){
                        currentList = m_classList;
                    }
                    if( m_markerTypeName.hasFocus() == true ){
                        currentList = m_markerTypeName;
                    }
                    totalItems = currentList.getItemCount();
                    newItem = currentList.getSelectedIndex();
                    newItem--;
                    if( newItem == 0){
                        newItem = totalItems;
                    }
                    currentList.setSelectedIndex( newItem );
                break;
                // --- CASE NAME
                case 9008:
                    m_markName.requestFocus();
                break;
                // --- CASE ADDRESS
                case 9009:
                    m_markAssigned.requestFocus();
                break;
                case 9010:
                    m_markDue.requestFocus();
                break;
                case 9011:
                    m_markPercent.requestFocus();
                break;
                case 9012:
                    m_markMin.requestFocus();
                break;
                case 9013:
                    m_markMax.requestFocus();
                break;
                case 9014:
                    clearForm();
                break;
                default:
                break;
            }
            // --- ENDDO CASE OF COMMAND
        }
        // --- ELSE
        else if( cmdKey > SCHOOL_OFFSET ){
            // --- SET SCHOOL
            //schoolId = cmdKey - SCHOOL_OFFSET;
            //schoolInfo = m_appHandle.getModel().getSchool(schoolId);
            //m_schoolList.setSelectedItem( schoolInfo.getName() );
        }
        // --- ENDIF THE COMMAND IS A FORM COMMAND
    }
    // --- END
    
      
    /** 
     * @brief clear input from user
     */
    public void clearForm()
    // --- BEGIN
    {
        // --- DECLARE LOCAL VALUES
        Calendar c = new GregorianCalendar();
        
        // --- CLEAR FIELDS
        c.set( Calendar.HOUR_OF_DAY, 0 ); //anything 0 - 23
        c.set( Calendar.MINUTE, 0 );
        c.set( Calendar.SECOND, 0 );
        java.util.Date d1 = c.getTime(); //the midnight, that's the first second of the day.            m_className.setEnabled( false );
        m_markerTypeName.setSelectedItem( 0 );
        m_markName.setText( "" );
        m_markAssigned.setDate( d1 );
        m_markDue.setDate( d1 );
        m_markPercent.setText( "" );
        m_markMin.setText( "" );
        m_markMax.setText( "" );
        m_markModifier.setText( "" );
    }
    // --- END
    
      
    /** 
     * @brief clear input from user
     */
    @Override
    public void disableForm()
    // --- BEGIN
    {
        // --- DISABLE FIELDS
        m_engineEnabled = false;
        m_markerTypeName.setEnabled( false );
        m_markName.setEnabled( false );
        m_markName.setBackground( Color.GRAY );
        m_markAssigned.setEnabled( false );
        m_markDue.setEnabled( false );
        m_markPercent.setEnabled( false );
        m_markPercent.setBackground( Color.GRAY );
        m_markMin.setEnabled( false );
        m_markMin.setBackground( Color.GRAY );
        m_markMax.setEnabled( false );
        m_markMax.setBackground( Color.GRAY );
        m_markModifier.setEnabled( false );
        m_markModifier.setBackground( Color.GRAY );
    }
    // --- END
    
      
    /** 
     * @brief clear input from user
     */
    @Override
    public void enableForm()
    // --- BEGIN
    {
        // --- ENABLE FIELDS
        m_engineEnabled = true;
        m_markerTypeName.setEnabled( true );
        m_markName.setEnabled( true );
        m_markName.setBackground( Color.WHITE );
        m_markAssigned.setEnabled( true );
        m_markDue.setEnabled( true );
        m_markPercent.setEnabled( true );
        m_markPercent.setBackground( Color.WHITE );
        m_markMin.setEnabled( true );
        m_markMin.setBackground( Color.WHITE );
        m_markMax.setEnabled( true );
        m_markMax.setBackground( Color.WHITE );
        m_markModifier.setEnabled( true );
        m_markModifier.setBackground( Color.WHITE );
    }
    // --- END
    

    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    public void createWin() {
        //Create and set up the content pane.
        setOpaque(true); //content panes must be opaque

    }    
    
}
