/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package schoolhouse.client;

import java.awt.Color;
import schoolhouse.common.PersonListRenderer;
import schoolhouse.common.SchoolDbIntfc;
import schoolhouse.common.QueryThread;
import schoolhouse.common.SchoolModel;
import schoolhouse.model.SchoolPerson;
import schoolhouse.common.SchoolApp;
import schoolhouse.model.SchoolPolicy.SchoolRoles;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import schoolhouse.common.DbEngine;
import static javax.swing.SwingConstants.CENTER;
import schoolhouse.common.AppCommands;
import schoolhouse.common.HandleObjectAction;
import schoolhouse.model.SchoolStudent;

/**
 * @brief The class provides functionality to add a participant
 * 
 * @author wpeets
 */
public class PersonView extends DbEngine implements ActionListener {
    protected JComboBox          m_personList;
    protected JComboBox          m_parentList;
    protected JComboBox          m_watcherList;
    protected JTextArea          m_parentNames;
    protected JTextArea          m_watcherNames;
    protected JTextField         m_personId;
    protected JButton            m_watcherClear;
    protected JButton            m_parentClear;
    protected JTextField         m_personOrgId;
    protected JTextField         m_personLastName;
    protected JTextField         m_personFirstName;
    protected JTextField         m_personMiddleName;
    protected JTextField         m_personNickName;
    protected JComboBox          m_personRole;
    protected JTextField         m_personTitle;
    protected JTextField         m_personEmail;
    protected JTextField         m_personAddress;
    protected JTextField         m_personPhone;
    protected JTextField         m_personCellPhone;
    protected JTextField         m_userName;
              QueryThread        m_roleQuery;
    private   SchoolPerson       m_currentPerson;
    protected HandleObjectAction m_objectHandler;
    private   final static String m_newline = "\n"; 
    
    /**
     * list of parent to be added to student
     */
    protected ArrayList <SchoolPerson> m_studentParentList;

    /**
     * list of watcher to be added to parent
     */
    protected ArrayList <SchoolPerson> m_studentWatcherList;

    /**
     * @param appHandle
     * @brief constructor
     * 
     * @author wpeets
     */
    public PersonView( SchoolApp appHandle )
    // --- BEGIN
    {
           
        // --- INITIALIZE CLASS VARIABLES
        super( appHandle );
        m_studentParentList  = new ArrayList<SchoolPerson>();
        m_studentWatcherList = new ArrayList<SchoolPerson>();
        m_currentPerson      = null;
        
        // --- CREATE VIEW
        createView();        
    }
    // --- END
    
    
    /**
     * @brief create view for adding participants
     * 
     * @author wpeets
     */
    private void createView( )
    // --- BEGIN
    {        
        // --- CREATE ADD ACTIONS
        m_personList = new JComboBox( );
        m_personList.setRenderer( new PersonListRenderer(null) );
        m_personList.addActionListener(this);
        m_personList.setActionCommand("personList");
        addRow( "People List", m_personList );

        m_personLastName = new JTextField( "Last Name");
        m_personLastName.setActionCommand("lastName");
        m_personLastName.setToolTipText("Click this button to add person");
        m_personLastName.addActionListener(this);
        m_personLastName.setHorizontalAlignment( CENTER );
        m_personLastName.setColumns( 40 );
        addRow( "Last Name", m_personLastName );

        m_personFirstName = new JTextField( "First Name");
        m_personFirstName.setActionCommand("firstName");
        m_personFirstName.setToolTipText("Click this button to add person");
        m_personFirstName.addActionListener(this);
        m_personFirstName.setHorizontalAlignment( CENTER );
        m_personFirstName.setColumns( 40 );
        addRow( "First Name", m_personFirstName );

        m_personMiddleName = new JTextField( "Middle Name");
        m_personMiddleName.setActionCommand("firstName");
        m_personMiddleName.addActionListener(this);
        m_personMiddleName.setHorizontalAlignment( CENTER );
        m_personMiddleName.setColumns( 40 );
        addRow( "Middle Name", m_personMiddleName );

        m_personNickName = new JTextField( "Nick Name");
        m_personNickName.setActionCommand("nickName");
        m_personNickName.setToolTipText("Click this button to add person");
        m_personNickName.addActionListener(this);
        m_personNickName.setHorizontalAlignment( CENTER );
        m_personNickName.setColumns( 40 );
        addRow( "Nick Name", m_personNickName );

        m_personOrgId = new JTextField( "Org. Assigned Id");
        m_personOrgId.setActionCommand("orgid");
        m_personOrgId.setToolTipText("Automatic field");
        m_personOrgId.addActionListener(this);
        m_personOrgId.setEditable( true );
        m_personOrgId.setHorizontalAlignment( CENTER );
        m_personOrgId.setColumns( 20 );
        addRow( "Org. Assigned Id", m_personOrgId );

        m_personId = new JTextField( "Id");
        m_personId.setActionCommand("personid");
        m_personId.setToolTipText("Automatic field");
        m_personId.addActionListener(this);
        m_personId.setBackground( Color.GRAY );
        m_personId.setEditable( false );
        m_personId.setHorizontalAlignment( CENTER );
        m_personId.setColumns( 3 );
        addRow( "Person Id", m_personId );

        m_personEmail = new JTextField( "Email");
        m_personEmail.setActionCommand("email");
        m_personEmail.setToolTipText("Automatic field");
        m_personEmail.addActionListener(this);
        m_personEmail.setHorizontalAlignment( CENTER );
        m_personEmail.setColumns( 40 );
        addRow( "Email", m_personEmail );

        m_personPhone = new JTextField( "Phone");
        m_personPhone.setActionCommand("phone");
        m_personPhone.setToolTipText("Automatic field");
        m_personPhone.addActionListener(this);
        m_personPhone.setHorizontalAlignment( CENTER );
        m_personPhone.setColumns( 40 );
        addRow( "Phone", m_personPhone );

        m_personCellPhone = new JTextField( "Cell Phone");
        m_personCellPhone.setActionCommand("cell_phone");
        m_personCellPhone.setToolTipText("Cell contaxt");
        m_personCellPhone.addActionListener(this);
        m_personCellPhone.setHorizontalAlignment( CENTER );
        m_personCellPhone.setColumns( 40 );
        addRow( "Cell Phone", m_personCellPhone );

        m_personRole = new JComboBox( );
        m_personRole.setActionCommand( "rolelist" );
        m_personRole.setToolTipText("Click this to enter address");
        m_personRole.addActionListener(this);
        addRow( "Role", m_personRole );
        
        // --- ADD PARENT LIST
        m_parentList = new JComboBox( );
        m_parentList.setRenderer( new PersonListRenderer(null) );
        m_parentList.addActionListener(this);
        m_parentList.setActionCommand( "parentlist" );
        addRow( "Parents Avaialble", m_parentList );

        // --- CREATE LIST AREA FOR PARENTS
        m_parentNames = new JTextArea();
        addRow( "Parent List", m_parentNames );
        m_parentNames.setColumns( 40 );
        
        m_parentClear = new JButton( "Clear" );
        m_parentClear.addActionListener( this );
        m_parentClear.setActionCommand( "parentclear" );
        addRow( "Clear List", m_parentClear );
        
        // --- ADD WATCHER LIST
        m_watcherList = new JComboBox( );
        m_watcherList.setRenderer( new PersonListRenderer(null) );
        m_watcherList.addActionListener(this);
        m_watcherList.setActionCommand( "watcherlist" );
        addRow( "Watchers Avaialble", m_watcherList );

        // --- CREATE LIST AREA FOR WATCHERS
        m_watcherNames = new JTextArea();
        addRow( "Watcher List", m_watcherNames );
        m_watcherNames.setColumns( 40 );
        
        m_watcherClear = new JButton( "Clear" );
        m_watcherClear.addActionListener( this );
        m_watcherClear.setActionCommand( "watcherclear" );
        addRow( "Clear List", m_watcherClear );
        

        m_personTitle = new JTextField( "Title" );
        m_personTitle.setActionCommand("id");
        m_personTitle.setToolTipText("Click this to enter address");
        m_personTitle.addActionListener(this);
        m_personTitle.setHorizontalAlignment( CENTER );
        m_personTitle.setColumns( 5 );
        addRow( "Title", m_personTitle );
        
        m_personAddress = new JTextField( "Address" );
        m_personAddress.setActionCommand("id");
        m_personAddress.setToolTipText("Click this to enter address");
        m_personAddress.addActionListener(this);
        m_personAddress.setHorizontalAlignment( CENTER );
        m_personAddress.setColumns( 40 );
        addRow( "Address", m_personAddress );
        
        m_userName = new JTextField( "User Name" );
        m_userName.setActionCommand( "userName" );
        m_userName.setToolTipText( "Database username for person" );
        m_userName.addActionListener(this);
        m_userName.setHorizontalAlignment( CENTER );
        m_userName.setColumns( 40 );
        addRow( "User Name", m_userName );
    }
    // --- END

    /**
     * @param objectHandler object that handles submit events
     * @brief initial action on creating action 
     */
    public void initialize( HandleObjectAction objectHandler )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolModel appModel    = m_appHandle.getModel();
        Vector      dataRequest = new Vector();
        
        // --- SAVE HANDLER
        m_objectHandler = objectHandler;
        setId( m_appHandle.getAppName() + "[PERSON]" );
        
        // --- MAKE DATABASE QUERIES NECESSARY FOR ADDING ADDING PARTICIPANT
        dataRequest.add( SchoolDbIntfc.SchoolQuery.ROLE );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.PARTICIPANT  );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.PARENT );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.WATCHER );
        appModel.getData( dataRequest );
        
        // --- CREATE INITIAL HINTS
        m_engineHints.addHint( "Person ",     1  + FORM_OFFSET );
        m_engineHints.addHint( "Parent ",     2  + FORM_OFFSET );
        m_engineHints.addHint( "Watcher ",    3  + FORM_OFFSET );
        m_engineHints.addHint( "Role",        4  + FORM_OFFSET );
        m_engineHints.addHint( "Up",          5  + FORM_OFFSET );
        m_engineHints.addHint( "Down",        6  + FORM_OFFSET );
        m_engineHints.addHint( "Last",        7  + FORM_OFFSET );
        m_engineHints.addHint( "First",       8  + FORM_OFFSET );
        m_engineHints.addHint( "Middle",      9  + FORM_OFFSET );
        m_engineHints.addHint( "Nick",        10 + FORM_OFFSET );
        m_engineHints.addHint( "Org",         11 + FORM_OFFSET );
        m_engineHints.addHint( "Email",       12 + FORM_OFFSET );
        m_engineHints.addHint( "Phone",       13 + FORM_OFFSET );
        m_engineHints.addHint( "Cell",        14 + FORM_OFFSET );
        m_engineHints.addHint( "Title",       15 + FORM_OFFSET );
        m_engineHints.addHint( "Address",     16 + FORM_OFFSET );
        m_engineHints.addHint( "User",        17 + FORM_OFFSET );
        m_engineHints.addHint( "Clear",       18 + FORM_OFFSET );
        m_engineHints.addHint( "Role",        19 + FORM_OFFSET );
        m_engineHints.addHint( "Parent",      20 + FORM_OFFSET );
        m_engineHints.addHint( "Watcher",     21 + FORM_OFFSET );
      
        // --- SEND HINTS
        m_appHandle.setActiveEngine( this );
        updateHints();
        m_appHandle.setFocus();
        
    }
    // --- END
    

    /**
     * @brief handle actions from GUI events 
     */
    @Override
    public void actionPerformed(ActionEvent e)
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        Integer      personKey   = 0;
        int          roleValue   = 0;
        int          entryStatus = 1;
        String       actionCmd   = e.getActionCommand();
        String       personName;
        SchoolRoles  currentRole;
        SchoolPerson newPerson;
        SchoolPerson personInfo;
        SchoolPerson parentInfo;
        int          index       = 0;
        System.out.println( "AddPerson::actionPerformed Cmd: " + actionCmd );
        
        try{
            // --- IF COMMAND IS SET
            if( null != actionCmd ){ 
                // --- DO CASE OF COMMAND
                switch (actionCmd) {
                // --- CASE RETURN TO MAIN DISPLAY
                case "return":
                    m_appHandle.showMainPanel();
                    break;
                // --- CASE CLEAR DISPLAY
                case "clear":
                    // --- CLEAR FIELDS
                    clearForm();
                break;
                // --- CASE ADD A PERSON TO DATA
                case "submit":
                    // --- GET ROLE TO BE ADDED
                    roleValue = m_personRole.getSelectedIndex();
                    roleValue++;
                    currentRole = SchoolRoles.fromValue( roleValue );

                    // --- IF THE ROLE IS A STUDENT
                    if( currentRole == SchoolRoles.STUDENT ){
                        // --- CREATE MODEL OBJECT
                        SchoolStudent newStudent = new SchoolStudent();
                        newPerson = (SchoolPerson)newStudent;
                        
                        // --- DO FOR EACH PARENT
                        for( index=0;index < m_studentParentList.size(); index++ ){
                            // --- ADD PERSON
                            personInfo = (SchoolPerson)m_studentParentList.get( index );

                            // --- IF PERSON EXISTS
                            if( personInfo != null ){
                                // --- ADD ADMIN TO SCHOOL
                                newStudent.addParent( personInfo );
                            } 
                            // --- ENDIF PERSON EXISTS
                        }
                        // --- ENDDO FOR EACH PARENT 
                        
                        // --- DO FOR EACH WATCHER
                        for( index=0;index < m_studentWatcherList.size(); index++ ){
                            // --- ADD PERSON
                            personInfo = (SchoolPerson)m_studentWatcherList.get( index );

                            // --- IF PERSON EXISTS
                            if( personInfo != null ){
                                // --- ADD ADMIN TO SCHOOL
                                newStudent.addWatcher( personInfo );
                            } 
                            // --- ENDIF PERSON EXISTS
                        }
                        // --- ENDDO FOR EACH WATCHER  
                    }
                    // --- ELSE
                    else {
                        // --- CREATE MODEL OBJECT
                        newPerson = new SchoolPerson();
                        
                    }
                    // --- ENIF THE ROLE IS A STUDENT
                    
                    // --- EXTRACT DATA TO BE UPDATED
                    // --- ...TBD confirm person is unique
                    newPerson.setLastName( m_personLastName.getText() );
                    newPerson.setFirstName( m_personFirstName.getText() );
                    newPerson.setMiddleName( m_personMiddleName.getText() );
                    newPerson.setNickName( m_personNickName.getText() );
                    newPerson.setRole( currentRole );
                    newPerson.setTitle( m_personTitle.getText() );
                    newPerson.setEmail( m_personEmail.getText() );
                    newPerson.setPhone( m_personPhone.getText() );
                    newPerson.setCellPhone( m_personCellPhone.getText() );
                    newPerson.setAddress( m_personAddress.getText() );
                    newPerson.setUserName( m_userName.getText() );
                    newPerson.setOrgAssigned( m_personOrgId.getText() );
                    
                    // --- IF A PERSON IS SELECTED
                    if( m_currentPerson != null ){
                        newPerson.setId( m_currentPerson.getId() );
                    }
                    // --- ENDIF A PERSON IS SELECTED
                        
                    // --- IF CLIENT IS NOT ADMIN
                    if( m_appHandle.getRole() != SchoolRoles.ADMINISTRATOR ){
                        // --- MARK ENTRY STATUS
                        entryStatus = 0;
                    }
                    // --- ENDIF CLIENT IS NOT ADMIN

                    // --- MARK DATA
                    newPerson.setUserStatus( entryStatus );
                        
                    // --- HANDLE SUBMIT EVENT
                    m_objectHandler.handleObject( newPerson );
                break;
                case "personList":
                    // --- POPULATE WITH INFO
                    m_currentPerson = (SchoolPerson)m_personList.getSelectedItem();

                    // --- IF THE DATA HAS BEEN RETRIEVED
                    if( m_currentPerson != null ){
                        // --- UPDATE SCREEN
                        personKey = m_currentPerson.getId();
                        m_personId.setText( personKey.toString() );
                        m_personLastName.setText( m_currentPerson.getLastName() );
                        m_personFirstName.setText( m_currentPerson.getFirstName() );
                        m_personMiddleName.setText( m_currentPerson.getMiddleName() );
                        m_personNickName.setText( m_currentPerson.getNickName() );
                        m_personOrgId.setText( m_currentPerson.getOrgAssignedId() );
                        currentRole = m_currentPerson.getRole();
                        m_personRole.setSelectedIndex( currentRole.getValue()-1 );
                        m_personTitle.setText( m_currentPerson.getTitle() );
                        m_personEmail.setText( m_currentPerson.getEmail() );
                        m_personPhone.setText( m_currentPerson.getPhone() );
                        m_personCellPhone.setText( m_currentPerson.getCellPhone() );
                        m_personAddress.setText( m_currentPerson.getAddress() );
                        m_userName.setText( m_currentPerson.getUserName() );
                        
                        // --- IF THE PERSON IS A STUDENT
                        if( currentRole == SchoolRoles.STUDENT ){
                            // --- GET THE PARENTS FOR THE SCHOOL
                            m_studentParentList.clear();
                            m_parentNames.setText( "" );
                            m_studentWatcherList.clear();
                            m_watcherNames.setText( "" );
                            SchoolStudent currentStudent = (SchoolStudent)m_currentPerson;
                            Iterator    parentIter   = (currentStudent.getParentList()).iterator();
                            Iterator    watcherIter   = (currentStudent.getWatcherList()).iterator();
 
                            // --- IF THE FORM ALLOWS USER INPUT
                            if( m_engineEnabled == true ){
                                // --- ENABLE
                                m_watcherList.setEnabled( true );
                                m_parentList.setEnabled( true );
                            }
                            // --- ENDIF THE FORM ALLOWS USER INPUT
                            

                            // --- IF THERE IS A PARENT
                            while( parentIter.hasNext( ) ) {
                                // --- GET DATA
                                parentInfo = (SchoolPerson)parentIter.next( );
                              //m_adminList.setSelectedItem( adminInfo );
                                personName = parentInfo.getLastName()  + ", " +  
                                            parentInfo.getFirstName() + "("  +
                                            parentInfo.getId() + ")";

                                // --- STORE ADMIN
                                m_studentParentList.add( parentInfo );
                                m_parentNames.append( personName + m_newline );                            
                            }
                            // --- ENDIF THERE IS A PARENT    
                            
                            // --- IF THERE IS A WATCHER
                            while( watcherIter.hasNext( ) ) {
                                // --- GET DATA
                                parentInfo = (SchoolPerson)watcherIter.next( );
                              //m_adminList.setSelectedItem( adminInfo );
                                personName = parentInfo.getLastName()  + ", " +  
                                            parentInfo.getFirstName() + "("  +
                                            parentInfo.getId() + ")";

                                // --- STORE WATCHER
                                m_studentWatcherList.add( parentInfo );
                                m_watcherNames.append( personName + m_newline );                            
                            }
                            // --- ENDIF THERE IS A WATCHER                               
                        }
                        // --- ELSE 
                        else{
                            // --- CLEAR LIST
                            m_parentNames.setText("");
                            m_watcherNames.setText("");
                            m_studentParentList.clear();
                            m_studentWatcherList.clear();
                            
                        }
                    }
                    // --- ENDIF THE DATA HAS BEEN RETRIEVED
                    break;
                // --- CASE PARENT SELECTED
                case "parentlist":
                    // --- GET ADMIN FOR LIST
                    personInfo = (SchoolPerson)m_parentList.getSelectedItem();
                    personName = personInfo.getLastName()  + ", " +  
                                 personInfo.getFirstName() + "("  +
                                 personInfo.getId() + ")";
                    
                    // --- STORE ADMIN
                    m_studentParentList.add( personInfo );
                    m_parentNames.append( personName + m_newline );
                break;
                // --- CASE PARENT CLEAR SELECTED
                case "parentclear":
                    // --- CLEAR PARENTS
                    m_studentParentList.clear();
                    m_parentNames.setText( "" );
                break;
                // --- CASE PARENT SELECTED
                case "watcherlist":
                    // --- GET ADMIN FOR LIST
                    personInfo = (SchoolPerson)m_watcherList.getSelectedItem();
                    personName = personInfo.getLastName()  + ", " +  
                                 personInfo.getFirstName() + "("  +
                                 personInfo.getId() + ")";
                    
                    // --- STORE ADMIN
                    m_studentWatcherList.add( personInfo );
                    m_watcherNames.append( personName + m_newline );
                break;
                // --- CASE PARENT CLEAR SELECTED
                case "watcherclear":
                    // --- CLEAR WATCHERS
                    m_studentWatcherList.clear();
                    m_watcherNames.setText( "" );
                break;
                // --- CASE ROLE OF PERSON IS SELECTED
                case "rolelist":
                    // -- RESET ON SELECTION
                    roleValue = m_personRole.getSelectedIndex();
                    roleValue++;
                    currentRole = SchoolRoles.fromValue( roleValue );
                    m_studentWatcherList.clear();
                    m_watcherNames.setText("");
                    m_studentParentList.clear();
                    m_parentNames.setText( "" );

                    // --- IF THE ROLE IS A STUDENT
                    if( currentRole == SchoolRoles.STUDENT ){
                        // --- IF THE FORM ALLOWS USER INPUT
                        if( m_engineEnabled == true ){
                            // --- ENABLE
                            m_watcherList.setEnabled( true );
                            m_parentList.setEnabled( true );
                        }
                        // --- ENDIF THE FORM ALLOWS USER INPUT
                    }
                    // --- ELSE
                    else {
                        // --- DISABLE LISTS
                        m_watcherList.setEnabled( false );
                        m_parentList.setEnabled( false );
                    }
                    // --- ENDIF THE ROLE IS A STUDENT  
                break;
                }                              
                // --- ENDDO CASE OF COMMAND
            }
            // --- ENDIF COMMAND IS SET
        }
        catch(Exception err){
            System.out.println("SQL Problem..."+err.getMessage());
        }
    }
    // --- END
    
       
    /** 
     * @
     * @return brief handle non gui generated events
     */
    @Override
    public boolean handleEvent(String currEvent) 
    // --- BEGIN
    {
        // --- DECLARE LocAL VARIABLES
        boolean     returnState = true;
        
        // --- DO CASE OF EVENT
        switch( currEvent ){
            case "Focus":
                // --- SEND VOICE FOCUS
                updateState( AppCommands.CommandValue.Focus );
            break;
            default:
            break;    
        }
        // --- ENDDO CASE OF EVENT
        
        // --- RETURN
        return( returnState );    
    }
    // --- END


    /**
     * @return 
     * @brief handle non database generated events
     * @param currEvent
     * @param data
     */
    @Override
    public boolean handleQuery( String currEvent, ResultSet data ) 
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        boolean  eventProcessed = super.handleQuery( currEvent, data );
        SchoolPerson personInfo;
        ArrayList<SchoolPerson> peopleList;

        // --- IF DATA HAS BEEN RETRIEVED
        if( eventProcessed == true ){
            try {
                // --- DO CASE OF DATA
                switch( currEvent ){
                    // --- CASE PARTICIPANT DATA IS RETRIEVED
                    case DbEngine.PEOPLE_DATA:
                        // --- POPULATE PERSON  COMBO BOX
                        m_personList.removeActionListener(this);
                        m_parentList.removeActionListener(this);
                        m_watcherList.removeActionListener(this);
                        m_personList.removeAllItems();
                        m_parentList.removeAllItems();
                        m_watcherList.removeAllItems();
                        peopleList = m_appHandle.getModel().getPeopleList();
                        Iterator peopleIter = peopleList.iterator();

                        // --- DO FOR EACH PERSON
                        while( peopleIter.hasNext() ){
                            // --- ADD NAME TO LIST
                            personInfo = (SchoolPerson)peopleIter.next( );
                            m_personList.addItem( personInfo );
                            
                            // --- IF THE PERSON IS A PARENT
                            if( personInfo.getRole() == SchoolRoles.PARENT ){
                                // --- ADD TO PARENT LIST
                                m_parentList.addItem( personInfo );
                            }
                            // --- ENDIF THE PERSON IS A PARENT
                            // --- IF THE PERSON IS A PARENT
                            if( personInfo.getRole() == SchoolRoles.WATCHER ){
                                // --- ADD TO PARENT LIST
                                m_watcherList.addItem( personInfo );
                            }
                            // --- ENDIF THE PERSON IS A PARENT
                        }
                        // --- ENDDO FOR EACH PERSON
                        
                        // --- RE-ENABLEE CALLBACK
                        m_personList.addActionListener(this);
                        m_parentList.addActionListener(this);
                        m_watcherList.addActionListener(this);
                                
                    break;
                    // --- CASE ROLE DATA
                    case DbEngine.ROLE_DATA:
                        // --- POPULATE ROLE COMBO BOX
                        m_personRole.removeAllItems();
            
                        // --- DO WHILE REPRESENTATION NOT FOUND
                        for( SchoolRoles enumVal: SchoolRoles.values() ) {
                            // --- ADD ROLE TO GUI LIST
                            m_personRole.addItem( enumVal.toString() );                               
                        }  
                        // --- ENDDO WHILE REPRESENTATION NOT FOUND 
                    break;
                    default:
                    break;
                        
                }
                // --- ENDDO DO CASE OF DATA       
            }
            catch(Exception e){
                 System.out.println( "SQL Problem..." + e.getMessage() );
                 eventProcessed = false;
            }
        }
        // --- ELSE 
        else {
             System.out.println( "AddPerson::handleQuery  data not handled" );
        }
        // --- ENDIF DATA HAS BEEN RETRIEVED
           
        // --- RETURN
        return( eventProcessed );
    }
    // --- END

    /**
     * @brief communicates commands from the schoolhouse interface 
     *
     */
    @Override
    public void handleCmd( String cmd, String cmdData, int cmdKey )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        int         totalItems;
        int         newItem;
        int         schoolId;
        JComboBox   currentList = null;
               
        // --- SET STATUS AREA
        m_appHandle.setStatus( "Command: " + cmd + ":" + cmdData );
        
        // --- IF THE COMMAND IS A FORM COMMAND
        if( cmdKey > FORM_OFFSET ){
            // --- DO CASE OF COMMAND
            switch( cmdKey ){
                // --- CASE SCHOOL LIST
                case 9001:
                    // --- SET FOCUS ON SCHOOL LIST
                    m_personList.requestFocus();
                break;
                // --- CASE PARENT LIST
                case 9002:
                    // --- SET FOCUS ON PARENT LIST
                    m_parentList.requestFocus();
                break;
                // --- CASE WATCHER LIST
                case 9003:
                    // --- SET FOCUS ON WATCHER LIST
                    m_watcherList.requestFocus();
                break;
                // --- CASE WATCHER LIST
                case 9004:
                    // --- SET FOCUS ON WATCHER LIST
                    m_personRole.requestFocus();
                break;
                // --- CASE UP LIST
                case 9005:
                    if( m_personList.hasFocus() == true ){
                        currentList = m_personList;
                    }
                    if( m_parentList.hasFocus() == true ){
                        currentList = m_parentList;
                    }
                    if( m_watcherList.hasFocus() == true ){
                        currentList = m_watcherList;
                    }
                    if( m_personRole.hasFocus() == true ){
                        currentList = m_personRole;
                    }
                    totalItems = currentList.getItemCount();
                    newItem = currentList.getSelectedIndex();
                    newItem++;
                    if( newItem > totalItems){
                        newItem = 0;
                    }
                    currentList.setSelectedIndex(newItem);
                break;
                // --- CASE DOWN LIST
                case 9006:
                    if( m_personList.hasFocus() == true ){
                        currentList = m_personList;
                    }
                    if( m_parentList.hasFocus() == true ){
                        currentList = m_parentList;
                    }
                    if( m_watcherList.hasFocus() == true ){
                        currentList = m_watcherList;
                    }
                    if( m_personRole.hasFocus() == true ){
                        currentList = m_personRole;
                    }
                    totalItems = currentList.getItemCount();
                    newItem = currentList.getSelectedIndex();
                    newItem--;
                    if( newItem == 0){
                        newItem = totalItems;
                    }
                    currentList.setSelectedIndex( newItem );
                break;
                // --- CASE NAME
                case 9007:
                    m_personLastName.requestFocus();
                break;
                // --- CASE ADDRESS
                case 9008:
                    m_personFirstName.requestFocus();
                break;
                case 9009:
                    m_personMiddleName.requestFocus();
                break;
                case 9010:
                    m_personNickName.requestFocus();
                break;
                case 9011:
                    m_personOrgId.requestFocus();
                break;
                case 9012:
                    m_personEmail.requestFocus();
                break;
                case 9013:
                    m_personPhone.requestFocus();
                break;
                case 9014:
                    m_personCellPhone.requestFocus();
                break;
                case 9015:
                    m_personTitle.requestFocus();
                break;
                case 9016:
                    m_personAddress.requestFocus();
                break;
                case 9017:
                    m_userName.requestFocus();
                break;
                case 9018:
                    clearForm();
                break;
                default:
                break;
            }
            // --- ENDDO CASE OF COMMAND
        }
        // --- ELSE
        else if( cmdKey > SCHOOL_OFFSET ){
            // --- SET SCHOOL
            //schoolId = cmdKey - SCHOOL_OFFSET;
            //schoolInfo = m_appHandle.getModel().getSchool(schoolId);
            //m_schoolList.setSelectedItem( schoolInfo.getName() );
        }
        // --- ENDIF THE COMMAND IS A FORM COMMAND
    }
    // --- END
    
    /**
     * @brief clear input from user
     */
    public void clearForm()
    // --- BEGIN
    {
        // --- DECLARE LOCAL VALUES
        int studentRoleVale = ((SchoolRoles.STUDENT).getValue()) - 1;
        
        // --- CLEAR FIELDS
        m_personId.setText( "" );
        m_personOrgId.setText( "" );
        m_personLastName.setText( "" );
        m_personFirstName.setText( "" );
        m_personMiddleName.setText( "" );
        m_personNickName.setText( "" );
        m_personEmail.setText( "" );
        m_personPhone.setText( "" );
        m_personCellPhone.setText( "" );
        m_personRole.setSelectedIndex( studentRoleVale );
        m_personTitle.setText( "" );
        m_personAddress.setText( "" );
    }
    // --- END    

    
    /**
     * @brief disable form input from the user
     */
    public void disableForm()
    // --- BEGIN
    {        
        // --- DISALE FIELDS
        m_engineEnabled = false;
        m_parentList.setEnabled( false );
        m_watcherList.setEnabled( false );
        m_parentNames.setBackground( Color.GRAY );
        m_watcherNames.setBackground( Color.GRAY );
        m_parentClear.setEnabled( false );
        m_watcherClear.setEnabled( false );
        m_personOrgId.setText( "" );
        m_personOrgId.setBackground( Color.GRAY );
        m_personOrgId.setEditable( false );
        m_personOrgId.setEnabled( false );       
        m_personLastName.setText( "" );
        m_personLastName.setBackground( Color.GRAY );
        m_personLastName.setEditable( false );
        m_personLastName.setEnabled( false );
        m_personFirstName.setText( "" );
        m_personFirstName.setBackground( Color.GRAY );
        m_personFirstName.setEditable( false );
        m_personFirstName.setEnabled( false );
        m_personMiddleName.setText( "" );
        m_personMiddleName.setBackground( Color.GRAY );
        m_personMiddleName.setEditable( false );
        m_personMiddleName.setEnabled( false );
        m_personNickName.setText( "" );
        m_personNickName.setBackground( Color.GRAY );
        m_personNickName.setEditable( false );
        m_personNickName.setEnabled( false );
        m_personEmail.setText( "" );
        m_personEmail.setBackground( Color.GRAY );
        m_personEmail.setEditable( false );
        m_personEmail.setEnabled( false );
        m_personPhone.setText( "" );
        m_personPhone.setBackground( Color.GRAY );
        m_personPhone.setEditable( false );
        m_personPhone.setEnabled( false );
        m_personCellPhone.setText( "" );
        m_personCellPhone.setBackground( Color.GRAY );
        m_personCellPhone.setEditable( false );
        m_personPhone.setEnabled( false );
        m_personRole.setEnabled( false );
        m_personRole.setBackground( Color.GRAY );
        m_personRole.setEditable( false );
        m_personRole.setEnabled( false );
        m_personTitle.setText( "" );
        m_personTitle.setBackground( Color.GRAY );
        m_personTitle.setEditable( false );
        m_personTitle.setEnabled( false );
        m_personTitle.setText( "" );
        m_personTitle.setBackground( Color.GRAY );
        m_personTitle.setEditable( false );
        m_personTitle.setEnabled( false );
        m_personAddress.setBackground( Color.GRAY );
        m_personAddress.setEditable( false );
        m_personAddress.setEnabled( false );
        m_userName.setBackground( Color.GRAY );
        m_userName.setEditable( false );
        m_userName.setEnabled( false );
    }
    // --- END    
    
    
    /**
     * @brief enable form input from the user
     */
    public void enableForm()
    // --- BEGIN
    {        
        // --- CLEAR FIELDS
        m_engineEnabled = true;
        m_parentList.setEnabled( true );
        m_watcherList.setEnabled( true );
        m_parentNames.setBackground( Color.WHITE );
        m_watcherNames.setBackground( Color.WHITE );
        m_parentClear.setEnabled( true );
        m_watcherClear.setEnabled( true );
        m_personOrgId.setBackground( Color.WHITE );
        m_personOrgId.setEditable( true );
        m_personOrgId.setEnabled( true );       
        m_personLastName.setBackground( Color.WHITE );
        m_personLastName.setEditable( true );
        m_personLastName.setEnabled( true );
        m_personFirstName.setBackground( Color.WHITE );
        m_personFirstName.setEditable( true );
        m_personFirstName.setEnabled( true );
        m_personMiddleName.setBackground( Color.WHITE );
        m_personMiddleName.setEditable( true );
        m_personMiddleName.setEnabled( true );
        m_personNickName.setBackground( Color.WHITE );
        m_personNickName.setEditable( true );
        m_personNickName.setEnabled( true );
        m_personEmail.setBackground( Color.WHITE );
        m_personEmail.setEditable( true );
        m_personEmail.setEnabled( true );
        m_personPhone.setBackground( Color.WHITE );
        m_personPhone.setEditable( true );
        m_personPhone.setEnabled( true );
        m_personCellPhone.setBackground( Color.WHITE );
        m_personCellPhone.setEditable( true );
        m_personCellPhone.setEnabled( true );
        m_personRole.setEnabled( true );
        m_personRole.setBackground( Color.WHITE );
        m_personRole.setEditable( true );
        m_personRole.setEnabled( true );
        m_personTitle.setBackground( Color.WHITE );
        m_personTitle.setEditable( true );
        m_personTitle.setEnabled( true );
        m_personAddress.setBackground( Color.WHITE );
        m_personAddress.setEditable( true );
        m_personAddress.setEnabled( true );
        m_userName.setBackground( Color.WHITE );
        m_userName.setEditable( true );
        m_userName.setEnabled( true );
    }
    // --- END    
}
