/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Template
 * and open the template in the editor.
 */

package schoolhouse.client;

import java.awt.Color;
import schoolhouse.common.PersonListRenderer;
import schoolhouse.common.SchoolDbIntfc;
import schoolhouse.common.QueryChain;
import schoolhouse.model.SchoolHouse;
import schoolhouse.common.SchoolModel;
import schoolhouse.model.SchoolPerson;
import schoolhouse.common.SchoolApp;
import schoolhouse.model.SchoolPolicy.SchoolRoles;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import schoolhouse.common.DbEngine;
import static javax.swing.SwingConstants.CENTER;
import schoolhouse.common.AppCommands;
import schoolhouse.common.HandleObjectAction;

/**
 * This class provides the form capability to add a new school
 * 
 * @author wpeets
 */
public class SchoolView extends DbEngine implements ActionListener {
    protected JComboBox          m_schoolList;
    protected JComboBox          m_adminList;
    protected JTextArea          m_adminNames;
    protected JButton            m_adminClear;
    protected JTextField         m_schoolId;
    protected JTextField         m_schoolName;
    protected JTextField         m_schoolAddress;
    protected SchoolHouse        m_currentSchool;
    protected HandleObjectAction m_objectHandler;
    private   final static String m_newline   = "\n";
                      

    /**
     * list of admins to be added to school
     */
    protected ArrayList <SchoolPerson> m_schoolAdminList;
              
    
   /**
    * constructor
    * 
    * @author wpeets
    * @param appHandle
    */
    public SchoolView( SchoolApp appHandle )
    // --- BEGIN
    {
        
        // --- INITIALIZE CLASS VARIABLES
        super( appHandle );
        m_schoolAdminList = new ArrayList<SchoolPerson>();
        m_objectHandler   = null;
        m_currentSchool   = null;
        
        // --- CREATE DISPLAY
        createView();
    }
    // --- END

    
    /**
     * The method builds a display
     * 
     * @author wpeets
     */
    private void createView(  )
    // --- BEGIN
    {
        // --- CREATE ADD ACTIONS
        m_schoolList = new JComboBox( );
        m_schoolList.addActionListener(this);
        m_schoolList.setActionCommand("schoollist");
        addRow( "School List", m_schoolList );
        
        m_schoolId = new JTextField( "Id");
        m_schoolId.setActionCommand("schoolid");
        m_schoolId.setToolTipText("Automatic field");
        m_schoolId.addActionListener(this);
        m_schoolId.setEditable( false );
        m_schoolId.setBackground( Color.gray );
        m_schoolId.setHorizontalAlignment( CENTER );
        m_schoolId.setColumns( 3 );
        addRow( "Id", m_schoolId );

        m_schoolName = new JTextField( "School Name");
        m_schoolName.setActionCommand("schoolname");
        m_schoolName.setToolTipText("Click this button to add person");
        m_schoolName.addActionListener(this);
        m_schoolName.setHorizontalAlignment( CENTER );
        m_schoolName.setColumns( 40 );
        addRow( "Name", m_schoolName );

        m_schoolAddress = new JTextField( "School Address" );
        m_schoolAddress.setActionCommand("address");
        m_schoolAddress.setToolTipText("Click this to enter address");
        m_schoolAddress.addActionListener(this);
        m_schoolAddress.setHorizontalAlignment( CENTER );
        m_schoolAddress.setColumns( 40 );
        addRow( "Address", m_schoolAddress );
        
        // --- ADD ADMIN LIST
        m_adminList = new JComboBox( );
        m_adminList.setRenderer( new PersonListRenderer(null) );
        m_adminList.addActionListener(this);
        m_adminList.setActionCommand( "adlist" );
        addRow( "Admins Avaialble", m_adminList );

        // --- CREATE LIST AREA FOR ADMiNS
        m_adminNames = new JTextArea();
        addRow( "Admin List", m_adminNames );
        m_adminNames.setColumns( 40 );
        
        m_adminClear = new JButton( "Clear" );
        m_adminClear.addActionListener( this );
        m_adminClear.setActionCommand( "adclear" );
        addRow( "Clear List", m_adminClear );
    }
    // --- END

    
    /**
     * @brief initial action on creating action 
     * 
     * This method will request all the data needed to service action
     */
    public void initialize( HandleObjectAction objectHandler )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolModel appModel    = m_appHandle.getModel();
        Vector      dataRequest = new Vector();
        
        // --- SAVE OBJECT HANDLER
        m_objectHandler = objectHandler;
        setId( m_appHandle.getAppName() + "[SCHOOL]" );
        m_hasHints = true;
        
        // --- MAKE DATABASE QUERIES NECESSARY FOR ADDING ADDING PARTICIPANT
        //dataRequest.add( SchoolDbIntfc.SchoolQuery.ROLE );
        //dataRequest.add( SchoolDbIntfc.SchoolQuery.PARTICIPANT  );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.SCHOOL );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.ADMINLIST );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.ADMININFO );
        appModel.getData( dataRequest );
        
        // --- CREATE INITIAL HINTS
        m_engineHints.addHint( "School List", 1 + FORM_OFFSET );
        m_engineHints.addHint( "Admin List",  2 + FORM_OFFSET );
        m_engineHints.addHint( "Up",          3 + FORM_OFFSET );
        m_engineHints.addHint( "Down",        4 + FORM_OFFSET );
        m_engineHints.addHint( "Name",        5 + FORM_OFFSET );
        m_engineHints.addHint( "Address",     6 + FORM_OFFSET );
        m_engineHints.addHint( "Clear",       7 + FORM_OFFSET );
        
        // --- SEND HINTS
        m_appHandle.setActiveEngine( this );
        updateHints();
        m_appHandle.setFocus();
    }
    // --- END
    
    
    /** 
     * Listens to actions created by user 
     */
    @Override
    public void actionPerformed( ActionEvent e ) 
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        String                  currentSchool;
        String                  adminName;
        String                  actionCmd     = e.getActionCommand();
        Integer                 adminId;
        SchoolHouse             schoolInfo;
        SchoolModel             schoolModel;
        SchoolPerson            adminInfo     = null;
        ArrayList<SchoolPerson> adminList;
        int                     index         = 0;
        System.out.println( "AddSchool::actionPerformed Cmd: " + actionCmd );
        
        try {
        // --- IF THE COMMAND IS SET
        if( null != actionCmd) 
            // --- DO CASE OF COMMAND
            switch (actionCmd) {
                // --- CASE OF RETURN TO PREVIOUS MENU
                case "return":
                    m_appHandle.showMainPanel();
                    break;
                // --- CASE CLEAR DISPLAY
                case "clear":
                    // --- CLEAR FIELDS
                    clearForm();
                break;
                // --- CASE ADMIN SELECTED
                case "adlist":
                    // --- GET ADMIN FOR LIST
                    adminInfo = (SchoolPerson)m_adminList.getSelectedItem();
                    adminName = adminInfo.getLastName()  + ", " +  
                                adminInfo.getFirstName() + "("  +
                                adminInfo.getId() + ")";
                    
                    // --- STORE ADMIN
                    m_schoolAdminList.add( adminInfo );
                    m_adminNames.append( adminName + m_newline );
                break;
                // --- CASE ADMIN CLEAR SELECTED
                case "adclear":
                    // --- CLEAR ADMIN
                    m_schoolAdminList.clear();
                    m_adminNames.setText( "" );
                break;
                // --- CASE ADD SCHOOL TO DATABASE
                case "submit":                    
                    // --- ADD A NEW RECORD
                    schoolInfo = new SchoolHouse( m_appHandle );
                    schoolInfo.setAdminList( m_schoolAdminList );
                    schoolInfo.setName( m_schoolName.getText() );
                    schoolInfo.setAddress( m_schoolAddress.getText() );
                    
                    // --- DO FOR EACH PARENT
                    for( index=0;index < m_schoolAdminList.size(); index++ ){
                        // --- ADD PERSON
                        adminInfo = (SchoolPerson)m_schoolAdminList.get( index );

                        // --- IF PERSON EXISTS
                        if( adminInfo != null ){
                            // --- ADD ADMIN TO SCHOOL
                            schoolInfo.addAdmin( adminInfo );
                        } 
                        // --- ENDIF PERSON EXISTS
                    }
                    // --- ENDDO FOR EACH PARENT 

                    // --- IF IT IS NOT IN THE DATABASE
                    if( m_currentSchool != null ){
                        // --- SET ID
                        schoolInfo.setId( m_currentSchool.getId() );
                    }
                    // --- ENDIF IT IS NOT IN THE DATABASE
                  
                    // --- HANDLE SUBMIT ACTION
                    m_objectHandler.handleObject( schoolInfo );
                break;
                case "schoollist":
                    // --- POPULATE WITH INFO
                    currentSchool   = (String)m_schoolList.getSelectedItem();
                    schoolModel     = m_appHandle.getModel();
                    m_currentSchool = schoolModel.getSchool( currentSchool );
                    
                    // --- IF THE DATA HAS BEEN RETRIEVED
                    if( m_currentSchool != null ){
                        // --- UPDATE SCREEN
                        Integer schId = m_currentSchool.getId();
                        m_schoolId.setText( schId.toString() );
                        m_schoolName.setText( m_currentSchool.getName() );
                        m_schoolAddress.setText( m_currentSchool.getAddress() );
                        m_adminNames.setText( "" );
                        m_schoolAdminList.clear();
                        
                        // --- GET THE ADMIN FOR THE SCHOOL
                        adminList = m_currentSchool.getAdminList();
                        Iterator    adminIter   = adminList.iterator();
        
                        // --- IF THERE IS AN AMIN
                        while( adminIter.hasNext( ) ) {
                            // --- GET DATA
                            adminInfo = (SchoolPerson)adminIter.next( );
                                                        
                            // --- CREATE FULL NAME FOR LIST
                            String adminAppend = adminInfo.getLastName()  + ", " +  
                                        adminInfo.getFirstName() + "("  +
                                        adminInfo.getId() + ")";
                    
                            // --- STORE ADMIN
                            m_adminNames.append( adminAppend + m_newline );   
                            m_schoolAdminList.add( adminInfo );
                        }
                        // --- ENDIF THERE IS AN AMIN
                        
                        // --- SET ITEM
                        // --- ... probably not needed; maybe looks better
                        m_adminList.removeActionListener(this);
                        m_adminList.setSelectedItem( adminInfo );
                        m_adminList.addActionListener(this);
                    }
                    // --- ENDIF THE DATA HAS BEEN RETRIEVED
                break;
                default:
                break;
            }
        }
        catch(Exception err){
             System.out.println("SQL Problem..."+err.getMessage());
        }        
    }
    // --- END
    
    /** 
     * Handle application events 
     * @param e
     * @return 
     */
    @Override
    public boolean handleAction(ActionEvent e) 
    // --- BGIN
    {
        // --- DECLARE LOCAL VARIABLES
        boolean eventProcessed = true;
        
        // --- IF THE RETURN BUTTON IS PRESSED
        if( "return".equals(e.getActionCommand())) {
           m_appHandle.showMainPanel();
        }
        // --- ENDIF THE RETURN BUTTON IS PRESSED
        
        // --- RETURN
        return( eventProcessed );
    }
    // --- END
    
    /**
     * @param currEvent
     * @return 
     * @brief handle non gui generated events
     */
    @Override
    public boolean handleEvent(String currEvent) 
    // --- BEGIN
    {
        // --- DECLARE LocAL VARIABLES
        boolean     returnState = true;
        
        // --- DO CASE OF EVENT
        switch( currEvent ){
            case "Focus":
                // --- SEND VOICE FOCUS
                updateState( AppCommands.CommandValue.Focus );
            break;
            default:
            break;    
        }
        // --- ENDDO CASE OF EVENT
        
        // --- RETURN
        return( returnState );    
    }
    // --- END

    
    /**
     * @param currEvent
     * @param data
     * @return 
     * @brief handle database generated events
     */
    @Override
    public boolean handleQuery( String currEvent, ResultSet data ) 
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        boolean                 eventProcessed;
        SchoolHouse             schoolInfo;
        SchoolPerson            schoolAdmin;
        String                  schoolName;
        ArrayList<SchoolHouse>  schoolList;
        ArrayList<SchoolPerson> peopleList;
        System.out.println( "SchoolView::handleQuery " + currEvent );
        
        // --- LET BASE CLASS HANDLE DATA
        eventProcessed = super.handleQuery( currEvent, data );
        
        // --- IF DATA HAS BEEN RETRIEVED
        if( eventProcessed == true ){
            try {
                // --- DO CASE OF DATA TYPE
                switch( currEvent ){
                    // --- CASE THE SCHOOL DATA IS RETRIEVED
                    case DbEngine.SCHOOL_DATA:
                        // --- POPULATE SCHOOL COMBO BOX
                        m_schoolList.removeActionListener( this );
                        m_schoolList.removeAllItems();
                        schoolList = m_appHandle.getModel().getSchoolList();
                        Iterator schoolIter = schoolList.iterator();

                        // --- DO FOR EACH SCHOOL
                        while( schoolIter.hasNext() ){
                            // --- ADD NAME TO LIST
                            schoolInfo = (SchoolHouse)schoolIter.next( );
                            schoolName = schoolInfo.getName();
                            m_schoolList.addItem( schoolName );                               
                            m_engineHints.addHint( schoolName, 
                                                   schoolInfo.getId() + SCHOOL_OFFSET );
                        }
                        // --- ENDDO FOR EACH SCHOOL
        
                        // --- REINSTATE LISTENER
                        m_schoolList.addActionListener( this );
                        
                        // --- SEND HINTS
                        updateHints();
                        
                    break;
                    // --- CASE THE SCHOOL DATA IS RETRIEVED
                    case DbEngine.ADMIN_DATA:
                        // --- POPULATE ADMIN LIST COMBO BOX
                        m_adminList.removeActionListener(this);
                        m_adminList.removeAllItems();
                        peopleList = m_appHandle.getModel().getPeopleList();
                        
                        // --- IF THERE ARE PEOPLE
                        if( peopleList != null ){
                            Iterator peopleIter = peopleList.iterator();
                   
                            // --- DO FOR EACH PERSON
                            while( peopleIter.hasNext() ){
                                // --- ADD NAME TO LIST
                                schoolAdmin = (SchoolPerson)peopleIter.next();
                                // --- IF PERSON IS AN ADMIN 
                                if( (schoolAdmin.getRole() == SchoolRoles.ADMINISTRATOR ) ||
                                    (schoolAdmin.getRole() == SchoolRoles.MANAGER) ){ 
                                    // --- ADD TO LIST
                                    m_adminList.addItem( schoolAdmin ); 
                                }
                                // --- ENDIF PERSON IS AN ADMIN                                 
                            }
                            // --- ENDDO FOR EACH PERSON
                        }
                        // --- ENDIF THERE ARE PEOPLE
                        
                        // --- SET LISTENER
                        m_adminList.addActionListener( this );
                    break;                        
                    // --- CASE NO ACTION NEEDED BY FORM
                    default:
                        // --- NO ACTION NEEDED BY FORM
                    break;
                }
                // --- ENDDO CASE OF DATA TYPED       
            }
            catch(Exception e){
                 System.out.println( "SQL Problem..." + e.getMessage() );
                 eventProcessed = false;
            }
        }
        // --- ELSE 
        else {
             System.out.println( "AddSchool::handleQuery  data not handled" );
        }
        // --- ENDIF DATA HAS BEEN RETRIEVED
       
        // --- RETURN
        return( eventProcessed );
    }
    // --- END

    /**
     * @brief communicates commands from the schoolhouse interface 
     *
     */
    @Override
    public void handleCmd( String cmd, String cmdData, int cmdKey )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolHouse schoolInfo;
        int         totalItems;
        int         newItem;
        int         schoolId;
        JComboBox   currentList = null;
                       
        // --- IF THE COMMAND IS A FORM COMMAND
        if( cmdKey > FORM_OFFSET ){
            // --- DO CASE OF COMMAND
            switch( cmdKey ){
                // --- CASE SCHOOL LIST
                case 9001:
                    // --- SET FOCUS ON SCHOOL LIST
                    m_schoolList.requestFocus();
                break;
                // --- CASE ADMIN LIST
                case 9002:
                    // --- SET FOCUS ON ADMIN LIST
                    m_adminList.requestFocus();
                break;
                // --- CASE UP LIST
                case 9003:
                    if( m_schoolList.hasFocus() == true ){
                        currentList = m_schoolList;
                    }
                    if( m_adminList.hasFocus() == true ){
                        currentList = m_adminList;
                    }
                    totalItems = currentList.getItemCount();
                    newItem = currentList.getSelectedIndex();
                    newItem++;
                    if( newItem > totalItems){
                        newItem = 0;
                    }
                    currentList.setSelectedIndex( newItem );
                break;
                // --- CASE DOWN LIST
                case 9004:
                    if( m_schoolList.hasFocus() == true ){
                        currentList = m_schoolList;
                    }
                    if( m_adminList.hasFocus() == true ){
                        currentList = m_adminList;
                    }
                    totalItems = currentList.getItemCount();
                    newItem = currentList.getSelectedIndex();
                    newItem--;
                    if( newItem == 0){
                        newItem = totalItems;
                    }
                    currentList.setSelectedIndex( newItem );
                break;
                // --- CASE NAME
                case 9005:
                    m_schoolName.requestFocus();
                break;
                // --- CASE ADDRESS
                case 9006:
                    m_schoolAddress.requestFocus();
                break;
                // --- CASE CLEAR
                case 9007:
                    clearForm();
                break;
                default:
                break;
            }                    

            // --- ENDDO CASE OF COMMAND
        }
        // --- ELSE
        else if( cmdKey > SCHOOL_OFFSET ){
            // --- SET SCHOOL
            schoolId = cmdKey - SCHOOL_OFFSET;
            schoolInfo = m_appHandle.getModel().getSchool(schoolId);
            m_schoolList.setSelectedItem( schoolInfo.getName() );
        }
        // --- ENDIF THE COMMAND IS A FORM COMMAND
    }
    // --- END
    
    
    /**
     * @brief clear input from user
     */
    public void clearForm()
    // --- BEGIN
    {
        // --- CLEAR FIELDS
        m_schoolId.setText( "" );
        m_schoolName.setText( "" );
        m_schoolAddress.setText( "" );
        m_adminNames.setText( "" );
    }
    // --- END
    
    /**
     * @brief clear input from user
     */
    public void disableForm()
    // --- BEGIN
    {
        // --- DISABLE FIELDS
        m_schoolName.setText( "" );
        m_schoolName.setEditable( false );
        m_schoolName.setBackground( Color.gray );        
        m_schoolAddress.setText( "" );
        m_schoolAddress.setEditable( false );
        m_schoolAddress.setBackground( Color.gray );        
        m_adminList.setEnabled( false );
        m_adminNames.setEnabled( false );        
        m_adminClear.setEnabled( false );
    }
    // --- END
    
    /**
     * @brief clear input from user
     */
    public void enableForm()
    // --- BEGIN
    {
        // --- ENABLE FIELDS
        m_schoolName.setEditable( true );
        m_schoolName.setBackground( Color.white );        
        m_schoolAddress.setText( "" );
        m_schoolAddress.setEditable( true );
        m_schoolAddress.setBackground( Color.white );        
        m_adminList.setEnabled( true );
        m_adminNames.setEnabled( true );        
        m_adminClear.setEnabled( true );
    }
    // --- END
    
    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    public void createWin() {
        //Create and set up the content pane.
        setOpaque(true); //content panes must be opaque

    }    
    
}
