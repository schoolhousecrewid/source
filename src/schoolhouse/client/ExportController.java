/*
 *  FILE:           ExportEngine.java
 *
 *  CLASS:          ExportEngine
 *
 *  DESCRIPTION:    This class provides the high level application
 *                  behavior for schoolhouse export actions.
 *
 *  CLASS NOTES:
 *
 *  CLASS REFERENCES:
 *
 *
 * Pattern(s)  : None
 *
 * Constraints : None
 *
 *  Assertions  : None
 *
 ******************************************************************************
 *
 *  Revision History:
 *
 *  Date        RI    Purpose
 *  ---------   ---   ------------------------------------------------------
 *  OCT-16-14   WAP   Original Release
 *
 ******************************************************************************
 */

package schoolhouse.client;

import schoolhouse.common.SchoolDbIntfc;
import schoolhouse.common.CmdPane;
import schoolhouse.common.SchoolApp;
import schoolhouse.model.SchoolPolicy;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import javax.swing.AbstractButton;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import schoolhouse.common.AppCommands;
import schoolhouse.common.DbEngine;

/**
 * @brief provide behavior for export capability
 * @author wpeets
 */
public class ExportController extends DbEngine implements ActionListener {
    protected JButton[]  m_objectList; 
              JButton    m_nextButton;
              JButton    m_prevButton;
              ExportView m_exportForm;
              CmdPane    m_cmdPane;
    
    /**
     * @brief constructor
     * @param application handle
     */
    public ExportController( SchoolApp appHandle )
    // --- BEGIN
    {
        super( appHandle );
        
        // --- DECLARE LOCAL VARIABLES
        SchoolPolicy.SchoolActions action = SchoolPolicy.SchoolActions.EXPORT;
        boolean                    objState;
        int                        count     = 0;
        GridBagConstraints         btnVal    = new GridBagConstraints();
        
        // --- INITIALIZE CLASS VARIABLE
        m_objectList = new JButton[SchoolPolicy.SchoolObjects.getSize() + SchoolPolicy.AppActions.getSize()];
        
        // --- SET LAYOUT
        setLayout( new GridBagLayout() );  
        
        // --- CREATE ADDITIONAL FORMS
        m_exportForm = new ExportView( appHandle );        
        m_cmdPane    = new CmdPane( appHandle );
        m_nextButton = new JButton( "Next" );
        m_nextButton.setVerticalTextPosition( AbstractButton.CENTER );
        m_nextButton.setHorizontalTextPosition( AbstractButton.LEADING );
        m_nextButton.setActionCommand("next");
        m_nextButton.addActionListener( m_exportForm );
        m_cmdPane.add( m_nextButton );
        m_prevButton = new JButton( "Prev" );
        m_prevButton.setVerticalTextPosition( AbstractButton.CENTER );
        m_prevButton.setHorizontalTextPosition( AbstractButton.LEADING );
        m_prevButton.setActionCommand("prev");
        m_prevButton.addActionListener( m_exportForm );
        m_cmdPane.add( m_prevButton );
        
        // --- DO FOR EACH ACTION
        for( SchoolPolicy.SchoolObjects currObject: SchoolPolicy.SchoolObjects.values() ){
            // --- IF NOT LAST ITEM
            if( currObject != SchoolPolicy.SchoolObjects.UNKNOWN ) {
                // --- CREATE BUTTON
                btnVal.gridx      = 0;
                btnVal.gridy      = count;
                btnVal.fill       = GridBagConstraints.BOTH;
                btnVal.gridheight = 1;
                btnVal.gridwidth  = 1;
                m_objectList[count] = new JButton( "Export " + currObject.name() );
                m_objectList[count].setVerticalTextPosition( AbstractButton.CENTER );
                m_objectList[count].setHorizontalTextPosition(AbstractButton.LEADING);
                m_objectList[count].setActionCommand( currObject.name() );
                m_objectList[count].addActionListener( this );
                add( m_objectList[count], btnVal );
                
                // --- GET ACTION STATE
                objState = m_appHandle.hasAccessState( action, currObject );
                
                // --- IF ROLE HAS NO ACCESS FOR ACTION
                if( objState == false ){
                    // --- DISABLE ACTION
                    m_objectList[count].setEnabled( false );
                }
                // --- ENDIF ROLE HAS NO ACCESS FOR ACTION
            }
            // --- ENDIF NOT LAST ITEM
            
            // --- INCREMENT
            count++;
        }
        // --- ENDDO FOR EACH ACTION
        
        // --- DO FOR EACH GUI ACTION
        for( SchoolPolicy.AppActions guiAction: SchoolPolicy.AppActions.values() ){
            // --- CREATE BUTTON
            btnVal.gridx      = 0;
            btnVal.gridy      = count;
            btnVal.fill       = GridBagConstraints.BOTH;
            btnVal.gridheight = 1;
            btnVal.gridwidth  = 1;
            m_objectList[count] = new JButton( guiAction.name() );
            m_objectList[count].setVerticalTextPosition( AbstractButton.CENTER );
            m_objectList[count].setHorizontalTextPosition(AbstractButton.LEADING);
            m_objectList[count].setActionCommand( guiAction.name() );
            m_objectList[count].addActionListener( this );
            add( m_objectList[count], btnVal );
            
            // --- INCREMENT
            count++;
        }
        // --- ENDDO FOR EACH GUI ACTION
    }
    // --- END
    
    /**
     * @brief initial action on creating action 
     */
    public void initialize( )
    // --- BEGIN
    {
        // --- SAVE STATE
        setId( m_appHandle.getAppName() + "[EXPORT]" );

        // --- SEND HINTS
        m_appHandle.setActiveEngine( this );
        updateHints();
    }
    // --- END
    
    /** 
     * @brief Listens to the combo box.
     * @param e current action
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        // --- DECLARE LOCAL VARIABLES
        String    currentCommand = e.getActionCommand();
        int       commandKey     = 0;
        
        // --- DO CASE OF COMMAND
        switch( currentCommand ){
            case "RETURN":
                // --- RETURN TO MAIN PANEL
                commandKey = 100;
            break;
            case "SCHOOL":
                // --- SET FORM TO ADD SCHOOL
                commandKey = 1;
            break;
            case "PERSON":
                // --- SET FORM TO ADD PERSON
                commandKey = 2;
            break;
            case "COURSE":
                // --- SET FORM TO ADD COURSE
                commandKey = 3;
            break;
            case "CLASS":
                // --- SET FORM TO ADD CLASS
                commandKey = 4;
            break;
            case "MARK":
                // --- SET FORM TO ADD MARK
                commandKey = 5;
            break;
            case "GRADE":
                // --- SET FORM TO ADD GRADE
                commandKey = 6;
            break;
            case "NOTIFICATION":
                // --- SET FORM TO ADD NOTIFICATION
                commandKey = 7;
            break;
            case "Focus":
                // --- SEND VOICE FOCUS
                updateState( AppCommands.CommandValue.Focus );
            break;
            case "EXIT":
                // --- SEND VOICE FOCUS
                m_appHandle.getActiveDash().clearRole( m_appHandle.getRole() );
                m_appHandle.closeMainPanel();
            break;
            default:
                // --- SET FORM TO DO NOTHING
                commandKey = -1;
            break;
        }
        // --- ENDDO CASE OF COMMAND
       
        // --- EXECUTE COMMAND
        handleCmd( currentCommand, null, commandKey );       
    }
    // --- END
    
    /**
     * @brief communicates commands from the schoolhouse interface 
     *
     */
    public void handleCmd( String cmd, String cmdData, int cmdKey )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        Container actionArea     = m_appHandle.getActionPane();
        Container workArea       = m_appHandle.getWorkPane();
        Container cmdArea        = m_appHandle.getCmdPane();
        JFrame    frame          = m_appHandle.getFrame();
                       
        // --- DO CASE OF COMMAND
        switch( cmdKey ){
            case 100:
                // --- RETURN TO MAIN PANEL
                m_appHandle.showMainPanel();
            break;
            case 1:
                // --- SET FORM TO PROCESS SCHOOL
                m_appHandle.setHeader( "EXPORT SCHOOL" );
                m_appHandle.setActiveEngine( m_exportForm );
                m_exportForm.initialize( SchoolDbIntfc.SchoolQuery.SCHOOL );
                workArea.removeAll();
                workArea.add( m_exportForm );
                cmdArea.removeAll( );
                cmdArea.add( m_cmdPane );
                m_appHandle.setStatus( "Export Information" );
                frame.setResizable( true );
                frame.pack();
            break;
            case 2:
                // --- SET FORM TO PROCESS PERSON
                // --- SET ADD PANE
                m_appHandle.setHeader( "EXPORT PERSON" );
                m_appHandle.setActiveEngine( m_exportForm );
                m_exportForm.initialize( SchoolDbIntfc.SchoolQuery.PARTICIPANT );
                workArea.removeAll();
                workArea.add( m_exportForm );
                cmdArea.removeAll( );
                cmdArea.add( m_cmdPane );
                m_appHandle.setStatus( "Export Information" );
                frame.setResizable( true );
                frame.pack();
            break;
            case 3:
                // --- SET FORM TO PROCESS COURSE
                m_appHandle.setHeader( "EXPORT COURSE" );
                m_appHandle.setActiveEngine( m_exportForm );
                m_exportForm.initialize( SchoolDbIntfc.SchoolQuery.COURSE );
                //actionArea.removeAll();
                workArea.removeAll();
                workArea.add( m_exportForm );
                cmdArea.removeAll( );
                cmdArea.add( m_cmdPane );
                m_appHandle.setStatus( "Export Information" );
                frame.setResizable( true );
                frame.pack();
            break;
            case 4:
                // --- SET FORM TO PROCESS CLASS
                 // --- SET ADD PANE
                m_appHandle.setHeader( "EXPORT CLASS" );
                m_appHandle.setActiveEngine( m_exportForm );
                m_exportForm.initialize( SchoolDbIntfc.SchoolQuery.CLASS );
                workArea.removeAll();
                workArea.add( m_exportForm );
                cmdArea.removeAll( );
                cmdArea.add( m_cmdPane );
                m_appHandle.setStatus( "Export Information" );
                frame.setResizable( true );
                frame.pack();
            break;
            case 5:
                // --- SET FORM TO PROCESS MARK
                // --- SET ADD PANE
                m_appHandle.setHeader( "EXPORT MARK" );
                m_appHandle.setActiveEngine( m_exportForm );
                m_exportForm.initialize( SchoolDbIntfc.SchoolQuery.MARKER );
                //actionArea.removeAll();
                workArea.removeAll();
                workArea.add( m_exportForm );
                cmdArea.removeAll( );
                cmdArea.add( m_cmdPane );
                m_appHandle.setStatus( "Export Information" );
                frame.setResizable( true );
                frame.pack();
            break;
            case 6:
                // --- SET FORM TO ADD GRADE
                // --- SET ADD PANE
                m_appHandle.setHeader( "EXPORT GRADE" );
                m_appHandle.setActiveEngine( m_exportForm );
                m_exportForm.initialize( SchoolDbIntfc.SchoolQuery.GRADE );
                workArea.removeAll();
                workArea.add( m_exportForm );
                cmdArea.removeAll( );
                cmdArea.add( m_cmdPane );
                m_appHandle.setStatus( "Export Information" );
                frame.setResizable( true );
                frame.pack();
            break;
            case 7:
                // --- SET FORM TO ADD NOTIICATION
                 // --- SET ADD PANE
                m_appHandle.setHeader( "EXPORT NOTIFICATION" );
                m_appHandle.setActiveEngine( m_exportForm );
                m_exportForm.initialize( SchoolDbIntfc.SchoolQuery.NOTIFICATION );
                workArea.removeAll();
                workArea.add( m_exportForm );
                cmdArea.removeAll( );
                cmdArea.add( m_cmdPane );
                m_appHandle.setStatus( "Export Information" );
                frame.setResizable( true );
                frame.pack();
            break;
            default:
                // --- SET FORM TO ADD PERSON
            break;
        }
        // --- ENDDO CASE OF COMMAND 
    }
    // --- END
    
    
    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    public void createWin() {
        //Create and set up the content pane.
        setOpaque(true); //content panes must be opaque

    }    
    
}
