/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package schoolhouse.client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.Vector;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import schoolhouse.common.SchoolApp;
import schoolhouse.model.SchoolGradeRank;
import schoolhouse.model.SchoolGradeScale;

/**
 *
 * @author wpeets
 */
public class ScaleDialog extends JDialog implements ActionListener {
    SchoolApp          m_appHandle;
    SchoolGradeScale   m_currentScaleData;
    JPanel             m_scalePane;
    JPanel             m_rankPane;
    JButton            m_closeButton;
    JLabel             m_titleLabel;
    JTable             m_table;
    // --- ...TBD maybe headers don't show unless you add it to a
    // --- ...specific container the JSCrollPane which sounds 
    // --- ...absolutely crazy; that should be documented in BIG
    // --- ... letters if that is true
    JScrollPane        m_scrollPane;
    
    ScaleDialog( SchoolApp appHandle)
    // --- BEGIN
    {
        super();
        m_appHandle = appHandle;
        
        // --- DECLARE LOCAL VARIABLES            
        JComponent newContentPane;
 
        //--- CREATE AND SETUP DIALOG
        m_scalePane = new JPanel( new BorderLayout() );
        
        // --- CREATE PANEL FOR RANKS
        m_rankPane = new JPanel();
        m_rankPane.setBackground( Color.blue );
        m_rankPane.setLayout( new BorderLayout() );
 
        // --- BUILD DISPLAY
        newContentPane = m_scalePane;
        newContentPane.setOpaque(true); //content panes must be opaque
        setContentPane( newContentPane );
        setBounds( 750, 250, 200, 400 );
        m_titleLabel  = new JLabel();
        m_closeButton = new JButton( "Close" );
        m_closeButton.addActionListener( this );
        m_scalePane.add( m_titleLabel, BorderLayout.NORTH );
        m_scalePane.add( m_closeButton, BorderLayout.SOUTH );
        m_scalePane.add( m_rankPane, BorderLayout.CENTER );
        
        
    }
    // --- END
    
    public void show( SchoolGradeScale scaleData )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolGradeRank rankData;
        int             rowCount = 0;
        JLabel          rowData;
        String[]        names = { "Grade", "Min", "Points" };
        Vector          rows = new Vector();
        Vector          colNames = new Vector();
        String          currGrade;
        Integer         currMin;
        Float           currPoints;
         
        // --- IF THE DATA IS NEW
        if( m_currentScaleData != scaleData ){
            // --- SAVE DATA
            m_currentScaleData = scaleData;
            
            // --- IF A TABLE IS PRESENT
            if( m_table != null ){
                // --- CLEAR FORM
                m_rankPane.remove( m_scrollPane );
            }
            // --- ENDIF A TABLE IS PRESENT
            
            // --- SET HEADER
            m_titleLabel.setText( scaleData.getName() );
            colNames.add( names[0] );
            colNames.add( names[1] );
            colNames.add( names[2] );
            

            // --- DO WHILE REPRESENTATION NOT FOUND
            for( SchoolGradeScale.ScalePoint enumVal: SchoolGradeScale.ScalePoint.values() ) { 
                // --- GET RANK INFO
                rankData = scaleData.getRank( enumVal );
                
                // --- IF DATA EXISTS
                if( rankData != null ) { 
                    // --- ADD  DATA
                    Vector newRow = new Vector();
                    currGrade = rankData.getName();
                    currMin = rankData.getLevel();
                    currPoints = rankData.getPoints();
                    newRow.add( currGrade );
                    newRow.add( currMin );
                    newRow.add( currPoints );
                    rows.add( newRow );
                }  
                // --- ENDIF DATA EXISTS
            }  
            // --- ENDDO WHILE REPRESENTATION NOT FOUND
            
            // --- CREATE COLUMN HEADERS
            m_table = new JTable( rows, colNames );
            m_table.setPreferredScrollableViewportSize(
                                                 new Dimension(200,200) );
            m_table.setFillsViewportHeight( true );
            //m_rankPane.add( m_table );
            
            //Create the scroll pane and add the table to it.
            m_scrollPane = new JScrollPane(m_table);
            m_rankPane.add( m_scrollPane );
             
        }
        // --- ENDIF THE DATA IS NEW
        
        // --- MAKE FORM VISIBLE
        setVisible( true );
    }
    // --- END

    @Override
    public void actionPerformed( ActionEvent e )
    // --- BEGIN
    {
        setVisible( false );
    }
    // --- END
}
