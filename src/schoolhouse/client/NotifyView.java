 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package schoolhouse.client;

import schoolhouse.common.SchoolApp;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import schoolhouse.common.AppCommands;
import schoolhouse.common.DbEngine;
import schoolhouse.common.HandleObjectAction;
import schoolhouse.common.PersonListRenderer;
import schoolhouse.model.SchoolClass;
import schoolhouse.model.SchoolCourse;
import schoolhouse.common.SchoolDbIntfc;
import schoolhouse.common.SchoolModel;
import schoolhouse.model.SchoolMark;
import schoolhouse.model.SchoolNotification;
import schoolhouse.model.SchoolNotification.SchoolContactType;
import schoolhouse.model.SchoolNotification.SchoolNotifyType;
import schoolhouse.model.SchoolPerson;
import schoolhouse.model.SchoolStudent;

/**
 *
 * @author wpeets
 */
public class NotifyView extends DbEngine implements ActionListener {
    protected JComboBox          m_studentList;
    protected JTextField         m_studentId;
    protected JComboBox          m_classList;
    protected JTextField         m_className;
    protected JComboBox          m_watcherList;
    protected JComboBox          m_notifyList;
    protected JTextField         m_notifyId;
    protected JComboBox          m_notifyType;
    protected JTextField         m_criteria;
    protected JComboBox          m_contactTypeList;
    protected JTextField         m_specialContact;
              SchoolStudent      m_currentStudent;
              SchoolClass        m_currentClass;
              SchoolNotification m_currentNotify;
              HandleObjectAction m_objectHandler;
    
    public NotifyView( SchoolApp appHandle )
    // --- BEGIN
    {
        // --- INITIALIZE CLASS VARIABLES
        super( appHandle );
        m_currentNotify = null;
        m_objectHandler = null;    
        
        // --- CREATE VIEW
        createWin();
    }
    // --- END
    
    /**
     * @brief create view for displaying notification information
     * 
     * @author wpeets
     */
    @Override
    public void createWin()
    // --- BEGIN
    {
        // --- CREATE ADD ACTIONS
        m_studentList = new JComboBox( );
        m_studentList.setRenderer( new PersonListRenderer(null) );
        m_studentList.addActionListener(this);
        m_studentList.setActionCommand("studentlist");
        addRow( "Student List", m_studentList );
        
        m_studentId = new JTextField( "Student Id" );
        m_studentId.addActionListener(this);
        m_studentId.setEditable( false );
        m_studentId.setBackground( Color.gray );
        m_studentId.setActionCommand("studentid");
        addRow( "Student Id", m_studentId );        

        m_classList = new JComboBox( );
        m_classList.setToolTipText( "Automatic field" );
        m_classList.addActionListener(this);
        m_classList.setActionCommand( "classlist" );
        addRow( "Class List", m_classList );        

        m_className = new JTextField( "Class Name" );
        m_className.addActionListener( this );
        m_className.setEditable( false );
        m_className.setBackground( Color.gray );
        m_className.setActionCommand( "classname" );
        m_className.setColumns( 50 );
        addRow( "Class Name", m_className );        

        m_notifyList = new JComboBox( );
        m_notifyList.setActionCommand( "notifylist" );
        m_notifyList.setToolTipText( "Automatic field" );
        m_notifyList.addActionListener( this );
        addRow( "Notifications", m_notifyList );        
        
        addSeparator();

        m_watcherList = new JComboBox( );
        m_watcherList.setRenderer( new PersonListRenderer(null) );
        m_watcherList.setActionCommand("watcherlist");
        m_watcherList.setToolTipText("Automatic field");
        m_watcherList.addActionListener(this);
        m_watcherList.setActionCommand("watcherlist");
        addRow( "Watcher List", m_watcherList );        
               
        m_notifyType = new JComboBox( );
        m_notifyType.setActionCommand( "typelist" );
        m_notifyType.setToolTipText("Select type of notification");
        m_notifyType.addActionListener(this);
        addRow( "Type", m_notifyType );
        
        m_criteria = new JTextField( "Grade Criteria" );
        m_criteria.setActionCommand("crieria");
        m_criteria.setToolTipText("Click this to enter criteria");
        m_criteria.addActionListener( this );
        addRow( "Criteria", m_criteria );        
               
        m_contactTypeList = new JComboBox( );
        m_contactTypeList.setActionCommand( "contacttypelist" );
        m_contactTypeList.setToolTipText("Select type of contact");
        m_contactTypeList.addItem( SchoolNotification.SchoolContactType.CELL.toString() );
        m_contactTypeList.addItem( SchoolNotification.SchoolContactType.EMAIL.toString() );                               
        m_contactTypeList.addActionListener(this);
        addRow( "Contact Type", m_contactTypeList );
        
        m_specialContact = new JTextField( "Different Contact" );
        m_specialContact.setActionCommand("contact");
        m_specialContact.setToolTipText("Enter phone or email");
        m_specialContact.addActionListener( this );
        addRow( "New Contact", m_specialContact );        
    }
    // --- END    
    
    
    /**
     * @param objectHandler object to inform of data
     * @brief initial action on creating action 
     */
    public void initialize( HandleObjectAction objectHandler )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolModel  appModel    = m_appHandle.getModel();
        Vector       dataRequest = new Vector();
        
        // --- STORE HANDLER
        m_objectHandler = objectHandler;
        setId( m_appHandle.getAppName() + "[NOTIFY]" );
        
        // --- MAKE DATABASE QUERIES NECESSARY FOR ADDING ADDING PARTICIPANT
        dataRequest.add( SchoolDbIntfc.SchoolQuery.PARTICIPANT );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.PARENT  );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.WATCHER );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.SCHOOL );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.COURSE );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.CLASS );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.STUDENT );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.MARK_TYPE );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.NOTIFICATION );
        appModel.getData( dataRequest );
        
        // --- CREATE INITIAL HINTS
        m_engineHints.addHint( "Student List",  1  + FORM_OFFSET );
        m_engineHints.addHint( "Notify List",   2  + FORM_OFFSET );
        m_engineHints.addHint( "Class List",    3  + FORM_OFFSET );
        m_engineHints.addHint( "Watcher List",  4  + FORM_OFFSET );
        m_engineHints.addHint( "Type List",     5  + FORM_OFFSET );
        m_engineHints.addHint( "Contact",       6  + FORM_OFFSET );
        m_engineHints.addHint( "Special",       7  + FORM_OFFSET );
        m_engineHints.addHint( "Up",            8  + FORM_OFFSET );
        m_engineHints.addHint( "Down",          9  + FORM_OFFSET );
        m_engineHints.addHint( "Criteria",      10 + FORM_OFFSET );
        m_engineHints.addHint( "Clear",         11 + FORM_OFFSET );
        
        // --- SEND HINTS
        m_appHandle.setActiveEngine( this );
        updateHints();
        m_appHandle.setFocus();        
    }
    // --- END
    
    
    /** Listens to the combo box. */
    public void actionPerformed(ActionEvent e) {
        // --- DECLARE LOCAL VARIABLES
        int               typeValue;
        String            currentCourse;
        String            currentMark;
        String            currentClassString;
        String            notifyString;
        Integer           currentClass;
        Integer           notifyId;
        String            actionCmd = e.getActionCommand();
        String            currentSchool;
        String            markName;
        SchoolCourse      courseInfo;
        SchoolClass       classInfo;
        SchoolMark        markInfo;
        SchoolNotifyType  selectedType;
        SchoolContactType contactType;
        Integer           typeIndex;
        Integer           contactIndex;
        SchoolNotification notifyInfo;
        Integer           studentId;
        Integer           classId;
        Date              sqlDateAssigned;
        Date              sqlDateDue;
        boolean           markState = false;        
        boolean           markExists  = false;
        SchoolPerson      watcherInfo;
        ArrayList<SchoolPerson> watcherList;
        ArrayList<SchoolPerson> parentList;
        ArrayList<SchoolNotification> notifyList;
        System.out.println( "AddNotify::actionPerformed Cmd: " + actionCmd );
        
        try{
            // --- DO CASE OF COMMAND
            switch( actionCmd ) {
                // --- CASE RETURN TO MAIN DISPLAY
                case "return":
                    m_appHandle.showMainPanel();
                break;
                // --- CASE CLEAR DISPLAY
                case "clear":
                    // --- CLEAR FIELDS
                    clearForm();
                break;
                case "submit":
                    // --- GET NOTIFICATION TYPE
                    typeIndex    = m_notifyType.getSelectedIndex();
                    contactIndex = m_contactTypeList.getSelectedIndex();
                    typeIndex++;
                    contactIndex++;
                    selectedType = SchoolNotifyType.fromValue( typeIndex );
                    contactType = SchoolContactType.fromValue( contactIndex );
                    
                    // --- SETE NOTIFY INFO
                    notifyInfo = new SchoolNotification( m_appHandle );
                    notifyInfo.setStudent( m_currentStudent );
                    notifyInfo.setWatcher( (SchoolPerson)m_watcherList.getSelectedItem() );
                    notifyInfo.setSchoolClass( m_currentClass );
                    notifyInfo.setType( selectedType );
                    notifyInfo.setContactType( contactType );
                    notifyInfo.setCriteria( m_criteria.getSelectedText() );
                    notifyInfo.setContact( m_specialContact.getSelectedText() );
                    
                    // --- IF THERE IS A MESSAGE
                    //if( m_currentNotify != null ){
                        // --- SET ID
                    //    notifyInfo.setId( m_currentNotify.getId() );
                    //}
                    // --- ENDIF THERE IS A MESSAGE
                    
                    // --- HANDLE SUBMIT
                    m_objectHandler.handleObject( notifyInfo );
            break;
            case "studentlist":
                // --- GET CURRENT STUDENT
                m_currentStudent = (SchoolStudent)m_studentList.getSelectedItem();
                studentId = m_currentStudent.getId();
                m_appHandle.setStatus( "Student(" + studentId + ")" );
                
                // --- GET CLASS LIST FOR STUDENT
                m_studentId.setText(studentId.toString() );
                m_className.setText( "" );
                populateClassList(); 
                
                // --- GET WATCHER LIST FOR STUDENT
                populateWatcherList(); 
                m_notifyList.removeAllItems();
                clearNotify();
            break;
            case "classlist":
                // --- POPULATE WITH INFO
                currentClassString = (String)m_classList.getSelectedItem();
                currentClass = new Integer( currentClassString );
                m_currentClass = m_appHandle.getModel().getClass( currentClass );
                m_className.setText( m_currentClass.getClassName() );
                populateNotifyList();
                clearNotify();
            break;
            case "notifylist":
                // --- POPULATE WITH INFO
                m_notifyList.removeActionListener( this );
                notifyString = (String)m_notifyList.getSelectedItem();
                notifyId = new Integer( notifyString );
                
                // --- IF THERE IS A NOTIfICATION
                if( notifyId > -1 ){
                    // --- SET DATA
                    m_currentNotify = m_currentStudent.getNotification( notifyId );
                    selectedType = m_currentNotify.getType();
                    contactType = m_currentNotify.getContactType();
                    typeIndex = selectedType.getValue();
                    contactIndex = contactType.getValue();
                    typeIndex--;
                    contactIndex--;
                    
                    // --- SET FORM
                    watcherInfo = m_currentNotify.getWatcher();
                    m_watcherList.setSelectedItem( watcherInfo );
                    m_notifyType.setSelectedIndex( typeIndex );
                    m_contactTypeList.setSelectedIndex( contactIndex );
                    m_notifyList.addActionListener( this );
                }
                // --- ENDIF THERE IS A NOTIfICATION
            break;
            default:
            break;
            }                              
            // --- ENDDO CASE OF COMMAND
        }
        catch(Exception err){
            System.out.println("SQL Problem..."+err.getMessage());
        }

    }

    /** 
     * @brief add the users allowed to register for notifications
     */
    public void populateWatcherList(  ) 
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        Integer                       notifyId;
        SchoolNotification            notifyInfo;
        SchoolPerson                  watcherInfo;
        ArrayList<SchoolPerson>       watcherList;
        ArrayList<SchoolPerson>       parentList;
        ArrayList<SchoolNotification> notifyList;
                
        // --- GET WATCHER LIST FOR STUDENT
        watcherList = m_currentStudent.getWatcherList();
        parentList = m_currentStudent.getParentList();
        notifyList = m_currentStudent.getNotificationList();
        m_watcherList.removeActionListener( this );
        Iterator watcherIter = watcherList.iterator();
        Iterator parentIter = parentList.iterator();
        Iterator notifyIter = notifyList.iterator();
        m_watcherList.removeAllItems();
        m_notifyList.removeAllItems();

        // --- ADD STUDENT TO LIST
        // --- ... the individual should always be able to register for notify
        m_watcherList.addItem( m_currentStudent );
        
        // --- DO FOR EACH COURSE IN THE MODEL
        while( watcherIter.hasNext( ) ) {
            // --- GET DATA
            watcherInfo = (SchoolPerson)watcherIter.next( );
            m_watcherList.addItem( watcherInfo );
        }
        // --- ENDDO FOR EACH COURSE IN THE MODEL

        // --- DO FOR EACH COURSE IN THE MODEL
        while( parentIter.hasNext( ) ) {
            // --- GET DATA
            watcherInfo = (SchoolPerson)parentIter.next( );
            m_watcherList.addItem( watcherInfo );
        }
        // --- ENDDO FOR EACH COURSE IN THE MODEL

        // --- RESET
        m_watcherList.addActionListener( this );
    }
    // --- END
    
    /** 
     * @brief add the classes of the selected student
     */
    public void populateClassList(  ) 
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        ResultSet classData;
        int       currentId = m_currentStudent.getId();
        int       studentId;
        Integer   classId;
               
        // --- HANDLE DATABASE DATA
        try {
            // --- GET CLASS DATA
            classData = (ResultSet)m_dataMap.get( DbEngine.STUDENT_DATA );
            classData.beforeFirst();
            m_classList.removeAllItems();
            m_classList.removeActionListener(this);

            // --- DO FOR EACH STUDENT NAME
            while( classData.next() ){
                // --- SAVE NAME IN SCHOOL LIST
                studentId = classData.getInt( "student_participant_id" );
                
                // --- IF THE STUDENT IS TAKING THE CLASS
                if( studentId == currentId ){
                    // --- ADD CLASS TO LIST
                    classId = classData.getInt( "class_id" );
                    m_classList.addItem( classId.toString() );
                }
                // --- ENDIF THE STUDENT IS TAKING THE CLASS
            }
            // --- ENDDO FOR EACH STUDENT NAME
            
            // --- RESET
            m_classList.addActionListener( this );
        }
        catch(Exception e){
            // --- IF THE DATA IS EMPTY
            if( e.getMessage() == "null" ){
                // --- LET IT PASS
            }
            // --- ELSE
            else {
                System.out.println( "SQL Problem..." + e.getMessage() );
            }
        }
        // --- ENDOF HANDLE DATABASE   
    }
    // --- END

    /** 
     * @brief add the classes of the selected student
     */
    public void populateNotifyList(  ) 
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        ResultSet notifyData;
        int       studentId = m_currentStudent.getId();
        int       classId = m_currentClass.getId();
        int       currentId;
        Integer                       notifyId;
        SchoolNotification            notifyInfo;
        SchoolClass                   notifyClass;
        ArrayList<SchoolNotification> notifyList;
                
        // --- GET WATCHER LIST FOR STUDENT
        notifyList = m_currentStudent.getNotificationList();
        Iterator notifyIter = notifyList.iterator();
        m_notifyList.removeActionListener( this );
        m_notifyList.removeAllItems();
               
        // --- DO FOR EACH COURSE IN THE MODEL
        while( notifyIter.hasNext( ) ) {
            // --- GET DATA
            notifyInfo = (SchoolNotification)notifyIter.next( );
            notifyClass = notifyInfo.getSchoolClass();
            
            // --- IF NOTIFICATION MATCHES CLASS
            if( notifyClass.getId() == classId ){
                // --- ADD NOTIFICATION TO CURRENT LIST
                notifyId = notifyInfo.getId();
                m_notifyList.addItem( notifyId.toString() );
            }
            // --- ENDIF NOTIFICATION MATCHES CLASS
        }
        // --- ENDDO FOR EACH COURSE IN THE MODEL

        // --- RESET
        m_notifyList.addActionListener( this );
    }
    // --- END
    
    
    /** 
     * @brief handle non gui generated events
     */
    @Override
    public boolean handleEvent(String currEvent) 
    // --- BEGIN
    {
        // --- DECLARE LocAL VARIABLES
        boolean returnState = true;
        
        // --- DO CASE OF EVENT
        switch( currEvent ){
            case "Focus":
                // --- SEND VOICE FOCUS
                updateState( AppCommands.CommandValue.Focus );
            break;
            default:
            break;    
        }
        // --- ENDDO CASE OF EVENT
        
        // --- RETURN
        return( returnState );    
    }
    // --- END

    
    
    /** 
     * @brief handle non database generated events
     */
    public boolean handleQuery( String currEvent, ResultSet data ) 
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        boolean  eventProcessed = super.handleQuery( currEvent, data );
        SchoolPerson personInfo;
        ArrayList<SchoolPerson> peopleList;
        SchoolPerson             clientUser  =  m_appHandle.getPerson();
        ArrayList<SchoolStudent> userList;
        Iterator                 studentIter;
        SchoolStudent            studentInfo;
        System.out.println( "NotifyView::handleQuery" );
        
        // --- IF DATA HAS BEEN RETRIEVED
        if( eventProcessed == true ){
            try {
                // --- DO CASE OF DATA
                switch( currEvent ){
                    // --- CASE PARTICIPANT DATA IS RETRIEVED
                    case DbEngine.PEOPLE_DATA:
                        // -- GET USER STUDENT LIST
                        System.out.println( "NotifyView::handleQuery People Data" );
                        clientUser.createStudentList( m_appHandle );
                    break;    
                    case DbEngine.PARENTS_STD:
                        // --- POPULATE PERSON COMBO BOX
                        System.out.println( "NotifyView::handleQuery List Data" );
                        m_studentList.removeActionListener(this);

                        // --- POPULATE WIDGET
                        userList    = clientUser.getStudentList();
                        studentIter = userList.iterator();
        
                        // --- DO FOR EACH COURSE IN THE MODEL
                        while( studentIter.hasNext( ) ) {
                            // --- GET DATA
                            studentInfo = (SchoolStudent)studentIter.next( );
                            System.out.println( "NotifyView::handleQuery Add: " + studentInfo.getFirstName() );
                            m_studentList.addItem( (SchoolPerson)studentInfo );
                        }
                        // --- ENDDO FOR EACH COURSE IN THE MODEL
                                
                        // --- RE-ENABLEE CALLBACK
                        m_studentList.addActionListener(this);
                    break;
                    // --- CASE ROLE DATA
                    case DbEngine.TYPE_DATA:
                        // --- POPULATE ROLE COMBO BOX
                        m_notifyType.removeAllItems();
            
                        // --- DO WHILE REPRESENTATION NOT FOUND
                        for( SchoolNotifyType enumVal: SchoolNotifyType.values() ) {
                            // --- ADD ROLE TO GUI LIST
                            m_notifyType.addItem( enumVal.toString() );                               
                        }  
                        // --- ENDDO WHILE REPRESENTATION NOT FOUND 
                    break;
                    default:
                    break;
                        
                }
                // --- ENDDO DO CASE OF DATA       
            }
            catch(Exception e){
                 System.out.println( "SQL Problem..." + e.getMessage() );
                 eventProcessed = false;
            }
        }
        // --- ELSE 
        else {
             System.out.println( "NotifyView::handleQuery  data not handled" );
        }
        // --- ENDIF DATA HAS BEEN RETRIEVED
           
        // --- RETURN
        return( eventProcessed );
    }
    // --- END
 
    /**
     * @brief clear input from user
     */
    public void clearForm()
    // --- BEGIN
    {
        // --- CLEAR FIELDS
        m_className.setText( "" );
        m_studentId.setText( "" );
        clearNotify();
    }
    // --- END
   
    /**
     * @brief clear notify specific related data
     */
    public void clearNotify()
    // --- BEGIN
    {
        // --- CLEAR FIELDS
        m_criteria.setText( "" );
        m_watcherList.setSelectedIndex( 0 );
        m_contactTypeList.setSelectedIndex( 0 );
        m_specialContact.setText( "" );
    }
    // --- END
   

    /**
     * @brief disable user input
     */
    public void disableForm()
    // --- BEGIN
    {
        // --- CLEAR FIELDS
        m_classList.setEnabled( false );
        m_watcherList.setEnabled( false );
        m_notifyType.setEnabled( false );
        m_criteria.setText( "" );
        m_criteria.setEnabled( false );
        m_criteria.setBackground(Color.GRAY);
        m_specialContact.setText( "" );
        m_specialContact.setEnabled( false );
        m_specialContact.setBackground(Color.GRAY);
        m_contactTypeList.setEnabled( false );
    }
    // --- END
   

    /**
     * @brief disable user input
     */
    public void enableForm()
    // --- BEGIN
    {
        // --- CLEAR FIELDS
        m_classList.setEnabled( true );
        m_watcherList.setEnabled( true );
        m_notifyType.setEnabled( true );
        m_contactTypeList.setEnabled( true );
        m_criteria.setEnabled( true );
        m_criteria.setBackground( Color.WHITE );
        m_specialContact.setEnabled( true );
        m_specialContact.setBackground( Color.WHITE );
    }
    // --- END
   

    /**
     * @brief communicates commands from the schoolhouse interface 
     *
     */
    @Override
    public void handleCmd( String cmd, String cmdData, int cmdKey )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        int         totalItems;
        int         newItem;
        JComboBox   currentList = null;
               
        // --- SET STATUS AREA
        m_appHandle.setStatus( "Command: " + cmd + ":" + cmdData );
        
        // --- IF THE COMMAND IS A FORM COMMAND
        if( cmdKey > FORM_OFFSET ){
            // --- DO CASE OF COMMAND
            switch( cmdKey ){
                case 9001:
                    m_studentList.requestFocus();
                break;
                case 9002:
                    m_notifyList.requestFocus();
                break;
                case 9003:
                    m_classList.requestFocus();
                break;
                case 9004:
                    m_watcherList.requestFocus();
                break;
                case 9005:
                    m_notifyType.requestFocus();
                break;
                // --- CASE UP LIST
                case 9006:
                    if( m_studentList.hasFocus() == true ){
                        currentList = m_studentList;
                    }
                    if( m_notifyList.hasFocus() == true ){
                        currentList = m_notifyList;
                    }
                    if( m_classList.hasFocus() == true ){
                        currentList = m_classList;
                    }
                    if( m_watcherList.hasFocus() == true ){
                        currentList = m_watcherList;
                    }
                    if( m_notifyType.hasFocus() == true ){
                        currentList = m_notifyType;
                    }
                    totalItems = currentList.getItemCount();
                    newItem = currentList.getSelectedIndex();
                    newItem++;
                    if( newItem > totalItems){
                        newItem = 0;
                    }
                    currentList.setSelectedIndex(newItem);
                break;
                // --- CASE DOWN LIST
                case 9007:
                    if( m_studentList.hasFocus() == true ){
                        currentList = m_studentList;
                    }
                    if( m_notifyList.hasFocus() == true ){
                        currentList = m_notifyList;
                    }
                    if( m_classList.hasFocus() == true ){
                        currentList = m_classList;
                    }
                    if( m_watcherList.hasFocus() == true ){
                        currentList = m_watcherList;
                    }
                    if( m_notifyType.hasFocus() == true ){
                        currentList = m_notifyType;
                    }
                    totalItems = currentList.getItemCount();
                    newItem = currentList.getSelectedIndex();
                    newItem--;
                    if( newItem == 0){
                        newItem = totalItems;
                    }
                    currentList.setSelectedIndex( newItem );
                break;
                // --- CASE NAME
                case 9008:
                    m_criteria.requestFocus();
                break;
                // --- CASE ADDRESS
                case 9009:
                    clearForm();
                break;
                default:
                break;
            }
            // --- ENDDO CASE OF COMMAND
        }
        // --- ELSE
        else if( cmdKey > SCHOOL_OFFSET ){
            // --- SET SCHOOL
            //schoolId = cmdKey - SCHOOL_OFFSET;
            //schoolInfo = m_appHandle.getModel().getSchool(schoolId);
            //m_schoolList.setSelectedItem( schoolInfo.getName() );
        }
        // --- ENDIF THE COMMAND IS A FORM COMMAND
    }
    // --- END
    
    
}
