/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package schoolhouse.client;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

/**
 * A custom combo box with its own renderer and editor.
 * @author wpeets
 *
 */
public class AdminViewBox extends JComboBox {

    /**
     * Customer renderer for JComboBox
     * @author www.codejava.net
     *
     */
    public class AdminItemRenderer extends JPanel implements ListCellRenderer {
        private JLabel labelItem = new JLabel();

        public AdminItemRenderer() {
                setLayout(new GridBagLayout());
                GridBagConstraints constraints = new GridBagConstraints();
                constraints.fill = GridBagConstraints.HORIZONTAL;
                constraints.weightx = 1.0;
                constraints.insets = new Insets(2, 2, 2, 2);

                labelItem.setOpaque(true);
                labelItem.setHorizontalAlignment(JLabel.LEFT);

                add( labelItem, constraints);
                setBackground(Color.LIGHT_GRAY);
        }

        @Override
        public Component getListCellRendererComponent(JList list, Object value,
                        int index, boolean isSelected, boolean cellHasFocus) {
                String[] countryItem = (String[]) value;

                // --- IF VALUE
                if( labelItem != null ){
                    // --- IF VALID STRING
                    if( countryItem[0] != null ){
                        // set country name
                        labelItem.setText(countryItem[0]);

                        if( countryItem[1] != null ){
                            // set country flag
                            labelItem.setIcon(new ImageIcon(countryItem[1]));
                        }
                        
                        if (isSelected) {
                                labelItem.setBackground(Color.BLUE);
                                labelItem.setForeground(Color.YELLOW);
                        } else {
                                labelItem.setForeground(Color.BLACK);
                                labelItem.setBackground(Color.LIGHT_GRAY);
                        }
                    }
                    // --- ENDIF VALID STRING
                }
                // --- ENDIF VALUE
                
                return this;
        }

    }

    private DefaultComboBoxModel model;

    public AdminViewBox() {
            model = new DefaultComboBoxModel();
            setModel(model);
            setRenderer(new AdminItemRenderer());
            //setEditor(new CountryItemEditor());
    }
	
    /**
     * Add an array items to this combo box.
     * Each item is an array of two String elements:
     * - first element is country name.
     * - second element is path of an image file for country flag.
     * @param items
     */
    public void addItems(String[][] items) {
            for (String[] anItem : items) {
                    model.addElement(anItem);
            }
    }
}
