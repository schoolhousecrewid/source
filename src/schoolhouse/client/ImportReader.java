/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package schoolhouse.client;

import schoolhouse.common.QueryThread;
import schoolhouse.common.SchoolApp;
import schoolhouse.common.SchoolDbIntfc.SchoolInsert;
import java.awt.Color;
import java.awt.List;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ListIterator;
import static java.util.Locale.US;
import java.util.Map;
import java.util.Vector;
import javax.swing.JLabel;
import javax.swing.JTextField;

import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
//import org.apache.commons.csv.CSVStrategy;

/**
 *
 * @author wpeets
 */
public class ImportReader extends Thread{
    private SchoolApp m_appHandle;
    private String    m_tableToWrite;
    private String    m_dataName;
    private String    m_fileToRead;
    final   String    TYPE_MARKER  = "-- Data:";
    final   String    TABLE_MARKER = "-- Table:";
    
    public ImportReader( SchoolApp appHandle, String tableToWrite, 
                         String dataName, String fileToRead )
    // --- BEGIN
    {
        // --- STORE ELEMENTS
        m_appHandle    = appHandle;
        m_tableToWrite = tableToWrite;
        m_dataName     = dataName;
        m_fileToRead   = fileToRead;
    }
    // --- END
    
    public void run()
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        ResultSetMetaData  dbMetaData;
        PrintWriter        fileHandle;
        CSVPrinter         printer;
        int                columnCount = 0;
        int                index       = 0;
        int                insertCount = 0;
      //StringBuffer       dataHeader;
        ArrayList<String>  dataHeader = new ArrayList<String>();
        int                dataType   = 0;
        String             dbText;
        Integer            dbValue;
        SchoolInsert       updateCmd = SchoolInsert.fromString( m_dataName );
        
        FileReader              fileBuffer;
        File                    fileData;
	CSVParser               parser;
        BufferedReader          fileReader;
        boolean                 headerFound = false;
        boolean                 hasData     = true;
        String                  currentLine;
        String[]                currentValues;
        String                  tableType;
        int                     markerLength = 0;
        Map<String,Integer>     headerList;
        Charset charset = Charset.forName("ISO-8859-1"); 
        String                  insertString;
        String                  writeString;
        boolean                 firstRecord = false;
        QueryThread             dbCommand;
        String                  queryMarker = "VALUES ";
        int                     markerLen   = queryMarker.length();
        String                  importQuery = updateCmd.dbQuery();
        int                     startIndex = importQuery.indexOf( '(' );
        int                     stopIndex = importQuery.indexOf( ')' );
        int                     queryIndex = importQuery.indexOf( queryMarker );
        String                  importKey;
        Integer                 fileIndex;
        Vector                  addList;
        Vector                  addLocation;
        int                     valIndex = 0;
        
        try {
            // --- GET HEADERS TO IMPORT
            String headerString = importQuery.substring( startIndex+1, stopIndex );
	    String[] headerValues = headerString.split(",");
            addList = new Vector( headerValues.length );
            addLocation = new Vector( headerValues.length );
            headerList = new HashMap<String,Integer>();
            
            // --- SET START OF UPDATE QUERY
            insertString = importQuery.substring( 0, queryIndex+markerLen );

                    // --- DO FOR EACH COLUMN HEADER
            for( String importHeader: headerValues ){
                // --- ADD COLUMN HEADER
                headerList.put( importHeader, 0 );
                addList.add( importHeader );
                insertCount++;
            }
            // --- ENDDO FOR EACH COLUMN HEADER
                        
            // --- OPEN FILE TO PARSE
            fileBuffer = new FileReader( m_fileToRead );
            parser = new CSVParser( fileBuffer, CSVFormat.MYSQL.withCommentMarker( '-' ) );
            fileReader = new BufferedReader( fileBuffer );
            currentLine = fileReader.readLine();
              
            // --- DO FOR EACH FILE HEADER LINE
            while( (currentLine != null) && (headerFound == false) ){
                // --- IF THE LINE IS A COMMENT
                if( currentLine.startsWith( "--" ) ){
                    // --- IF THE LINE INDICATES A TYPE
                    if( currentLine.startsWith( TYPE_MARKER ) ){
                        // --- GET TABLE TYPE
                        markerLength = TYPE_MARKER.length();
                        tableType = currentLine.substring( markerLength ); 
                    }
                    // --- ENDIF THE LINE INDICATES A TYPE
                       
                    // --- IF THE LINE INDICATES A TABLE
                    if( currentLine.startsWith( TABLE_MARKER) ){
                        // --- GET TABLE TYPE
                        markerLength = TABLE_MARKER.length();
                        m_tableToWrite = currentLine.substring( markerLength ); 
                    }
                    // --- ENDIF THE LINE INDICATES A TABLE
                       
                    // --- READ THE NEXT LINE
                    currentLine = fileReader.readLine();
                }
                // --- ELSE
                else {
                    headerFound = true;
                }
                // --- ENDIF THE LINE IS A COMMENT
            }
            // --- ENDDO FOR EACH FILE HEADER LINE
               
            // --- TRY TO READ HEADER
	    String[] values = currentLine.split(",");
            columnCount = 0;
                       
            // --- DO FOR EACH COLUMN HEADER IN IMPORT FILE
            for( String columnHeader: values ){
                // --- ADD COLUMN HEADER
                ListIterator iter = addList.listIterator();
                Iterator<String> keySetIterator = headerList.keySet().iterator();
                columnCount++;

                // --- DO FOR EACH ITEM THAT NEES TO BE IMPORTED
                while (iter.hasNext()) {
                    importKey = (String)iter.next();
                    if( importKey.equals( columnHeader ) ){
                        // --- ADD COLUMN HEADER
                        addLocation.add( columnCount-1 );
                    }
                }
                // --- ENDDO FOR EACH ITEM THAT NEES TO BE IMPORTED
            }
            // --- ENDDO FOR EACH COLUMN HEADER IN IMPORT FILE
                
            currentLine = fileReader.readLine();
                
            // --- DO FOR EACH FILE HEADER LINE
            while( currentLine != null ){
                values = currentLine.split(",");
            
                if( firstRecord == false ){
                    firstRecord = true;
                }
                else {
                    insertString += ",";
                }
                    
                insertString += "(";
                index = 0;
                ListIterator iter = addLocation.listIterator();
                   
                // --- DO FOR EACH ITEM THAT NEES TO BE IMPORTED
                while (iter.hasNext()) {
                    valIndex = (int)iter.next();
                    index++;
                    
                    // --- ADD VALUE
                    // --- ...TBD insertString should probably be a stringbuffer
                    insertString += "'" + values[valIndex] + "'";
                    
                    // --- IF THE COLUMN IS NOT LAST
                    if( index != insertCount ){
                        // --- THROW IN A COMMA FOR GOODNESS SAKE
                        insertString += ",";
                    }
                    // --- ENDIF THE COLUMN IS NOT LAST
                }
                // --- ENDDO FOR EACH ITEM THAT NEES TO BE IMPORTED

                // --- TERMINDATE DATA LINE
                insertString += ")";
                currentLine = fileReader.readLine();
            }
            // --- ENDDO FOR EACH FILE HEADER LINE
            
            // --- CLOSE FILE
            insertString += ";";
            fileReader.close();
            System.out.println( insertString );
            dbCommand = new QueryThread( m_appHandle );
            dbCommand.setUpdate( insertString, "insert" );
            dbCommand.start();
        }
        catch(Exception exc){
             System.out.println( "File Problem..." + exc.getMessage() );
        }
    }
    // --- END

    void printValues( int lineNumber, String[] as ) 
    {
	System.out.println("Line " + lineNumber + " has " + as.length + " values:");
	for (String s: as) {
	    System.out.println("\t|" + s + "|");
	}
	System.out.println();
    }

}
