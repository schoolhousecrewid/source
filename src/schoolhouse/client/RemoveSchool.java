/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Template
 * and open the template in the editor.
 */

package schoolhouse.client;

import schoolhouse.common.SchoolDbIntfc;
import schoolhouse.common.QueryChain;
import schoolhouse.model.SchoolHouse;
import schoolhouse.common.SchoolModel;
import schoolhouse.model.SchoolPerson;
import schoolhouse.common.SchoolApp;
import schoolhouse.model.SchoolPolicy.SchoolRoles;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import schoolhouse.common.DbEngine;
import schoolhouse.common.PersonListRenderer;
import static javax.swing.SwingConstants.CENTER;

/**
 * This class provides the form capability to add a new school
 * 
 * @author wpeets
 */
public class RemoveSchool extends DbEngine implements ActionListener {
    protected JComboBox    m_schoolList;
    protected JComboBox    m_adminList;
    protected JTextField   m_schoolId;
    protected JTextField   m_schoolName;
    protected JTextField   m_schoolAddress;
              
    
   /**
    * constructor
    * 
    * @author wpeets
    * @param appHandle
    */
    public RemoveSchool( SchoolApp appHandle )
    // --- BEGIN
    {
        // --- INITIALIZE CLASS VARIABLES
        super( appHandle );
        
        // --- CREATE DISPLAY
        createView();
    }
    // --- END

    
    /**
     * The method builds a display
     * 
     * @author wpeets
     */
    private void createView(  )
    // --- BEGIN
    {
        // --- CREATE ADD ACTIONS
        m_schoolList = new JComboBox( );
        m_schoolList.addActionListener(this);
        m_schoolList.setActionCommand("schoollist");
        addRow( "School List", m_schoolList );
        
        m_schoolId = new JTextField( "Id");
        m_schoolId.setActionCommand("schoolid");
        m_schoolId.setToolTipText("Automatic field");
        m_schoolId.addActionListener(this);
        m_schoolId.setEditable( false );
        m_schoolId.setHorizontalAlignment( CENTER );
        m_schoolId.setColumns( 3 );
        addRow( "Id", m_schoolId );

        m_schoolName = new JTextField( "School Name");
        m_schoolName.setActionCommand("schoolname");
        m_schoolName.setToolTipText("Click this button to add person");
        m_schoolName.addActionListener(this);
        m_schoolName.setHorizontalAlignment( CENTER );
        m_schoolName.setColumns( 40 );
        m_schoolName.setEditable( false );
        addRow( "Name", m_schoolName );

        m_schoolAddress = new JTextField( "School Address" );
        m_schoolAddress.setActionCommand("address");
        m_schoolAddress.setToolTipText("Click this to enter address");
        m_schoolAddress.addActionListener(this);
        m_schoolAddress.setHorizontalAlignment( CENTER );
        m_schoolAddress.setColumns( 40 );
        m_schoolAddress.setEditable( false );
        addRow( "Address", m_schoolAddress );
        
        // --- ADD ADMIN LIST
        m_adminList = new JComboBox( );
        m_adminList.setRenderer( new PersonListRenderer(null) );
        m_adminList.addActionListener(this);
        m_adminList.setActionCommand( "adlist" );
        addRow( "Admin List", m_adminList );
    }
    // --- END

    
    /**
     * @brief initial action on creating action 
     * 
     * This method will request all the data needed to service action
     */
    public void initialize()
    // --- BEGIN
    {
        // --- MAKE DATABASE QUERIES NECESSARY FOR ADDING ADDING SCHOOL
        // --- ... TBD a school must have an admin that must be added
        m_dbChain = new QueryChain( m_appHandle, true );
        m_dbChain.setHandler( m_appHandle.getModel() );
        m_dbChain.addQuery( SchoolDbIntfc.SchoolQuery.SCHOOL.dbQuery(),
                            SchoolDbIntfc.SchoolQuery.SCHOOL.dbName()  );
        m_dbChain.addQuery( SchoolDbIntfc.SchoolQuery.PARTICIPANT.dbQuery(),
                            SchoolDbIntfc.SchoolQuery.PARTICIPANT.dbName()  );
        m_dbChain.addQuery( SchoolDbIntfc.SchoolQuery.ADMINLIST.dbQuery(),
                            SchoolDbIntfc.SchoolQuery.ADMINLIST.dbName()  );
        m_dbChain.addQuery( SchoolDbIntfc.SchoolQuery.ADMININFO.dbQuery(),
                            SchoolDbIntfc.SchoolQuery.ADMININFO.dbName()  );
        m_dbChain.start(); 
        
        // --- CREATE INITIAL HINTS
        m_engineHints.addHint( "School List", 1);
        m_engineHints.addHint( "Admin List",2);
        m_engineHints.addHint( "Up",3);
        m_engineHints.addHint( "Down",4);
        m_engineHints.addHint( "Id",5);
        m_engineHints.addHint( "Name",6);
        m_engineHints.addHint( "Address",7);
        
        // --- SEND HINTS
        // --- ... TBD jeff
        
    }
    // --- END
    
    
    /** 
     * Listens to actions created by user 
     */
    @Override
    public void actionPerformed( ActionEvent e ) 
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        String                  currentSchool;
        String                  actionCmd       = e.getActionCommand();
        Integer                 adminId;
        Integer                 schoolId;
        SchoolHouse             schoolInfo;
        SchoolModel             schoolModel;
        SchoolPerson            adminInfo;
        ArrayList<SchoolPerson> adminList;
        int                     selectedItem;
        
        try {
        // --- IF THE COMMAND IS SET
        if( null != actionCmd) 
            // --- DO CASE OF COMMAND
            switch (actionCmd) {
                // --- CASE OF RETURN TO PREVIOUS MENU
                case "return":
                    m_appHandle.showMainPanel();
                    break;
                // --- CASE ADD SCHOOL TO DATABASE
                case "submit":
                    // --- ADD SCHOOL TO DATABASE IF NAME IS UNIQUE
                    schoolId = new Integer( m_schoolId.getText() );
                    schoolModel   = m_appHandle.getModel();
                    schoolModel.removeSchool( schoolId );
                    selectedItem = m_schoolList.getSelectedIndex();
                    m_schoolList.remove( selectedItem );
                    
                    break;
                case "schoollist":
                    // --- POPULATE WITH INFO
                    currentSchool = (String)m_schoolList.getSelectedItem();
                    schoolModel   = m_appHandle.getModel();
                    schoolInfo    = schoolModel.getSchool( currentSchool );
                    
                    // --- IF THE DATA HAS BEEN RETRIEVED
                    if( schoolInfo != null ){
                        // --- UPDATE SCREEN
                        Integer schId = schoolInfo.getId();
                        m_schoolId.setText( schId.toString() );
                        m_schoolName.setText( schoolInfo.getName() );
                        m_schoolAddress.setText( schoolInfo.getAddress() );
                        
                        // --- GET THE ADMIN FOR THE SCHOOL
                        adminList = schoolInfo.getAdminList();
                        Iterator    adminIter   = adminList.iterator();
        
                        // --- IF THERE IS AN AMIN
                        if( adminIter.hasNext( ) ) {
                            // --- GET DATA
                            adminInfo = (SchoolPerson)adminIter.next( );
                            m_adminList.setSelectedItem( adminInfo );
                        }
                        // --- ENDIF THERE IS AN AMIN                        
                    }
                    // --- ENDIF THE DATA HAS BEEN RETRIEVED
                break;
                default:
                break;
            }
        }
        catch(Exception err){
             System.out.println("SQL Problem..."+err.getMessage());
        }        
    }
    // --- END
    
    /** 
     * Handle application events 
     * @param e
     * @return 
     */
    @Override
    public boolean handleAction(ActionEvent e) 
    // --- BGIN
    {
        // --- DECLARE LOCAL VARIABLES
        boolean eventProcessed = true;
        
        // --- IF THE RETURN BUTTON IS PRESSED
        if( "return".equals(e.getActionCommand())) {
           m_appHandle.showMainPanel();
        }
        // --- ENDIF THE RETURN BUTTON IS PRESSED
        
        // --- RETURN
        return( eventProcessed );
    }
    // --- END
    
    /**
     * @param currEvent
     * @return 
     * @brief handle non gui generated events
     */
    @Override
    public boolean handleEvent( String currEvent )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        boolean   eventProcessed = super.handleEvent( currEvent );
 
        // --- RETURN
        return( eventProcessed );
    }
    // --- END
    
    /**
     * @param currEvent
     * @param data
     * @return 
     * @brief handle database generated events
     */
    @Override
    public boolean handleQuery( String currEvent, ResultSet data ) 
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        boolean      eventProcessed = super.handleQuery( currEvent, data );
        ResultSet    schoolData;
        Iterator     entries;
        String       dbValue;
        Integer      dbKey          = null;
        String       insertString;
        SchoolHouse  schoolInfo;
        SchoolPerson schoolAdmin;
        SchoolModel  schoolModel;
        String       schoolName;
        String       schoolAddress;
        ArrayList<SchoolHouse> schoolList;
        ArrayList<SchoolPerson> peopleList;
        
        // --- IF DATA HAS BEEN RETRIEVED
        if( eventProcessed == true ){
            try {
                // --- DO CASE OF DATA TYPE
                switch( currEvent ){
                    // --- CASE THE SCHOOL DATA IS RETRIEVED
                    case DbEngine.SCHOOL_DATA:
                        // --- POPULATE SCHOOL COMBO BOX
                        m_schoolList.removeActionListener( this );
                        m_schoolList.removeAllItems();
                        schoolList = m_appHandle.getModel().getSchoolList();
                        Iterator schoolIter = schoolList.iterator();

                        // --- DO FOR EACH SCHOOL
                        while( schoolIter.hasNext() ){
                            // --- ADD NAME TO LIST
                            schoolInfo = (SchoolHouse)schoolIter.next( );
                            m_schoolList.addItem( schoolInfo.getName() );                               
                        }
                        // --- ENDDO FOR EACH SCHOOL
                        
                        // --- REINSTATE LISTENER
                        m_schoolList.addActionListener( this );
                    break;
                    // --- CASE THE SCHOOL DATA IS RETRIEVED
                    case DbEngine.ADMIN_DATA:
                    case DbEngine.PEOPLE_DATA:
                        // --- POPULATE ADMIN LIST COMBO BOX
                        m_adminList.removeAllItems();
                        peopleList = m_appHandle.getModel().getPeopleList();
                        
                        // --- IF THERE ARE PEOPLE
                        if( peopleList != null ){
                            Iterator peopleIter = peopleList.iterator();
                   
                            // --- DO FOR EACH PERSON
                            while( peopleIter.hasNext() ){
                                // --- ADD NAME TO LIST
                                schoolAdmin = (SchoolPerson)peopleIter.next();
                                // --- IF PERSON IS AN ADMIN 
                                if( (schoolAdmin.getRole() == SchoolRoles.ADMINISTRATOR ) ||
                                    (schoolAdmin.getRole() == SchoolRoles.MANAGER) ){ 
                                    // --- ADD TO LIST
                                    m_adminList.addItem( schoolAdmin ); 
                                }
                                // --- ENDIF PERSON IS AN ADMIN                                 
                            }
                            // --- ENDDO FOR EACH PERSON
                        }
                        // --- ENDIF THERE ARE PEOPLE
                    break;                        
                    // --- CASE NO ACTION NEEDED BY FORM
                    default:
                        // --- NO ACTION NEEDED BY FORM
                    break;
                }
                // --- ENDDO CASE OF DATA TYPED       
            }
            catch(Exception e){
                 System.out.println( "SQL Problem..." + e.getMessage() );
                 eventProcessed = false;
            }
        }
        // --- ELSE 
        else {
             System.out.println( "AddSchool::handleQuery  data not handled" );
        }
        // --- ENDIF DATA HAS BEEN RETRIEVED
       
        // --- RETURN
        return( eventProcessed );
    }
    // --- END
    
    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    public void createWin() {
        //Create and set up the content pane.
        setOpaque(true); //content panes must be opaque

    }    
    
}
