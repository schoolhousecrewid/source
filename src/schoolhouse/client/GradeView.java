/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package schoolhouse.client;

import com.toedter.calendar.JDateChooser;
import schoolhouse.common.SchoolDbIntfc;
import schoolhouse.common.QueryChain;
import schoolhouse.common.SchoolApp;
import java.awt.Color;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JTextField;
import schoolhouse.common.AppCommands;
import schoolhouse.common.DbEngine;
import schoolhouse.common.HandleObjectAction;
import schoolhouse.common.PersonListRenderer;
import schoolhouse.model.SchoolClass;
import schoolhouse.model.SchoolMark;
import schoolhouse.model.SchoolMark.SchoolMarkType;
import schoolhouse.model.SchoolCourse;
import schoolhouse.model.SchoolGrade;
import schoolhouse.model.SchoolHouse;
import schoolhouse.common.SchoolModel;
import schoolhouse.model.SchoolPerson;
import schoolhouse.model.SchoolStudent;

/**
 *
 * @author wpeets
 */
public class GradeView extends DbEngine implements ActionListener {
    protected JComboBox                 m_schoolList;
    protected JComboBox                 m_courseList;
    protected JComboBox                 m_classList;
    protected JTextField                m_className;
    protected JComboBox                 m_markerList;
    protected JComboBox                 m_studentList;
    protected JTextField                m_gradeId;
    protected JDateChooser              m_gradeReturned;
    protected JTextField                m_grade;
    protected JTextField                m_gradeChange;
              ArrayList<SchoolCourse>   m_currentCourseList;
              ArrayList<SchoolClass>    m_currentClassList;
              ArrayList<SchoolMark>     m_currentMarkList;
              ArrayList<SchoolGrade>    m_currentGradeList;
              ArrayList<SchoolPerson>   m_currentStudentList;
              SchoolHouse               m_currentSchool;
              SchoolCourse              m_currentCourse;
              SchoolClass               m_currentClass;
              SchoolMark                m_currentMark;
              SchoolGrade               m_currentGrade;
              SchoolMarkType            m_currentType;
              HandleObjectAction        m_objectHandler;
              JComponent                m_currentFocus;
              Integer                   m_classId;

    public GradeView( SchoolApp appHandle )
    // --- BEGIN
    {
        // --- INITIALIZE CLASS VARIABLES
        super( appHandle );
        m_objectHandler = null;
        m_currentGrade  = null;
        
        // ---  CREATE VIEW INTERFACE
        createViews();
    }
    // --- END
              
    private void createViews( )
    // --- BEGIN
    {
        // --- CREATE ADD ACTIONS
        m_schoolList = new JComboBox( );
        m_schoolList.setActionCommand( "schoollist" );
        m_schoolList.addActionListener( this );
        addRow( "School List", m_schoolList );

        m_courseList = new JComboBox( );
        m_courseList.setActionCommand( "courselist" );
        m_courseList.addActionListener( this );
        addRow( "Course List", m_courseList );

        m_classList = new JComboBox( );
        m_classList.setActionCommand("classlist");
        m_classList.addActionListener(this);
        addRow( "Class List[Class Id]", m_classList );

        m_className = new JTextField( "Class Name");
        m_className.setActionCommand("classname");
        m_className.setToolTipText("Automatic field");
        m_className.setEditable( false );
        m_className.setBackground( Color.gray );
        addRow( "Class Id", m_className );
        
        m_markerList = new JComboBox(  );
        m_markerList.setActionCommand("marklist");
        m_markerList.setToolTipText("Marker Id");
        m_markerList.addActionListener( this );
        addRow( "Mark List", m_markerList );

        m_studentList = new JComboBox(  );
        m_studentList.setRenderer( new PersonListRenderer(null) );
        m_studentList.setActionCommand( "studentlist" );
        m_studentList.setToolTipText( "Students for class" );
        m_studentList.addActionListener(this);
        addRow( "Student", m_studentList );

        addSeparator();
           
        m_gradeId = new JTextField( );
        m_gradeId.setEditable( false );
        m_gradeId.setBackground( Color.gray );
        m_gradeId.setToolTipText( "Grade Id" );
        addRow( "Grade Id", m_gradeId );

        m_gradeReturned = new JDateChooser( );
        //m_classEnd.setActionCommand("id");
        //m_classEnd.setToolTipText("Click this to enter end date");
        //m_classEnd.addActionListener(this);
        addRow( "Date", m_gradeReturned );

        m_grade = new JTextField( "0");
        m_grade.setActionCommand("grade");
        m_grade.setToolTipText("Date Due");
        m_grade.addActionListener(this);
        addRow( "Grade", m_grade );
   
        m_gradeChange = new JTextField( "0");
        m_gradeChange.setActionCommand("gradeChange");
        m_gradeChange.setToolTipText("Changed Grade");
        m_gradeChange.addActionListener(this);
        addRow( "Grade Update", m_gradeChange );
    }
    // --- END
    
    /**
     * @brief initial action on creating action 
     */
    public void initialize( HandleObjectAction objectHandler )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolModel appModel    = m_appHandle.getModel();
        Vector      dataRequest = new Vector();
        
        // --- STORE OBJECT HANDLER
        m_objectHandler = objectHandler;
        setId( m_appHandle.getAppName() + "[GRADE]" );
        
        // --- MAKE DATABASE QUERIES NECESSARY FOR ADDING ADDING PARTICIPANT
        // --- ... notification data is not needed for view but may be needed
        // --- ... for processing
        dataRequest.add( SchoolDbIntfc.SchoolQuery.ROLE );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.PARTICIPANT  );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.SCHOOL );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.COURSE );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.RANK );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.SCALE );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.CLASS );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.EDUCATOR );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.GRADER );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.MARK_TYPE );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.MARKER );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.GRADE );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.STUDENT );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.NOTIFICATION );
        appModel.getData( dataRequest );
        
        // --- CREATE INITIAL HINTS
        m_engineHints.addHint( "School List",   1  + FORM_OFFSET );
        m_engineHints.addHint( "Course List",   2  + FORM_OFFSET );
        m_engineHints.addHint( "Class List",    3  + FORM_OFFSET );
        m_engineHints.addHint( "Mark List",     4  + FORM_OFFSET );
        m_engineHints.addHint( "Student List",  5  + FORM_OFFSET );
        m_engineHints.addHint( "Grade List",    6  + FORM_OFFSET );
        m_engineHints.addHint( "Up",            7  + FORM_OFFSET );
        m_engineHints.addHint( "Down",          8  + FORM_OFFSET );
        m_engineHints.addHint( "Grade",         9  + FORM_OFFSET );
        m_engineHints.addHint( "Change",        10 + FORM_OFFSET );
        m_engineHints.addHint( "0",             11 + FORM_OFFSET );
        m_engineHints.addHint( "1",             12 + FORM_OFFSET );
        m_engineHints.addHint( "2",             13 + FORM_OFFSET );
        m_engineHints.addHint( "3",             14 + FORM_OFFSET );
        m_engineHints.addHint( "4",             15 + FORM_OFFSET );
        m_engineHints.addHint( "5",             16 + FORM_OFFSET );
        m_engineHints.addHint( "6",             17 + FORM_OFFSET );
        m_engineHints.addHint( "7",             18 + FORM_OFFSET );
        m_engineHints.addHint( "8",             19 + FORM_OFFSET );
        m_engineHints.addHint( "9",             20 + FORM_OFFSET );
        m_engineHints.addHint( "10",            21 + FORM_OFFSET );
        m_engineHints.addHint( "15",            22 + FORM_OFFSET );
        m_engineHints.addHint( "20",            23 + FORM_OFFSET );
        m_engineHints.addHint( "25",            24 + FORM_OFFSET );
        m_engineHints.addHint( "30",            25 + FORM_OFFSET );
        m_engineHints.addHint( "35",            26 + FORM_OFFSET );
        m_engineHints.addHint( "40",            27 + FORM_OFFSET );
        m_engineHints.addHint( "45",            28 + FORM_OFFSET );
        m_engineHints.addHint( "50",            29 + FORM_OFFSET );
        m_engineHints.addHint( "55",            30 + FORM_OFFSET );
        m_engineHints.addHint( "60",            31 + FORM_OFFSET );
        m_engineHints.addHint( "65",            32 + FORM_OFFSET );
        m_engineHints.addHint( "70",            33 + FORM_OFFSET );
        m_engineHints.addHint( "75",            34 + FORM_OFFSET );
        m_engineHints.addHint( "80",            35 + FORM_OFFSET );
        m_engineHints.addHint( "85",            36 + FORM_OFFSET );
        m_engineHints.addHint( "90",            37 + FORM_OFFSET );
        m_engineHints.addHint( "95",            38 + FORM_OFFSET );
        m_engineHints.addHint( "100",           39 + FORM_OFFSET );
        
        // --- SEND HINTS
        m_appHandle.setActiveEngine( this );
        updateHints();
        m_appHandle.setFocus();
    }
    // --- END
    

    /**
     * @brief handle actions from GUI events 
     */
    public void actionPerformed(ActionEvent e) {
        // --- DECLARE LOCAL VARIABLES
        String        currentCourse;
        String        currentMark;
        String        currentClassString;
        Integer       currentClass;
        String        className;
        String        classIdString;
        SchoolStudent currentStudent;
        int           studentId;
        String        actionCmd = e.getActionCommand();
        String        currentSchool;
        String        markName;
        SchoolCourse  courseInfo;
        SchoolClass   classInfo;
        SchoolMark    markInfo;
        SchoolGrade   gradeInfo;
        SchoolPerson  studentInfo;
        java.sql.Date sqlGradeDate;
        Integer       classId;
        Integer       gradeId;       
        boolean       validState  = false;
        System.out.println( "AddGrade::actionPerformed Cmd: " + actionCmd );
        
        try {
            // --- IF THE RETURN BUTTON IS PRESSED
            if( "return".equals( actionCmd )) {
               m_appHandle.showMainPanel();
            }
            // --- IF THE RETURN BUTTON IS PRESSED
            else if( "clear".equals( actionCmd )) {
                // --- CLEAR FIELDS
                clearForm();
            }
            // --- IF THE RETURN BUTTON IS PRESSED
            else if( "submit".equals( actionCmd ) ){
                //currentPerson = (String)m_personList.getSelectedItem();
                System.out.println( "GradeView::actionPerformed add to Class" + m_currentClass.getId() );
                
                // --- EVALUATE EVENT DATA IN THE FORM
                validState = validGrade();
                
                // --- IF THE COURSE DOES NOT EXISTS
                if( validState == true ){
                    // --- ADD TO DATABASE
                    SchoolGrade newGrade = new SchoolGrade( m_appHandle );
                    newGrade.setMark( m_currentMark );
                    newGrade.setSchoolClass(m_currentClass);
                    newGrade.setStudent((SchoolStudent)m_studentList.getSelectedItem());
                    
                    // --- PUT IN TODAY'S DATE
                    // --- ...TBD maybe don't replace if user puts in one
                    Calendar calendar = Calendar.getInstance();
                    java.util.Date currentDate = calendar.getTime();
                    java.sql.Date date = new java.sql.Date(currentDate.getTime());
                    m_gradeReturned.setDate( date );
                    newGrade.setDateReturned( date );
                    Float currGrade = new Float( m_grade.getText() );
                    Float currChange = new Float( m_gradeChange.getText() );
                    newGrade.setGrade( currGrade );
                    newGrade.setGradeChange( currChange );
                    
                    // --- IF GRADE IS SET
                    if( m_currentGrade != null ){
                        // --- SET ID
                        newGrade.setId( m_currentGrade.getId() );
                    }
                    // --- ENDIF GRADE IS SET
                    
                    // --- HANDLE SUBMIT
                    m_objectHandler.handleObject( newGrade );
                }
                // --- ELSE
                else {
                    // --- WARN USERT DATA ALREADY EXIST VIA STATUS BAR
                    m_appHandle.setStatus("Info: course already exist");
                }
                // --- ENDIF THE COURSE DOES NOT EXISTS                
            }
            // --- IF THE RETURN BUTTON IS PRESSED
            else if( "schoollist".equals( actionCmd ) ){
                // --- FILL COURSE LIST FOR SCHOOL
                currentSchool = (String)m_schoolList.getSelectedItem();
                m_currentSchool = m_appHandle.getModel().getSchool( currentSchool );
                m_currentCourseList = m_currentSchool.getCourseList();
                Iterator courseIter = m_currentCourseList.iterator();
                m_courseList.removeActionListener( this );
                m_courseList.removeAllItems();
                    
                // --- DO FOR EACH COURSE IN THE SCHOOL
                while( courseIter.hasNext( ) ) {
                    // --- GET DATA
                    courseInfo = (SchoolCourse)courseIter.next( );
                    m_courseList.addItem( courseInfo.getName() );                               
                    System.out.println("Course List: " + courseInfo.getName() );

                }   
                // --- ENDDO FOR EACH COURSE IN THE SCHOOL

                // --- SET CURRENT SCHOOL
                m_courseList.addActionListener( this );
            }
            else if( "courselist".equals( actionCmd ) ){                   
                // --- CLEAR CLASS LIST
                currentCourse = (String)m_courseList.getSelectedItem();
                m_currentCourse = m_currentSchool.getCourse( currentCourse );
                m_currentClassList = m_currentCourse.getClassList();
                m_classList.removeAllItems();
                m_classList.removeActionListener( this );
                Iterator classIter = m_currentClassList.iterator();

                // --- DO FOR EACH COURSE IN THE MODEL
                while( classIter.hasNext( ) ) {
                    // --- GET DATA
                    classInfo = (SchoolClass)classIter.next( );
                    classId = classInfo.getId();
                    currentClass = new Integer( classId );
                    className = classInfo.getClassName() + "[" +
                                                 currentClass.toString()  + "]";
                    m_classList.addItem( className );
                }
                // --- ENDDO FOR EACH COURSE IN THE MODEL
                
                // --- SET CURRENT COURSE
                m_classList.addActionListener( this );
            }
            // --- ELSE
            else if( "classlist".equals( actionCmd ) ){
                // --- POPULATE WITH INFO
                currentClassString = (String)m_classList.getSelectedItem();
                classIdString = currentClassString.substring(
                                                currentClassString.indexOf("[")+1,
                                              currentClassString.indexOf("]") );
                currentClass = new Integer( classIdString );
                m_currentClass = m_currentCourse.getClass( currentClass );
                m_className.setText( classIdString );
                //m_className.setText( currentClassString.substring(
                //                                currentClassString.indexOf("[") ));
                
                // --- GET MARKS DATA
                m_currentMarkList = m_currentClass.getMarkList();
                m_currentStudentList = m_currentClass.getStudentList();
                m_markerList.removeAllItems();
                m_markerList.removeActionListener( this );
                m_studentList.removeAllItems();
                m_studentList.removeActionListener( this );
                Iterator markIter = m_currentMarkList.iterator();
                Iterator studentIter = m_currentStudentList.iterator();
            
                // --- DO FOR EACH COURSE IN THE MODEL
                while( markIter.hasNext( ) ) {
                    // --- GET DATA
                    markInfo = (SchoolMark)markIter.next( );
                    markName = markInfo.getAssignmentName();
                    m_markerList.addItem( markName );
                }
                // --- ENDDO FOR EACH COURSE IN THE MODEL
                
                // --- DO FOR EACH COURSE IN THE MODEL
                while( studentIter.hasNext( ) ) {
                    // --- GET DATA
                    studentInfo = (SchoolPerson)studentIter.next( );
                    m_studentList.addItem( studentInfo );
                }
                // --- ENDDO FOR EACH COURSE IN THE MODEL
                
                // --- SET CURRENT CLASS
                m_markerList.addActionListener( this );
                m_studentList.addActionListener( this );
            }
            // --- ELSE
            else if( "marklist".equals( actionCmd ) ){
                // --- RESET STUDENT LIST
                // --- TBD if there are any students
                m_studentList.setSelectedIndex( 0 );                
            }
            // --- ELSE
            else if( "studentlist".equals( actionCmd ) ){
                // --- POPULATE WITH INFO
                currentMark = (String)m_markerList.getSelectedItem();
                m_currentMark = m_currentClass.getMark( currentMark );
                currentStudent = (SchoolStudent)m_studentList.getSelectedItem();
                studentId = currentStudent.getId();
                
                // --- CLEAR GRADE DATA
                m_gradeId.setText( "0" );
                Calendar calendar = Calendar.getInstance();
                java.util.Date currentDate = calendar.getTime();
                java.sql.Date date = new java.sql.Date(currentDate.getTime());
                m_gradeReturned.setDate( date );
                m_grade.setText( "0" );
                m_gradeChange.setText( "-1" );

                
                // --- GET MARKS DATA
                m_currentGradeList = m_currentMark.getGradeList();
                Iterator gradeIter = m_currentGradeList.iterator();
            
                // --- DO FOR EACH COURSE IN THE MODEL
                while( gradeIter.hasNext( ) ) {
                    // --- GET DATA
                    gradeInfo = (SchoolGrade)gradeIter.next( );
                    
                    // --- IF THE GRADE MATCHES THE STUDENT SELECTED
                    if( gradeInfo.getStudent().getId() == studentId ){
                        // --- POPULATE WITH INFO
                        gradeId = gradeInfo.getId();
                        m_gradeId.setText( gradeId.toString() );
                        m_currentGrade = gradeInfo;

                        // --- SET FORM
                        m_gradeReturned.setDate( m_currentGrade.getDateReturned() );
                        Float currGrade = m_currentGrade.getGrade();
                        m_grade.setText( currGrade.toString() );
                        Float changeGrade = m_currentGrade.getGradeChange();
                        m_gradeChange.setText( changeGrade.toString() );
                    }
                    // --- ENDIF THE GRADE MATCHES THE STUDENT SELECTED
                }
                // --- ENDDO FOR EACH COURSE IN THE MODEL               
            }
            // --- ENDIF THE RETURN BUTTON IS PRESSED
        }
        catch( Exception err ){
            System.out.println("SQL Problem..."+err.getMessage());
        }        
    }
    // --- END
    
    
    /** 
     * @brief determine if the form has valid event data
     */
    public boolean validGrade()
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        boolean dataValid = true;
        
        // --- IF ANY FIELD IS BLANK
        if( m_grade.equals("")    ){
            // --- GRADE CANNOT BE ADDED
            dataValid = false;
        }    
        // --- ENDIF ANY FIELD IS BLANK
        
        // -- RETURN STATE 
        return( dataValid );       
    }
    // --- END


    /** 
     * @brief determine if the grade data exists
     */
    public boolean existsGrades()
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        boolean   dataFound = false;
       
        // -- RETURN STATE 
        return( dataFound );
        
    }
    // --- END

    /** 
     * @brief handle non gui generated events
     */
    @Override
    public boolean handleEvent(String currEvent) 
    // --- BEGIN
    {
        // --- DECLARE LocAL VARIABLES
        boolean returnState = true;
        
        // --- DO CASE OF EVENT
        switch( currEvent ){
            case "Focus":
                // --- SEND VOICE FOCUS
                updateState( AppCommands.CommandValue.Focus );
            break;
            default:
            break;    
        }
        // --- ENDDO CASE OF EVENT
        
        // --- RETURN
        return( returnState );    
    }
    // --- END

    /**
     * @param currEvent
     * @param data
     * @return 
     * @brief handle database generated events
     */
    @Override
    public boolean handleQuery( String currEvent, ResultSet data ) 
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        boolean                eventProcessed = super.handleQuery( currEvent, data );
        SchoolHouse            schoolInfo;
        ArrayList<SchoolHouse> schoolList;
        
        // --- IF DATA HAS BEEN RETRIEVED
        if( eventProcessed == true ){
            try {
                // --- DO CASE OF DATA TYPE
                switch( currEvent ){
                    // --- CASE THE SCHOOL DATA IS RETRIEVED
                    case DbEngine.SCHOOL_DATA:
                        // --- POPULATE SCHOOL COMBO BOX
                        m_schoolList.removeActionListener( this );
                        m_schoolList.removeAllItems();
                        schoolList = m_appHandle.getModel().getSchoolList();
                        Iterator schoolIter = schoolList.iterator();

                        // --- DO FOR EACH SCHOOL
                        while( schoolIter.hasNext() ){
                            // --- ADD NAME TO LIST
                            schoolInfo = (SchoolHouse)schoolIter.next( );
                            m_schoolList.addItem( schoolInfo.getName() );                               
                        }
                        // --- ENDDO FOR EACH SCHOOL
                        
                        // --- REINSTATE LISTENER
                        m_schoolList.addActionListener( this );
                    break;
                    // --- CASE NO ACTION NEEDED BY FORM
                    default:
                        // --- NO ACTION NEEDED BY FORM
                    break;
                }
                // --- ENDDO CASE OF DATA TYPED       
            }
            catch(Exception e){
                 System.out.println( "SQL Problem..." + e.getMessage() );
                 eventProcessed = false;
            }
        }
        // --- ELSE 
        else {
             System.out.println( "AddSchool::handleQuery  data not handled" );
        }
        // --- ENDIF DATA HAS BEEN RETRIEVED
       
        // --- RETURN
        return( eventProcessed );
    }
    // --- END
    
  /**
     * @brief communicates commands from the schoolhouse interface 
     *
     */
    @Override
    public void handleCmd( String cmd, String cmdData, int cmdKey )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        int         totalItems;
        int         newItem;
        int         schoolId;
        JComboBox   currentList = null;
        JTextField  currentText = null;
        HashMap     hintVec;
        String      newText;
        String      currText;
        String      hintText;
               
        // --- SET STATUS AREA
        m_appHandle.setStatus( "Command: " + cmd + ":" + cmdData );
        
        // --- IF THE COMMAND IS A FORM COMMAND
        if( cmdKey > FORM_OFFSET ){
            // --- DO CASE OF COMMAND
            switch( cmdKey ){
                case 9001:
                    m_schoolList.requestFocus();
                    m_currentFocus = m_schoolList;
                break;
                case 9002:
                    m_courseList.requestFocus();
                    m_currentFocus = m_courseList;
                break;
                case 9003:
                    m_classList.requestFocus();
                    m_currentFocus = m_classList;
                break;
                case 9004:
                    m_markerList.requestFocus();
                    m_currentFocus = m_markerList;
                break;
                case 9005:
                    m_studentList.requestFocus();
                    m_currentFocus = m_studentList;
                break;
                case 9006:
                    m_gradeId.requestFocus();
                    m_currentFocus = m_gradeId;
                break;
                // --- CASE UP LIST
                case 9007:
                    if( m_schoolList.hasFocus() == true ){
                        currentList = m_schoolList;
                    }
                    if( m_markerList.hasFocus() == true ){
                        currentList = m_markerList;
                    }
                    if( m_courseList.hasFocus() == true ){
                        currentList = m_courseList;
                    }
                    if( m_classList.hasFocus() == true ){
                        currentList = m_classList;
                    }
                    if( m_studentList.hasFocus() == true ){
                        currentList = m_studentList;
                    }
                    totalItems = currentList.getItemCount();
                    newItem = currentList.getSelectedIndex();
                    newItem++;
                    if( newItem > totalItems){
                        newItem = 0;
                    }
                    currentList.setSelectedIndex(newItem);
                break;
                // --- CASE DOWN LIST
                case 9008:
                    if( m_schoolList.hasFocus() == true ){
                        currentList = m_schoolList;
                    }
                    if( m_markerList.hasFocus() == true ){
                        currentList = m_markerList;
                    }
                    if( m_courseList.hasFocus() == true ){
                        currentList = m_courseList;
                    }
                    if( m_classList.hasFocus() == true ){
                        currentList = m_classList;
                    }
                    if( m_studentList.hasFocus() == true ){
                        currentList = m_studentList;
                    }
                    totalItems = currentList.getItemCount();
                    newItem = currentList.getSelectedIndex();
                    newItem--;
                    if( newItem == 0){
                        newItem = totalItems;
                    }
                    currentList.setSelectedIndex( newItem );
                break;
                // --- CASE NAME
                case 9009:
                    m_grade.requestFocus();
                break;
                case 9010:
                    m_gradeChange.requestFocus();
                break;
                case 9011:
                    // --- IF THE COMPONENT ACCEPTS TEXT
                    if( ( m_currentFocus == m_grade       ) || 
                        ( m_currentFocus == m_gradeChange ) ){
                        // --- ADD TEXT
                        currentText = (JTextField)m_currentFocus;
                        hintVec = m_engineHints.getHints();
                        hintText = (String)hintVec.get( cmdKey );
                        currText =  currentText.getText();
                        newText = currText + hintText;
                        currentText.setText( newText );
                    }
                    // --- ENDIF THE COMPONENT ACCEPTS TEXT
                break;
                case 9099:
                    clearForm();
                break;
                default:
                break;
            }
            // --- ENDDO CASE OF COMMAND
        }
        // --- ELSE
        else if( cmdKey > SCHOOL_OFFSET ){
            // --- SET SCHOOL
            //schoolId = cmdKey - SCHOOL_OFFSET;
            //schoolInfo = m_appHandle.getModel().getSchool(schoolId);
            //m_schoolList.setSelectedItem( schoolInfo.getName() );
        }
        // --- ENDIF THE COMMAND IS A FORM COMMAND
    }
    // --- END
    
      
    /** 
     * @brief clear user data
     */
    public void clearForm()
    // --- BEGIN
    {
        // --- DECLARE LOAL VARIABLES
        Calendar c = new GregorianCalendar();
        
        // --- CLEAR FIELDS
        c.set( Calendar.HOUR_OF_DAY, 0 ); //anything 0 - 23
        c.set( Calendar.MINUTE, 0 );
        c.set( Calendar.SECOND, 0 );
        Date d1 = c.getTime(); //the midnight, that's the first second of the day.            
        m_gradeReturned.setDate( d1 );
        m_grade.setText( "" );
        m_gradeChange.setText( "" );
    }
    // --- END
    
    /** 
     * @brief disable user data
     */
    public void disableForm()
    // --- BEGIN
    {
        // --- DISABLE FIELDS
        m_gradeReturned.setEnabled( false );
        m_grade.setEnabled( false );
        m_grade.setBackground(Color.GRAY);
        m_gradeChange.setEnabled( false );
        m_gradeChange.setBackground(Color.GRAY);
    }
    // --- END
    
 
    /** 
     * @brief enable user data
     */
    public void enableForm()
    // --- BEGIN
    {
        // --- DISABLE FIELDS
        m_gradeReturned.setEnabled( true );
        m_grade.setEnabled( true );
        m_grade.setBackground( Color.WHITE );
        m_gradeChange.setEnabled( false );
        m_gradeChange.setBackground( Color.WHITE );
    }
    // --- END

    
    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    public void createWin() {
        //Create and set up the content pane.
        setOpaque(true); //content panes must be opaque

    }    
    
}
