/*
 * File:  ClassView.java
 * Description: Provides a panel that allows for the entry of 
 *              course and class data
 *
 */

package schoolhouse.client;

import java.awt.Color;
import schoolhouse.common.PersonListRenderer;
import schoolhouse.common.SchoolDbIntfc;
import schoolhouse.common.QueryChain;
import schoolhouse.model.SchoolCourse;
import schoolhouse.model.SchoolHouse;
import schoolhouse.model.SchoolPerson;
import schoolhouse.common.SchoolApp;
import schoolhouse.model.DataObject.DataState;
import schoolhouse.model.SchoolClass;
import schoolhouse.model.SchoolPolicy.SchoolObjects;
import schoolhouse.model.SchoolPolicy.SchoolRoles;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import schoolhouse.common.DbEngine;
import com.toedter.calendar.JDateChooser;
import com.toedter.components.JSpinField;
import lu.tudor.santec.jtimechooser.JTimeChooser;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import schoolhouse.common.AppCommands;
import schoolhouse.common.HandleObjectAction;
import schoolhouse.common.SchoolDbIntfc.SchoolQuery;
import schoolhouse.model.SchoolGradeScale;
import schoolhouse.common.SchoolModel;

/**
 * 
 * @author wpeets
 * @brief This class provides the capability to add a new course and classes to the database
 * 
 */
public class ClassView extends DbEngine implements ActionListener {
    protected JComboBox               m_schoolList;
    protected JComboBox               m_courseList;
    protected JTextField              m_courseId;
    protected JTextField              m_courseOrgId;
    protected JTextField              m_courseCredits;
    protected JTextField              m_schoolId;
    protected JTextField              m_courseName;
    protected JDateChooser            m_classStart;
    protected JDateChooser            m_classEnd;
    protected JTimeChooser            m_classTime;
    protected JSpinField              m_classDuration;
    protected JComboBox               m_classList;
    protected JTextField              m_className;
    protected JComboBox               m_educatorList;
    protected JComboBox               m_graderList;
    protected JComboBox               m_studentList;
    protected JComboBox               m_scaleList;
    protected JTextArea               m_educatorNames;
    protected JTextArea               m_graderNames;
    protected JTextArea               m_studentNames;
    protected JButton                 m_educatorClear;
    protected JButton                 m_graderClear;
    protected JButton                 m_studentClear;
              ResultSet               m_schoolData;
              Vector                  m_schoolIndex;
              ArrayList<SchoolCourse> m_currentCourseList;
              SchoolClass             m_currentClass;
              ScaleDialog             m_scaleDialog;
              HandleObjectAction      m_objectHandler;
    private   final static String     m_newline = "\n"; 
              
    /**
     * list of educators to be added to class
     */
    protected ArrayList <SchoolPerson> m_classEducatorList;

    /**
     * list of graders to be added to class
     */
    protected ArrayList <SchoolPerson> m_classGraderList;
    
    /**
     * list of watcher to be added to parent
     */
    protected ArrayList <SchoolPerson> m_classStudentList;
    
    /**
     * constructor
     * 
     * @author wpeets
     */
    public ClassView( SchoolApp appHandle  )
    // --- BEGIN
    {
        super( appHandle );
        m_schoolIndex       = new Vector();
        m_currentCourseList = null;
        m_currentClass      = null;
        m_objectHandler     = null;
        m_classEducatorList = new ArrayList<SchoolPerson>();
        m_classGraderList   = new ArrayList<SchoolPerson>();
        m_classStudentList  = new ArrayList<SchoolPerson>();
        m_scaleDialog       = new ScaleDialog( m_appHandle );
        
        // --- CREATE VIEW
        createView();
    }
    // --- END
        
    /**
     * @brief create graphical components for view
     * 
     * @author wpeets
     */
    public void createView(  )
    // --- BEGIN
    {        
        // --- CREATE ADD ACTIONS
        m_schoolList = new JComboBox( );
        m_schoolList.setActionCommand( "schoollist" );
        m_schoolList.addActionListener( this );
        addRow( "School List", m_schoolList );

        m_courseList = new JComboBox( );
        m_courseList.setActionCommand( "courselist" );
        m_courseList.addActionListener( this );
        addRow( "Course List", m_courseList );

        m_courseId = new JTextField( "Course Id");
        m_courseId.setToolTipText( "Automatic field" );
        m_courseId.setBackground( Color.gray );
        m_courseId.setEditable( false );
        m_courseId.addActionListener( this );
        m_courseId.setColumns( 4 );
        addRow( "Course Id", m_courseId );

        m_courseOrgId = new JTextField( "School Course Id");
        m_courseOrgId.setToolTipText( "Automatic field" );
        m_courseOrgId.addActionListener( this );
        m_courseOrgId.setColumns( 6 );
        addRow( "School Course Id", m_courseOrgId );

        m_courseName = new JTextField( "Course Name" );
        m_courseName.setActionCommand("id");
        m_courseName.setToolTipText("Click this to enter address");
        m_courseName.addActionListener(this);
        m_courseName.setColumns( 20 );
        addRow( "Course Name", m_courseName );
        
        m_courseCredits = new JTextField( "Course Credits" );
        m_courseCredits.setActionCommand("credits");
        m_courseCredits.setToolTipText("Click this to enter credits for course");
        m_courseCredits.addActionListener(this);
        m_courseCredits.setColumns( 3 );
        addRow( "Course Credits", m_courseCredits );
        
        // --- ADD CLASS PARAMETERS
        addSeparator( );

        m_classList = new JComboBox(  );
        m_classList.setActionCommand( "classlist" );
        m_classList.setToolTipText("Class Event Id");
        m_classList.addActionListener(this);
        addRow( "Class  ID", m_classList );

        m_className = new JTextField( "Class Name" );
        m_className.setActionCommand("className");
        m_className.setToolTipText("Click this to enter class");
        m_className.addActionListener(this);
        addRow( "Class Name (Optional)", m_className );

        m_classStart = new JDateChooser();
        //m_classStartDate.setActionCommand("id");
        //m_classStart.setToolTipText("Click this to enter class start");
        //m_classStartDate.addActionListener(this);
        addRow( "Class Start Date", m_classStart );

        m_classEnd = new JDateChooser( );
        //m_classEnd.setActionCommand("id");
        //m_classEnd.setToolTipText("Click this to enter end date");
        //m_classEnd.addActionListener(this);
        addRow( "Class End Time", m_classEnd );

        m_classTime = new JTimeChooser( );
        //m_classTime.setActionCommand("id");
        //m_classTime.setToolTipText("Click this to enter time");
        //m_classTime.addActionListener(this);
        addRow( "Class Time", m_classTime );

        m_classDuration = new JSpinField( );
        //m_classDuration.setActionCommand("id");
        //m_classDuration.setToolTipText("Click this to enter address");
        //m_classDuration.addActionListener(this);
        addRow( "Class Duration", m_classDuration );

        // --- SCALE LIST
        m_scaleList = new JComboBox(  );
        m_scaleList.setActionCommand( "scalelist" );
        m_scaleList.setToolTipText( "Possible School Scales" );
        m_scaleList.addActionListener(this);
        addRow( "Grade Scale", m_scaleList );

        // --- PEOPLE LIST
        m_educatorList = new JComboBox(  );
        m_educatorList.setRenderer( new PersonListRenderer(null) );
        m_educatorList.setActionCommand( "educatorlist" );
        m_educatorList.setToolTipText( "Teachers for class" );
        m_educatorList.addActionListener(this);
        addRow( "Educator List", m_educatorList );

        m_educatorNames = new JTextArea();
        addRow( "Class Educators", m_educatorNames );
        m_educatorNames.setColumns( 40 );
        
        m_educatorClear = new JButton( "Clear" );
        m_educatorClear.addActionListener( this );
        m_educatorClear.setActionCommand( "educatorclear" );
        addRow( "", m_educatorClear );
        
        m_graderList = new JComboBox(  );
        m_graderList.setRenderer( new PersonListRenderer(null) );
        m_graderList.setActionCommand( "graderlist" );
        m_graderList.setToolTipText( "Graders for class" );
        m_graderList.addActionListener(this);
        addRow( "Grader List", m_graderList );

        // --- CREATE LIST AREA FOR PARENTS
        m_graderNames = new JTextArea();
        addRow( "Class Graders", m_graderNames );
        m_graderNames.setColumns( 40 );
        
        m_graderClear = new JButton( "Clear" );
        m_graderClear.addActionListener( this );
        m_graderClear.setActionCommand( "graderclear" );
        addRow( "", m_graderClear );
        
        m_studentList = new JComboBox(  );
        m_studentList.setRenderer( new PersonListRenderer(null) );
        m_studentList.setActionCommand( "studentlist" );
        m_studentList.setToolTipText( "Students for class" );
        m_studentList.addActionListener(this);
        addRow( "Student List", m_studentList );
        
        // --- CREATE LIST AREA FOR PARENTS
        m_studentNames = new JTextArea();
        addRow( "Class Students", m_studentNames );
        m_studentNames.setColumns( 40 );
        
        m_studentClear = new JButton( "Clear" );
        m_studentClear.addActionListener( this );
        m_studentClear.setActionCommand( "studentclear" );
        addRow( "", m_studentClear );       
    }
    // --- END
     
    /**
     * @param objectHandler
     * @brief initial action on creating action 
     */
    public void initialize( HandleObjectAction objectHandler )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolModel appModel    = m_appHandle.getModel();
        Vector      dataRequest = new Vector();
        
        // --- MAKE DATABASE QUERIES NECESSARY FOR ADDING ADDING PARTICIPANT
        dataRequest.add( SchoolDbIntfc.SchoolQuery.ROLE );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.PARTICIPANT  );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.SCHOOL );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.COURSE );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.RANK );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.SCALE );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.CLASS );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.EDUCATOR );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.GRADER );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.STUDENT );
        appModel.getData( dataRequest );
        
        // --- SAVE STATE
        m_objectHandler = objectHandler;
        setId( m_appHandle.getAppName() + "["
                                        + SchoolObjects.CLASS.toString()+ "]" );
        
        // --- ENABLE COMPONENTS
        m_className.setEnabled( true );
        m_className.setBackground( Color.white );
        m_classStart.setEnabled( true );
        m_classStart.setBackground( Color.white );
        m_classEnd.setEnabled( true );
        m_classEnd.setBackground( Color.white );
        m_classTime.setEnabled( true );
        m_classTime.setBackground( Color.white );
        m_classDuration.setEnabled( true );
        m_classDuration.setBackground( Color.white );
        m_classList.setEnabled( true );
        m_classList.setBackground( Color.white );
        m_educatorList.setEnabled( true );
        m_educatorList.setBackground( Color.white );
        m_graderList.setEnabled( true );
        m_graderList.setBackground( Color.white );
        m_studentList.setEnabled( true );
        m_studentList.setBackground( Color.white );
            
        // --- CREATE INITIAL HINTS
        m_engineHints.addHint( "School List",   1  + FORM_OFFSET );
        m_engineHints.addHint( "Educator List", 2  + FORM_OFFSET );
        m_engineHints.addHint( "Grader List",   3  + FORM_OFFSET );
        m_engineHints.addHint( "Course List",   4  + FORM_OFFSET );
        m_engineHints.addHint( "Class List",    5  + FORM_OFFSET );
        m_engineHints.addHint( "Student List",  6  + FORM_OFFSET );
        m_engineHints.addHint( "Up",            7  + FORM_OFFSET );
        m_engineHints.addHint( "Down",          8  + FORM_OFFSET );
        m_engineHints.addHint( "Id",            9  + FORM_OFFSET );
        m_engineHints.addHint( "Start Date",    10 + FORM_OFFSET );
        m_engineHints.addHint( "End Date",      11 + FORM_OFFSET );
        m_engineHints.addHint( "Time",          12 + FORM_OFFSET );
        m_engineHints.addHint( "Duration",      13 + FORM_OFFSET );
        m_engineHints.addHint( "Scale",         14 + FORM_OFFSET );
        m_engineHints.addHint( "Clear",         15 + FORM_OFFSET );
        
        // --- SEND HINTS
        m_appHandle.setActiveEngine( this );
        updateHints();
        m_appHandle.setFocus();
   }
    // --- END
    

    /**
     * @brief handle actions from GUI events 
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        // --- DECLARE LOCAL VARIABLES
        boolean          courseFound = false;
        boolean          dataFound = false;
        Integer          courseValue = 0;
        Integer          courseEventValue = 0;
        Integer          eventId = 0;
        Integer          courseId = 0;
        String           courseOrgId;
        Integer          classId;
        int              schoolValue = 0;
        Integer          schoolId;
        int              schoolIndex = 0;
        Integer          dbValue = 0;
        String           currentSchool;
        String           currentCourse;
        String           currentScale;
        String           currentCourseEvent;
        String           nameEntry;
        String           dbStringVal;
        String           insertString;
        String           formString;
        String           actionCmd = e.getActionCommand();
        ResultSet        courseSet = (ResultSet)m_dataMap.get( DbEngine.COURSE_DATA );
        ResultSet        classSet;
        boolean          courseState = false;
        boolean          classState  = false;
        SchoolGradeScale scaleInfo;
        SchoolHouse      schoolInfo;
        SchoolCourse     courseInfo;
        SchoolCourse     newCourse;
        SchoolClass      classInfo;
        SchoolClass      newClass;
        ArrayList<SchoolClass> currentClassList;
        boolean          getClasses = false;
        SchoolQuery      classQuery  = SchoolDbIntfc.SchoolQuery.SCHOOLCLASS;
        String           queryName;
        Integer          courseCredits;
        SchoolPerson     personInfo;
        String           personName;
        System.out.println( "AddCourse::actionPerformed Cmd: " + actionCmd );
        String           classStartDate;
        String           classEndDate;
        Integer          classDuration;
        java.sql.Date    sqlStartDate;
        java.sql.Date    sqlEndDate;
        java.sql.Time    classTime;
        java.util.Date   newTime;
        int              index;
        
        try {
            // --- IF THE RETURN BUTTON IS PRESSED
            if( null != actionCmd ) 
                switch( actionCmd ){
                // --- CASE TERMINATE COuRSE ACTION
                case "return":
                    m_appHandle.showMainPanel();
                break;
                // --- CASE CLEAR FORM
                case "clear":
                    clearForm();
                break;
                // --- CASE ADD OBJECT
                case "submit":
                    // --- EVALUATE EVENT DATA IN THE FORM
                    currentSchool = (String)m_schoolList.getSelectedItem();
                    currentCourse = m_courseName.getText();
                    schoolInfo = m_appHandle.getModel().getSchool( currentSchool );
                    courseInfo = schoolInfo.getCourse( currentCourse );
                    courseState = existsCourse();

                    // --- ADD DATA
                    newClass = new SchoolClass( m_appHandle );
                    newClass.setCourseId( courseInfo.getId() );

                    sqlStartDate = new java.sql.Date(m_classStart.getDate().getTime());                                    
                    newClass.setStartDate( sqlStartDate );
                    sqlEndDate = new java.sql.Date(m_classEnd.getDate().getTime());                                    
                    newClass.setEndDate( sqlEndDate );

                    Calendar ct = new GregorianCalendar();
                    ct.set( Calendar.HOUR_OF_DAY, 0 ); //anything 0 - 23
                    ct.set( Calendar.MINUTE, 0 );
                    ct.set( Calendar.SECOND, 0 );
                    Date d2 = ct.getTime(); //the midnight, that's the first second of the day.            m_className.setEnabled( false );
                    newTime = m_classTime.getDateWithTime(d2); 
                    classTime = new java.sql.Time( newTime.getTime() );
                    newClass.setTime( classTime );

                    classDuration = m_classDuration.getValue();
                    newClass.setDuration( classDuration.toString() );
                    newClass.setClassName( m_className.getText() );


                    // --- DO FOR EACH TEACHER
                    for( index=0;index < m_classEducatorList.size(); index++ ){
                        // --- ADD PERSON
                        personInfo = (SchoolPerson)m_classEducatorList.get( index );

                        // --- IF PERSON EXISTS
                        if( personInfo != null ){
                            // --- ADD ADMIN TO SCHOOL
                            newClass.addEducator( personInfo );
                        } 
                        // --- ENDIF PERSON EXISTS
                    }
                    // --- ENDDO FOR EACH TEACHER 

                    // --- DO FOR EACH GRADER
                    for( index=0;index < m_classGraderList.size(); index++ ){
                        // --- ADD PERSON
                        personInfo = (SchoolPerson)m_classGraderList.get( index );

                        // --- IF PERSON EXISTS
                        if( personInfo != null ){
                            // --- ADD ADMIN TO SCHOOL
                            newClass.addGrader( personInfo );
                        } 
                        // --- ENDIF PERSON EXISTS
                    }
                    // --- ENDDO FOR EACH GRADER  

                    // --- DO FOR EACH STUDENT
                    for( index=0;index < m_classStudentList.size(); index++ ){
                        // --- ADD PERSON
                        personInfo = (SchoolPerson)m_classStudentList.get( index );

                        // --- IF PERSON EXISTS
                        if( personInfo != null ){
                            // --- ADD ADMIN TO SCHOOL
                            newClass.addStudent( personInfo );
                        } 
                        // --- ENDIF PERSON EXISTS
                    }
                    // --- ENDDO FOR EACH STUDENT 

                    // --- SET SCALE
                    currentScale = (String)m_scaleList.getSelectedItem();
                    scaleInfo = m_appHandle.getModel().getScale( currentScale );
                    newClass.setScale( scaleInfo );

                    // --- IF THE CLASS IS SET
                    if( m_currentClass != null ){
                        // --- SET ID
                        newClass.setId( m_currentClass.getId() );
                    }
                    // --- ENDIF THE CLASS IS SET

                    // --- HANDLE SUBMIT EVENT
                    m_objectHandler.handleObject( newClass );
                break;
                case "courselist":
                    currentSchool = (String)m_schoolList.getSelectedItem();
                    schoolInfo = m_appHandle.getModel().getSchool( currentSchool );
                    currentCourse = (String)m_courseList.getSelectedItem();
                    courseInfo = schoolInfo.getCourse( currentCourse );
                    courseCredits = courseInfo.getCredits();
                    courseId = courseInfo.getId();
                    courseOrgId = courseInfo.getOrgAssignedId();
                    m_courseId.setText( courseId.toString() );
                    m_courseOrgId.setText( courseOrgId );
                    m_courseName.setText( currentCourse );
                    m_courseCredits.setText( courseCredits.toString() );
                    
                    // --- CLEAR CLASS LIST
                    currentClassList = courseInfo.getClassList();
                    m_classList.removeAllItems();
                    m_classList.removeActionListener( this );
                    Iterator classIter = currentClassList.iterator();

                    // --- DO FOR EACH COURSE IN THE MODEL
                    while( classIter.hasNext( ) ) {
                        // --- GET DATA
                        classInfo = (SchoolClass)classIter.next( );
                        classId = classInfo.getId();
                        m_classList.addItem( classId.toString() );
                    }
                    // --- ENDDO FOR EACH COURSE IN THE MODEL
                    m_classList.addActionListener( this );
                break;
                case "classlist":
                    // --- POPULATE WITH INFO
                    currentSchool = (String)m_schoolList.getSelectedItem();
                    schoolInfo = m_appHandle.getModel().getSchool( currentSchool );
                    currentCourse = (String)m_courseList.getSelectedItem();
                    courseInfo = schoolInfo.getCourse( currentCourse );
                    classId = new Integer( (String)m_classList.getSelectedItem() );
                    m_currentClass = courseInfo.getClass( classId );
                                                
                    // --- IF THE DATA EXISTS
                    if( m_currentClass != null ){
                        // --- UPDATE SCREEN
                        formString = m_currentClass.getClassName();
                        m_className.setText( formString );
                        //formString = classInfo.getStartDate();
                        //Date startDate = new Date( formString );
                        m_classStart.setDate( m_currentClass.getStartDate() );
                        //formString = classInfo.getEndDate();
                        //Date endDate = new Date( formString );
                        m_classEnd.setDate( m_currentClass.getEndDate() );
                        //formString = classInfo.getTime();
                        m_classTime.setTime( m_currentClass.getTime() );
                        formString = m_currentClass.getDuration();
                        Integer currDuration = new Integer( formString );
                        m_classDuration.setValue( currDuration );
                        currentScale = m_currentClass.getScale().getName();
                        m_scaleList.removeActionListener(this);
                        m_scaleList.setSelectedItem( currentScale );
                        m_scaleList.addActionListener(this);
                        
                        // --- GET THE PARENTS FOR THE SCHOOL
                        m_classEducatorList.clear();
                        m_educatorNames.setText( "" );
                        m_classGraderList.clear();
                        m_graderNames.setText( "" );
                        m_classStudentList.clear();
                        m_studentNames.setText( "" );
                        ArrayList<SchoolPerson> educatorList = m_currentClass.getEducatorList();
                        ArrayList<SchoolPerson> graderList   = m_currentClass.getGraderList();
                        ArrayList<SchoolPerson> studentList  = m_currentClass.getStudentList();
                        Iterator    educatorIter   = educatorList.iterator();
                        Iterator    graderIter   = graderList.iterator();
                        Iterator    studentIter   = studentList.iterator();

                        // --- IF THERE IS A EDUCATOR
                        while( educatorIter.hasNext( ) ) {
                            // --- GET DATA
                            personInfo = (SchoolPerson)educatorIter.next( );
                          //m_adminList.setSelectedItem( adminInfo );
                            personName = personInfo.getLastName()  + ", " +  
                                        personInfo.getFirstName() + "("  +
                                        personInfo.getId() + ")";

                            // --- STORE ADMIN
                            m_classEducatorList.add( personInfo );
                            m_educatorNames.append( personName + m_newline );                            
                        }
                        // --- ENDIF THERE IS A EDUCATOR    
                            
                        // --- IF THERE IS A GRADER
                        while( graderIter.hasNext( ) ) {
                            // --- GET DATA
                            personInfo = (SchoolPerson)graderIter.next( );
                          //m_adminList.setSelectedItem( adminInfo );
                            personName = personInfo.getLastName()  + ", " +  
                                         personInfo.getFirstName() + "("  +
                                         personInfo.getId() + ")";

                            // --- STORE ADMIN
                            m_classGraderList.add( personInfo );
                            m_graderNames.append( personName + m_newline );                            
                        }
                        // --- ENDIF THERE IS A GRADER  
                        
                        // --- IF THERE IS A STUDENT
                        while( studentIter.hasNext( ) ) {
                            // --- GET DATA
                            personInfo = (SchoolPerson)studentIter.next( );
                          //m_adminList.setSelectedItem( adminInfo );
                            personName = personInfo.getLastName()  + ", " +  
                                         personInfo.getFirstName() + "("  +
                                         personInfo.getId() + ")";

                            // --- STORE ADMIN
                            m_classStudentList.add( personInfo );
                            m_studentNames.append( personName + m_newline );                            
                        }
                        // --- ENDIF THERE IS A STUDENT  
                    }
                    // --- ENDIF THE DATA EXISTS
                break;
                // --- CASE PARENT SELECTED
                case "educatorlist":
                    // --- GET ADMIN FOR LIST
                    personInfo = (SchoolPerson)m_educatorList.getSelectedItem();
                    personName = personInfo.getLastName()  + ", " +  
                                 personInfo.getFirstName() + "("  +
                                 personInfo.getId() + ")";
                    
                    // --- STORE ADMIN
                    m_classEducatorList.add( personInfo );
                    m_educatorNames.append( personName + m_newline );
                break;
                // --- CASE PARENT CLEAR SELECTED
                case "educatorclear":
                    // --- CLEAR PARENTS
                    m_classEducatorList.clear();
                    m_educatorNames.setText( "" );
                break;
                // --- CASE PARENT SELECTED
                case "graderlist":
                    // --- GET ADMIN FOR LIST
                    personInfo = (SchoolPerson)m_graderList.getSelectedItem();
                    personName = personInfo.getLastName()  + ", " +  
                                 personInfo.getFirstName() + "("  +
                                 personInfo.getId() + ")";
                    
                    // --- STORE ADMIN
                    m_classGraderList.add( personInfo );
                    m_graderNames.append( personName + m_newline );
                break;
                // --- CASE PARENT CLEAR SELECTED
                case "graderclear":
                    // --- CLEAR PARENTS
                    m_classGraderList.clear();
                    m_graderNames.setText( "" );
                break;
                // --- CASE PARENT SELECTED
                case "studentlist":
                    // --- GET ADMIN FOR LIST
                    personInfo = (SchoolPerson)m_studentList.getSelectedItem();
                    personName = personInfo.getLastName()  + ", " +  
                                 personInfo.getFirstName() + "("  +
                                 personInfo.getId() + ")";
                    
                    // --- STORE ADMIN
                    m_classStudentList.add( personInfo );
                    m_studentNames.append( personName + m_newline );
                break;
                // --- CASE PARENT CLEAR SELECTED
                case "studentclear":
                    // --- CLEAR PARENTS
                    m_classStudentList.clear();
                    m_studentNames.setText( "" );
                break;
                case "schoollist":
                    // --- FILL COURSE LIST FOR SCHOOL
                    currentSchool = (String)m_schoolList.getSelectedItem();
                    //schoolId = (Integer)m_schoolIndex.elementAt( schoolIndex );
                    schoolInfo = m_appHandle.getModel().getSchool( currentSchool );
                    schoolId = schoolInfo.getId();
                    m_currentCourseList = schoolInfo.getCourseList();
                    Iterator courseIter = m_currentCourseList.iterator();
                    m_courseList.removeActionListener( this );
                    m_courseList.removeAllItems();
                    System.out.println("Course List: " + currentSchool );
                    // --- DO FOR EACH COURSE IN THE MODEL
                    while( courseIter.hasNext( ) ) {
                        // --- GET DATA
                        courseInfo = (SchoolCourse)courseIter.next( );
                        m_courseList.addItem( courseInfo.getName() );                               
                        System.out.println("Course List: " + courseInfo.getName() );
                        
                        // --- IF DATA IS NOT ACQUIRED
                        if( courseInfo.getState() == DataState.UNKNOWN ){
                            getClasses = true;                              
                        }
                    }   // --- ENDDO FOR EACH COURSE IN THE
                    m_courseList.addActionListener( this );
                    // --- IF CLASS DATA IS REQUIRED
                    if( getClasses == true ){
                        // --- GET CLASS DATA FOR SCHOOL
                        m_dbChain = new QueryChain( m_appHandle, true );
                        m_dbChain.setHandler( m_appHandle.getModel() );
                        classQuery.dbQueryId( schoolId, 0 );
                        queryName = classQuery.dbName() + "_" + schoolId.toString();
                        m_dbChain.addQuery( classQuery.dbQuery(), queryName );
                        m_dbChain.start();                     
                    }   
                break;
                case "scalelist":
                    // --- SCHOW SCLE DATA FOR SELECTION
                    System.out.println( "ClassView::actionPerformed Scales" );
                    currentScale = (String)m_scaleList.getSelectedItem();
                    //schoolId = (Integer)m_schoolIndex.elementAt( schoolIndex );
                    scaleInfo = m_appHandle.getModel().getScale( currentScale );
                    m_scaleDialog.show( scaleInfo );
                break;
                    
            }
        }
        catch( Exception err ){
            System.out.println("SQL Problem..."+err.getMessage());
        }        
    }

    /** 
     * @brief determine if the form has valid course data
     */
    public boolean validCourse()
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        boolean dataValid = true;
    
        // --- IF ANY FIELD IS BLANK
        if( m_courseName.equals("") ){
            // --- COURSE CANNOT BE ADDED
            dataValid = false;
        }    
        // --- ENDIF ANY FIELD IS BLANK
        
        // -- RETURN STATE 
        return( dataValid );
    }
    // --- END

    /** 
     * @brief determine if the form has valid event data
     */
    public boolean validEvent()
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        boolean dataValid = true;
        
        // --- IF ANY FIELD IS BLANK
        if( (m_classStart.equals("")    ) ||
            (m_classEnd.equals("")      ) ||
            (m_classDuration.equals("") ) ){
            // --- COURSE CANNOT BE ADDED
            dataValid = false;
        }    
        // --- ENDIF ANY FIELD IS BLANK
        
        // -- RETURN STATE 
        return( dataValid );
        
    }
    // --- END


    /** 
     * @brief determine if the course data exists
     */
    public boolean existsCourse()
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolModel            dataModel  = m_appHandle.getModel();
        ArrayList<SchoolHouse> schoolList = dataModel.getSchoolList();
        String                 nameEntry;
        String                 courseName = (String)m_courseName.getText();
        Iterator               schoolIter = schoolList.iterator();
        SchoolHouse            schoolInfo;
        SchoolCourse           courseInfo;
        boolean                dataFound  = false;
        
        // --- DO FOR EACH SCHOOL IN THE MODEL
        while( schoolIter.hasNext( ) && (dataFound == false) ){
            // --- GET DATA
            schoolInfo = (SchoolHouse)schoolIter.next( );
            ArrayList<SchoolCourse> courseList = schoolInfo.getCourseList();
            Iterator  courseIter = courseList.iterator();
            
            // --- DO FOR EACH COURSE IN THE SCHOOL
            while( courseIter.hasNext( ) && (dataFound == false) ){
                // --- GET DATA
                courseInfo = (SchoolCourse)courseIter.next( );
                
                // --- IF THERE IS A MATCH
                if( courseInfo.getName().equals(courseName) ){
                    // --- SET RETURN VALUE
                    // --- ... TBD need to break out
                    dataFound = true;
                }
                // --- ENDIF THERE IS A MATCH           
            }
            // --- ENDDO FOR EACH COURSE IN THE SCHOOL
        }  
        // --- ENDDO FOR EACH SCHOOL IN THE MODEL
        
        // --- RETURN
        return( dataFound );
    }
    // --- END

    /** 
     * @brief determine if the event data exists
     */
    public boolean existEvent()
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolModel            dataModel  = m_appHandle.getModel();
        ArrayList<SchoolHouse> schoolList = dataModel.getSchoolList();
        String                 nameEntry;
        String                 courseName = (String)m_courseName.getText();
        Iterator               schoolIter = schoolList.iterator();
        SchoolHouse            schoolInfo;
        SchoolCourse           courseInfo;
        SchoolClass            classInfo;
        boolean                dataFound  = false;
        String                 classStartDate = m_classStart.getCalendar().toString();
        String                 classEndDate = m_classEnd.getCalendar().toString();
        Integer                classDuration = m_classDuration.getValue();
        String                 className     = m_className.getText( );
        
        // --- DO FOR EACH SCHOOL IN THE MODEL
        while( schoolIter.hasNext( ) && (dataFound == false) ){
            // --- GET DATA
            schoolInfo = (SchoolHouse)schoolIter.next( );
            ArrayList<SchoolCourse> courseList = schoolInfo.getCourseList();
            Iterator  courseIter = courseList.iterator();
            
            // --- DO FOR EACH COURSE IN THE SCHOOL
            while( courseIter.hasNext( ) && (dataFound == false) ){
                // --- GET DATA
                courseInfo = (SchoolCourse)courseIter.next( );
                ArrayList<SchoolClass> classList = courseInfo.getClassList();
                Iterator  classIter = classList.iterator();
                
                // --- DO FOR EACH CLASS IN THE COURSE
                while( classIter.hasNext( ) && (dataFound == false) ){
                    // --- GET DATA
                    classInfo = (SchoolClass)classIter.next( );

                    // --- IF THE RECORD MATCHES
                    if( ( classInfo.getStartDate().equals( classStartDate )         ) && 
                        ( classInfo.getEndDate().equals( classEndDate )             ) &&
                        ( classInfo.getDuration().equals( classDuration.toString()) ) &&
                        ( classInfo.getClassName().equals( className )              ) ){
                        // --- UPDATE SCREEN
                        dataFound = true;
                    }
                    // --- ENDIF THE RECORD MATCHES
                }
                // --- DO FOR EACH CLASS IN THE COURSE
            }
            // --- ENDDO FOR EACH COURSE IN THE SCHOOL       
        }
        // --- ENDDO FOR EACH SCHOOL IN THE MODEL
       
        // -- RETURN STATE 
        return( dataFound );
        
    }
    // --- END


 
    /** 
     * @brief handle non gui generated events
     */
    @Override
    public boolean handleEvent(String currEvent) 
    // --- BEGIN
    {
        // --- DECLARE LocAL VARIABLES
        boolean     returnState = true;
        
        // --- DO CASE OF EVENT
        switch( currEvent ){
            case "Focus":
                // --- SEND VOICE FOCUS
                updateState( AppCommands.CommandValue.Focus );
            break;
            default:
            break;    
        }
        // --- ENDDO CASE OF EVENT
        
        // --- RETURN
        return( returnState );    
    }
    // --- END


    /** 
     * @brief handle database generated events
     */
    public boolean handleQuery( String currEvent, ResultSet data )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        boolean                 eventProcessed = super.handleQuery( currEvent, data );
        Iterator                    entries;
        String                      dbValue;
        Integer                     schoolId  = 0;
        Integer                     schoolKey = 0;
        Integer                     eventId   = 0;
        Integer                     courseId  = 0;
        SchoolHouse                 schoolInfo;
        SchoolPerson                personInfo;
        ArrayList<SchoolHouse>      schoolList;
        ArrayList<SchoolPerson>     peopleList;
        SchoolRoles                 personRole;
        ArrayList<SchoolGradeScale> scaleList;
        SchoolGradeScale            scaleInfo;
        
        // --- IF DATA HAS BEEN RETRIEVED
        if( eventProcessed == true ){
            try {
                // --- DO CASE OF DATA
                switch( currEvent ){
                    // --- CASE COURSE DATA IS RETRIEVED
                    case DbEngine.COURSE_DATA:
                        // --- NO ACTION FOR NOW UNTIL SCHOOL IS CHOSEN
                    break;
                    // --- CASE COURSE DATA IS RETRIEVED
                    case DbEngine.SCHOOL_DATA:
                        // --- POPULATE SCHOOL COMBO BOX
                        m_schoolList.removeActionListener( this );
                        m_schoolList.removeAllItems();
                        m_schoolIndex.clear();
                        schoolList = m_appHandle.getModel().getSchoolList();
                        Iterator schoolIter = schoolList.iterator();

                        // --- DO FOR EACH SCHOOL
                        while( schoolIter.hasNext() ){
                            // --- ADD NAME TO LIST
                            schoolInfo = (SchoolHouse)schoolIter.next( );
                            m_schoolList.addItem( schoolInfo.getName() );                               
                            m_schoolIndex.add( schoolInfo.getId() );
                        }
                        // --- ENDDO FOR EACH SCHOOL
                        m_schoolList.addActionListener( this );
                    break;
                    // --- CASE COURSE DATA IS RETRIEVED
                    case DbEngine.SCALE_DATA:
                        // --- POPULATE SCHOOL COMBO BOX
                        m_scaleList.removeActionListener( this );
                        m_scaleList.removeAllItems();
                        m_schoolIndex.clear();
                        scaleList = m_appHandle.getModel().getScaleList();
                        Iterator scaleIter = scaleList.iterator();

                        // --- DO FOR EACH SCHOOL
                        while( scaleIter.hasNext() ){
                            // --- ADD NAME TO LIST
                            scaleInfo = (SchoolGradeScale)scaleIter.next( );
                            m_scaleList.addItem( scaleInfo.getName() );                               
                        }
                        // --- ENDDO FOR EACH SCHOOL
                        m_scaleList.addActionListener( this );
                    break;
                    // --- CASE CLASS DATA IS RETRIEVED
                    case DbEngine.CLASS_DATA:
                    default:
                    // --- CASE CLASS DATA IS RETRIEVED
                    case DbEngine.PEOPLE_DATA:
                        // --- FILL LIST OF NAMES
                        // --- POPULATE PERSON  COMBO BOX
                        m_educatorList.removeActionListener(this);
                        m_graderList.removeActionListener(this);
                        m_studentList.removeActionListener(this);
                        m_educatorList.removeAllItems();
                        m_graderList.removeAllItems();
                        m_educatorList.removeAllItems();
                        peopleList = m_appHandle.getModel().getPeopleList();
                        Iterator peopleIter = peopleList.iterator();

                        // --- DO FOR EACH PERSON
                        while( peopleIter.hasNext() ){
                            // --- ADD NAME TO LIST
                            personInfo = (SchoolPerson)peopleIter.next( );
                            personRole = personInfo.getRole();
                            
                            // --- IF ROLE IS EDUCATOR
                            if( personRole == SchoolRoles.EDUCATOR ){
                                // --- APPEND LIST
                                //m_educatorList.addItem( personInfo.getLastName() );   
                                m_educatorList.addItem( personInfo );   
                            }
                            // --- ENDIF ROLE IS EDUCATOR
                            
                            // --- IF ROLE IS GRADER
                            if( personRole == SchoolRoles.GRADER ){
                                // --- APPEND LIST
                                //m_graderList.addItem( personInfo.getLastName() );   
                                m_graderList.addItem( personInfo );   
                            }
                            // --- ENDIF ROLE IS GRADER
                            
                            // --- IF ROLE IS STUDENT
                            if( personRole == SchoolRoles.STUDENT ){
                                // --- APPEND LIST
                                //m_studentList.addItem( personInfo.getLastName() );   
                                m_studentList.addItem( personInfo );   
                            }
                            // --- ENDIF ROLE IS STUDENT
                        }
                        // --- ENDDO FOR EACH PERSON
                        
                        // --- RE-ENABLEE CALLBACK
                        m_educatorList.addActionListener(this);
                        m_graderList.addActionListener(this);
                        m_studentList.addActionListener(this);
                    break;                                        
                }
                // --- ENDDO CASE OF DATA
            }
            catch(Exception e){
                 System.out.println( "SQL Problem..." + e.getMessage() );
                 eventProcessed = false;
            }
        }
        // --- ELSE 
        else {
             System.out.println( "AddCourse::handleQuery  data not handled" );
        }
        // --- ENDIF DATA HAS BEEN RETRIEVED        
        
        // --- RETURN
        return( eventProcessed );
    }
    // --- END

  /**
     * @brief communicates commands from the schoolhouse interface 
     *
     */
    @Override
    public void handleCmd( String cmd, String cmdData, int cmdKey )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        int         totalItems;
        int         newItem;
        int         schoolId;
        JComboBox   currentList = null;
               
        // --- SET STATUS AREA
        m_appHandle.setStatus( "Command: " + cmd + ":" + cmdData );
        
        // --- IF THE COMMAND IS A FORM COMMAND
        if( cmdKey > FORM_OFFSET ){
            // --- DO CASE OF COMMAND
            switch( cmdKey ){
                case 9001:
                    m_schoolList.requestFocus();
                break;
                case 9002:
                    m_educatorList.requestFocus();
                break;
                case 9003:
                    m_graderList.requestFocus();
                break;
                case 9004:
                    m_courseList.requestFocus();
                break;
                case 9005:
                    m_classList.requestFocus();
                break;
                case 9006:
                    m_studentList.requestFocus();
                break;
                // --- CASE UP LIST
                case 9007:
                    if( m_schoolList.hasFocus() == true ){
                        currentList = m_schoolList;
                    }
                    if( m_educatorList.hasFocus() == true ){
                        currentList = m_educatorList;
                    }
                    if( m_graderList.hasFocus() == true ){
                        currentList = m_graderList;
                    }
                    if( m_courseList.hasFocus() == true ){
                        currentList = m_courseList;
                    }
                    if( m_classList.hasFocus() == true ){
                        currentList = m_classList;
                    }
                    if( m_studentList.hasFocus() == true ){
                        currentList = m_studentList;
                    }
                    totalItems = currentList.getItemCount();
                    newItem = currentList.getSelectedIndex();
                    newItem++;
                    if( newItem > totalItems){
                        newItem = 0;
                    }
                    currentList.setSelectedIndex(newItem);
                break;
                // --- CASE DOWN LIST
                case 9008:
                    if( m_schoolList.hasFocus() == true ){
                        currentList = m_schoolList;
                    }
                    if( m_educatorList.hasFocus() == true ){
                        currentList = m_educatorList;
                    }
                    if( m_graderList.hasFocus() == true ){
                        currentList = m_graderList;
                    }
                    if( m_courseList.hasFocus() == true ){
                        currentList = m_courseList;
                    }
                    if( m_classList.hasFocus() == true ){
                        currentList = m_classList;
                    }
                    if( m_studentList.hasFocus() == true ){
                        currentList = m_studentList;
                    }
                    totalItems = currentList.getItemCount();
                    newItem = currentList.getSelectedIndex();
                    newItem--;
                    if( newItem == 0){
                        newItem = totalItems;
                    }
                    currentList.setSelectedIndex( newItem );
                break;
                // --- CASE NAME
                case 9009:
                    m_courseOrgId.requestFocus();
                break;
                // --- CASE ADDRESS
                case 9010:
                    m_classStart.requestFocus();
                break;
                case 9011:
                    m_classEnd.requestFocus();
                break;
                case 9012:
                    m_classTime.requestFocus();
                break;
                case 9013:
                    m_classDuration.requestFocus();
                break;
                case 9014:
                    m_scaleList.requestFocus();
                break;
                case 9015:
                    clearForm();
                break;
                default:
                break;
            }
            // --- ENDDO CASE OF COMMAND
        }
        // --- ELSE
        else if( cmdKey > SCHOOL_OFFSET ){
            // --- SET SCHOOL
            //schoolId = cmdKey - SCHOOL_OFFSET;
            //schoolInfo = m_appHandle.getModel().getSchool(schoolId);
            //m_schoolList.setSelectedItem( schoolInfo.getName() );
        }
        // --- ENDIF THE COMMAND IS A FORM COMMAND
    }
    // --- END
    
      
    /**
     * @brief clear input from user
     */
    public void clearForm()
    // --- BEGIN
    {
        // --- DECLARE LOCAL VALUES
        Calendar c = new GregorianCalendar();
        
        // --- CLEAR FIELDS
        c.set( Calendar.HOUR_OF_DAY, 0 ); //anything 0 - 23
        c.set( Calendar.MINUTE, 0 );
        c.set( Calendar.SECOND, 0 );
        Date d1 = c.getTime(); //the midnight, that's the first second of the day.            m_className.setEnabled( false );
        m_courseId.setText( "" );
        m_courseOrgId.setText( "" );
        m_courseName.setText( "" );
        m_courseCredits.setText( "" );
        m_classStart.setDate( d1 );
        m_classEnd.setDate( d1 );
        m_classTime.setTime( d1 );
    }
    // --- END
   
    /**
     * @brief disable user input
     */
    @Override
    public void disableForm()
    // --- BEGIN
    {       
        // --- DISABLE FIELDS
        m_engineEnabled = false;
        m_courseId.setText( "" );
        m_courseOrgId.setText( "" );
        m_courseName.setText( "" );
        m_courseCredits.setText( "" );
        m_classStart.setEnabled( false );
        m_classEnd.setEnabled( false );
        m_classTime.setEnabled( false );
        m_className.setText("");
        m_className.setEditable( false );
        m_className.setEnabled( false );
        m_classStart.setEnabled(false);
        m_classEnd.setEnabled(false);
        m_classDuration.setEnabled(false);
        m_educatorList.setEnabled(false);
        m_studentList.setEnabled(false);
        m_graderList.setEnabled(false);
        m_studentNames.setEditable(false);
        m_educatorNames.setEnabled(false);
        m_graderNames.setEnabled(false);
        m_educatorClear.setEnabled(false);
        m_graderClear.setEnabled(false);
        m_studentClear.setEnabled(false);
    }
    // --- END
   
    /**
     * @brief enable user input
     */
    @Override
    public void enableForm()
    // --- BEGIN
    {       
        // --- ENABLE FIELDS
        m_engineEnabled = true;
        m_classStart.setEnabled( true );
        m_classEnd.setEnabled( true );
        m_classTime.setEnabled( true );
        m_className.setEditable( true );
        m_className.setEnabled( true );
        m_classStart.setEnabled(true);
        m_classEnd.setEnabled(true);
        m_classDuration.setEnabled(true);
        m_educatorList.setEnabled(true);
        m_graderList.setEnabled(true);
        m_studentList.setEnabled(true);
        m_educatorNames.setEditable(true);
        m_studentNames.setEnabled(true);
        m_graderNames.setEditable(true);
        m_educatorClear.setEnabled(true);
        m_graderClear.setEnabled(true);
        m_studentClear.setEnabled(true);
    }
    // --- END
   
    
    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    public void createWin() {
        //Create and set up the content pane.
        setOpaque(true); //content panes must be opaque

    }    
    
}
