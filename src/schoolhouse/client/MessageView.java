 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package schoolhouse.client;

import com.toedter.calendar.JDateChooser;
import schoolhouse.common.SchoolApp;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Vector;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import schoolhouse.common.AppCommands;
import schoolhouse.common.DbEngine;
import schoolhouse.common.HandleObjectAction;
import schoolhouse.common.PersonListRenderer;
import schoolhouse.common.QueryChain;
import schoolhouse.model.SchoolClass;
import schoolhouse.model.SchoolCourse;
import schoolhouse.common.SchoolDbIntfc;
import schoolhouse.common.SchoolModel;
import schoolhouse.model.SchoolMark;
import schoolhouse.model.SchoolMessage;
import schoolhouse.model.SchoolNotification;
import schoolhouse.model.SchoolNotification.SchoolNotifyType;
import schoolhouse.model.SchoolPerson;
import schoolhouse.model.SchoolPolicy.SchoolRoles;

/**
 *
 * @author wpeets
 */
public class MessageView extends DbEngine implements ActionListener {
    protected JComboBox          m_messageList;
    protected JDateChooser       m_msgDate;
    protected JTextField         m_msgTime;
    protected JTextField         m_msgSubject;
    protected JComboBox          m_sourceList;
    protected JComboBox          m_destList;
    protected JTextField         m_msg;
              Date               m_sendDate;
              SchoolMessage      m_currentMsg;
              HandleObjectAction m_objectHandler;
              
    public MessageView( SchoolApp appHandle )
    // --- BEGIN
    {
        super( appHandle );
        m_currentMsg    = null;
        m_objectHandler = null;

        // ---  CREATE VIEW INTERFACE
        createViews();
    }
    // --- END
    
    
    private void createViews( )
    // --- BEGIN
    {
        // --- CREATE ADD ACTIONS
        m_messageList = new JComboBox( );
        m_messageList.addActionListener(this);
        m_messageList.setActionCommand("messagelist");
        addRow( "Message List", m_messageList );
        
        m_msgDate = new JDateChooser( );
        m_msgDate.setToolTipText("Date of Message");
        //m_msgDate.setEnabled( false );
        addRow( "Message Date", m_msgDate );

        m_msgTime = new JTextField( "Time of Message" );
        m_msgTime.addActionListener( this );
        m_msgTime.setEditable( false );
        m_msgTime.setBackground( Color.gray );
        m_msgTime.setActionCommand( "classname" );
        m_msgTime.setColumns( 20 );
        addRow( "Message Time", m_msgTime );        

        m_sourceList = new JComboBox( );
        m_sourceList.setRenderer( new PersonListRenderer(null) );
        m_sourceList.addActionListener(this);
        m_sourceList.setActionCommand("sourcelist");
        addRow( "Message source", m_sourceList );
        
        addSeparator();
        
        m_destList = new JComboBox( );
        m_destList.setRenderer( new PersonListRenderer(null) );
        m_destList.addActionListener(this);
        m_destList.setActionCommand("destlist");
        addRow( "Message Destination", m_destList );
                
        m_msgSubject = new JTextField( "Subject" );
        m_msgSubject.setActionCommand("subject");
        m_msgSubject.setToolTipText("Subject of message");
        m_msgSubject.addActionListener( this );
        addRow( "Subject", m_msgSubject ); 
        
        m_msg = new JTextField( "Text" );
        m_msg.setActionCommand("message");
        m_msg.setToolTipText("Click this to enter criteria");
        m_msg.addActionListener( this );
        addRow( "Message", m_msg );  
    }
    // --- END
    /**
     * @brief initial action on creating action 
     */
    public void initialize( HandleObjectAction objectHandler )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolModel appModel    = m_appHandle.getModel();
        Vector      dataRequest = new Vector();
        
        // --- STORE HANDLER
        m_objectHandler = objectHandler;
        setId( m_appHandle.getAppName() + "[MESSAGE]" );
        
        // --- MAKE DATABASE QUERIES NECESSARY FOR ADDING ADDING PARTICIPANT
        dataRequest.add( SchoolDbIntfc.SchoolQuery.PARTICIPANT );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.MESSAGE );
        appModel.getData( dataRequest );
        
        // --- CREATE INITIAL HINTS
        m_engineHints.addHint( "Message List",     1  + FORM_OFFSET );
        m_engineHints.addHint( "Destination List", 2  + FORM_OFFSET );
        m_engineHints.addHint( "Source List",      3  + FORM_OFFSET );
        m_engineHints.addHint( "Date",             4  + FORM_OFFSET );
        m_engineHints.addHint( "Subject",          5  + FORM_OFFSET );
        m_engineHints.addHint( "Text",             6  + FORM_OFFSET );
        m_engineHints.addHint( "Clear",            7  + FORM_OFFSET );
        
        // --- SEND HINTS
        m_appHandle.setActiveEngine( this );
        updateHints();
        m_appHandle.setFocus();
    }
    // --- END
    
    
    /**
     * @brief handle actions from GUI events 
     */
    public void actionPerformed(ActionEvent e) {
        // --- DECLARE LOCAL VARIABLES
        int              typeValue;
        String           currentCourse;
        String           currentMark;
        String           currentClassString;
        String           notifyString;
        Integer          currentClass;
        Integer          notifyId;
        String           actionCmd = e.getActionCommand();
        String           currentSchool;
        String           markName;
        SchoolCourse     courseInfo;
        SchoolClass      classInfo;
        SchoolMark       markInfo;
        SchoolNotifyType selectedType;
        Integer          typeIndex;
        SchoolNotification notifyInfo;
        Integer          studentId;
        Integer          classId;
        Date             sqlDateAssigned;
        Date             sqlDateDue;
        boolean          markState = false;        
        boolean          markExists  = false;
        SchoolPerson     watcherInfo;
        String           currentMessage;
        Integer          currentMsgId;
        SchoolMessage    currentMsg;
        SchoolPerson     thisPerson = m_appHandle.getPerson();
        ArrayList<SchoolPerson> watcherList;
        ArrayList<SchoolPerson> parentList;
        ArrayList<SchoolNotification> notifyList;
        System.out.println( "AddMessage::actionPerformed Cmd: " + actionCmd );

        try{
            // --- DO CASE OF COMMAND
            switch( actionCmd ) {
                // --- CASE RETURN TO MAIN DISPLAY
                case "messagelist":
                // --- GET CURRENT MESSAGE
                currentMessage = (String)m_messageList.getSelectedItem();
                currentMsgId = new Integer( currentMessage );
                m_currentMsg = m_appHandle.getModel().getMessage(currentMsgId);
                m_appHandle.setStatus( "AddMessage::actionPerformed Msg(" + currentMsgId + ")" );
                
                // --- GET MESSAGE INFO
                m_sourceList.setSelectedItem( m_currentMsg.getSource() );
                m_destList.setSelectedItem( m_currentMsg.getDestination() );
                m_msg.setText(m_currentMsg.getMessage() );
                m_msgSubject.setText(m_currentMsg.getSubject() );
                m_msgDate.setDate( m_currentMsg.getDate() );
                java.sql.Time msgTime = m_currentMsg.getTime();
                m_msgTime.setText( msgTime.toString() );
 
                break;
                case "return":
                    m_appHandle.showMainPanel();
                break;
                // --- CASE CLEAR DISPLAY
                case "clear":
                    // --- CLEAR FIELDS
                    clearForm();
                break;

                case "submit":
                    // --- SET DATE TO CURRENT DATE
                    Calendar calendar = Calendar.getInstance();
                    java.util.Date currentDate = calendar.getTime();
                    java.sql.Date date = new java.sql.Date(currentDate.getTime());
                    java.sql.Time now = java.sql.Time.valueOf(
                                        calendar.get(Calendar.HOUR_OF_DAY) + ":" +
                                        calendar.get(Calendar.MINUTE) 
                                                + ":" +
                                        calendar.get(Calendar.SECOND));                    
                    m_msgDate.setDate( date );
                    m_msgTime.setText( now.toString() );
                    
                    // --- SEND MESSAGE
                    currentMsg = new SchoolMessage( m_appHandle );
                    currentMsg.setDate(date);
                    currentMsg.setDestination((SchoolPerson)m_destList.getSelectedItem());
                    currentMsg.setSource( m_appHandle.getPerson() );
                    currentMsg.setMessage( m_msg.getText() );
                    currentMsg.setSubject( m_msgSubject.getText() );
                    currentMsg.setTime( now );
                    currentMsg.setMsgType( SchoolMessage.SchoolMessageType.GENERAL );
                    currentMsg.setMsgState( SchoolMessage.SchoolMessageState.UNREAD );
                    
                    // --- IF THERE IS A MESSAGE
                    if( m_currentMsg != null ){
                        // --- SET ID
                        currentMsg.setId( m_currentMsg.getId() );
                    }
                    // --- ENDIF THERE IS A MESSAGE
                    
                    // --- HANDLE SUBMIT
                    m_objectHandler.handleObject( currentMsg );
                    
                    // --- SET SOURCE TO USER
                    m_sourceList.setSelectedItem( thisPerson );                    
            break;
            default:
                break;
                }                              
                // --- ENDDO CASE OF COMMAND
        }
        catch(Exception err){
            System.out.println("SQL Problem..."+err.getMessage());
        }
    }

    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    public void createWin() {
        //Create and set up the content pane.
        setOpaque(true); //content panes must be opaque

    }
    
    /** 
     * @brief handle non gui generated events
     */
    @Override
    public boolean handleEvent(String currEvent) 
    // --- BEGIN
    {
        // --- DECLARE LocAL VARIABLES
        boolean returnState = true;
        
        // --- DO CASE OF EVENT
        switch( currEvent ){
            case "Focus":
                // --- SEND VOICE FOCUS
                updateState( AppCommands.CommandValue.Focus );
            break;
            default:
            break;    
        }
        // --- ENDDO CASE OF EVENT
        
        // --- RETURN
        return( returnState );    
    }
    // --- END

    
    
    /** 
     * @brief handle non database generated events
     */
    public boolean handleQuery( String currEvent, ResultSet data ) 
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        boolean       eventProcessed = super.handleQuery( currEvent, data );
        SchoolMessage            msgInfo;
        Integer                  msgId;
        ArrayList<SchoolMessage> messageList;
        ArrayList<SchoolPerson>  peopleList;
        SchoolPerson             msgSource;
        SchoolPerson             msgDestination;
        SchoolPerson             thisPerson = m_appHandle.getPerson();
        SchoolRoles              userRole = m_appHandle.getRole();
        String                   userName = m_appHandle.getUser();
        System.out.println( "AddMessage::handleQuery Cuurent User: " + userName );

        try {
        // --- IF DATA HAS BEEN RETRIEVED
        if( eventProcessed == true ){
            // --- DO CASE OF DATA
            switch( currEvent ){
            // --- CASE PARTICIPANT DATA IS RETRIEVED
            case DbEngine.MESSAGE_DATA:
                // --- ADD CURRENT USER TO SOURCE
                m_sourceList.addItem( thisPerson );
                
                // --- POPULATE MESSAGE COMBO BOX
                m_messageList.removeActionListener(this);
                messageList = m_appHandle.getModel().getMessageList();
                Iterator msgIter = messageList.iterator();
                

                // --- DO FOR EACH MESAGE
                while( msgIter.hasNext() ){
                    // --- ADD NAME TO LIST
                    msgInfo   = (SchoolMessage)msgIter.next( );
                    msgId     = msgInfo.getId();                            
                    msgSource = msgInfo.getSource();
                    msgDestination = msgInfo.getDestination();

                    // --- IF MESSAGE APPLIES TO USER
                    if( (msgDestination.getUserName().equals(userName) ) ||
                        (msgSource.getUserName().equals(userName)      ) ||
                        (userRole == SchoolRoles.ADMINISTRATOR         ) ){    
                            // --- ADD MESSAGE SOURCE TO LIST
                            m_messageList.addItem( msgId.toString() );
                            
                            // --- IF THE PERSON IS NOT ON THE SOURCE LIST
                            // --- ... TBD probably a better way with internal model
                        if( (msgSource.getUserName().equals(userName)      ) ||
                            (userRole == SchoolRoles.ADMINISTRATOR         ) ){    
                                // --- ADD TO PARENT LIST
                                m_sourceList.addItem( msgSource );
                            }
                            // --- ENDIF THE PERSON IS NOT ON THE SOURCE LIST
                            
                            // --- ADD MESSAGE DESTINATION TO LIST
                            
                            // --- IF THE PERSON IS NOT ON THE DEESTINATION LIST
                            // --- ... TBD probably a better way with internal model
                            if( (msgDestination.getUserName().equals(userName) ) ||
                                (userRole == SchoolRoles.ADMINISTRATOR         ) ){    
                                // --- ADD TO PARENT LIST
                                m_destList.addItem( msgDestination );
                            }
                            // --- ENDIF THE PERSON IS NOT ON THE DEESTINATION LIST
                    }
                    // --- ENDIF MESSAGE APPLIES TO USER
                }
                // --- ENDDO FOR EACH MESSAGE
                        
                // --- RE-ENABLEE CALLBACK
                m_messageList.addActionListener(this);                                
            break;
            // --- CASE PARTICIPANT DATA IS RETRIEVED
            case DbEngine.PEOPLE_DATA:
                // --- POPULATE DESTINATION WITH ALL POSSIBLE DESTINATIONS
                m_destList.removeActionListener(this);
                m_destList.removeAllItems();
                peopleList = m_appHandle.getModel().getPeopleList();
                Iterator peopleIter = peopleList.iterator();

                // --- DO FOR EACH PERSON
                while( peopleIter.hasNext() ){
                    // --- ADD NAME TO LIST
                    msgDestination = (SchoolPerson)peopleIter.next( );
                    m_destList.addItem( msgDestination );
                }
                // --- ENDDO FOR EACH PERSON
            break;        
            default:
            break;
            }
            // --- ENDDO DO CASE OF DATA       
        }
        // --- ELSE 
        else {
             System.out.println( "AddMessage::handleQuery  data not handled" );
        }
        // --- ENDIF DATA HAS BEEN RETRIEVED
        }
        catch(Exception e){
            System.out.println( "SQL Problem..." + e.getMessage() );
            eventProcessed = false;
        }
 
        // --- RETURN
        return( eventProcessed );
    }
    // --- END
     
   /**
     * @brief clear input from user
     */
    public void clearForm()
    // --- BEGIN
    {
        // --- CLEAR FIELDS
        m_msg.setText( "" );
        m_msgSubject.setText( "" );
    }
    // --- END
   
   /**
     * @brief disable input from user
     */
    @Override
    public void disableForm()
    // --- BEGIN
    {
        // --- DISABLE FIELDS
        m_destList.setEnabled( false );
        m_msg.setEnabled( false );
        m_msg.setBackground(Color.GRAY);
        m_msg.setText( "" );
        m_msgSubject.setText( "" );
        m_msg.setEnabled( false );
        m_msg.setBackground(Color.GRAY);
    }
    // --- END
   
   /**
     * @brief disable input from user
     */
    @Override
    public void enableForm()
    // --- BEGIN
    {
        // --- DISABLE FIELDS
        m_destList.setEnabled( false );
        m_msg.setEnabled( false );
        m_msg.setBackground(Color.GRAY);
        m_msg.setText( "" );
        m_msgSubject.setText( "" );
        m_msg.setEnabled( false );
        m_msg.setBackground(Color.GRAY);
    }
    // --- END
   

    /**
     * @brief communicates commands from the schoolhouse interface 
     *
     */
    @Override
    public void handleCmd( String cmd, String cmdData, int cmdKey )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        int         totalItems;
        int         newItem;
        JComboBox   currentList = null;
               
        // --- SET STATUS AREA
        m_appHandle.setStatus( "Command: " + cmd + ":" + cmdData );
        
        // --- IF THE COMMAND IS A FORM COMMAND
        if( cmdKey > FORM_OFFSET ){
            // --- DO CASE OF COMMAND
            switch( cmdKey ){
                case 9001:
                    m_messageList.requestFocus();
                break;
                case 9002:
                    m_destList.requestFocus();
                break;
                case 9003:
                    m_sourceList.requestFocus();
                break;
                // --- CASE UP LIST
                case 9004:
                    if( m_messageList.hasFocus() == true ){
                        currentList = m_messageList;
                    }
                    if( m_destList.hasFocus() == true ){
                        currentList = m_destList;
                    }
                    if( m_sourceList.hasFocus() == true ){
                        currentList = m_sourceList;
                    }
                    totalItems = currentList.getItemCount();
                    newItem = currentList.getSelectedIndex();
                    newItem++;
                    if( newItem > totalItems){
                        newItem = 0;
                    }
                    currentList.setSelectedIndex(newItem);
                break;
                // --- CASE DOWN LIST
                case 9005:
                    if( m_messageList.hasFocus() == true ){
                        currentList = m_messageList;
                    }
                    if( m_destList.hasFocus() == true ){
                        currentList = m_destList;
                    }
                    if( m_sourceList.hasFocus() == true ){
                        currentList = m_sourceList;
                    }
                    totalItems = currentList.getItemCount();
                    newItem = currentList.getSelectedIndex();
                    newItem--;
                    if( newItem == 0){
                        newItem = totalItems;
                    }
                    currentList.setSelectedIndex( newItem );
                break;
                // --- CASE NAME
                case 9006:
                    m_msgDate.requestFocus();
                break;
                case 9007:
                    m_msgSubject.requestFocus();
                break;
                case 9008:
                    m_msg.requestFocus();
                break;
                // --- CASE ADDRESS
                case 9009:
                    clearForm();
                break;
                default:
                break;
            }
            // --- ENDDO CASE OF COMMAND
        }
        // --- ELSE
        else if( cmdKey > SCHOOL_OFFSET ){
            // --- SET SCHOOL
            //schoolId = cmdKey - SCHOOL_OFFSET;
            //schoolInfo = m_appHandle.getModel().getSchool(schoolId);
            //m_schoolList.setSelectedItem( schoolInfo.getName() );
        }
        // --- ENDIF THE COMMAND IS A FORM COMMAND
    }
    // --- END
    

}
