/*
 *  FILE:           ChangeController.java
 *
 *  CLASS:          ChangeController
 *
 *  DESCRIPTION:    This class provides the high level application
 *                  behavior for schoolhouse change actions.
 *
 *  CLASS NOTES:
 *
 *  CLASS REFERENCES:
 *
 *
 * Pattern(s)  : None
 *
 * Constraints : None
 *
 *  Assertions  : None
 *
 ******************************************************************************
 *
 *  Revision History:
 *
 *  Date        RI    Purpose
 *  ---------   ---   ------------------------------------------------------
 *  JAN-03-14   WAP   Original Release
 *
 ******************************************************************************
 */

package schoolhouse.client;

import schoolhouse.common.CmdPane;
import schoolhouse.common.SchoolApp;
import schoolhouse.model.SchoolPolicy;
import schoolhouse.model.SchoolPolicy.SchoolObjects;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.AbstractButton;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import schoolhouse.common.AppCommands;
import schoolhouse.model.DataObject;
import schoolhouse.common.DbEngine;
import schoolhouse.common.HandleObjectAction;
import schoolhouse.common.SchoolModel;
import schoolhouse.model.SchoolClass;
import schoolhouse.model.SchoolCourse;
import schoolhouse.model.SchoolGrade;
import schoolhouse.model.SchoolHouse;
import schoolhouse.model.SchoolMark;
import schoolhouse.model.SchoolMessage;
import schoolhouse.model.SchoolNotification;
import schoolhouse.model.SchoolPerson;
import schoolhouse.model.SchoolStudent;

/**
 * @brief provide behavior for add capability
 * @author wpeets
 */
public class ChangeController extends DbEngine implements ActionListener, HandleObjectAction {
    protected JButton[]   m_objectList;   
              SchoolView  m_schoolForm;
              PersonView  m_personForm;
              CourseView  m_courseForm;
              ClassView   m_classForm;
              MarkView    m_markForm;
              GradeView   m_gradeForm;
              NotifyView  m_notifyForm;
              MessageView m_messageForm;
              CmdPane     m_cmdPane;
    
    /**
     * @brief constructor
     * @param appHandle application handle
     */
    public ChangeController( SchoolApp appHandle )
    // --- BEGIN
    {
        super( appHandle );
        
        // --- DECLARE LOCAL VARIABLES
        SchoolPolicy.SchoolActions action = SchoolPolicy.SchoolActions.CHANGE;
        boolean                    objState;
        int                        count     = 0;
        GridBagConstraints         btnVal    = new GridBagConstraints();
        
        // --- INITIALIZE CLASS VARIABLE
        m_objectList = new JButton[SchoolPolicy.SchoolObjects.getSize() + SchoolPolicy.AppActions.getSize()];
        
        // --- SET LAYOUT
        setLayout( new GridBagLayout() );  
        
        // --- CREATE ADDITIONAL FORMS
        m_schoolForm  = new SchoolView( appHandle );
        m_personForm  = new PersonView( appHandle );
        m_courseForm  = new CourseView( appHandle );
        m_classForm   = new ClassView( appHandle );
        m_markForm    = new MarkView( appHandle );
        m_gradeForm   = new GradeView( appHandle );
        m_notifyForm  = new NotifyView( appHandle );
        m_messageForm = new MessageView( appHandle );
        m_cmdPane     = new CmdPane( appHandle );
        
        // --- DO FOR EACH ACTION
        for( SchoolPolicy.SchoolObjects currObject: SchoolPolicy.SchoolObjects.values() ){
            // --- IF NOT LAST ITEM
            if( currObject != SchoolPolicy.SchoolObjects.UNKNOWN ) {
                // --- CREATE BUTTON
                btnVal.gridx      = 0;
                btnVal.gridy      = count;
                btnVal.fill       = GridBagConstraints.BOTH;
                btnVal.gridheight = 1;
                btnVal.gridwidth  = 1;
                m_objectList[count] = new JButton( currObject.name() );
                m_objectList[count].setVerticalTextPosition( AbstractButton.CENTER );
                m_objectList[count].setHorizontalTextPosition(AbstractButton.LEADING);
                m_objectList[count].setActionCommand( currObject.name() );
                m_objectList[count].addActionListener( this );
                add( m_objectList[count], btnVal );
                //m_addCmd.setMnemonic(KeyEvent.VK_A);
                //m_addCmd.setToolTipText("Click this button to add data to the database");
                
                // --- GET ACTION STATE
                objState = m_appHandle.hasAccessState( action, currObject );
                
                // --- IF ROLE HAS NO ACCESS FOR ACTION
                if( objState == false ){
                    // --- DISABLE ACTION
                    m_objectList[count].setEnabled( false );
                }
                // --- ELSE
                else {
                    // --- ADD TO HINT LIST
                    m_engineHints.addHint( currObject.name(), currObject.getValue() );
                }
                // --- ENDIF ROLE HAS NO ACCESS FOR ACTION
            }
            // --- ENDIF NOT LAST ITEM
            
            // --- INCREMENT
            count++;
        }
        // --- ENDDO FOR EACH ACTION
        
        // --- DO FOR EACH GUI ACTION
        for( SchoolPolicy.AppActions guiAction: SchoolPolicy.AppActions.values() ){
            // --- CREATE BUTTON
            btnVal.gridx      = 0;
            btnVal.gridy      = count;
            btnVal.fill       = GridBagConstraints.BOTH;
            btnVal.gridheight = 1;
            btnVal.gridwidth  = 1;
            m_objectList[count] = new JButton( guiAction.name() );
            m_objectList[count].setVerticalTextPosition( AbstractButton.CENTER );
            m_objectList[count].setHorizontalTextPosition(AbstractButton.LEADING);
            m_objectList[count].setActionCommand( guiAction.name() );
            m_objectList[count].addActionListener( this );
            add( m_objectList[count], btnVal );
            m_engineHints.addHint( guiAction.name(), guiAction.getValue() );
            
            // --- INCREMENT
            count++;
        }
        // --- ENDDO FOR EACH GUI ACTION        
    }
    // --- END
    
    /**
     * @brief initial action on creating action 
     */
    public void initialize( )
    // --- BEGIN
    {
        // --- SAVE STATE
        setId( m_appHandle.getAppName() + "[CHANGE]" );

        // --- SEND HINTS
        m_appHandle.setActiveEngine( this );
        updateHints();
    }
    // --- END
    
    /** 
     * @brief Listens to the combo box.
     * @param e current action
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        // --- DECLARE LOCAL VARIABLES
        String    currentCommand = e.getActionCommand();
        int       commandKey     = -1;
        
        // --- DO CASE OF COMMAND
        switch( currentCommand ){
            // --- CASE SCHOOL OBJECT
            // --- ... TBD should probably use the school value here
            case "SCHOOL":
                // --- SET FORM TO ADD SCHOOL
                commandKey = 1;
            break;
            case "PERSON":
                // --- SET FORM TO ADD PERSON
                commandKey = 2;
            break;
            case "COURSE":
                // --- SET FORM TO ADD COURSE
                commandKey = 3;
            break;
            case "CLASS":
                // --- SET FORM TO ADD CLASS
                commandKey = 4;
            break;
            case "MARK":
                // --- SET FORM TO ADD MARK
                commandKey = 5;
            break;
            case "GRADE":
                // --- SET FORM TO ADD GRADE
                commandKey = 6;
            break;
            case "NOTIFICATION":
                // --- SET FORM TO ADD NOTIFICATION
                commandKey = 7;
            break;
            case "MESSAGE":
                // --- SET FORM TO ADD NOTIFICATION
                commandKey = 8;
            break;
            case "RETURN":
                // --- RETURN TO MAIN PANEL
                commandKey = 100;
            break;
            case "Focus":
                // --- SEND VOICE FOCUS
                updateState( AppCommands.CommandValue.Focus );
            break;
            case "EXIT":
                // --- SEND VOICE FOCUS
                m_appHandle.getActiveDash().clearRole( m_appHandle.getRole() );
                m_appHandle.closeMainPanel();
            break;
            default:
                // --- SET FORM TO DO NOTHING
                //commandKey = -1;
            break;
        }
        // --- ENDDO CASE OF COMMAND
       
        // --- EXECUTE COMMAND
        handleCmd( currentCommand, null, commandKey );       
    }
    // --- END
    
    /**
     * @brief communicates commands from the schoolhouse interface 
     *
     */
    @Override
    public void handleCmd( String cmd, String cmdData, int cmdKey )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        Container actionArea     = m_appHandle.getActionPane();
        Container workArea       = m_appHandle.getWorkPane();
        Container cmdArea        = m_appHandle.getCmdPane();
        JFrame    frame          = m_appHandle.getFrame();
               
        // --- DO CASE OF COMMAND
        switch( cmdKey ){
            // --- CASE RETURN
            case 100:
                // --- RETURN TO MAIN PANEL
                m_appHandle.showMainPanel();
                m_appHandle.setHeader( "ACTION" );
                m_appHandle.setStatus( "Select an action" );
            break;
            // --- CASE SCHOOL OBJECT
            case 1:
                // --- SET FORM TO ADD SCHOOL
                // --- ... TBD need to store preferred size with form and
                // --- ... shrink al these cases to 1 paramterizing header and such
                m_schoolForm.initialize( this );
                m_appHandle.setHeader( "CHANGE SCHOOL" );
                m_appHandle.setActiveEngine( m_schoolForm );
                //actionArea.removeAll();
                workArea.removeAll();
                workArea.add( m_schoolForm );
                cmdArea.removeAll( );
                cmdArea.add( m_cmdPane );
                m_appHandle.setStatus( "Change a School" );
                frame.setResizable( true );
                frame.pack();
                m_appHandle.getFrame().setBounds( 25, 325, 710, 400 );
            break;
            case 2:
                // --- SET FORM TO ADD PERSON
                m_personForm.initialize( this );
                m_appHandle.setHeader( "CHANGE PERSON" );
                m_appHandle.setActiveEngine( m_personForm );
                //actionArea.removeAll();
                workArea.removeAll();
                workArea.add( m_personForm );
                cmdArea.removeAll( );
                cmdArea.add( m_cmdPane );
                m_appHandle.setStatus( "Change a Person" );
                frame.setResizable( true );
                frame.pack();
                m_appHandle.getFrame().setBounds( 25, 125, 710, 750 );
            break;
            case 3:
                // --- SET FORM TO ADD COURSE
                m_courseForm.initialize( this );
                m_appHandle.setHeader( "CHANGE COURSE" );
                m_appHandle.setActiveEngine( m_courseForm );
                //actionArea.removeAll();
                workArea.removeAll();
                workArea.add( m_courseForm );
                cmdArea.removeAll( );
                cmdArea.add( m_cmdPane );
                m_appHandle.setStatus( "Change a Course" );
                frame.setResizable( true );
                frame.pack();
                m_appHandle.getFrame().setBounds( 25, 325, 710, 400 );
            break;
            case 4:
                // --- SET FORM TO ADD CLASS
                m_classForm.initialize( this );
                m_appHandle.setHeader( "CHANGE CLASS" );
                m_appHandle.setActiveEngine( m_classForm );
                //actionArea.removeAll();
                workArea.removeAll();
                workArea.add( m_classForm );
                cmdArea.removeAll( );
                cmdArea.add( m_cmdPane );
                m_appHandle.setStatus( "Change a Class" );
                frame.setResizable( true );
                frame.pack();
                m_appHandle.getFrame().setBounds( 25, 125, 710, 700 );
            break;
            case 5:
                // --- SET FORM TO ADD MARK
                m_markForm.initialize( this );
                m_appHandle.setHeader( "CHANGE MARK" );
                m_appHandle.setActiveEngine( m_markForm );
                //actionArea.removeAll();
                workArea.removeAll();
                workArea.add( m_markForm );
                cmdArea.removeAll( );
                cmdArea.add( m_cmdPane );
                m_appHandle.setStatus( "Change a Mark" );
                frame.setResizable( true );
                frame.pack();
                m_appHandle.getFrame().setBounds( 25, 125, 710, 700 );
            break;
            case 6:
                // --- SET FORM TO ADD GRADE
                m_gradeForm.initialize( this );
                m_appHandle.setHeader( "CHANGE GRADE" );
                m_appHandle.setActiveEngine( m_gradeForm );
                //actionArea.removeAll();
                workArea.removeAll();
                workArea.add( m_gradeForm );
                cmdArea.removeAll( );
                cmdArea.add( m_cmdPane );
                m_appHandle.setStatus( "Change a Grade" );
                frame.setResizable( true );
                frame.pack();
                m_appHandle.getFrame().setBounds( 25, 325, 710, 400 );
            break;
            case 7:
                // --- SET FORM TO ADD NOTIICATION
                m_notifyForm.initialize( this );
                m_appHandle.setHeader( "CHANGE NOTIFICATION" );
                m_appHandle.setActiveEngine( m_notifyForm );
                //actionArea.removeAll();
                workArea.removeAll();
                workArea.add( m_notifyForm );
                cmdArea.removeAll( );
                cmdArea.add( m_cmdPane );
                m_appHandle.setStatus( "Change a Notification" );
                frame.setResizable( true );
                frame.pack();
                m_appHandle.getFrame().setBounds( 25, 325, 710, 400 );
            break;
            case 8:
                // --- SET FORM TO ADD MESSAGE
                m_messageForm.initialize( this );
                m_appHandle.setHeader( "CHANGE MESSAGE" );
                m_appHandle.setActiveEngine( m_messageForm );
                //actionArea.removeAll();
                workArea.removeAll();
                workArea.add( m_messageForm );
                cmdArea.removeAll( );
                cmdArea.add( m_cmdPane );
                m_appHandle.setStatus( "Change a Message" );
                frame.setResizable( true );
                frame.pack();
                m_appHandle.getFrame().setBounds( 25, 325, 710, 400 );
            break;
            default:
                // --- SET FORM TO ADD PERSON
            break;
        }
        // --- ENDDO CASE OF COMMAND 
    }
    // --- END
    
    /** 
     * @brief handle non gui generated events
     */
    @Override
    public boolean handleEvent(String currEvent) 
    // --- BEGIN
    {
        // --- DECLARE LocAL VARIABLES
        boolean     returnState = true;
        
        // --- DO CASE OF EVENT
        switch( currEvent ){
            case "Focus":
                // --- SEND VOICE FOCUS
                updateState( AppCommands.CommandValue.Focus );
            break;
            default:
            break;    
        }
        // --- ENDDO CASE OF EVENT
        
        // --- RETURN
        return( returnState );    
    }
    // --- END

    
    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    public void createWin() {
        //Create and set up the content pane.
        setOpaque(true); //content panes must be opaque

        // --- SEND HINTS
        // --- ...send hints to VE TBD JEFF
    }    

    @Override
    public boolean handleObject( DataObject objectUnderTest )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        boolean     returnVal = false;
        SchoolModel model     = m_appHandle.getModel();
        
        // --- IF THE OBJECT IS A PERSON
        if( (objectUnderTest.getClass() == SchoolPerson.class)  ||
            (objectUnderTest.getClass() == SchoolStudent.class) ){
            // --- HANDLE PERSON 
            System.out.println( "AddEngine::handleObject Person " );
            model.changePerson( (SchoolPerson)objectUnderTest );
            returnVal = true;        
        }
        // --- ENDIF THE OBJECT IS A PERSON
        
        // --- IF THE OBJECT IS A SCHOOL
        if( objectUnderTest.getClass() == SchoolHouse.class ){
            // --- HANDLE SCHOOL
            // --- ...TBD do we need to check if school exists
            System.out.println( "AddEngine::handleObject School " );
            model.changeSchool( (SchoolHouse)objectUnderTest );
            returnVal = true;        
        }
        // --- ENDIF THE OBJECT IS A SCHOOL
        
        // --- IF THE OBJECT IS A COURSE
        if( objectUnderTest.getClass() == SchoolCourse.class ){
            // --- HANDLE COURSE
            // --- ...TBD do we need to check if exists
            System.out.println( "AddEngine::handleObject Course " );
            model.changeCourse( (SchoolCourse)objectUnderTest );
            returnVal = true;        
        }
        // --- ENDIF THE OBJECT IS A COURSE
        
        // --- IF THE OBJECT IS A CLASS
        if( objectUnderTest.getClass() == SchoolClass.class ){
            // --- HANDLE CLASS
            // --- ...TBD do we need to check if exists
            System.out.println( "AddEngine::handleObject Class " );
            model.changeClass( (SchoolClass)objectUnderTest );
            returnVal = true;        
        }
        // --- ENDIF THE OBJECT IS A CLASS
        
        // --- IF THE OBJECT IS A MARK
        if( objectUnderTest.getClass() == SchoolMark.class ){
            // --- HANDLE MARK
            // --- ...TBD do we need to check if exists
            System.out.println( "AddEngine::handleObject Mark " );
            model.changeMark( (SchoolMark)objectUnderTest );
            returnVal = true;        
        }
        // --- ENDIF THE OBJECT IS A MARK
        
        // --- IF THE OBJECT IS A GRADE
        if( objectUnderTest.getClass() == SchoolGrade.class ){
            // --- HANDLE GRADE
            // --- ...TBD do we need to check if exists
            System.out.println( "AddEngine::handleObject Grade " );
            model.changeGrade( (SchoolGrade)objectUnderTest );
            returnVal = true;        
        }
        // --- ENDIF THE OBJECT IS A GRADE
        
        // --- IF THE OBJECT IS A NOTIFICATION
        if( objectUnderTest.getClass() == SchoolNotification.class ){
            // --- HANDLE GRADE
            // --- ...TBD do we need to check if exists
            System.out.println( "AddEngine::handleObject Notification " );
            model.changeNotification( (SchoolNotification)objectUnderTest );
            returnVal = true;        
        }
        // --- ENDIF THE OBJECT IS A NOTIFICATION
        
        // --- IF THE OBJECT IS A MESSAGE
        if( objectUnderTest.getClass() == SchoolMessage.class ){
            // --- HANDLE MESSAGE
            // --- ...TBD do we need to check if exists
            System.out.println( "AddEngine::handleObject Message " );
            model.changeMessage( (SchoolMessage)objectUnderTest );
            returnVal = true;        
        }
        // --- ENDIF THE OBJECT IS A MESSAGE
        
        // --- RETURN
        return( returnVal );
    }
    // --- END
    
}
