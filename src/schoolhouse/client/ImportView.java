/*
 *  FILE:           ImportView.java
 *
 *  CLASS:          ImportView
 *
 *  DESCRIPTION:    This class provides a view for a table.
 *
 *  CLASS NOTES:
 *
 *  CLASS REFERENCES:
 *
 *
 * Pattern(s)  : None
 *
 * Constraints : None
 *
 *  Assertions  : None
 *
 ******************************************************************************
 *
 *  Revision History:
 *
 *  Date        RI    Purpose
 *  ---------   ---   ------------------------------------------------------
 *  OCT-16-14   WAP   Original Release
 *
 ******************************************************************************
 */


package schoolhouse.client;

import schoolhouse.common.QueryChain;
import schoolhouse.common.SchoolApp;
import schoolhouse.common.SchoolDbIntfc.SchoolQuery;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import schoolhouse.common.DbEngine;

/**
 * This class provides the capability to add a new school to the database
 * 
 * @author wpeets
 */
public class ImportView extends DbEngine implements ActionListener
                                                   
{
    private   ArrayList <String> m_columnNames;
    protected JTextField[]       m_objectList; 
    private   int                m_columnCount;            
              ResultSetMetaData  m_dbMetaData;
              String             m_tableName;
              String             m_importName;
              String             m_importFile;
    final     String             TYPE_MARKER = "-- Data:";
    final     String             TABLE_MARKER = "-- Table:";
    private   int                m_LineEntryIndex;
    
    
    BufferedReader               m_fileReader;
    
    // --- JRW ADD
    List <ImportDataBuffer> m_DataArrayList;
    ArrayList<Integer>      m_EligibleColumnIndexes; 
    
    private String[] m_importHeaderTokens;
    private int m_currentIndexState   = 0;
    
    private   final int           UP   = 1;
    private   final int           DOWN = -1;
    
    
    ConcurrentHashMap<Integer,Integer>       dbColumns;    
    
    
    
/**
 * constructor
 * 
 * @author wpeets
 */
    public ImportView( SchoolApp appHandle )
    // --- BEGIN
    {
        // --- INITIALIZE CLASS VARIABLES
        super( appHandle );
        m_dataSet     = null;  
        m_columnCount = 0;
        m_columnNames = new ArrayList<String>();
        m_DataArrayList = new ArrayList<>(); 
        
        m_LineEntryIndex =0;      
        m_EligibleColumnIndexes = new ArrayList<>();
        
        
        dbColumns = new ConcurrentHashMap<Integer,Integer>();        
    }
    // --- END

    /**
     * The method builds a display
     * 
     * @author wpeets
     */
    private void createView( )
    // --- BEGIN
    {
    }
    // --- END

    /**
     * @brief initial action on creating action 
     * 
     * This method will request all the data needed to service action
     */
    public void initialize( SchoolQuery currentQuery )
    // --- BEGIN
    {
        // --- MAKE DATABASE QUERIES NECESSARY TO RENDER VIEW
        // --- ... TB maybe there is no need to do a query for an import
        // --- ... the metadata could be obtained elsewhere
        m_dbChain = new QueryChain( m_appHandle, true );
        m_dbChain.addQuery( currentQuery.dbQuery(), currentQuery.dbName() );
        m_importName = currentQuery.dbName();
        m_dbChain.start();        
    }
    // --- END
    
    
    /** 
     * Listens to actions created by user 
     */
    public void actionPerformed( ActionEvent e ) 
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        Boolean                 dataFound       = false;
        Integer                 dbValue;
        String                  currentSchool;
        String                  currentAdmin;
        String                  dbStringVal;
        String                  actionCmd       = e.getActionCommand();
        ResultSet               schoolData;
        int                     posStart        = 0;
        int                     posEnd          = 0;
        Integer                 adminId;
        JFileChooser            chooser;
        FileNameExtensionFilter filter;
        int                     returnVal;
        ExportWriter            dataWriter;
        int                     sqlIndex          = 0;        
        int                     entryIndex         = 0;
	FileReader              fileBuffer;
	CSVParser               parser;
        //BufferedReader          fileReader;
        boolean                 headerFound = false;
        boolean                 hasData     = true;
        String                  currentLine;
        String                  tableType;
        int                     markerLength = 0;
        ImportReader            dataReader;
        int                     genericIndex =0;
        int                     outerIndex =0;

        
        
        int                     lineReadResult=-1;           
        

        
        try {
        // --- IF THE RETURN BUTTON IS PRESSED
        if( "return".equals( actionCmd )) {
           m_appHandle.showMainPanel();
        }
        // --- ELSE IF THE RETURN BUTTON IS PRESSED
        else if( "clear".equals( actionCmd )) {
            // --- CLEAR FIELDS
            System.out.println("ImportView::actionPerformed Clearing command" );
            
            // --- DO FOR EACH FIELD
            for( sqlIndex=0; sqlIndex<m_columnCount; sqlIndex++ ){
                // --- CLEAR DATA
                m_objectList[sqlIndex].setText( "" );                
            }
            // --- ENDDO FOR EACH FIELD
        }
        // --- ELSE IF THE RETURN BUTTON IS PRESSED
        else if( "submit".equals( actionCmd )) {
            // --- CLEAR FIELDS
            System.out.println("ImportView::actionPerformed Submit command" );
               
           // --- READ DATA
           dataReader = new ImportReader( m_appHandle, m_tableName, m_importName,
                                          m_importFile );
           dataReader.start();
            
            // --- DO FOR EACH FIELD
            for( sqlIndex=0; sqlIndex<m_columnCount; sqlIndex++ ){
                // --- CLEAR DATA
                m_objectList[sqlIndex-1].setText( "" );                
            }
            // --- ENDDO FOR EACH FIELD
        }
        // --- ELSE IF THE RETURN BUTTON IS PRESSED
        else if( "open".equals( actionCmd )) {
            // --- GET FILE NAME
            chooser = new JFileChooser();
            filter  = new FileNameExtensionFilter( "CSV Files", "csv", "txt" );
            chooser.setFileFilter( filter );
            returnVal = chooser.showOpenDialog( this );
            
            // --- IF THE OPERATOR HAS ENTERED A FILE
            if( returnVal == JFileChooser.APPROVE_OPTION ) {
               // --- READ DATA HEADER FROM FILE
               m_importFile = chooser.getSelectedFile().getAbsoluteFile().toString();
               fileBuffer = new FileReader( m_importFile );
               parser = new CSVParser( fileBuffer, CSVFormat.MYSQL );
               m_fileReader = new BufferedReader( fileBuffer );
               currentLine = m_fileReader.readLine();
              
               // --- DO FOR EACH FILE HEADER LINE
               while( (currentLine != null) && (headerFound == false) ){
                   
                   // --- IF THE LINE IS A COMMENT
                   if( currentLine.startsWith( "--" ) ){
                       // --- IF THE LINE INDICATES A TYPE
                       if( currentLine.startsWith( TYPE_MARKER) ){
                           // --- GET TABLE TYPE
                           markerLength = TYPE_MARKER.length();
                           tableType = currentLine.substring( markerLength ); 
                       }
                       // --- ENDIF THE LINE INDICATES A TYPE
                       
                       // --- IF THE LINE INDICATES A TABLE
                       if( currentLine.startsWith( TABLE_MARKER) ){
                           // --- GET TABLE TYPE
                           markerLength = TABLE_MARKER.length();
                           m_tableName = currentLine.substring( markerLength ); 
                       }
                       // --- ENDIF THE LINE INDICATES A TABLE
                                            // --- READ THE NEXT LINE
                       currentLine = m_fileReader.readLine();
  
                   }
                   // --- ELSE
                   else {
                       headerFound = true;
                   }
                   // --- ENDIF THE LINE IS A COMMENT
               }
               // --- ENDDO FOR EACH FILE HEADER LINE
               
               // --- TRY TO READ HEADER
	       m_importHeaderTokens = currentLine.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
               
               // --- JRW ADD READ THE NEXT LINE
               while((currentLine = m_fileReader.readLine()).trim().length()== 0){
                   System.out.println("++++  Detected blank line!");
               }
               
               String [] entries = currentLine.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
               
  
               
               m_DataArrayList.add(new ImportDataBuffer());
                
               
               // --- DO FOR EACH COLUMN HEADER
               for( String columnImportHeaderNames: m_importHeaderTokens ){
                   if( m_columnNames.contains(columnImportHeaderNames) ){
                       sqlIndex = m_columnNames.indexOf(columnImportHeaderNames );
                       dbColumns.put(genericIndex, sqlIndex);
                       JLabel cName = getLabel(sqlIndex );
                       cName.setForeground(Color.blue);                                                
                       m_objectList[sqlIndex].setText(entries[/*genericIndex*/outerIndex]);
                       
                       // --- TBD SHOULD THE FIRST RECORD BE INSERTED INTO VIEW    
                       m_EligibleColumnIndexes.add(outerIndex/*sqlIndex*/);
                       
                       //m_DataArrayList.get(entryIndex).addItem(entries[entryIndex]);
                       genericIndex++;                         
                   }
                   outerIndex++;
                                    
               }
               
               //do{
               for(Integer x1: m_EligibleColumnIndexes){   
                      System.out.println("x1:"+x1);
                      System.out.println("m_LineEntryIndex:"+m_LineEntryIndex);
                      System.out.println("m_EligibleColumnIndexes.get(entryIndex):"+m_EligibleColumnIndexes.get(entryIndex));       
                      m_DataArrayList.get(m_LineEntryIndex).addItem(entries[m_EligibleColumnIndexes.get(entryIndex)]);                   
                      entryIndex++;
                   }
               
 
                   
             do{
                   currentLine = m_fileReader.readLine();
                   
                   if(currentLine==null){
                       // --- EOF
                       break;
                   }
                   
                   // --- JRW ADD READ THE NEXT LINE
                   if(currentLine.trim().length()== 0){
                     System.out.println("++++  Detected blank line!");
                   }
                   else{
                       //  --- JRW Reset Entry Index Counter
                       entryIndex=0;
                       m_LineEntryIndex++;  
                       
                       entries = currentLine.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
                       m_DataArrayList.add(new ImportDataBuffer());
                       
                       for(Integer x1: m_EligibleColumnIndexes){   
                              System.out.println("x1:"+x1);
                              System.out.println("m_LineEntryIndex:"+m_LineEntryIndex);
                              System.out.println("m_EligibleColumnIndexes.get(entryIndex):"+m_EligibleColumnIndexes.get(entryIndex));       
                              m_DataArrayList.get(m_LineEntryIndex).addItem(entries[m_EligibleColumnIndexes.get(entryIndex)]);                   
                              entryIndex++;
                        }

                   }
               }
               while(true);

            }
            // --- ENDIF THE OPERATOR HAS ENTERED A FILE            
        }
        // --- JRW Add next
        else if ("next".equals( actionCmd )){
            
            populateFields(UP);  // --- This method use data stored in memory
            //showNextRecord();  // --- This method uses data 
                        
        }
        
        else if ("prev".equals( actionCmd )){
            //showPreviousRecord();
            populateFields(DOWN);
            
        }
        
        // --- ENDIF THE RETURN BUTTON IS PRESSED
        }
        catch(Exception exc){
             System.out.println( "File Problem..." + exc.getMessage() );
        }
    }
    // --- END
    
    
    public void populateFields(int aUpDown){
        int entryIndex =0;  

        
        if(((m_currentIndexState + aUpDown) < (m_DataArrayList.size())) && 
           ((m_currentIndexState + aUpDown) >-1)) {        
           
        m_currentIndexState+=aUpDown;    
        
        for(Integer x1: m_EligibleColumnIndexes){   
               m_objectList[dbColumns.get(entryIndex)].setText(m_DataArrayList.get(m_currentIndexState).getM_ArrayList().get(entryIndex));                    
               entryIndex++;
            }
        }
    }
        
    /** 
     * Handle application events 
     */
    public boolean handleAction(ActionEvent e) 
    // --- BGIN
    {
        // --- DECLARE LOCAL VARIABLES
        boolean eventProcessed = true;
        
        // --- IF THE RETURN BUTTON IS PRESSED
        if( "return".equals(e.getActionCommand())) {
           m_appHandle.showMainPanel();
        }
        else if( "next".equals(e.getActionCommand() )){
            showNextRecord();
        }
        else if( "prev".equals(e.getActionCommand() )){
            showPreviousRecord();
        }
        // --- ENDIF THE RETURN BUTTON IS PRESSED
        
        // --- RETURN
        return( eventProcessed );
    }
    // --- END
    
    /** 
     * @brief handle non gui generated events
     */
    public boolean handleEvent( String currEvent )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        boolean   eventProcessed = super.handleEvent( currEvent );
 
        // --- RETURN
        return( eventProcessed );
    }
    // --- END
    
    /** 
     * @brief handle database generated events
     */
    public boolean handleQuery( String currEvent, ResultSet data ) 
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        boolean            eventProcessed = super.handleQuery( currEvent, data );
        ResultSet          schoolData;
        Iterator           entries;
        String             dbValue;
        Integer            dbKey = null;
        String             insertString;
        int                index = 0;
        JFrame             frame          = m_appHandle.getFrame();
        
        // --- IF DATA HAS BEEN RETRIEVED
        if( eventProcessed == true ){
            try {
                // --- DO CASE OF DATA TYPE
                switch( currEvent ){
                    // --- CASE THE SCHOOL DATA IS RETRIEVED
                    case DbEngine.SCHOOL_DATA:
                    case DbEngine.ADMIN_DATA:
                    case DbEngine.SCHOOL_ID:
                    case DbEngine.PEOPLE_DATA:
                    case DbEngine.COURSE_DATA:
                    case DbEngine.CLASS_DATA:
                    case DbEngine.ROLE_DATA:
                    case DbEngine.TYPE_DATA:
                    case DbEngine.MARK_DATA:
                    case DbEngine.GRADE_DATA:
                    case DbEngine.EDUCATOR_DATA:
                    case DbEngine.WATCHER_DATA:
                    case DbEngine.GRADER_DATA:
                    case DbEngine.NOTIFICATION_DATA:
                    case DbEngine.SCHOOL_NAMES:
                    case DbEngine.PEOPLE_NAMES:
                    case DbEngine.COURSE_NAMES:
                    case DbEngine.CLASS_NAMES:
                    case DbEngine.ROLE_NAMES:
                    case DbEngine.USERROLES:
                        // --- RESET
                        removeAll();
                        m_columnNames.clear();
                        m_dataSet = data;
                        m_dataSet.beforeFirst();
                        m_dbMetaData = m_dataSet.getMetaData();
                        m_columnCount = m_dbMetaData.getColumnCount();
                        m_objectList = new JTextField[m_columnCount];
         
                        // --- DO FOR EACH COLUMN
                        for( index=0;index < m_columnCount; index++ ){
                            // --- ADD ROW
                            m_columnNames.add( m_dbMetaData.getColumnLabel(index+1) );
                            m_objectList[index] = new JTextField( "", 20 );
                            addRow( m_dbMetaData.getColumnLabel(index+1), m_objectList[index] );                           
                        }
                        // --- ENDDO FOR EACH COLUMN   
                        
                        // --- ADJUST FRAME
                        frame.pack();                                        
                    break;
                }
                // --- ENDDO CASE OF DATA TYPED       
            }
            catch(Exception e){
                 System.out.println( "SQL Problem..." + e.getMessage() );
                 eventProcessed = false;
            }
        }
        // --- ELSE 
        else {
             System.out.println( "AddSchool::handleQuery  data not handled" );
        }
        // --- ENDIF DATA HAS BEEN RETRIEVED
       
        // --- RETURN
        return( eventProcessed );
    }
    // --- END
    
    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    public void createWin() 
    // --- BEGIN
    {
        //Create and set up the content pane.
        setOpaque(true); //content panes must be opaque

    }   
    // --- END
    
    
    /**
     * @brief display the next record in a current set
     */
    public void showPreviousRecord()
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        boolean rsStatus   = false; 
        int     currentRow = 0;
        int     index      = 0;
        int     dataType   = 0;
        String  dbText;
        Integer dbValue;
         
        try{
            // --- GET ROW DATA
            currentRow = m_dataSet.getRow();
            System.out.println("ImportView::showNextRow #:"+ currentRow );
         
            // --- POINT TO DATA
            rsStatus = m_dataSet.previous();
            
            // --- IF THERE IS AN ERROR
            if( !rsStatus ){
                // --- POINT TO END
                m_dataSet.first();
            }
            // --- ENDIF THERE IS AN ERROR
                 
            // --- IF NEW DATA IS ACQUIRED
            if( rsStatus == true){ 
                // --- DO FOR EACH DATA ITEM IN THE TABLE
                for( index=1; index <= m_columnCount; index++){
                    // --- DETERMINE DATATYPE OF THE COLUMN
                    dataType = m_dbMetaData.getColumnType(index);
                    m_objectList[index-1].setText(""); 
                    
                    // --- IF STRING DATA
                    if( (dataType == Types.VARCHAR) || (dataType == Types.CHAR) ){
                        // --- SET DATA
                        dbText = m_dataSet.getString(m_columnNames.get(index-1));
                        m_objectList[index-1].setText( dbText );
                    }
                    // --- ELSE IF A NUMERIC TYPE
                    else if( dataType == Types.SMALLINT ){
                        // --- SET DATA
                        dbValue = m_dataSet.getInt(m_columnNames.get(index-1));
                        m_objectList[index-1].setText( dbValue.toString() );
                    }                        
                    // --- ENDIF STRING DATA
                }            
                // --- ENDDO FOR EACH DATA ITEM IN THE TABLE
            }         
            // --- ENDIF NEW DATA IS ACQUIRED
        }
        catch(Exception e){
          System.out.println("Error ---"+e.getMessage());
        }      
    }
    // --- END
  
    /**
     * @brief display the next record in a current set
     */
    public void showNextRecord()
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        boolean rsStatus   = false; 
        int     currentRow = 0;
        int     index      = 0;
        int     dataType   = 0;
        String  dbText;
        Integer dbValue;
         
        try{
            // --- GET ROW DATA
            currentRow = m_dataSet.getRow();
            System.out.println("ImportView::showNextRow #:"+ currentRow );
         
            // --- POINT TO DATA
            rsStatus = m_dataSet.next();
            
            // --- IF THERE IS AN ERROR
            if( !rsStatus ){
                // --- POINT TO END
                m_dataSet.last();
            }
            // --- ENDIF THERE IS AN ERROR
                 
            // --- IF NEW DATA IS ACQUIRED
            if( rsStatus == true){ 
                // --- DO FOR EACH DATA ITEM IN THE TABLE
                for( index=1; index <= m_columnCount; index++){
                    // --- DETERMINE DATATYPE OF THE COLUMN
                    dataType = m_dbMetaData.getColumnType(index);
                    m_objectList[index-1].setText(""); 
                    
                    // --- IF STRING DATA
                    if( (dataType == Types.VARCHAR) || (dataType == Types.CHAR) ){
                        // --- SET DATA
                        dbText = m_dataSet.getString(m_columnNames.get(index-1));
                        m_objectList[index-1].setText( dbText );
                    }
                    // --- ELSE IF A NUMERIC TYPE
                    else if( dataType == Types.SMALLINT ){
                        // --- SET DATA
                        dbValue = m_dataSet.getInt(m_columnNames.get(index-1));
                        m_objectList[index-1].setText( dbValue.toString() );
                    }                        
                    // --- ENDIF STRING DATA
                }            
                // --- ENDDO FOR EACH DATA ITEM IN THE TABLE
            }         
            // --- ENDIF NEW DATA IS ACQUIRED
        }
        catch(Exception e){
          System.out.println("Error ---"+e.getMessage());
        }      
    }
    // --- END   
}