/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package schoolhouse.client;

import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.awt.event.WindowListener;
import schoolhouse.common.SchoolApp;
import schoolhouse.model.SchoolPolicy.SchoolRoles;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import schoolhouse.model.SchoolPerson;

/**
 *
 * @author wpeets
 * 
 */
public class ClientApp extends SchoolApp implements Runnable, 
                                                    WindowFocusListener,
                                                    WindowListener {
        
    public ClientApp( SchoolRoles userRole, String userName )
    // --- BEGIN
    {
        // --- SATISFY SUPERCLASS
        super( userRole );
        setUser( userName );
        
        // --- CREATE VIEW
        setAppName( userRole.toString() + "_APP" );
        m_mainView = new MainView( this );
        m_mainView.setOpaque(true);
    }
    // --- END

    public ClientApp( SchoolPerson clientUser )
    // --- BEGIN
    {
        // --- SATISFY SUPERCLASS
        super( clientUser.getRole() );
        setPerson( clientUser );
        
        // --- FORCE USER TO CREATE STUDENT LIST FOR FUTURE USE
        getPerson().getStudentList();
        
        // --- CREATE VIEW
        setAppName( clientUser.getRole().toString() + "_APP" );
        m_mainView = new MainView( this );
        m_mainView.setOpaque(true);
    }
    // --- END

    /**
     *
     * @author wpeets
     * 
     * #brief display user interface
     */

    @Override
    public void run()
    // --- BEGIN
    {
        // --- MAKE MAIN WINDOW VISIBLE
        setHeader( "ACTION" );
        setStatus( "Select an action" );
        m_frame.setDefaultCloseOperation( DISPOSE_ON_CLOSE );
        m_frame.setBounds( 25, 375, 550, 400 );
        m_frame.addWindowFocusListener( this );
        m_frame.addWindowListener( this );
        showMainPanel();
    }
    // --- END

    @Override
    public void windowGainedFocus(WindowEvent e) 
    // --- BEGIN
    {        
        // --- SEND VOICE FOCUS
        setFocus();
    }
    // --- END
   
    
    @Override
    public void windowLostFocus(WindowEvent e) {
        // --- DO NOTHING
    }


    @Override
    public void windowOpened(WindowEvent e)
    {
    }

    @Override
    public void windowClosing(WindowEvent e)
    // --- BEGIN
    {
    }
    // --- END
    
    @Override
    public void windowClosed(WindowEvent e)
    // --- BEGIN
    {
        // --- INFORM THE DASH?
        System.out.println( "ClientApp::windowClosed" );
        getActiveDash().clearRole( getRole() );
    }
    // --- END
    

    @Override
    public void windowIconified(WindowEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowActivated(WindowEvent e)
    // --- BEGIN
    {
    }
    // --- END
    
    
    @Override
    public void windowDeactivated(WindowEvent e)
    {
    }
}
