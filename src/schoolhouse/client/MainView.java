/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package schoolhouse.client;

import schoolhouse.common.SchoolApp;
import schoolhouse.model.SchoolPolicy.AppActions;
import schoolhouse.model.SchoolPolicy.SchoolActions;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.sql.ResultSet;
import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JFrame;
import schoolhouse.common.AppCommands;
import schoolhouse.common.DbEngine;
import schoolhouse.common.QueryThread;
import schoolhouse.common.SchoolDbIntfc;
import schoolhouse.common.SchoolDbIntfc.SchoolQuery;
import schoolhouse.model.SchoolPerson;
import schoolhouse.model.SchoolPolicy;
import schoolhouse.model.SchoolPolicy.SchoolRoles;

/**
 *
 * @author wpeets
 */
public class MainView extends DbEngine {
    protected JButton[]          m_actionList;   
    protected AddController      m_addEngine;
    protected RemoveController   m_removeEngine;
    protected ChangeController   m_changeEngine;
    protected DbEngine           m_viewEngine;
    protected ImportController   m_importEngine;
    protected ExportController   m_exportEngine;
    
    /**
     *
     * @param appHandle
     * @brief constructor for main view
     */
    public MainView( SchoolApp appHandle )
    // --- BEGIN
    {
        // --- SATISFY SUBCLASS
        super( appHandle );
        
        // --- DECLARE LOCAL VARIABLES
        boolean            actionState = true;
        int                cmdWidth    = 150;
        int                count       = 0;
        QueryThread        userQuery;
        SchoolQuery        dataQuery   = SchoolDbIntfc.SchoolQuery.USER_ID;
        GridBagConstraints c           = new GridBagConstraints();

        // --- INITIALIZE CLASS VARIABLE
        m_actionList = new JButton[SchoolActions.getSize() + AppActions.getSize()];

        // --- CREATE ENGINES
        m_addEngine    = new AddController( m_appHandle );
        m_removeEngine = new RemoveController( m_appHandle );
        m_changeEngine = new ChangeController( m_appHandle );
        m_viewEngine   = new ViewController( m_appHandle );
        m_importEngine = new ImportController( m_appHandle );
        m_exportEngine = new ExportController( m_appHandle );
               
        // --- CREATE TOP LEVEL ACTIONS
        m_parentFrame = appHandle.getFrame();
        setLayout( new GridBagLayout() );  
        m_parentFrame.setLocation( 100, 300 );
        
        // --- QUERY FOR USER INFORMATION
        userQuery = new QueryThread( m_appHandle );
        userQuery.setHandler( this );
        
        // --- ADD QUERY TO THE MAP
        dataQuery.dbQueryString( m_appHandle.getUser(), 0 );
        userQuery.setQuery( dataQuery.dbQuery(), dataQuery.dbName() );
            
        // --- RUN
        userQuery.start();
        
        // --- DO FOR EACH ACTION
        for( SchoolActions currAction: SchoolActions.values() ){
            // --- IF NOT LAST ITEM
            if( currAction != SchoolActions.UNKNOWN ) {
                // --- CREATE BUTTON
                c.gridx      = 0;
                c.gridy      = count;
                c.fill       = GridBagConstraints.BOTH;
                c.gridheight = 1;
                c.gridwidth  = 1;
                m_actionList[count] = new JButton( currAction.name() );
                m_actionList[count].setVerticalTextPosition( AbstractButton.CENTER );
                m_actionList[count].setHorizontalTextPosition(AbstractButton.LEADING);
                m_actionList[count].setActionCommand( currAction.name() );
                m_actionList[count].addActionListener( this );
                add( m_actionList[count], c );
                
                // --- GET ACTION STATE
                actionState = m_appHandle.hasAccessState( currAction, null );
                
                // --- IF ROLE HAS NO ACCESS FOR ACTION
                if( actionState == false ){
                    // --- DISABLE ACTION
                    m_actionList[count].setEnabled( false );
                }
                // --- ELSE
                else{
                    // --- ADD HINT
                    m_engineHints.addHint( currAction.name(), currAction.getValue()+ACTION_OFFSET );
                }
                // --- ENDIF ROLE HAS NO ACCESS FOR ACTION
            }
            // --- ENDIF NOT LAST ITEM
            
            // --- INCREMENT
            count++;
        }
        // --- ENDDO FOR EACH ACTION
        
        // --- DO FOR EACH ACTION
        for( AppActions guiAction: AppActions.values() ){
            // --- CREATE BUTTON
            c.gridx      = 0;
            c.gridy      = count;
            c.fill       = GridBagConstraints.BOTH;
            c.gridheight = 1;
            c.gridwidth  = 1;
            m_actionList[count] = new JButton( guiAction.name() );
            m_actionList[count].setVerticalTextPosition( AbstractButton.CENTER );
            m_actionList[count].setHorizontalTextPosition(AbstractButton.LEADING);
            m_actionList[count].setSize( cmdWidth, 0 );
            m_actionList[count].setActionCommand( guiAction.name() );
            m_actionList[count].addActionListener( this );
            add( m_actionList[count], c );
            m_engineHints.addHint( guiAction.name(), 
                                   guiAction.getValue() + FORM_OFFSET );                                       
            // --- INCREMENT
            count++;
        }
        // --- ENDDO FOR EACH ACTION
        
        // --- PROVIDE HINTS TO VOICE API
        setId( m_appHandle.getAppName() );
        updateHints();
    }
    // --- END

    /**
     *
     * @brief handle top level action
     * @param userAction gui event
     */
    @Override
    public void actionPerformed( ActionEvent userAction )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolActions currentAction;
        int           count       = 0;
        String        actionVal   = userAction.getActionCommand();
        boolean       actionFound = false;
        
        // --- DO FOR EACH ACTION
        for( AppActions guiAction: AppActions.values() ){
            // --- IF ACTION IS TRIGGERED
            if( guiAction.name().equals(actionVal) ){
                // --- HANDLE APP ACTION
                actionFound = true;
                handleEvent( actionVal );
            }           
        }
        // --- ENDDO FOR EACH ACTION
        
        // --- IF THE ACTION IS NOT FOOUND
        if( actionFound == false ){
            // --- DETERMINE ACTION
            // --- ...TBD handle invalid action
            currentAction = SchoolActions.valueOf( userAction.getActionCommand() );
            runAction( currentAction );
        }
        // --- ENDIF THE ACTION IS NOT FOOUND
    }
    // --- END
    
    
    /**
     * communicates commands from the GUI interface 
     *
     * @param event
     * @return 
     */
    @Override
    public boolean handleAction( ActionEvent event ) 
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        boolean eventState = true;
        
        // --- PASS IT ON
        actionPerformed( event );
        
        // --- RETURN
        return( eventState );
    }
    // --- END
    
    
    @Override
    public boolean handleEvent( String currEvent ) 
    // --- BEGIN
    {
        // --- DECLARE LocAL VARIABLES
        boolean returnState = true;
        
        // --- DO CASE OF EVENT
        switch( currEvent ){
            case "Focus":
                // --- SEND VOICE FOCUS
                updateState( AppCommands.CommandValue.Focus );
            break;
            case "RETURN":
                // --- KILL CLIENT
                m_appHandle.getActiveDash().clearRole( m_appHandle.getRole() );
                m_appHandle.closeMainPanel();
            break;
            case "EXIT":
                // --- SEND VOICE FOCUS
                m_appHandle.getActiveDash().clearRole( m_appHandle.getRole() );
                m_appHandle.closeMainPanel();
            break;
            default:
            break;    
        }
        // --- ENDDO CASE OF EVENT
        
        // --- RETURN
        return( returnState );    
    }
    // --- END
    
    
    @Override
    public boolean handleQuery( String currEvent, ResultSet resultSet )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        Integer      personId;
        String       orgId;
        String       lastName;
        String       firstName;
        String       middleName;
        String       nickName;
        String       title;
        String       email;
        String       address;
        String       phone;
        String       userName;
        Integer      dataVal;
        SchoolRoles  userRole;
        SchoolPerson personInfo;
        boolean      userFound = false;
        
        try {
            // --- DO CASE OF QUERY TYPE
            switch( currEvent ){
                // --- CASE THE SCHOOL DATA IS RETRIEVED
                case DbEngine.USER_DATA:
                    // --- DO FOR EACH PERSON
                    while( resultSet.next() && (userFound == false) ){
                        // --- SAVE NAME IN SCHOOL LIST
                        personId   = resultSet.getInt( "participant_id" );
                        orgId      = resultSet.getString( "org_id" );
                        lastName   = resultSet.getString( "last_name" );
                        firstName  = resultSet.getString( "first_name" );
                        middleName = resultSet.getString( "middle_name" );
                        nickName   = resultSet.getString( "nick_name" );
                        title      = resultSet.getString( "title" );
                        dataVal    = resultSet.getInt( "role_id" );
                        userRole   = SchoolPolicy.SchoolRoles.fromValue( dataVal );
                        email      = resultSet.getString( "email" );
                        address    = resultSet.getString( "address" );
                        phone      = resultSet.getString( "phone" );
                        userName   = resultSet.getString( "username" );
                        dataVal    = resultSet.getInt( "user_status" );
                        
                        // --- IF THE PERSON MATCHES
                        if( (userName.equals( m_appHandle.getUser())) &&
                            (userRole == m_appHandle.getRole()      ) ){
                            // --- SAVE INFO
                            // --- ... TBD do not overwrite role or name
                            System.out.println( "MainView::handleQuery Found" ); 
                            userFound  = true;
                            personInfo = m_appHandle.getPerson();
                            personInfo.setId( personId );
                            personInfo.setOrgAssigned( orgId );
                            personInfo.setLastName( lastName );
                            personInfo.setFirstName( firstName );
                            personInfo.setNickName( nickName );
                            personInfo.setMiddleName( middleName );
                            personInfo.setTitle( title );
                            personInfo.setEmail( email );
                            personInfo.setPhone( phone );
                            personInfo.setAddress( address );
                            personInfo.setUserStatus( dataVal );
                        }
                        // --- ENDIF THE PERSON MATCHES        
                    }
                    // --- ENDDO FOR EACH COURSE ID
                break;
                default:
                    // --- DO NOTHING
                break;
            }
            // --- ENDDO CASE OF QUERY TYPE
        }
        catch(Exception e){
            // --- IF THE DATA IS EMPTY
            if( e.getMessage() == "null" ){
                // --- LET IT PASS
                userFound = false;
            }
            // --- ELSE
            else {
                System.out.println( "SQL Problem..." + e.getMessage() );
                userFound = false;
            }
        }
        
        // --- RETURN
        return( userFound );
    }
    // --- END

    
    @Override
    public void handleCmd(String cmd, String cmdData, int cmdKey) {
        // --- DECLARE LOCAL VARIABLES
        SchoolActions userAction;
        int           actionVal;
        System.out.println("MainView::handleCmd[" + cmd + "]" );
        
        // --- IF THE COMMAND IS A MAiN WiNDOW COMMAND
        if( cmdKey > 9000 ){
        } else if( cmdKey > 9000 ){
        } else if( cmdKey > 8000 ){
        } else if( cmdKey > 7000 ){
        } else if( cmdKey > 6000 ){
        } else if( cmdKey > 1000 ){
            // --- CONVERT TO AN ACTION FORM_ORRSET
            // --- ... TBD probably need to handle specialized add here as well
            actionVal = cmdKey - ACTION_OFFSET;
            userAction = SchoolActions.fromValue( actionVal );
            runAction( userAction );            
        }
        // --- ELSE
        else{
            // --- DO CASE OF EVENT
            switch( cmdKey ){
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                    m_addEngine.handleCmd( cmd, cmdData, cmdKey );
                break;
                case 10000:
                     System.out.println( "MainView::handleCmd  Acknowledge received" );
                break;   
                default:
                break;    
            }
            // --- ENDDO CASE OF EVENT
        }
        // --- ENDIF THE COMMAND IS A MAiN WiNDOW COMMAND

    }
    // --- END
/**
 *
 * @param actionToRun
 * @brief handle top level action
 */
    public void runAction( SchoolActions actionToRun )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        Container actionArea = m_appHandle.getActionPane();
        Container workArea   = m_appHandle.getWorkPane();
        Container cmdArea    = m_appHandle.getCmdPane();
        JFrame    frame      = m_appHandle.getFrame();
        String    actionName = actionToRun.name();
        DbEngine  currEngine = null;

        // --- DO CASE OF ACTION
        switch( actionToRun ){
            // --- CASE ADD
            case ADD:
                // --- SET ADD PANE
                currEngine = m_addEngine;
            break;
            case REMOVE:
                // --- SET REMOVE PANE
                currEngine = m_removeEngine;
            break;
            // --- CASE VIEW, CHANGE
            case VIEW:
                m_appHandle.setStatus( "Action not supported" );
            break;
            // --- CASE VIEW, CHANGE
            case CHANGE:
                currEngine = m_changeEngine;
            break;
            // --- CASE IMPORT
            case IMPORT:
                // --- SET IMPORT PANE
                currEngine = m_importEngine;
            break;
            // --- CASE EXPORT
            case EXPORT:
                // --- SET EXPORT PANE
                currEngine = m_exportEngine;
            break;   
            case MESSAGE:
            case UNKNOWN:
            default:
                m_appHandle.setStatus( "Action: " + actionName + "not supported" );
            break;
        }
        // --- ENDDO CASE OF ACTION    
        
        // --- IF AN ENGiNE IS POVIDED
        if( currEngine != null ){
            // --- SET PANE
            m_appHandle.setHeader( actionName );
            actionArea.removeAll();
            workArea.removeAll();
            cmdArea.removeAll();
            m_appHandle.setStatus( "Select an object to " + actionName.toLowerCase() );
            
            // --- ACTIVATE VIEW
            currEngine.createWin();
            currEngine.initialize();
            actionArea.add( currEngine );
            frame.setResizable( true );
            frame.pack();
            m_appHandle.setFocus();
        }
        // --- ENDIF AN ENGINE IS POVIDED
    }
    // --- END
}
