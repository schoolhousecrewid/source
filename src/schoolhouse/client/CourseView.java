/*
 * File:  AddCourse.java
 * Description: Provides a panel that allows for the entry of 
 *              course and class data
 *
 */

package schoolhouse.client;

import java.awt.Color;
import schoolhouse.common.SchoolDbIntfc;
import schoolhouse.common.QueryChain;
import schoolhouse.model.SchoolCourse;
import schoolhouse.model.SchoolHouse;
import schoolhouse.model.SchoolPerson;
import schoolhouse.common.SchoolApp;
import schoolhouse.model.DataObject.DataState;
import schoolhouse.model.SchoolClass;
import schoolhouse.model.SchoolPolicy.SchoolObjects;
import schoolhouse.model.SchoolPolicy.SchoolRoles;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import schoolhouse.common.DbEngine;
import java.util.Calendar;
import java.util.GregorianCalendar;
import schoolhouse.common.AppCommands;
import schoolhouse.common.HandleObjectAction;
import schoolhouse.common.SchoolDbIntfc.SchoolQuery;
import schoolhouse.common.SchoolModel;

/**
 * 
 * @author wpeets
 * @brief This class provides the capability to add a new course and classes to the database
 * 
 */
public class CourseView extends DbEngine implements ActionListener {
    protected JComboBox               m_schoolList;
    protected JComboBox               m_courseList;
    protected JTextField              m_courseId;
    protected JTextField              m_courseOrgId;
    protected JTextField              m_courseCredits;
    protected JTextField              m_schoolId;
    protected JTextField              m_courseName;
              ArrayList<SchoolCourse> m_currentCourseList;
              SchoolCourse            m_currentCourse;
    protected HandleObjectAction      m_objectHandler;
    private   final static String     m_newline = "\n"; 
              
    /**
     * list of educators to be added to class
     */
    protected ArrayList <SchoolPerson> m_classEducatorList;

    /**
     * list of graders to be added to class
     */
    protected ArrayList <SchoolPerson> m_classGraderList;
    
    /**
     * list of watcher to be added to parent
     */
    protected ArrayList <SchoolPerson> m_classStudentList;
    
    /**
     * constructor
     * 
     * @author wpeets
     */
    public CourseView( SchoolApp appHandle  )
    // --- BEGIN
    {
        // --- INITIALIZE CLASS
        super( appHandle );
        m_currentCourseList = null;
        m_currentCourse     = null;
        m_classEducatorList = new ArrayList<SchoolPerson>();
        m_classGraderList   = new ArrayList<SchoolPerson>();
        m_classStudentList  = new ArrayList<SchoolPerson>();
        
        // --- CREATE VIEW
        createView();
    }
    // --- END
        
    /**
     * @brief create graphical components for view
     * 
     * @author wpeets
     */
    public void createView(  )
    // --- BEGIN
    {        
        // --- CREATE ADD ACTIONS
        m_schoolList = new JComboBox( );
        m_schoolList.setActionCommand( "schoollist" );
        m_schoolList.addActionListener( this );
        addRow( "School List", m_schoolList );

        m_courseList = new JComboBox( );
        m_courseList.setActionCommand( "courselist" );
        m_courseList.addActionListener( this );
        addRow( "Course List", m_courseList );

        m_courseId = new JTextField( "Course Id");
        m_courseId.setToolTipText( "Automatic field" );
        m_courseId.setBackground( Color.gray );
        m_courseId.setEditable( false );
        m_courseId.addActionListener( this );
        m_courseId.setColumns( 4 );
        addRow( "Course Id", m_courseId );

        m_courseOrgId = new JTextField( "School Course Id");
        m_courseOrgId.setToolTipText( "Automatic field" );
        m_courseOrgId.addActionListener( this );
        m_courseOrgId.setColumns( 6 );
        addRow( "School Course Id", m_courseOrgId );

        m_courseName = new JTextField( "Course Name" );
        m_courseName.setActionCommand("id");
        m_courseName.setToolTipText("Click this to enter address");
        m_courseName.addActionListener(this);
        m_courseName.setColumns( 20 );
        addRow( "Course Name", m_courseName );
        
        m_courseCredits = new JTextField( "Course Credits" );
        m_courseCredits.setActionCommand("credits");
        m_courseCredits.setToolTipText("Click this to enter credits for course");
        m_courseCredits.addActionListener(this);
        m_courseCredits.setColumns( 3 );
        addRow( "Course Credits", m_courseCredits );
    }
    // --- END
     
    /**
     * @brief initial action on creating action 
     */
    public void initialize( HandleObjectAction objectHandler )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolModel appModel    = m_appHandle.getModel();
        Vector      dataRequest = new Vector();
        
        // --- MAKE DATABASE QUERIES NECESSARY FOR ADDING ADDING PARTICIPANT
        dataRequest.add( SchoolDbIntfc.SchoolQuery.ROLE );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.PARTICIPANT  );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.SCHOOL );
        dataRequest.add( SchoolDbIntfc.SchoolQuery.COURSE );
        appModel.getData( dataRequest );
                
        // --- SAVE STATE
        m_objectHandler = objectHandler;
        setId( m_appHandle.getAppName() + "["
                                       + SchoolObjects.COURSE.toString()+ "]" );
        
        // --- CREATE INITIAL HINTS
        m_engineHints.addHint( "School List",   1  + FORM_OFFSET );
        m_engineHints.addHint( "Educator List", 2  + FORM_OFFSET );
        m_engineHints.addHint( "Grader List",   3  + FORM_OFFSET );
        m_engineHints.addHint( "Course List",   4  + FORM_OFFSET );
        m_engineHints.addHint( "Class List",    5  + FORM_OFFSET );
        m_engineHints.addHint( "Student List",  6  + FORM_OFFSET );
        m_engineHints.addHint( "Up",            7  + FORM_OFFSET );
        m_engineHints.addHint( "Down",          8  + FORM_OFFSET );
        m_engineHints.addHint( "Id",            9  + FORM_OFFSET );
        m_engineHints.addHint( "Start Date",    10 + FORM_OFFSET );
        m_engineHints.addHint( "End Date",      11 + FORM_OFFSET );
        m_engineHints.addHint( "Time",          12 + FORM_OFFSET );
        m_engineHints.addHint( "Duration",      13 + FORM_OFFSET );
        m_engineHints.addHint( "Clear",         14 + FORM_OFFSET );
        m_engineHints.addHint( "Name",          15 + FORM_OFFSET );
        m_engineHints.addHint( "Credits",       16 + FORM_OFFSET );
        
        // --- SEND HINTS
        m_appHandle.setActiveEngine( this );
        updateHints();
        m_appHandle.setFocus();
   }
    // --- END
    

    /**
     * @brief handle actions from GUI events 
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        // --- DECLARE LOCAL VARIABLES
        boolean      courseFound = false;
        boolean      dataFound = false;
        Integer      courseValue = 0;
        Integer      courseEventValue = 0;
        Integer      eventId = 0;
        Integer      courseId = 0;
        String       courseOrgId;
        Integer      classId;
        int          schoolValue = 0;
        Integer      schoolId;
        int          schoolIndex = 0;
        Integer      dbValue = 0;
        String       currentSchool;
        String       currentCourse;
        String       currentCourseEvent;
        String       nameEntry;
        String       dbStringVal;
        String       insertString;
        String       formString;
        String       actionCmd = e.getActionCommand();
        ResultSet    courseSet = (ResultSet)m_dataMap.get( DbEngine.COURSE_DATA );
        ResultSet    classSet;
        boolean      courseState = false;
        boolean      classState  = false;
        SchoolHouse  schoolInfo;
        SchoolCourse courseInfo;
        SchoolCourse newCourse;
        SchoolClass  classInfo;
        SchoolClass  newClass;
        ArrayList<SchoolClass> currentClassList;
        boolean      getClasses = false;
        SchoolQuery  classQuery  = SchoolDbIntfc.SchoolQuery.SCHOOLCLASS;
        String       queryName;
        Integer      courseCredits;
        SchoolPerson personInfo;
        String       personName;
        System.out.println( "AddCourse::actionPerformed Cmd: " + actionCmd );
        String     classStartDate;
        String     classEndDate;
        Integer    classDuration;
        java.sql.Date sqlStartDate;
        java.sql.Date sqlEndDate;
        java.sql.Time classTime;
        java.util.Date newTime;
        int        index;
        
        try {
            // --- IF THE RETURN BUTTON IS PRESSED
            if( null != actionCmd ) 
                switch( actionCmd ){
                // --- CASE TERMINATE COuRSE ACTION
                case "return":
                    m_appHandle.showMainPanel();
                break;
                // --- CASE CLEAR FORM
                case "clear":
                    clearForm();
                break;
                // --- CASE ADD OBJECT
                case "submit":
                    // --- MAKE SURE COURSE FORM IS COMPLETE
                    courseState = validCourse();
                    
                    // --- IF THE COURSE DATA IN THE FORM IS COMPLETE
                    if( courseState == true ){
                        // --- EVALUATE EVENT DATA IN THE FORM
                        currentSchool = (String)m_schoolList.getSelectedItem();
                        schoolInfo = m_appHandle.getModel().getSchool( currentSchool );
                        
                        // --- ADD TO DATABASE
                        newCourse = new SchoolCourse( m_appHandle );
                        newCourse.setOrgAssigned( m_courseOrgId.getText() );
                        courseCredits = new Integer( m_courseCredits.getText() );
                        newCourse.setCredits( courseCredits );
                        newCourse.setName( m_courseName.getText() );
                        newCourse.setSchool( schoolInfo.getId() );
                        
                        // --- IF THE COURSE IS SET
                        if( m_currentCourse != null ){
                            // --- SET ID
                            newCourse.setId( m_currentCourse.getId() );
                        }
                        // --- ENDIF THE COURSE IS SET
                            
                        // --- HANDLE SUBMIT EVENT
                        m_objectHandler.handleObject( newCourse );                 
                    }
                    // --- ELSE
                    else {
                        // --- HANDLE ERROR
                        m_appHandle.setStatus( "Info: course data problem" );
                    }
                    // --- ENIF THE COURSE DATA IN THE FORM IS COMPLETE
                break;
                case "courselist":
                    currentSchool = (String)m_schoolList.getSelectedItem();
                    schoolInfo = m_appHandle.getModel().getSchool( currentSchool );
                    currentCourse = (String)m_courseList.getSelectedItem();
                    m_currentCourse = schoolInfo.getCourse( currentCourse );
                    courseCredits = m_currentCourse.getCredits();
                    courseId = m_currentCourse.getId();
                    courseOrgId = m_currentCourse.getOrgAssignedId();
                    m_courseId.setText( courseId.toString() );
                    m_courseOrgId.setText( courseOrgId );
                    m_courseName.setText( currentCourse );
                    m_courseCredits.setText( courseCredits.toString() );                    
                break;
                case "schoollist":
                    // --- FILL COURSE LIST FOR SCHOOL
                    currentSchool = (String)m_schoolList.getSelectedItem();
                    //schoolId = (Integer)m_schoolIndex.elementAt( schoolIndex );
                    schoolInfo = m_appHandle.getModel().getSchool( currentSchool );
                    schoolId = schoolInfo.getId();
                    m_currentCourseList = schoolInfo.getCourseList();
                    Iterator courseIter = m_currentCourseList.iterator();
                    m_courseList.removeActionListener( this );
                    m_courseList.removeAllItems();
                    System.out.println("Course List: " + currentSchool );
                    
                    // --- DO FOR EACH COURSE IN THE MODEL
                    while( courseIter.hasNext( ) ) {
                        // --- GET DATA
                        courseInfo = (SchoolCourse)courseIter.next( );
                        m_courseList.addItem( courseInfo.getName() );                               
                        System.out.println("Course List: " + courseInfo.getName() );
                        
                        // --- IF DATA IS NOT ACQUIRED
                        if( courseInfo.getState() == DataState.UNKNOWN ){
                            getClasses = true;                              
                        }
                    }   // --- ENDDO FOR EACH COURSE IN THE
                    m_courseList.addActionListener( this );
                    
                    // --- IF CLASS DATA IS REQUIRED
                    if( getClasses == true ){
                        // --- GET CLASS DATA FOR SCHOOL
                        m_dbChain = new QueryChain( m_appHandle, true );
                        m_dbChain.setHandler( m_appHandle.getModel() );
                        classQuery.dbQueryId( schoolId, 0 );
                        queryName = classQuery.dbName() + "_" + schoolId.toString();
                        m_dbChain.addQuery( classQuery.dbQuery(), queryName );
                        m_dbChain.start();                     
                    }   
                break;
            }
        }
        catch( Exception err ){
            System.out.println("SQL Problem..."+err.getMessage());
        }        
    }

    /** 
     * @brief determine if the form has valid course data
     */
    public boolean validCourse()
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        boolean dataValid = true;
    
        // --- IF ANY FIELD IS BLANK
        if( m_courseName.equals("") ){
            // --- COURSE CANNOT BE ADDED
            dataValid = false;
        }    
        // --- ENDIF ANY FIELD IS BLANK
        
        // -- RETURN STATE 
        return( dataValid );
    }
    // --- END
    

    /** 
     * @brief determine if the course data exists
     */
    public boolean existsCourse()
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolModel            dataModel  = m_appHandle.getModel();
        ArrayList<SchoolHouse> schoolList = dataModel.getSchoolList();
        String                 nameEntry;
        String                 courseName = (String)m_courseName.getText();
        Iterator               schoolIter = schoolList.iterator();
        SchoolHouse            schoolInfo;
        SchoolCourse           courseInfo;
        boolean                dataFound  = false;
        
        // --- DO FOR EACH SCHOOL IN THE MODEL
        while( schoolIter.hasNext( ) && (dataFound == false) ){
            // --- GET DATA
            schoolInfo = (SchoolHouse)schoolIter.next( );
            ArrayList<SchoolCourse> courseList = schoolInfo.getCourseList();
            Iterator  courseIter = courseList.iterator();
            
            // --- DO FOR EACH COURSE IN THE SCHOOL
            while( courseIter.hasNext( ) && (dataFound == false) ){
                // --- GET DATA
                courseInfo = (SchoolCourse)courseIter.next( );
                
                // --- IF THERE IS A MATCH
                if( courseInfo.getName().equals(courseName) ){
                    // --- SET RETURN VALUE
                    // --- ... TBD need to break out
                    dataFound = true;
                }
                // --- ENDIF THERE IS A MATCH           
            }
            // --- ENDDO FOR EACH COURSE IN THE SCHOOL
        }  
        // --- ENDDO FOR EACH SCHOOL IN THE MODEL
        
        // --- RETURN
        return( dataFound );
    }
    // --- END

 
    /** 
     * @brief handle non gui generated events
     */
    @Override
    public boolean handleEvent(String currEvent) 
    // --- BEGIN
    {
        // --- DECLARE LocAL VARIABLES
        boolean returnState = true;
        
        // --- DO CASE OF EVENT
        switch( currEvent ){
            case "Focus":
                // --- SEND VOICE FOCUS
                updateState( AppCommands.CommandValue.Focus );
            break;
            default:
            break;    
        }
        // --- ENDDO CASE OF EVENT
        
        // --- RETURN
        return( returnState );    
    }
    // --- END


    /** 
     * @brief handle database generated events
     */
    public boolean handleQuery( String currEvent, ResultSet data )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        boolean                 eventProcessed = super.handleQuery( currEvent, data );
        Iterator                entries;
        String                  dbValue;
        Integer                 schoolId  = 0;
        Integer                 schoolKey = 0;
        Integer                 eventId   = 0;
        Integer                 courseId  = 0;
        SchoolHouse             schoolInfo;
        SchoolPerson            personInfo;
        ArrayList<SchoolHouse>  schoolList;
        ArrayList<SchoolPerson> peopleList;
        SchoolRoles             personRole;

        // --- IF DATA HAS BEEN RETRIEVED
        if( eventProcessed == true ){
            try {
                // --- DO CASE OF DATA
                switch( currEvent ){
                    // --- CASE COURSE DATA IS RETRIEVED
                    case DbEngine.COURSE_DATA:
                        // --- NO ACTION FOR NOW UNTIL SCHOOL IS CHOSEN
                    break;
                    // --- CASE COURSE DATA IS RETRIEVED
                    case DbEngine.SCHOOL_DATA:
                        // --- POPULATE SCHOOL COMBO BOX
                        m_schoolList.removeActionListener( this );
                        m_schoolList.removeAllItems();
                        schoolList = m_appHandle.getModel().getSchoolList();
                        Iterator schoolIter = schoolList.iterator();

                        // --- DO FOR EACH SCHOOL
                        while( schoolIter.hasNext() ){
                            // --- ADD NAME TO LIST
                            schoolInfo = (SchoolHouse)schoolIter.next( );
                            m_schoolList.addItem( schoolInfo.getName() );                               
                        }
                        // --- ENDDO FOR EACH SCHOOL
                        m_schoolList.addActionListener( this );
                    break;
                    default:
                    break;
                }
                // --- ENDDO CASE OF DATA
            }
            catch(Exception e){
                 System.out.println( "SQL Problem..." + e.getMessage() );
                 eventProcessed = false;
            }
        }
        // --- ELSE 
        else {
             System.out.println( "AddCourse::handleQuery  data not handled" );
        }
        // --- ENDIF DATA HAS BEEN RETRIEVED        
        
        // --- RETURN
        return( eventProcessed );
    }
    // --- END

  /**
     * @brief communicates commands from the schoolhouse interface 
     *
     */
    @Override
    public void handleCmd( String cmd, String cmdData, int cmdKey )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        int         totalItems;
        int         newItem;
        int         schoolId;
        JComboBox   currentList = null;
               
        // --- SET STATUS AREA
        m_appHandle.setStatus( "Command: " + cmd + ":" + cmdData );
        
        // --- IF THE COMMAND IS A FORM COMMAND
        if( cmdKey > FORM_OFFSET ){
            // --- DO CASE OF COMMAND
            switch( cmdKey ){
                case 9001:
                    m_schoolList.requestFocus();
                break;
                case 9002:
                    m_courseList.requestFocus();
                break;
                // --- CASE UP LIST
                case 9003:
                    if( m_schoolList.hasFocus() == true ){
                        currentList = m_schoolList;
                    }
                    if( m_courseList.hasFocus() == true ){
                        currentList = m_courseList;
                    }
                    totalItems = currentList.getItemCount();
                    newItem = currentList.getSelectedIndex();
                    newItem++;
                    if( newItem > totalItems){
                        newItem = 0;
                    }
                    currentList.setSelectedIndex(newItem);
                break;
                // --- CASE DOWN LIST
                case 9008:
                    if( m_schoolList.hasFocus() == true ){
                        currentList = m_schoolList;
                    }
                    if( m_courseList.hasFocus() == true ){
                        currentList = m_courseList;
                    }
                    totalItems = currentList.getItemCount();
                    newItem = currentList.getSelectedIndex();
                    newItem--;
                    if( newItem == 0){
                        newItem = totalItems;
                    }
                    currentList.setSelectedIndex( newItem );
                break;
                // --- CASE NAME
                case 9009:
                    m_courseOrgId.requestFocus();
                break;
                case 9014:
                    clearForm();
                break;
                default:
                break;
            }
            // --- ENDDO CASE OF COMMAND
        }
        // --- ELSE
        else if( cmdKey > SCHOOL_OFFSET ){
            // --- SET SCHOOL
            //schoolId = cmdKey - SCHOOL_OFFSET;
            //schoolInfo = m_appHandle.getModel().getSchool(schoolId);
            //m_schoolList.setSelectedItem( schoolInfo.getName() );
        }
        // --- ENDIF THE COMMAND IS A FORM COMMAND
    }
    // --- END
    
      
    /**
     * @brief clear input from user
     */
    public void clearForm()
    // --- BEGIN
    {
        // --- DECLARE LOCAL VALUES
        Calendar c = new GregorianCalendar();
        
        // --- CLEAR FIELDS
        m_courseId.setText( "" );
        m_courseOrgId.setText( "" );
        m_courseName.setText( "" );
        m_courseCredits.setText( "" );
    }
    // --- END
   
    
    /**
     * @brief disable input from user
     */
    public void disableForm()
    // --- BEGIN
    {
        // --- DECLARE LOCAL VALUES
        Calendar c = new GregorianCalendar();
        
        // --- CLEAR FIELDS
        m_courseId.setText( "" );
        m_courseOrgId.setText( "" );
        m_courseOrgId.setEditable( false );
        m_courseOrgId.setBackground( Color.gray );        
        m_courseName.setText( "" );
        m_courseName.setEditable( false );
        m_courseName.setBackground( Color.gray );        
        m_courseCredits.setText( "" );
        m_courseCredits.setEditable( false );
        m_courseCredits.setBackground( Color.gray );        
    }
    // --- END


    /**
     * @brief enable input from user
     */
    public void enableForm()
    // --- BEGIN
    {
        // --- CLEAR FIELDS
        m_courseOrgId.setEditable( false );
        m_courseOrgId.setBackground( Color.WHITE );        
        m_courseName.setEditable( false );
        m_courseName.setBackground( Color.WHITE );        
        m_courseCredits.setEditable( false );
        m_courseCredits.setBackground( Color.WHITE );        
    }
    // --- END
    
    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    public void createWin() {
        //Create and set up the content pane.
        setOpaque(true); //content panes must be opaque

    }    
    
}
