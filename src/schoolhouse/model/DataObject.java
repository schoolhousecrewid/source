/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package schoolhouse.model;

/**
 *
 * @author wpeets
 */
public class DataObject {
    /**
     * @brief state of classes in course
     * 
     * If data is acquired and the course has no classes, the course will be 
     * marked
     */
    public enum DataState { UNKNOWN(1), PARTIAL(2), HASDATA(3),  NODATA(4);

        private final int m_value;
        private static final int enumSize = DataState.values().length;

        /**
         * @brief constructor
         * 
         * This initializes values
         */
        private DataState( int value )
        // --- BEGIN
        {
            // --- SET VALUE
            this.m_value = value;
        }
        // --- END

        /**
         * @brief size of enum
         * 
         * returns the size of enum
         */
        static int getSize( ) {
            return( enumSize );
        } 

        /**
         * @brief get int cast of enum
         * 
         * @return the ordinal representation of enum
         */
        public int getValue()
        // --- BEGIN
        {
            // --- RETURN INTEGER REPRESENTATION
            return( m_value );
        }       
        // --- END


        /**
         * @brief allow conversion from ordinal value to enum object
         * 
         * This takes a value and returns an enumeration representation.
         */
        static DataState fromValue( int value )
        // --- BEGIN
        { 
            // --- DECLARE LOCAL VVARIABLES
            DataState enumRep = null;

            // --- DO WHILE REPRESENTATION NOT FOUND
            for( DataState enumVal: DataState.values() ) { 
                // --- IF ORDINAL VALUE MATCHES
                if( enumVal.m_value == value) { 
                    // --- RETURN ENUM
                    return enumVal;  
                }  
                // --- ENDIF ORDINAL VALUE MATCHES
            }  
            // --- ENDDO WHILE REPRESENTATION NOT FOUND

            // --- RETURN
            return( enumRep );  
        }  
        // --- END
    };

    DataState m_dataState;
    int       m_id;
    String    m_name;
    String    m_link;
    
    /**
     * constructor
     */
    public DataObject() 
    // --- BEGIN
    {
        // --- INITIALIZE
        m_dataState = DataState.UNKNOWN;
        m_id        = 0;
        m_name      = null;
        m_link      = null;
    }
    // --- END
    
    
    /**
     * @return the m_dataState
     */
    public DataState getState() {
        return m_dataState;
    }

    /**
     * @param dataState the m_classListState to set
     */
    public void setState( DataState dataState) {
        this.m_dataState = dataState;
    }
    
     /**
     * @return the id
     */
    public int getId() {
        return m_id;
    }

    /**
     * @param id to set
     */
    public void setId( int id ){
        this.m_id = id;
    }
    
     /**
     * @return the m_name
     */
    public String getName() {
        return m_name;
    }

    /**
     * @param name to set
     */
    public void setName( String name ){
        this.m_name = name;
    }
    
     /**
     * @return the link
     */
    public String getLink() {
        return m_link;
    }

    /**
     * @param link the m_classListState to set
     */
    public void setLink( String link ){
        this.m_link = link;
    }
    
}
