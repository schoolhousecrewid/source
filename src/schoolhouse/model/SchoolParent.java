/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package schoolhouse.model;

import java.util.ArrayList;
import schoolhouse.common.QueryChain;
import schoolhouse.common.QueryThread;
import schoolhouse.common.SchoolApp;
import schoolhouse.common.SchoolDbIntfc;

/*
 * @brief definition of a participant
 */
public class SchoolParent extends SchoolWatcher {

    /**
     * constructor
     */
    public SchoolParent() 
    // --- BEGIN
    {
      
    }
    // --- END
    
    /**
     * @brief default behavior is no associated students
     */
    @Override
    public void createStudentList( SchoolApp threadApp )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolDbIntfc.SchoolQuery studentQuery = SchoolDbIntfc.SchoolQuery.PARENT_STD;
        QueryThread               queryThread;
        QueryChain                queryChain;
        Integer                   parentId = getId();
        System.out.println( "SchoolParent::createStudentList " + getUserName() );
                
        // --- CREATE ARRAY
        m_studentList = new ArrayList<>();

        // --- QUERY ROLES FOR THE LIST OF STUDENTS ASSOCIATED WITH PARENT
        queryThread = new QueryThread( threadApp );
        queryThread.setHandler( threadApp );
        studentQuery.dbQueryId( parentId, 0 );
        queryThread.setQuery( studentQuery.dbQuery(), studentQuery.dbName() );
        //queryThread.start();  
        queryThread.executeQuery();
    }
    // --- END




}
