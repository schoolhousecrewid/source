/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package schoolhouse.model;

import java.util.ArrayList;

/*
 * @brief definition of a participant
 */
public class SchoolWatcher extends SchoolPerson {

    /**
     * constructor
     */
    public SchoolWatcher() 
    // --- BEGIN
    {
      
    }
    // --- END

    /**
     * @return the m_studentList
     */
    public ArrayList getStudentList()
    // --- BEGIN
    {
        return m_studentList;
    }
    // --- END
    
    /**
     * @param studentList
     */
    public void setStudentList( ArrayList studentList) {
        this.m_studentList = studentList;
    }
}
