/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package schoolhouse.model;

import schoolhouse.common.SchoolApp;
import schoolhouse.common.SchoolApp;

/**
 *
 * @author wpeets
 */
public class SchoolGradeScale extends DataObject {
    /**
     * @brief roles supported by schoolhouse
     * 
     * This application will enforce behavior based on known
     * roles
     */
    public enum ScalePoint { APLUS(1),  A(2),  AMINUS(3),
                             BPLUS(4),  B(5),  BMINUS(6),
                             CPLUS(7),  C(8),  CMINUS(9),
                             DPLUS(10), D(11), DMINUS(12),
                             FAIL(13);
                        
    private final int m_value;
    private static final int enumSize = ScalePoint.values().length;

    /**
     * @brief constructor
     * 
     * This initializes values
     */
        private ScalePoint( int value )
        // --- BEGIN
        {
            // --- SET VALUE
            this.m_value = value;
        }
        // --- END
        
        /**
         * @brief size of enum
         * 
         * returns the size of enum
         */
        static public int getSize( ) {
            return( enumSize );
        } 
        
        /**
         * @brief get int cast of enum
         * 
         * returns the ordinal representation of enum
         */
        public int getValue()
        // --- BEGIN
        {
            // --- RETURN INTEGER REPRESENTATION
            return( m_value );
        }       
        // --- END
        
        
        /**
         * @brief allow conversion from ordinal value to enum object
         * 
         * This takes a value and returns an enumeration representation.
         */
        static public ScalePoint fromValue( int value )
        // --- BEGIN
        { 
            // --- DECLARE LOCAL VVARIABLES
            ScalePoint enumRep = null;
            
            // --- DO WHILE REPRESENTATION NOT FOUND
            for( ScalePoint enumVal: ScalePoint.values() ) { 
                // --- IF ORDINAL VALUE MATCHES
                if( enumVal.m_value == value) { 
                    // --- RETURN ENUM
                    return enumVal;  
                }  
                // --- ENDIF ORDINAL VALUE MATCHES
            }  
            // --- ENDDO WHILE REPRESENTATION NOT FOUND
  
            // --- RETURN
            return( enumRep );  
        }  
        // --- END
    };

    private String            m_scaleName;
    private SchoolGradeRank[] m_scalePoints;
            SchoolApp         m_appHandle;
    
    /**
     * @param appHandle
     * @brief constructor
     * 
     * This initializes values
     */
    public SchoolGradeScale( SchoolApp appHandle )
    // --- BEGIN
    {
        // --- SET VALUE
        m_appHandle = appHandle;
        m_scalePoints = new SchoolGradeRank[13];
    }
    // --- END


    /**
     * @return the m_scaleName
     */
    public String getName() {
        return m_scaleName;
    }

    /**
     * @param scaleName rank tag
     */
    public void setName( String scaleName ) {
        this.m_scaleName = scaleName;
    }

    /**
     * @return the m_scalePoints
     */
    public SchoolGradeRank getRank( ScalePoint scaleIndex )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        int scaleValue = scaleIndex.getValue() - 1;
        
        // --- RETURN RANK
        return m_scalePoints[scaleValue];
    }
    // --- END
    /**
     * @param scaleIndex index of rank
     * @param gradeRank min grade
     * TBD may not be needed
     */
    public void setRank( ScalePoint scaleIndex, SchoolGradeRank gradeRank )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        int scaleValue = scaleIndex.getValue() - 1;
    
        // --- SET GRADE
        this.m_scalePoints[scaleValue] = gradeRank;
    }
    // --- END
}
