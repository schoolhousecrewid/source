/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package schoolhouse.model;

import schoolhouse.model.SchoolStudent;
import java.util.ArrayList;
import java.sql.Date;
import java.util.Iterator;
import schoolhouse.common.SchoolApp;

/**
 *
 * @author wpeets
 * #brief a type of grade for a class
 */
public class SchoolMark extends DataObject {
/**
 * @brief roles supported by schoolhouse
 * 
 * This application will enforce behavior based on known
 * roles
 */   
public enum SchoolMarkType { QUIZ(1), TEST(2),  ATTENDANCE(3),
                             LAB(4),   HOMEWORK(5), ADMIN(6),
                             POP_QUIZ(7),   IN_CLASS(8), OPEN_BOOK(9),
                             OTHER(10), UNKNOWN(11);
                        
    private final int m_value;
    private static final int enumSize = SchoolMarkType.values().length;

    /**
     * @brief constructor
     * 
     * This initializes values
     */
        private SchoolMarkType( int value )
        // --- BEGIN
        {
            // --- SET VALUE
            this.m_value = value;
        }
        // --- END
        
        /**
         * @brief size of enum
         * 
         * returns the size of enum
         */
        static public int getSize( ) {
            return( enumSize );
        } 
        
        /**
         * @brief get int cast of enum
         * 
         * returns the ordinal representation of enum
         */
        public int getValue()
        // --- BEGIN
        {
            // --- RETURN INTEGER REPRESENTATION
            return( m_value );
        }       
        // --- END
        
        
        /**
         * @brief allow conversion from ordinal value to enum object
         * 
         * This takes a value and returns an enumeration representation.
         */
        static public SchoolMarkType fromValue( int value )
        // --- BEGIN
        { 
            // --- DECLARE LOCAL VVARIABLES
            SchoolMarkType enumRep = null;
            
            // --- DO WHILE REPRESENTATION NOT FOUND
            for( SchoolMarkType enumVal: SchoolMarkType.values() ) { 
                // --- IF ORDINAL VALUE MATCHES
                if( enumVal.m_value == value) { 
                    // --- RETURN ENUM
                    return enumVal;  
                }  
                // --- ENDIF ORDINAL VALUE MATCHES
            }  
            // --- ENDDO WHILE REPRESENTATION NOT FOUND
  
            // --- RETURN
            return( enumRep );  
        }  
        // --- END
    };
    
    private SchoolClass m_class;
    private int         m_type;
    private String      m_assignName;
    private Date        m_assignedDate;
    private Date        m_dueDate;
    private int         m_gradePercent;
    private int         m_minGradeValue;
    private int         m_maxGradeValue;
    private int         m_passingGradeValue;
    private String      m_markModifier;
    SchoolApp           m_appHandle;
    SchoolGradeScale    m_scale;
    private             ArrayList<SchoolGrade> m_gradeList;
    
    /**
     * @brief constructor
     * 
     * This initializes values
     */
    public SchoolMark( SchoolApp appHandle )
    // --- BEGIN
    {
        // --- SET VALUE
        m_appHandle  = appHandle;
        m_gradeList  = new ArrayList<SchoolGrade>();
    }
    // --- END


    /**
     * @return the m_course_id
     */
    public SchoolClass getSchoolClass() {
        return m_class;
    }

    /**
     * @param markClass class pertaining to specific mark
     */
    public void setSchoolClass( SchoolClass markClass ) {
        this.m_class = markClass;
    }

    /**
     * @return the m_mark type
     * TBD should convert to the enum now that it is created
     */
    public int getType() {
        return m_type;
    }

    /**
     * @param newType type of mark
     * TBD should convert to the enum now that it is created
     */
    public void setType( int newType) {
        this.m_type = newType;
    }

    /**
     * @return the m_assignName
     */
    public String getAssignmentName() {
        return m_assignName;
    }

    /**
     * @param assignName set the name of the mark
     */
    public void setAssignmentName( String assignName ) {
        this.m_assignName = assignName;
    }

    /**
     * @return the m_assignedDate
     */
    public Date getAssignedDate() {
        return m_assignedDate;
    }

    /**
     * @param assignedDate set the assigned date
     */
    public void setAssignedDate(Date assignedDate) {
        this.m_assignedDate = assignedDate;
    }

    /**
     * @return the m_dueDate
     */
    public Date getDueDate() {
        return m_dueDate;
    }

    /**
     * @param m_dueDate set date assignment is due
     */
    public void setDueDate( Date dueDate ) {
        this.m_dueDate = dueDate;
    }

    /**
     * @return the m_gradePercent
     */
    public int getGradePercent() {
        return m_gradePercent;
    }

    /**
     * @param gradePercent weight with respect to final mark
     */
    public void setGradePercent( int gradePercent ) {
        this.m_gradePercent = gradePercent;
    }

    /**
     * @return the minimum grade value allowed
     */
    public int getMinGradeValue() {
        return m_minGradeValue;
    }

    /**
     * @param minGradeValue type of mark
     */
    public void setMinGradeValue( int minGradeValue ) {
        this.m_minGradeValue = minGradeValue;
    }

    /**
     * @return the m_maxGradeValue
     */
    public int getMaxGradeValue() {
        return m_maxGradeValue;
    }

    /**
     * @param maxGradeValue type of mark
     */
    public void setMaxGradeValue( int maxGradeValue ) {
        this.m_maxGradeValue = maxGradeValue;
    }


    /**
     * @return the m_markModifier
     */
    public String getModifier() {
        return m_markModifier;
    }

    /**
     * @param markModifier descriptor of mark
     */
    public void setModifier( String markModifier ) {
        this.m_markModifier = markModifier;
    }

    /**
     * 
     * @param gradeId
     * @return reference to grade data; null if not in mark
     */
    public SchoolGrade getGrade( Integer gradeId )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolGrade  returnGrade = null;
        Iterator     gradeIter   = m_gradeList.iterator();
        SchoolGrade  gradeInfo;
        boolean      foundGrade  = false;
        
        // --- DO FOR EACH MARK IN THE CLASS
        while( (gradeIter.hasNext( )) && ( foundGrade == false) ){
            // --- GET DATA
            gradeInfo = (SchoolGrade)gradeIter.next( );
            
            // --- IF THERE IS A MATCH
            if( gradeInfo.getId() == gradeId ){
                // --- SET RETURN VALUE
                returnGrade = gradeInfo;
                foundGrade = true;
            }
            // --- ENDIF THERE IS A MATCH
        }
        // --- ENDDO FOR EACH MARK IN THE CLASS
        
        // --- RETURN
        return( returnGrade );
    }
    // --- END  

    /**
     * @return the m_markList
     */
    public ArrayList<SchoolGrade> getGradeList() {
        return m_gradeList;
    }

    /**
     * @param gradeList the grade to set
     */
    public void setGradeList(ArrayList<SchoolGrade> gradeList) {
        this.m_gradeList = gradeList;
    }

    /**
     * @return the grading scale
     */
    public SchoolGradeScale getScale() {
        return m_scale;
    }

    /**
     * @param newScale the scale to store
     */
    public void setScale( SchoolGradeScale newScale ) {
        this.m_scale = newScale;
    }
    
    
    
    /**
     * 
     * @param markName
     * @return reference to mark data; null if not in class
     */
    public SchoolGrade getGrade( SchoolStudent personName )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolGrade   returnGrade = null;
        Iterator      gradeIter   = m_gradeList.iterator();
        SchoolGrade   gradeInfo;
        boolean       foundGrade  = false;
        
        // --- DO FOR EACH MARK IN THE CLASS
        while( (gradeIter.hasNext( )) && ( foundGrade == false) ){
            // --- GET DATA
            gradeInfo = (SchoolGrade)gradeIter.next( );
            
            // --- IF THERE IS A MATCH
            if( gradeInfo.getStudent() == personName ){
                // --- SET RETURN VALUE
                returnGrade = gradeInfo;
                foundGrade = true;
            }
            // --- ENDIF THERE IS A MATCH
        }
        // --- ENDDO FOR EACH MARK IN THE CLASS
        
        // --- RETURN
        return( returnGrade );
    }
    // --- END  
    

    /**
     * @param grade for the mar
     */
    public void addGrade( SchoolGrade grade ) 
    // --- BEGIN
    {
        m_gradeList.add( grade );
    }
    // --- END
    
    
}
