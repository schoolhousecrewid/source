/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package schoolhouse.model;

import schoolhouse.model.SchoolStudent;
import schoolhouse.common.SchoolApp;
import schoolhouse.model.SchoolMark;
import java.sql.Date;
import schoolhouse.common.SchoolApp;


/**
 *
 * @author wpeets
 */
public class SchoolGrade extends DataObject {
    private SchoolMark    m_mark;
    private SchoolClass   m_schoolClass;
    private SchoolStudent m_student;
    private Date          m_dateReturned;
    private float         m_grade;
    private float         m_gradeChange;
    SchoolApp             m_appHandle;
    
    /**
     * @param appHandle
     * @brief constructor
     * 
     * This initializes values
     */
    public SchoolGrade( SchoolApp appHandle )
    // --- BEGIN
    {
        // --- SET VALUE
        m_appHandle      = appHandle;
    }
    // --- END


    /**
     * @return the m_course_id
     */
    public SchoolMark getMark() {
        return m_mark;
    }

    /**
     * @param markId the mark definition
     */
    public void setMark( SchoolMark markId ) {
        this.m_mark = markId;
    }

    /**
     * @return the m_classId
     */
    public SchoolClass getSchoolClass() {
        return m_schoolClass;
    }

    /**
     * @param schoolClass the class containing the mark definition
     * TBD may not be needed
     */
    public void setSchoolClass( SchoolClass schoolClass ) {
        this.m_schoolClass = schoolClass;
    }

    /**
     * @return the  student for grade
     */
    public SchoolStudent getStudent() {
        return m_student;
    }

    /**
     * @param student who the grade applies to
     */
    public void setStudent( SchoolStudent student ) {
        this.m_student = student;
    }

    /**
     * @return the m_dateReturned
     */
    public Date getDateReturned() {
        return m_dateReturned;
    }

    /**
     * @param dateReturned date assignment is handed in
     */
    public void setDateReturned( Date dateReturned ) {
        this.m_dateReturned = dateReturned;
    }

    /**
     * @return the m_grade
     */
    public float getGrade() {
        return m_grade;
    }

    /**
     * @param grade grade for assignment
     */
    public void setGrade( float grade ) {
        this.m_grade = grade;
    }

    /**
     * @return the m_gradeChange
     */
    public float getGradeChange() {
        return m_gradeChange;
    }

    /**
     * @param gradeChange grade for assignment
     */
    public void setGradeChange( float gradeChange ) {
        this.m_gradeChange = gradeChange;
    }
        
}
