/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package schoolhouse.model;

import schoolhouse.model.SchoolPolicy;
import java.util.ArrayList;
import schoolhouse.common.SchoolApp;

/*
 * @brief definition of a participant
 */
public class SchoolPerson extends DataObject {
    private   String                   m_last_name;
    private   String                   m_first_name;
    private   String                   m_middle_name;
    private   String                   m_nick_name;
    private   String                   m_org_assigned_id;
    private   SchoolPolicy.SchoolRoles m_role_id;
    private   String                   m_title;
    private   String                   m_email;
    private   String                   m_phone;
    private   String                   m_cellPhone;
    private   String                   m_address;
    private   int                      m_user_status;
    private   String                   m_user_name;
    protected ArrayList<SchoolPerson>  m_studentList;

    /**
     * constructor
     */
    public SchoolPerson() 
    // --- BEGIN
    {
        // --- CREATE A LIST FOR STUENTS
        m_studentList = new ArrayList<>();      
    }
    // --- END

    /**
     * @return the m_studentList
     */
    public ArrayList getStudentList()
    // --- BEGIN
    {
        return m_studentList;
    }
    // --- END

    /**
     * @param threadApp  application for context
     * @brief default behavior is no associated students
     */
    public void createStudentList( SchoolApp threadApp )
    // --- BEGIN
    {
        this.m_studentList.clear();
    }
    // --- END

    /**
     * @param studentList
     */
    public void setStudentList( ArrayList studentList )
    // --- BEGIN
    {
        this.m_studentList = studentList;
    }
    // --- END
    
    
    /**
     * @param studentMatch
     * @brief add to student list
     */
    public void addToStudentList( SchoolStudent studentMatch )
    // --- BEGIN
    {
        // --- THE ONLY ASSOCIATED STUDENT IS ITSELF
        System.out.println( "SchoolPerson::addToStudentList[" + studentMatch.getId() + "]"  );
        m_studentList.add( studentMatch );
    }
    // --- END

    
    /**
     * @return the m_last_name
     */
    public String getLastName() {
        return m_last_name;
    }

    /**
     * @param last_name the m_last_name to set
     */
    public void setLastName(String last_name) {
        this.m_last_name = last_name;
    }

    /**
     * @return the id assigned by school
     */
    public String getOrgAssignedId() {
        return m_org_assigned_id;
    }

    /**
     * @brief set the person id with the id assigned by organization
     * @param m_org_assigned_id the id to set
     */
    public void setOrgAssigned(String org_assigned_id) {
        this.m_org_assigned_id = org_assigned_id;
    }

    /**
     * @return the m_first_name
     */
    public String getFirstName() {
        return m_first_name;
    }

    /**
     * @param first_name the m_first_name to set
     */
    public void setFirstName(String first_name) {
        this.m_first_name = first_name;
    }

    /**
     * @return the m_middle_name
     */
    public String getMiddleName() {
        return m_middle_name;
    }

    /**
     * @param middle_name the m_middle_name to set
     */
    public void setMiddleName(String middle_name) {
        this.m_middle_name = middle_name;
    }

    /**
     * @return the m_nick_name
     */
    public String getNickName() {
        return m_nick_name;
    }

    /**
     * @param m_nick_name the m_nick_name to set
     */
    public void setNickName(String nick_name) {
        this.m_nick_name = nick_name;
    }

    /**
     * @return the m_role_id
     */
    public SchoolPolicy.SchoolRoles getRole() {
        return m_role_id;
    }

    /**
     * @param m_role_id the m_role_id to set
     */
    public void setRole(SchoolPolicy.SchoolRoles role_id) {
        this.m_role_id = role_id;
    }

    /**
     * @return the m_title
     */
    public String getTitle() {
        return m_title;
    }

    /**
     * @param m_title the m_title to set
     */
    public void setTitle(String title) {
        this.m_title = title;
    }

    /**
     * @return the m_email
     */
    public String getEmail() {
        return m_email;
    }

    /**
     * @param m_email the m_email to set
     */
    public void setEmail(String email) {
        this.m_email = email;
    }

    /**
     * @return the m_phone
     */
    public String getPhone() {
        return m_phone;
    }

    /**
     * @param m_phone the m_phone to set
     */
    public void setPhone(String phone) {
        this.m_phone = phone;
    }

    /**
     * @return the m_phone
     */
    public String getCellPhone() {
        return m_cellPhone;
    }

    /**
     * @param m_phone the m_phone to set
     */
    public void setCellPhone(String phone) {
        this.m_cellPhone = phone;
    }

    /**
     * @return the m_address
     */
    public String getAddress() {
        return m_address;
    }

    /**
     * @param m_address the m_address to set
     */
    public void setAddress(String address) {
        this.m_address = address;
    }

    /**
     * @return the m_user_status
     */
    public int getUserStatus() {
        return m_user_status;
    }

    /**
     * @param user_status the m_user_status to set
     */
    public void setUserStatus(int user_status) {
        this.m_user_status = user_status;
    }

    /**
     * @return the m_username
     */
    public String getUserName() {
        return m_user_name;
    }

    /**
     * @param username the m_username to set
     */
    public void setUserName(String username) {
        this.m_user_name = username;
    }

    
}
