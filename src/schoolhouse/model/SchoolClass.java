/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package schoolhouse.model;

import schoolhouse.model.SchoolGradeScale;
import java.sql.Date;
import java.sql.Time;
import schoolhouse.model.SchoolPerson;
import schoolhouse.common.SchoolApp;
import schoolhouse.model.SchoolMark;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import schoolhouse.common.SchoolApp;

/**
 *
 * @author wpeets
 */
public class SchoolClass extends DataObject {
    private int                            m_course_id;
    private String                         m_name;
    private Date                           m_start_date;
    private Date                           m_end_date;
    private Time                           m_time;
    private String                         m_duration;
    private SchoolGradeScale               m_scale;
    private ArrayList<SchoolPerson>        m_studentList;
    private ArrayList<SchoolPerson>        m_educatorList;
    private ArrayList<SchoolPerson>        m_graderList;
    private ArrayList<SchoolMark>          m_markList;
    private ArrayList<SchoolNotification>  m_notificationList;   
    Map<SchoolStudent,Float>               m_classGrades;
    SchoolClass                            m_currentClass;
    SchoolApp                              m_appHandle;
    
    /**
     * @brief constructor
     * 
     * This initializes values
     */
    public SchoolClass( SchoolApp appHandle )
    // --- BEGIN
    {
        // --- SET VALUE
        m_appHandle    = appHandle;
        m_educatorList = new ArrayList<SchoolPerson>();
        m_graderList   = new ArrayList<SchoolPerson>();
        m_studentList  = new ArrayList<SchoolPerson>();
        m_markList     = new ArrayList<SchoolMark>();
        m_classGrades  = new HashMap<SchoolStudent,Float>();
    }
    // --- END


    /**
     * @return the m_course_id
     */
    public int getCourseId() {
        return m_course_id;
    }

    /**
     * @param m_course_id the m_course_id to set
     */
    public void setCourseId(int m_course_id) {
        this.m_course_id = m_course_id;
    }

    /**
     * @return the m_start_date
     */
    public Date getStartDate() {
        return m_start_date;
    }

    /**
     * @param m_start_date the m_start_date to set
     */
    public void setStartDate(Date start_date) {
        this.m_start_date = start_date;
    }

    /**
     * @return the m_end_date
     */
    public Date getEndDate() {
        return m_end_date;
    }

    /**
     * @param m_end_date the m_end_date to set
     */
    public void setEndDate(Date end_date) {
        this.m_end_date = end_date;
    }

    /**
     * @return the class time
     */
    public Time getTime() {
        return m_time;
    }

    /**
     * @param course_time the class time to set
     */
    public void setTime( Time class_time) {
        this.m_time = class_time;
    }

    /**
     * @return the m_course_duration
     */
    public String getDuration() {
        return m_duration;
    }

    /**
     * @param m_course_duration the m_course_duration to set
     */
    public void setDuration(String duration) {
        this.m_duration = duration;
    }

    /**
     * @return the m_studentList
     */
    public ArrayList<SchoolPerson> getStudentList() {
        return m_studentList;
    }

    /**
     * @param m_studentList the m_studentList to set
     */
    public void setStudentList(ArrayList<SchoolPerson> studentList) {
        this.m_studentList = studentList;
    }

    /**
     * @return the m_educatorList
     */
    public ArrayList<SchoolPerson> getEducatorList() {
        return m_educatorList;
    }

    /**
     * @param m_educatorList the m_educatorList to set
     */
    public void setEducatorList(ArrayList<SchoolPerson> educatorList) {
        this.m_educatorList = educatorList;
    }

    /**
     * @param educator the educator to set
     */
    public void addEducator( SchoolPerson educator ) 
    // --- BEGIN
    {
        m_educatorList.add( educator );
    }
    // --- END
    
    /**
     * @param grader the grader to set
     */
    public void addGrader( SchoolPerson grader ) 
    // --- BEGIN
    {
        m_graderList.add( grader );
    }
    // --- END
    
    /**
     * @param student the student to set
     */
    public void addStudent( SchoolPerson student ) 
    // --- BEGIN
    {
        m_studentList.add( student );
    }
    // --- END
    
    /**
     * @return the m_graderList
     */
    public ArrayList<SchoolPerson> getGraderList() {
        return m_graderList;
    }

    /**
     * @param m_graderList the m_graderList to set
     */
    public void setGraderList(ArrayList<SchoolPerson> graderList) {
        this.m_graderList = graderList;
    }

    /**
     * @return the m_markList
     */
    public ArrayList<SchoolMark> getMarkList() {
        return m_markList;
    }

    /**
     * @param m_markList the m_markList to set
     */
    public void setMarkList(ArrayList<SchoolMark> markList) {
        this.m_markList = markList;
    }

    /**
     * @return the m_notificationList
     */
    public ArrayList<SchoolNotification> getNotificationList() {
        return m_notificationList;
    }

    /**
     * @param m_notificationList the m_notificationList to set
     */
    public void setNotificationList(ArrayList<SchoolNotification> notificationList) {
        this.m_notificationList = notificationList;
    }

    /**
     * @return the m_name
     */
    public String getClassName() {
        return m_name;
    }

    /**
     * @param newName the m_name to set
     */
    public void setClassName( String newName ) {
        this.m_name = newName;
    }
    
    /**
     * @return the grading scale
     */
    public SchoolGradeScale getScale() {
        return m_scale;
    }

    /**
     * @param newScale the scale to store
     */
    public void setScale( SchoolGradeScale newScale ) {
        this.m_scale = newScale;
    }
    
    /**
     * 
     * @param markId
     * @return reference to mark data; null if not in class
     */
    public SchoolMark getMark( Integer markId )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolMark   returnClass = null;
        Iterator     markIter    = m_markList.iterator();
        SchoolMark   markInfo;
        boolean      foundMark   = false;
        
        // --- DO FOR EACH MARK IN THE CLASS
        while( (markIter.hasNext( )) && ( foundMark == false) ){
            // --- GET DATA
            markInfo = (SchoolMark)markIter.next( );
            
            // --- IF THERE IS A MATCH
            if( markInfo.getId() == markId ){
                // --- SET RETURN VALUE
                returnClass = markInfo;
                foundMark = true;
            }
            // --- ENDIF THERE IS A MATCH
        }
        // --- ENDDO FOR EACH MARK IN THE CLASS
        
        // --- RETURN
        return( returnClass );
    }
    // --- END  

        /**
     * 
     * @param markName
     * @return reference to mark data; null if not in class
     */
    public SchoolMark getMark( String markName )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolMark   returnClass = null;
        Iterator     markIter    = m_markList.iterator();
        SchoolMark   markInfo;
        boolean      foundMark   = false;
        
        // --- DO FOR EACH MARK IN THE CLASS
        while( (markIter.hasNext( )) && ( foundMark == false) ){
            // --- GET DATA
            markInfo = (SchoolMark)markIter.next( );
            
            // --- IF THERE IS A MATCH
            if( markInfo.getAssignmentName().equals( markName ) ){
                // --- SET RETURN VALUE
                returnClass = markInfo;
                foundMark = true;
            }
            // --- ENDIF THERE IS A MATCH
        }
        // --- ENDDO FOR EACH MARK IN THE CLASS
        
        // --- RETURN
        return( returnClass );
    }
    // --- END  
    

    /**
     * @param mark for the class
     */
    public void addMark( SchoolMark mark ) 
    // --- BEGIN
    {
        m_markList.add( mark );
    }
    // --- END
    
    /**
     * @param student the student to set
     * @param grade final grade for class
     */
    public void setFinalGrade( SchoolStudent student, Float grade ) 
    // --- BEGIN
    {
        // --- STORE FINAL GRADE
        m_classGrades.put( student, grade );
    }
    // --- END
    
    /**
     * @param student the student to set
     * @param grade final grade for class
     */
    public Float getFinalGrade( SchoolStudent student ) 
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        // --- ...TBD should we even create this if not needed
        Float returnGrade = new Float( 0 );
        
        // --- IF GRADE EXISTS
        if( m_classGrades.containsKey(student) == true ){
            // --- GET GRADE
            returnGrade = m_classGrades.get( student );
        }
        // --- ENDIF GRADE EXISTS
        
        // --- RETURN
        return( returnGrade );
    }
    // --- END
    
    
    /**
     * @param student the student to set
     * @return true if a final grade is entered
     */
    public boolean hasFinalGrade( SchoolStudent student ) 
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        // --- ...TBD should we even create this if not needed
        boolean returnState = m_classGrades.containsKey( student );
                
        // --- RETURN
        return( returnState );
    }
    // --- END    
}
