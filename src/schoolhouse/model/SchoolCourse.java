/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package schoolhouse.model;

import schoolhouse.common.SchoolApp;
import java.util.ArrayList;
import java.util.Iterator;
import schoolhouse.common.QueryChain;
import schoolhouse.common.QueryThread;
import schoolhouse.common.SchoolApp;

/**
 *
 * @author wpeets
 */
public class SchoolCourse extends DataObject {
    private int                    m_credits;
    private String                 m_courseName;  
    private String                 m_org_assigned_id;  
    private ArrayList<SchoolClass> m_classList;
    private int                    m_schoolId;

    /*
     * @brief used to interface with database
    */
    QueryChain             m_dbChain;     // --- USED FOR MASSIVE QUERIES
    QueryThread            m_dbCommand;   // --- USED TO INSERT INTO DATABASE
    SchoolApp              m_appHandle;
    
    /**
     * @brief constructor
     * 
     * This initializes values
     */
    public SchoolCourse( SchoolApp appHandle )
    // --- BEGIN
    {
        // --- SET VALUE
        m_classList      = new ArrayList<SchoolClass>();
        m_appHandle      = appHandle;
        m_credits        = 0;
        m_schoolId       = 0;
    }
    // --- END
    
    /**
     * @return the m_credits assigned by school
     */
    public int getCredits() {
        return m_credits;
    }

    /**
     * @brief set the id with the id assigned by organization
     * @param credits the id to set
     */
    public void setCredits( int credits ) {
        this.m_credits = credits;
    }

    /**
     * @return the school id of school
     */
    public int getSchool() {
        return m_schoolId;
    }

    /**
     * @brief set the school id
     * @param schoolId the id to set
     */
    public void setSchool( int schoolId ) {
        this.m_schoolId = schoolId;
    }

    /**
     * @return the id assigned by school
     */
    public String getOrgAssignedId() {
        return m_org_assigned_id;
    }

    /**
     * @brief set the id with the id assigned by organization
     * @param m_org_assigned_id the id to set
     */
    public void setOrgAssigned( String org_assigned_id ) {
        this.m_org_assigned_id = org_assigned_id;
    }

    /**
     * @return the m_courseName
     */
    public String getName() {
        return m_courseName;
    }

    /**
     * @param courseName the m_courseName to set
     */
    public void setName( String courseName) {
        this.m_courseName = courseName;
    }

    /**
     * @return the m_classList
     */
    public ArrayList<SchoolClass> getClassList() {
        return m_classList;
    }

    /**
     * @param classList the m_classList to set
     */
    public void setClassList(ArrayList<SchoolClass> classList) {
        this.m_classList = classList;
    }
    
    public void addClass( String className )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        String  insertString;
        String  courseQuery;
/*
        
        // --- DEFINE STRING
        insertString = "INSERT INTO coursetable "
                             + "(school_id, course_name) "
                             + "VALUES ("
                             + "'"+ schoolId.toString()      + "',"
                             + "'"+ courseName    + "'"
                             + ")";
        m_dbCommand = new QueryThread( m_appHandle );
        m_dbCommand.setUpdate( insertString );
        m_dbCommand.start();  
        
        // --- QUERY TO GET THE NEW ID
        m_dbChain = new QueryChain( m_appHandle );
        m_dbChain.setHandler( this );
        courseQuery = SchoolDbIntfc.SchoolQuery.COURSE_ID.dbQueryString( courseName );
        m_dbChain.addQuery( courseQuery,
                            SchoolDbIntfc.SchoolQuery.COURSE_ID.dbName()  );
        m_dbChain.start();
        */
    }
    // --- END
    
    public void addClass( SchoolClass newClass )
    // --- BEGIN
    {
        // --- ADD TO DATABASE
        // --- ...TBD check for duplicate
        m_classList.add( newClass );
    }
    // --- END
    
    /**
     * 
     * @param classId
     * @return reference to class data; null if not in model
     */
    public SchoolClass getClass( Integer classId )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolClass  returnClass  = null;
        Iterator     classIter    = m_classList.iterator();
        SchoolClass  classInfo;
        boolean      foundClass   = false;
        
        // --- DO FOR EACH CLASS IN THE COuRSE
        while( (classIter.hasNext()) && (foundClass==false) ) {
            // --- GET DATA
            classInfo = (SchoolClass)classIter.next( );
            
            // --- IF THERE IS A MATCH
            if( classInfo.getId() == classId ){
                // --- SET RETURN VALUE
                // --- ... TBD should probably break out here
                returnClass = classInfo;
                foundClass = true;
            }
            // --- ENDIF THERE IS A MATCH
        }
        // --- ENDDO FOR EACH CLASS IN THE COuRSE
        
        // --- RETURN
        return( returnClass );
    }
    // --- END  
    
    
}
