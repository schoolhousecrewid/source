/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package schoolhouse.model;

import java.awt.event.ActionEvent;
import java.sql.ResultSet;
import java.util.EnumMap;
import java.util.EnumSet;
import schoolhouse.common.HandleCmdAction;
    
/**
 *
 * @author wpeets
 * @brief stores access level of each role to each
 * 
 * This object store the access level to each object for each action
 * by each role.  I will give the enum map a try to see how it works
 * out.
 */
public class SchoolPolicy implements HandleCmdAction{ 
/**
 * @brief roles supported by schoolhouse
 * 
 * This application will enforce behavior based on known
 * roles
 */
public enum SchoolRoles { STUDENT(1), PARENT(2),  EDUCATOR(3),
                          GRADER(4),  WATCHER(5), ADMINISTRATOR(6),
                          MANAGER(7), OTHER(8),   UNKNOWN(9);
                        
    private final int m_value;
    private static final int enumSize = SchoolRoles.values().length;

    /**
     * @brief constructor
     * 
     * This initializes values
     */
        private SchoolRoles( int value )
        // --- BEGIN
        {
            // --- SET VALUE
            this.m_value = value;
        }
        // --- END
        
        /**
         * @brief size of enum
         * 
         * returns the size of enum
         */
        static public int getSize( ) {
            return( enumSize );
        } 
        
        /**
         * @brief get int cast of enum
         * 
         * returns the ordinal representation of enum
         */
        public int getValue()
        // --- BEGIN
        {
            // --- RETURN INTEGER REPRESENTATION
            return( m_value );
        }       
        // --- END
        
        
        /**
         * @brief allow conversion from ordinal value to enum object
         * 
         * This takes a value and returns an enumeration representation.
         */
        static public SchoolRoles fromValue( int value )
        // --- BEGIN
        { 
            // --- DECLARE LOCAL VVARIABLES
            SchoolRoles enumRep = null;
            
            // --- DO WHILE REPRESENTATION NOT FOUND
            for( SchoolRoles enumVal: SchoolRoles.values() ) { 
                // --- IF ORDINAL VALUE MATCHES
                if( enumVal.m_value == value) { 
                    // --- RETURN ENUM
                    return enumVal;  
                }  
                // --- ENDIF ORDINAL VALUE MATCHES
            }  
            // --- ENDDO WHILE REPRESENTATION NOT FOUND
  
            // --- RETURN
            return( enumRep );  
        }  
        // --- END
    };

    /**
     * @brief actions supported by schoolhouse
     * 
     * This application will allow certain actions with regards to the
     * information it stores
     */
     public enum SchoolActions { ADD(1),      REMOVE(2),  VIEW(3),
                                 CHANGE(4),   IMPORT(5),  EXPORT(6),
                                 MESSAGE(7), UNKNOWN(8);
                        
        private final int m_value;
        private static final int enumSize = SchoolActions.values().length;
        
        /**
         * @brief constructor
         * 
         * Set values of constructor
         */
        private SchoolActions( int value ) {
                this.m_value = value;
        }  
        
        /**
         * @brief size of enum
         * 
         * returns the size of enum
         */
        static public int getSize( ) {
            return( enumSize );
        }  
        
        /**
         * @brief get int cast of enum
         * 
         * returns the ordinal representation of enum
         */
        public int getValue()
        // --- BEGIN
        {
            // --- RETURN INTEGER REPRESENTATION
            return( m_value );
        }       
        // --- END
        
        /**
         * @brief allow conversion from ordinal value to enum object
         * 
         * This takes a value and returns an enumeration representation.
         */
        static public SchoolActions fromValue( int value )
        // --- BEGIN
        { 
            // --- DECLARE LOCAL VVARIABLES
            SchoolActions enumRep = null;
            
            // --- DO WHILE REPRESENTATION NOT FOUND
            for( SchoolActions enumVal: SchoolActions.values() ) { 
                // --- IF ORDINAL VALUE MATCHES
                if( enumVal.m_value == value) { 
                    // --- RETURN ENUM
                    return enumVal;  
                }  
                // --- ENDIF ORDINAL VALUE MATCHES
            }  
            // --- ENDDO WHILE REPRESENTATION NOT FOUND
  
            // --- RETURN
            return( enumRep );  
        }  
        // --- END
     };

    /**
     * @brief actions supported by schoolhouse
     * 
     * This application will allow certain actions with regards to the
     * information it stores
     */
     public enum AppActions { RETURN(90),  EXIT(100);
                        
        private final int m_value;
        private static final int enumSize = AppActions.values().length;
        
        /**
         * @brief constructor
         * 
         * Set values of constructor
         */
        private AppActions( int value ) {
                this.m_value = value;
        }  
        
        /**
         * @brief size of enum
         * 
         * returns the size of enum
         */
        static public int getSize( ) {
            return( enumSize );
        }  
        
        /**
         * @brief get int cast of enum
         * 
         * returns the ordinal representation of enum
         */
        public int getValue()
        // --- BEGIN
        {
            // --- RETURN INTEGER REPRESENTATION
            return( m_value );
        }       
        // --- END
        
        /**
         * @brief allow conversion from ordinal value to enum object
         * 
         * This takes a value and returns an enumeration representation.
         */
        static public AppActions fromValue( int value )
        // --- BEGIN
        { 
            // --- DECLARE LOCAL VVARIABLES
            AppActions enumRep = null;
            
            // --- DO WHILE REPRESENTATION NOT FOUND
            for( AppActions enumVal: AppActions.values() ) { 
                // --- IF ORDINAL VALUE MATCHES
                if( enumVal.m_value == value) { 
                    // --- RETURN ENUM
                    return enumVal;  
                }  
                // --- ENDIF ORDINAL VALUE MATCHES
            }  
            // --- ENDDO WHILE REPRESENTATION NOT FOUND
  
            // --- RETURN
            return( enumRep );  
        }  
        // --- END
     };

    /**
     * @brief info objects supported by schoolhouse
     * 
     * This list represents the data schoolhouse needs to perform its mission
     */
     public enum SchoolObjects { SCHOOL(1),       PERSON(2),   COURSE(3),
                                 CLASS(4),        MARK(5),     GRADE(6),  
                                 NOTIFICATION(7), MESSAGE(8),  POLICY(9), 
                                 UNKNOWN(10);
                        
        private final int m_value;
        private static final int enumSize = SchoolObjects.values().length;

        private SchoolObjects( int value ) {
                this.m_value = value;
        }   
        
        /**
         * @brief size of enum
         * 
         * returns the size of enum
         */
        static public int getSize( ) {
            return( enumSize );
        }  
        
        /**
         * @brief get int cast of enum
         * 
         * returns the ordinal representation of enum
         */
        public int getValue()
        // --- BEGIN
        {
            // --- RETURN INTEGER REPRESENTATION
            return( m_value );
        }       
        // --- END
        
        /**
         * @brief allow conversion from ordinal value to enum object
         * 
         * This takes a value and returns an enumeration representation.
         */
        static public SchoolObjects fromValue( int value )
        // --- BEGIN
        { 
            // --- DECLARE LOCAL VVARIABLES
            SchoolObjects enumRep = null;
            
            // --- DO WHILE REPRESENTATION NOT FOUND
            for( SchoolObjects enumVal: SchoolObjects.values() ) { 
                // --- IF ORDINAL VALUE MATCHES
                if( enumVal.m_value == value) { 
                    // --- RETURN ENUM
                    return enumVal;  
                }  
                // --- ENDIF ORDINAL VALUE MATCHES
            }  
            // --- ENDDO WHILE REPRESENTATION NOT FOUND
  
            // --- RETURN
            return( enumRep );  
        }  
        // --- END
     };

    /**
     * @brief behavior regarding an object in schoolhouse
     * 
     * This list defines how a role defined in schoolhouse might
     * interact with an object
     */
     public enum SchoolAccessLevel { FULL_ACCESS(1), SELECTIVE(2),  READ(3),
                                     NONE(4);
                             
        private final int m_value;
        private static final int enumSize = SchoolObjects.values().length;

        private SchoolAccessLevel( int value ) {
                this.m_value = value;
        }

        /**
         * @brief size of enum
         * 
         * returns the size of enum
         */
        static public int getSize( ) {
            return( enumSize );
        }  
        
        /**
         * @brief get int cast of enum
         * 
         * returns the ordinal representation of enum
         */
        public int getValue()
        // --- BEGIN
        {
            // --- RETURN INTEGER REPRESENTATION
            return( m_value );
        }       
        // --- END
        
        
        /**
         * @brief allow conversion from ordinal value to enum object
         * 
         * This takes a value and returns an enumeration representation.
         */
        static public SchoolAccessLevel fromValue( int value )
        // --- BEGIN
        { 
            // --- DECLARE LOCAL VVARIABLES
            SchoolAccessLevel enumRep = null;
            
            // --- DO WHILE REPRESENTATION NOT FOUND
            for( SchoolAccessLevel enumVal: SchoolAccessLevel.values() ) { 
                // --- IF ORDINAL VALUE MATCHES
                if( enumVal.m_value == value) { 
                    // --- RETURN ENUM
                    return enumVal;  
                }  
                // --- ENDIF ORDINAL VALUE MATCHES
            }  
            // --- ENDDO WHILE REPRESENTATION NOT FOUND
  
            // --- RETURN
            return( enumRep );  
        }  
        // --- END
    };
    
    final class ObjectPolicy{
               EnumMap<SchoolObjects, SchoolAccessLevel> m_objectMap;
            
        ObjectPolicy()
        // --- BEGIN
        {
            // --- CREATE POLICY MAP
            m_objectMap = new EnumMap<SchoolObjects, SchoolAccessLevel>(SchoolObjects.class);   
            
            // --- DO FOR EACH ROLE
            for( SchoolObjects p : SchoolObjects.values() ){
                // --- ADD VALUE
                m_objectMap.put( p, SchoolAccessLevel.NONE );
                //System.out.printf("Objects: %s %n", p  );
           }
            // --- ENDDO FOR EACH ROLE
        }
        // --- END  
 
        SchoolAccessLevel getAccessState( SchoolObjects sObject )
        // --- BEGIN
        {
            // --- DECLARE LOCAL VARIABLES
            SchoolAccessLevel currentAccess = SchoolAccessLevel.NONE;
            ActionPolicy      currActionSet;

            // --- IF OBJECT IS VALID
            if( sObject != null ){
                // --- GET STATE
                currentAccess = m_objectMap.get(sObject);
            }
            // --- ENDIF OBJECT IS VALID
                
            // --- RETURN STATE
            return( currentAccess );
        }
        // --- END 
        
        void  setAccessState( SchoolObjects sObject, SchoolAccessLevel newState)
        // --- BEGIN
        {                        
            // --- IF OBJECT IS VALID
            if( sObject != null ){
                // --- SET STATE
                m_objectMap.put( sObject, newState );
            }
            // --- ENDIF OBJECT IS VALID
            
        }
        // --- END 
        
    };
    
    final class ActionPolicy{
               EnumMap<SchoolActions, ObjectPolicy> m_actionMap;
            
        ActionPolicy()
        // --- BEGIN
        {
            // --- CREATE POLICY MAP
            m_actionMap = new EnumMap<SchoolActions, ObjectPolicy>(SchoolActions.class);   
            
            // --- DO FOR EACH ROLE
            for( SchoolActions p : SchoolActions.values() ){
                // --- ADD VALUE
                m_actionMap.put( p, new ObjectPolicy() );
                //System.out.printf("Action: %s %n", p  );
           }
            // --- ENDDO FOR EACH ROLE
        }
        // --- END        
        
        SchoolAccessLevel getAccessState( SchoolActions sAction, 
                                          SchoolObjects sObject)
        // --- BEGIN
        {
            // --- DECLARE LOCAL VARIABLES
            SchoolAccessLevel currentAccess = SchoolAccessLevel.NONE;
            ObjectPolicy      currActionSet;
            
            // --- GET STATE
            currActionSet = (ObjectPolicy)( m_actionMap.get( sAction ) );
            
            // --- IF ACTION IS VALID
            if( currActionSet != null ){
                // --- GET STATE
                currentAccess = currActionSet.getAccessState( sObject );
            }
            // --- ENDIF ACTION IS VALID
            
            // --- RETURN STATE
            return( currentAccess );
        }
        // --- END 

        void  setAccessState( SchoolActions sAction, SchoolObjects sObject, 
                              SchoolAccessLevel newState)
        // --- BEGIN
        {
            // --- DECLARE LOCAL VARIABLES
            ObjectPolicy      currObjectSet;
            
            // --- GET STATE
            currObjectSet = (ObjectPolicy)( m_actionMap.get( sAction ) );
            
            // --- IF OBJECT IS VALID
            if( currObjectSet !=null ){
                // --- SET STATE
                currObjectSet.setAccessState( sObject, newState );
            }
            // --- ENDIF OBJECT IS VALID  
        }
        // --- END 
    };


    /**
     * @brief list of all policies for each role
     */
    EnumMap<SchoolRoles, ActionPolicy> m_roleMap;
    boolean                            m_hasPolicy;
    
    /**
     * @brief constructor
     */
    public SchoolPolicy()
    // --- BEGIN
    {
        // --- CREATE POLICY MAP
        m_roleMap = new EnumMap<SchoolRoles, ActionPolicy>(SchoolRoles.class);   
        m_hasPolicy = false;
        
        // --- DO FOR EACH ROLE
        for( SchoolRoles roleId : SchoolRoles.values() ){
            // --- ADD VALUE
            m_roleMap.put( roleId, new ActionPolicy() );
            //System.out.printf("Role: %s %n", p  );
        }
        // --- ENDDO FOR EACH ROLE
    }
    // --- END  
        
    /**
     * @brief constructor
     */
    public boolean hasPolicy()
    // --- BEGIN
    {
        // --- RETURN CURRENT STATE
        return( m_hasPolicy );
    }
    // --- END  
        
    /**
     * @brief returns true or false if role has any kind of policy access
     * 
     * This object checks all the policies for an action or a policy and
     * returns true or false if any access exits. action or object can be null.
     * 
     */
    public boolean hasAccessState( SchoolRoles sRole, SchoolActions sAction, 
                                      SchoolObjects sObject)
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolAccessLevel currentAccess = SchoolAccessLevel.NONE;
        ActionPolicy      currActionSet;
        boolean           hasAccess = false;
        EnumSet           actionSet;
        EnumSet           objectSet;
        //SchoolActions     currAction; 
        //SchoolObjects     currObject;         
                    
        // --- IF ROLE IS VALID
        if( sRole != null ){
            // --- GET OBJECT
            currActionSet = (ActionPolicy)( m_roleMap.get( sRole ) );
        
            // --- IF CHECK IS SCOPED TO AN ACTION
            if( sAction != null ){
                // --- SET CHECK TO ONE ACTION
                actionSet = EnumSet.of( sAction );
            }
            // --- ELSE
            else{
                // --- CHECK ALL ACTIONS             
                actionSet = EnumSet.allOf( SchoolActions.class );
            } 
            // --- ENDIF CHECK IS SCOPED TO AN ACTION
        
            // --- IF CHECK IS SCOPED TO AN OBJECT
            if( sObject != null ){
                // --- SET CHECK TO ONE ACTION
                objectSet = EnumSet.of( sObject );
            }
            // --- ELSE
            else{
                // --- CHECK ALL ACTIONS             
                objectSet = EnumSet.allOf( SchoolObjects.class );
            } 
            // --- ENDIF CHECK IS SCOPED TO AN ACTION
              
            // --- DO FOR EACH ACTION
            for( Object currAction: actionSet ){
                 SchoolActions curAction = (SchoolActions)currAction;
                // --- DO FOR EACH OBJECT
                for( Object currObject: objectSet ){
                    SchoolObjects curObject = (SchoolObjects)currObject;
                     
                    // --- GET STATE
                    currentAccess = currActionSet.getAccessState( curAction, 
                                                                  curObject ); 
                       
                    // --- IF THE ROLE HAS ANY ACCESS
                    if( currentAccess != SchoolAccessLevel.NONE ){
                        hasAccess = true;
                    }
                    // --- ENDIF THE ROLE HAS ANY ACCESS

                }
                // --- ENDDO FOR EACH ACTION
            }
            // --- ENDDO FOR EACH ACTION
        }
        // --- ENDIF ROLE IS VALID
            
        // --- RETURN STATE
        return( hasAccess );
    }
    // --- END 
        

    /**
     * @brief returns access state for a policy specified by action and object
     * 
     * This method returns the access state specified by role, action, and
     * object.  If the policy does not exist an access state of none will
     * be returned.
     */
    SchoolAccessLevel getAccessState( SchoolRoles sRole, SchoolActions sAction, 
                                          SchoolObjects sObject)
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolAccessLevel currentAccess = SchoolAccessLevel.NONE;
        ActionPolicy      currActionSet;
           
        // --- GET OBJECT
        currActionSet = (ActionPolicy)( m_roleMap.get( sRole ) );
            
        // --- IF ROLE IS VALID
        if( currActionSet != null ){
            // --- GET STATE
            currentAccess = currActionSet.getAccessState( sAction, sObject );
        }
        // --- ENDIF ROLE IS VALID
            
        // --- RETURN STATE
        return( currentAccess );
    }
    // --- END 
        
    void  setAccessState( SchoolRoles sRole, SchoolActions sAction, 
                          SchoolObjects sObject, SchoolAccessLevel newState)
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        ActionPolicy currActionSet;
            
        // --- IF ROLE IS VALID
            
            // --- SET STATE
            currActionSet = (ActionPolicy)( m_roleMap.get( sRole ) );
            currActionSet.setAccessState( sAction, sObject, newState );
         
        // --- ENDIF ROLE IS VALID            
    }
    // --- END 
        
    @Override
    public boolean handleAction( ActionEvent event )
    // --- BEGIN
    {
        // --- DECLARE LOCA VARIABLES
        boolean actionProcessed = false;

        // --- RETURN STATE
        return( actionProcessed );
    }
    // --- END 

    @Override
    public boolean handleEvent( String currEvent )
    // --- BEGIN
    {
        // --- DECLARE LOCA VARIABLES
        boolean eventProcessed = false;

        // --- RETURN STATE
        return( eventProcessed );
    }
    // --- END

    @Override
    public boolean handleQuery( String currEvent, ResultSet resultSet )
    // --- BEGIN
    {
         // --- DECLARE LOCAL VARIABLES
        boolean           queryProcessed = true;
        SchoolRoles       sRole    = SchoolRoles.UNKNOWN; 
        SchoolActions     sAction  = SchoolActions.UNKNOWN;
        SchoolObjects     sObject  = SchoolObjects.UNKNOWN; 
        SchoolAccessLevel newState = SchoolAccessLevel.NONE;
        int               dbVal    = 0;

        try {
            // --- DO FOR EACH POLICY
            while( resultSet.next() ){
                // --- READ POLICY
                dbVal    = resultSet.getInt("role_id");
                sRole    = SchoolRoles.fromValue( dbVal );
                dbVal    = resultSet.getInt( "action_id" );
                sAction  = SchoolActions.fromValue( dbVal );
                dbVal    = resultSet.getInt( "object_id" );
                sObject  = SchoolObjects.fromValue( dbVal );
                dbVal    = resultSet.getInt("policy_state");
                newState = SchoolAccessLevel.fromValue( dbVal );

                // --- STORE POLICY
               setAccessState( sRole, sAction, sObject, newState );
            }
            // --- ENDDO FOR EACH SCHOOL NAME
            
            // --- SET STATE
            m_hasPolicy = true;
        }
        catch(Exception e){
             System.out.println( "SQL Problem..." + e.getMessage() );
             queryProcessed = false;
        }

        // --- RETURN STATE
        return( queryProcessed );
   }
    // --- END 
    
    /**
     * communicates commands from the schoolhouse interface 
     *
     */
    public void handleCmd( String cmd, String cmdData, int cmdKey )
    // --- BEGIN
    {

    }
    // --- END
    

};