/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package schoolhouse.model;

import java.util.ArrayList;
import java.util.Iterator;
import schoolhouse.common.SchoolApp;

/*
 * @brief definition of a participant
 */
public class SchoolStudent extends SchoolPerson {
    protected ArrayList<SchoolPerson> m_parentList;
    protected ArrayList<SchoolPerson> m_watcherList;
    protected ArrayList<SchoolNotification> m_notificationList;


/**
 *
 * @brief constructor for main view
 */
    public SchoolStudent( )
    // --- BEGIN
    {
        // --- SATISFY SUBCLASS
        super( );
        
        // --- INITIALIZE CLASS
        m_parentList       = new ArrayList<>();
        m_watcherList      = new ArrayList<>();
        m_notificationList = new ArrayList<>();
        
    }
    // --- END
    
    /**
     * @param studentParent
     * @brief add parent of student
     */
    public void addParent( SchoolPerson studentParent )
    // --- BEGIN
    {
        // --- ADD PARENT TO STUDENT
        m_parentList.add( studentParent );
    }
    // --- END
       
    /**
     * @param studentWatcher
     * @brief add parent of student
     */
    public void addWatcher( SchoolPerson studentWatcher )
    // --- BEGIN
    {
        // --- ADD PARENT TO STUDENT
        m_watcherList.add( studentWatcher );
    }
    // --- END

    /**
     * @param newNotification
     * @brief add notification
     */
    public void addNotification( SchoolNotification newNotification )
    // --- BEGIN
    {
        // --- ADD PARENT TO STUDENT
        m_notificationList.add( newNotification );
    }
    // --- END

        
    /**
     * @return the m_user_status
     */
    public ArrayList getParentList() {
        return m_parentList;
    }

    /**
     * @param parentList the m_user_status to set
     */
    public void setParentList( ArrayList parentList) {
        this.m_parentList = parentList;
    }

    /**
     * @return the m_user_status
     */
    public ArrayList getWatcherList() {
        return m_watcherList;
    }

    /**
     * @brief default behavior is no associated students
     */
    @Override
    public void createStudentList( SchoolApp threadApp )
    // --- BEGIN
    {
        // --- THE ONLY ASSOCIATED STUDENT IS ITSELF
        m_studentList.clear();
        m_studentList.add( this );
    }
    // --- END


    /**
     * @param watcherList the m_user_status to set
     */
    public void setWatcherList( ArrayList watcherList) {
        this.m_watcherList = watcherList;
    } 
    
    /**
     * @return the m_notificationList
     */
    public ArrayList getNotificationList() {
        return m_notificationList;
    }

    /**
     * @param notificationList set the list
     */
    public void setNotificationList( ArrayList notificationList) {
        this.m_notificationList = notificationList;
    } 
    
    /**
     * 
     * @param markName
     * @return reference to mark data; null if not in class
     */
    public SchoolNotification getNotification( int notifyId )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolNotification returnObj    = null;
        Iterator           notifyIter   = m_notificationList.iterator();
        SchoolNotification notifyInfo;
        boolean            foundNotify  = false;
        
        // --- DO FOR EACH MARK IN THE CLASS
        while( (notifyIter.hasNext( )) && ( foundNotify == false) ){
            // --- GET DATA
            notifyInfo = (SchoolNotification)notifyIter.next( );
            
            // --- IF THERE IS A MATCH
            if( notifyInfo.getId() == notifyId ){
                // --- SET RETURN VALUE
                returnObj = notifyInfo;
                foundNotify = true;
            }
            // --- ENDIF THERE IS A MATCH
        }
        // --- ENDDO FOR EACH MARK IN THE CLASS
        
        // --- RETURN
        return( returnObj );
    }
    // --- END  
    

}
