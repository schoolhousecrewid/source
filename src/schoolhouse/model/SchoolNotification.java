/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package schoolhouse.model;

import schoolhouse.common.SchoolApp;
import schoolhouse.model.SchoolPerson;
import schoolhouse.model.SchoolStudent;

/**
 *
 * @author wpeets
 */
public class SchoolNotification extends DataObject {
    /**
     *
     * @author wpeets
     * @brief notification object
     */
    public enum SchoolNotifyType { FAILING(1), BORDERLINE(2),  PASSING(3),
                             GOOD(4),   EXCELLENT(5), OTHER(6), UNKNOWN(7);
                        
    private final int m_value;
    private static final int enumSize = SchoolNotifyType.values().length;

    /**
     * @brief constructor
     * 
     * This initializes values
     */
        private SchoolNotifyType( int value )
        // --- BEGIN
        {
            // --- SET VALUE
            this.m_value = value;
        }
        // --- END
        
        /**
         * @return total number of ordinal types
         * @brief size of enum
         * 
         * returns the size of enum
         */
        static public int getSize( ) {
            return( enumSize );
        } 
        
        /**
         * @return integer representation
         * @brief get int cast of enum
         * 
         * returns the ordinal representation of enum
         */
        public int getValue()
        // --- BEGIN
        {
            // --- RETURN INTEGER REPRESENTATION
            return( m_value );
        }       
        // --- END
        
        
        /**
         * @param value
         * @return enumeration of type
         * @brief allow conversion from ordinal value to enum object
         * 
         * This takes a value and returns an enumeration representation.
         */
        static public SchoolNotifyType fromValue( int value )
        // --- BEGIN
        { 
            // --- DECLARE LOCAL VVARIABLES
            SchoolNotifyType enumRep = null;
            
            // --- DO WHILE REPRESENTATION NOT FOUND
            for( SchoolNotifyType enumVal: SchoolNotifyType.values() ) { 
                // --- IF ORDINAL VALUE MATCHES
                if( enumVal.m_value == value) { 
                    // --- RETURN ENUM
                    return enumVal;  
                }  
                // --- ENDIF ORDINAL VALUE MATCHES
            }  
            // --- ENDDO WHILE REPRESENTATION NOT FOUND
  
            // --- RETURN
            return( enumRep );  
        }  
        // --- END
        /**
         * @param value
         * @return enumeration of type
         * @brief allow conversion from ordinal value to enum object
         * 
         * This takes a value and returns an enumeration representation.
         */
        static public SchoolNotifyType fromString( String value )
        // --- BEGIN
        {
            // --- DECLARE LOCAL VVARIABLES
            SchoolNotifyType enumRep;
            
            // --- DO CASE OF STRING
            switch( value ){
                // --- CASE OF A KNOWN TYPE
                case "FAILING":
                case "BORDERLINE":
                case "PASSING":
                case "GOOD":
                case "EXCELLENT":
                case "OTHER":
                    enumRep = SchoolNotifyType.valueOf( value );
                break;
                default:
                    enumRep = SchoolNotifyType.UNKNOWN;
                break;             
             }  
            // --- ENDDO CASE OF STRING
  
            // --- RETURN
            return( enumRep );  
        }  
        // --- END
    };

    /**
     *
     * @author wpeets
     * @brief methods of contact
     */
    public enum SchoolContactType { CELL(1), EMAIL(2),  UNKNOWN(3);
                        
    private final int m_value;
    private static final int enumSize = SchoolContactType.values().length;

    /**
     * @brief constructor
     * 
     * This initializes values
     */
        private SchoolContactType( int value )
        // --- BEGIN
        {
            // --- SET VALUE
            this.m_value = value;
        }
        // --- END
        
        /**
         * @return total number of ordinal types
         * @brief size of enum
         * 
         * returns the size of enum
         */
        static public int getSize( ) {
            return( enumSize );
        } 
        
        /**
         * @return integer representation
         * @brief get int cast of enum
         * 
         * returns the ordinal representation of enum
         */
        public int getValue()
        // --- BEGIN
        {
            // --- RETURN INTEGER REPRESENTATION
            return( m_value );
        }       
        // --- END
        
        
        /**
         * @param value
         * @return enumeration of type
         * @brief allow conversion from ordinal value to enum object
         * 
         * This takes a value and returns an enumeration representation.
         */
        static public SchoolContactType fromValue( int value )
        // --- BEGIN
        { 
            // --- DECLARE LOCAL VVARIABLES
            SchoolContactType enumRep = null;
            
            // --- DO WHILE REPRESENTATION NOT FOUND
            for( SchoolContactType enumVal: SchoolContactType.values() ) { 
                // --- IF ORDINAL VALUE MATCHES
                if( enumVal.m_value == value) { 
                    // --- RETURN ENUM
                    return enumVal;  
                }  
                // --- ENDIF ORDINAL VALUE MATCHES
            }  
            // --- ENDDO WHILE REPRESENTATION NOT FOUND
  
            // --- RETURN
            return( enumRep );  
        }  
        // --- END
        /**
         * @param value
         * @return enumeration of type
         * @brief allow conversion from ordinal value to enum object
         * 
         * This takes a value and returns an enumeration representation.
         */
        static public SchoolContactType fromString( String value )
        // --- BEGIN
        {
            // --- DECLARE LOCAL VVARIABLES
            SchoolContactType enumRep;
            
            // --- DO CASE OF STRING
            switch( value ){
                // --- CASE OF A KNOWN TYPE
                case "CELL":
                case "EMAIL":
                    enumRep = SchoolContactType.valueOf( value );
                break;
                default:
                    enumRep = SchoolContactType.UNKNOWN;
                break;             
             }  
            // --- ENDDO CASE OF STRING
  
            // --- RETURN
            return( enumRep );  
        }  
        // --- END
    };

    private SchoolStudent     m_student;
    private SchoolClass       m_schoolClass;
    private SchoolPerson      m_person;
    private SchoolNotifyType  m_type;
    private SchoolContactType m_contact;
    private String            m_criteria;
    private String            m_updatedContact;
    SchoolApp                 m_appHandle;
    
    /**
     * @param appHandle   framework pointer
     * @brief constructor
     * 
     * This initializes values
     */
    public SchoolNotification( SchoolApp appHandle )
    // --- BEGIN
    {
        // --- SET VALUE
        m_appHandle   = appHandle;
        m_type        = SchoolNotifyType.FAILING;
        m_contact     = SchoolContactType.CELL;
        m_person      = null;
        m_schoolClass = null;
        m_student     = null;
    }
    // --- END


    /**
     * @return the m_person the participant registered for notification
     */
    public SchoolPerson getWatcher() {
        return m_person;
    }

    /**
     * @param newWatcher set the participant for notification
     */
    public void setWatcher( SchoolPerson newWatcher ) {
        this.m_person = newWatcher;
    }

    /**
     * @return the m_schoolClass class pertaining to notification
     */
    public SchoolClass getSchoolClass() {
        return m_schoolClass;
    }

    /**
     * @param schoolClass the class applied to notification
     */
    public void setSchoolClass( SchoolClass schoolClass ) {
        this.m_schoolClass = schoolClass;
    }

    /**
     * @return the  student for grade
     */
    public SchoolStudent getStudent() {
        return m_student;
    }

    /**
     * @param student target student of notification
     */
    public void setStudent( SchoolStudent student ) {
        this.m_student = student;
    }

    /**
     * @return the m_criteria for notification
     */
    public String getCriteria() {
        return m_criteria;
    }

    /**
     * @param newCriteria criteria for notification
     */
    public void setCriteria( String newCriteria ) {
        this.m_criteria = newCriteria;
    }

    /**
     * @return the m_updatedContact for notification
     */
    public String getContact() {
        return m_updatedContact;
    }

    /**
     * @param newContact criteria for notification
     */
    public void setContact( String newContact ) {
        this.m_updatedContact = newContact;
    }

    /**
     * @return the m_type type of notification
     */
    public SchoolNotifyType getType() {
        return m_type;
    }

    /**
     * @param newType type of notification
     */
    public void setType( SchoolNotifyType newType ) {
        this.m_type = newType;
    }  
    
    /**
     * @return the m_type type of contact
     */
    public SchoolContactType getContactType() {
        return m_contact;
    }

    /**
     * @param newContactType type of Contact
     */
    public void setContactType( SchoolContactType newContactType ) {
        this.m_contact = newContactType;
    }        
}
