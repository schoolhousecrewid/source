/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package schoolhouse.model;

import schoolhouse.model.SchoolPerson;
import java.sql.Date;
import java.sql.Time;
import schoolhouse.common.SchoolApp;
import schoolhouse.model.SchoolNotification.SchoolContactType;
import schoolhouse.model.SchoolNotification.SchoolNotifyType;

/**
 *
 * @author wpeets
 */
public class SchoolMessage extends DataObject {
    /**
     *
     * @author wpeets
     * @brief message notification type
     */
    public enum SchoolMessageType { NOTIFICATION(1), GENERAL(2), UNKNOWN(3);
                        
        private final int m_value;
        private static final int enumSize = SchoolMessageType.values().length;

        /**
         * @brief constructor
         * 
         * This initializes values
         */
        private SchoolMessageType( int value )
        // --- BEGIN
        {
            // --- SET VALUE
            this.m_value = value;
        }
        // --- END
        
        /**
         * @return total number of ordinal types
         * @brief size of enum
         * 
         * returns the size of enum
         */
        static public int getSize( ) {
            return( enumSize );
        } 
        
        /**
         * @return integer representation
         * @brief get int cast of enum
         * 
         * returns the ordinal representation of enum
         */
        public int getValue()
        // --- BEGIN
        {
            // --- RETURN INTEGER REPRESENTATION
            return( m_value );
        }       
        // --- END
        
        
        /**
         * @param value
         * @return enumeration of type
         * @brief allow conversion from ordinal value to enum object
         * 
         * This takes a value and returns an enumeration representation.
         */
        static public SchoolMessageType fromValue( int value )
        // --- BEGIN
        { 
            // --- DECLARE LOCAL VVARIABLES
            SchoolMessageType enumRep = null;
            
            // --- DO WHILE REPRESENTATION NOT FOUND
            for( SchoolMessageType enumVal: SchoolMessageType.values() ) { 
                // --- IF ORDINAL VALUE MATCHES
                if( enumVal.m_value == value) { 
                    // --- RETURN ENUM
                    return enumVal;  
                }  
                // --- ENDIF ORDINAL VALUE MATCHES
            }  
            // --- ENDDO WHILE REPRESENTATION NOT FOUND
  
            // --- RETURN
            return( enumRep );  
        }  
        // --- END
        /**
         * @param value
         * @return enumeration of type
         * @brief allow conversion from ordinal value to enum object
         * 
         * This takes a value and returns an enumeration representation.
         */
        static public SchoolMessageType fromString( String value )
        // --- BEGIN
        {
            // --- DECLARE LOCAL VVARIABLES
            SchoolMessageType enumRep;
            
            // --- DO CASE OF STRING
            switch( value ){
                // --- CASE OF A KNOWN TYPE
                case "NOTIFICATION":
                case "GENERAL":
                    enumRep = SchoolMessageType.valueOf( value );
                break;
                default:
                    enumRep = SchoolMessageType.UNKNOWN;
                break;             
            }  
            // --- ENDDO CASE OF STRING
  
            // --- RETURN
            return( enumRep );  
        }  
        // --- END
    };
    
    public enum SchoolMessageState { READ(1), UNREAD(2), UNKNOWN(3);
                        
        private final int m_value;
        private static final int enumSize = SchoolMessageState.values().length;

        /**
         * @brief constructor
         * 
         * This initializes values
         */
        private SchoolMessageState( int value )
        // --- BEGIN
        {
            // --- SET VALUE
            this.m_value = value;
        }
        // --- END
        
        /**
         * @return total number of ordinal types
         * @brief size of enum
         * 
         * returns the size of enum
         */
        static public int getSize( ) {
            return( enumSize );
        } 
        
        /**
         * @return integer representation
         * @brief get int cast of enum
         * 
         * returns the ordinal representation of enum
         */
        public int getValue()
        // --- BEGIN
        {
            // --- RETURN INTEGER REPRESENTATION
            return( m_value );
        }       
        // --- END
        
        
        /**
         * @param value
         * @return enumeration of type
         * @brief allow conversion from ordinal value to enum object
         * 
         * This takes a value and returns an enumeration representation.
         */
        static public SchoolMessageState fromValue( int value )
        // --- BEGIN
        { 
            // --- DECLARE LOCAL VVARIABLES
            SchoolMessageState enumRep = null;
            
            // --- DO WHILE REPRESENTATION NOT FOUND
            for( SchoolMessageState enumVal: SchoolMessageState.values() ) { 
                // --- IF ORDINAL VALUE MATCHES
                if( enumVal.m_value == value) { 
                    // --- RETURN ENUM
                    return enumVal;  
                }  
                // --- ENDIF ORDINAL VALUE MATCHES
            }  
            // --- ENDDO WHILE REPRESENTATION NOT FOUND
  
            // --- RETURN
            return( enumRep );  
        }  
        // --- END
        /**
         * @param value
         * @return enumeration of type
         * @brief allow conversion from ordinal value to enum object
         * 
         * This takes a value and returns an enumeration representation.
         */
        static public SchoolMessageState fromString( String value )
        // --- BEGIN
        {
            // --- DECLARE LOCAL VVARIABLES
            SchoolMessageState enumRep;
            
            // --- DO CASE OF STRING
            switch( value ){
                // --- CASE OF A KNOWN TYPE
                case "READ":
                case "UNREAD":
                    enumRep = SchoolMessageState.valueOf( value );
                break;
                default:
                    enumRep = SchoolMessageState.UNKNOWN;
                break;             
            }  
            // --- ENDDO CASE OF STRING
  
            // --- RETURN
            return( enumRep );  
        }  
        // --- END
    };
   
    
    private SchoolPerson       m_source;
    private SchoolPerson       m_destination;
    private Date               m_dateSent;
    private Time               m_timeSent;
    private SchoolMessageType  m_type;
    private SchoolContactType  m_contactType;
    private String             m_messagePath;
    private SchoolMessageState m_state;
    private String             m_subject;
    private String             m_message;
    SchoolApp                  m_appHandle;
    
    /**
     * @param appHandle   framework pointer
     * @brief constructor
     * 
     * This initializes values
     */
    public SchoolMessage( SchoolApp appHandle )
    // --- BEGIN
    {
        // --- SET VALUE
        m_appHandle      = appHandle;
    }
    // --- END


    /**
     * @return the m_source the participant sending message
     */
    public SchoolPerson getSource() {
        return m_source;
    }

    /**
     * @param newSource set the participant sending message
     */
    public void setSource( SchoolPerson newSource ) {
        this.m_source = newSource;
    }

    /**
     * @return the m_destination the participant receiving message
     */
    public SchoolPerson getDestination() {
        return m_destination;
    }

    /**
     * @param newDestination set the participant for receiving message
     */
    public void setDestination( SchoolPerson newDestination ) {
        this.m_destination = newDestination;
    }

    /**
     * @return the message date
     */
    public Date getDate() {
        return m_dateSent;
    }

    /**
     * @param newDate set message date
     */
    public void setDate( Date newDate ) {
        this.m_dateSent = newDate;
    }

    /**
     * @return the time sent
     */
    public Time getTime() {
        return m_timeSent;
    }

    /**
     * @param newTime set time sent
     */
    public void setTime( Time newTime ) {
        this.m_timeSent = newTime;
    }

    /**
     * @return the subject of the message
     */
    public String getSubject() {
        return m_subject;
    }

    /**
     * @param newSubject a new subject
     */
    public void setSubject( String newSubject ) {
        this.m_subject = newSubject;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return m_message;
    }

    /**
     * @param newMessage new message
     */
    public void setMessage( String newMessage ) {
        this.m_message = newMessage;
    }

    /**
     * @return the message
     */
    public String getPath() {
        return m_messagePath;
    }

    /**
     * @param newPath new message
     */
    public void setPath( String newPath ) {
        this.m_messagePath = newPath;
    }

    /**
     * @return the m_type type of notification
     */
    public SchoolMessageType getMsgType() {
        return m_type;
    }

    /**
     * @param newType type of notification
     */
    public void setMsgType( SchoolMessageType newType ) {
        this.m_type = newType;
    }        
    /**
     * @return the m_type type of notification
     */
    public SchoolContactType getNotifyType() {
        return m_contactType;
    }

    /**
     * @param newType type of notification
     */
    public void setNotifyType( SchoolContactType newType ) {
        this.m_contactType = newType;
    }        

   /**
     * @return the m_state type of notification
     */
    public SchoolMessageState getMsgState() {
        return m_state;
    }

    /**
     * @param newState type of notification
     */
    public void setMsgState( SchoolMessageState newState ) {
        this.m_state = newState;
    }        
}
