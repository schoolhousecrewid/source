/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package schoolhouse.model;

import schoolhouse.model.SchoolPerson;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.sql.ResultSet;
import java.util.Iterator;
import schoolhouse.common.DbEngine;
import schoolhouse.common.HandleCmdAction;
import schoolhouse.common.QueryChain;
import schoolhouse.common.QueryThread;
import schoolhouse.common.SchoolApp;
import schoolhouse.common.SchoolDbIntfc;
import schoolhouse.common.SchoolDbIntfc.SchoolQuery;

/**
 *
 * @author wpeets
 * 
 * @brief defines objects used within application for data manipulation
 */
public class SchoolHouse extends DataObject implements HandleCmdAction {
    
    private String                   m_schoolName;
    private String                   m_schoolAddress;
    private ArrayList<SchoolPerson>  m_adminList;
    private ArrayList<SchoolCourse>  m_courseList;
    private ArrayList<SchoolMessage> m_messageList;
    
    /*
     * @brief used to interface with database
    */
    QueryChain             m_dbChain;     // --- USED FOR MASSIVE QUERIES
    QueryThread            m_dbCommand;   // --- USED TO INSERT INTO DATABASE
    SchoolApp              m_appHandle;
    

    /**
     * @param appHandle
     * @brief constructor
     */
    public SchoolHouse(  SchoolApp appHandle )
    // --- BEGIN
    {
        // --- INITIALIZE CLASS
        m_adminList  = new ArrayList<SchoolPerson>();
        m_courseList = new ArrayList<SchoolCourse>();
        m_appHandle  = appHandle;
    }
    // --- END
    
     /**
     * @brief add admin to school data
     */
    public void addAdmin( SchoolPerson adminToAdd )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        Iterator     personIter   = m_adminList.iterator();
        SchoolPerson personInfo;
        boolean      adminFound   = false;
        
        // --- DO FOR EACH SCHOOL IN THE MODEL
        while( personIter.hasNext( ) ) {
            // --- GET DATA
            personInfo = (SchoolPerson)personIter.next( );
            
            // --- IF THERE IS A MATCH
            if( personInfo.getId() == adminToAdd.getId() ){
                // --- SET RETURN VALUE
                adminFound = true;
            }
            // --- ENDIF THERE IS A MATCH
        }
        // --- ENDDO FOR EACH SCHOOL IN THE MODEL
        
        // --- IF NOT ALREADY ON THE LIST
        if( adminFound == false ){
            // --- ADD
            m_adminList.add( adminToAdd );
        }
        // --- ENDIF NOT ALREADY ON THE LIST
    }
    // --- END
            
    /**
     * @return the m_schoolName
     */
    public String getName() {
        return m_schoolName;
    }

    /**
     * @param schoolName the m_schoolName to set
     */
    public void setName(String schoolName) {
        this.m_schoolName = schoolName;
    }

    /**
     * @return the m_schoolAddress
     */
    public String getAddress() {
        return m_schoolAddress;
    }

    /**
     * @param schoolAddress the m_schoolAddress to set
     */
    public void setAddress(String schoolAddress) {
        this.m_schoolAddress = schoolAddress;
    }

    /**
     * @return the m_courseList
     */
    public ArrayList<SchoolCourse> getCourseList() {
        return m_courseList;
    }

    /**
     * @param courseList the m_courseList to set
     */
    public void setCourseList(ArrayList<SchoolCourse> courseList) {
        this.m_courseList = courseList;
    }
    
    public void addCourse( String courseName )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        String       insertString;
        SchoolQuery  courseQuery = SchoolDbIntfc.SchoolQuery.COURSE_ID;
        Integer      schoolId = getId();
        
        // --- DEFINE STRING
        insertString = "INSERT INTO coursetable "
                             + "(school_id, course_name) "
                             + "VALUES ("
                             + "'"+ schoolId.toString()      + "',"
                             + "'"+ courseName    + "'"
                             + ")";
        m_dbCommand = new QueryThread( m_appHandle );
        m_dbCommand.setUpdate( insertString, "insert" );
        m_dbCommand.start();  
        
        // --- QUERY TO GET THE NEW ID
        m_dbChain = new QueryChain( m_appHandle, true );
        m_dbChain.setHandler( this );
        courseQuery.dbQueryString( courseName, 0 );
        m_dbChain.addQuery( courseQuery.dbQuery(), courseQuery.dbName()  );
        m_dbChain.start();
    }
    // --- END
    
    public void addCourse( SchoolCourse newCourse )
    // --- BEGIN
    {
        // --- ADD TO DATABASE
        // --- ...TBD check for duplicate
        m_courseList.add( newCourse );
    }
    // --- END
    
    /**
     * 
     * @param courseId
     * @return reference to course data; null if not in model
     */
    public SchoolCourse getCourse( Integer courseId )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolCourse returnCourse  = null;
        Iterator     courseIter    = m_courseList.iterator();
        SchoolCourse courseInfo;
        
        // --- DO FOR EACH COURSE IN THE MODEL
        while( courseIter.hasNext( ) ) {
            // --- GET DATA
            courseInfo = (SchoolCourse)courseIter.next( );
            
            // --- IF THERE IS A MATCH
            if( courseInfo.getId() == courseId ){
                // --- SET RETURN VALUE
                // --- ... TBD should probably break out here
                returnCourse = courseInfo;
            }
            // --- ENDIF THERE IS A MATCH
        }
        // --- ENDDO FOR EACH COURSE IN THE MODEL
        
        // --- RETURN
        return( returnCourse );
    }
    // --- END  
    
    
    /**
     * 
     * @param courseName
     * @return reference to course data; null if not in model
     */
    public SchoolCourse getCourse( String courseName )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        SchoolCourse returnCourse  = null;
        Iterator     courseIter    = m_courseList.iterator();
        SchoolCourse courseInfo;
        
        // --- DO FOR EACH CLASS IN THE MODEL
        while( courseIter.hasNext( ) ) {
            // --- GET DATA
            courseInfo = (SchoolCourse)courseIter.next( );
            // --- IF THERE IS A MATCH
            if( courseInfo.getName().equals( courseName ) ){
                // --- SET RETURN VALUE
                // --- ... TBD should probably break out here
                returnCourse = courseInfo;
            }
            // --- ENDIF THERE IS A MATCH
        }
        // --- ENDDO FOR EACH CLASS IN THE MODEL
        
        // --- RETURN
        return( returnCourse );
    }
    // --- END  

    /**
     * @return the m_adminList
     */
    public ArrayList<SchoolPerson> getAdminList() {
        return m_adminList;
    }

    /**
     * @param m_adminList the m_adminList to set
     */
    public void setAdminList(ArrayList<SchoolPerson> m_adminList) {
        this.m_adminList = m_adminList;
    }

    @Override
    public boolean handleAction(ActionEvent event) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean handleEvent(String currEvent) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean handleQuery( String currEvent, ResultSet resultSet )
    // --- BEGIN
    {
        // --- DECLARE LOCAL VARIABLES
        int          descriptorIndex = currEvent.indexOf( '_' );
        String       messageType     = null;
        String       descriptor      = null;
        Integer      descriptorVal   = null;
        boolean      eventProcessed  = false;
        Integer      schoolId;
        Integer      courseId;
        String       courseName;
        SchoolCourse courseInfo;

        // --- IF DATA IS SPECIFIC
        if( descriptorIndex != -1 ){
            // --- EXTRACT MESSAGE TYPE
            messageType = currEvent.substring(0, descriptorIndex );
            descriptor  = currEvent.substring(descriptorIndex, currEvent.length() );
            descriptorVal = new Integer( descriptor );
        }
        // --- ELSE
        else {
            // --- USE THE WHOLE STRING
            messageType = currEvent;
        }
        // --- ENDIF DATA IS SPECIFIC
        
        try {
            // --- DO CASE OF QUERY TYPE
            switch( messageType ){
                // --- CASE THE SCHOOL DATA IS RETRIEVED
                case DbEngine.COURSE_ID:
                    // --- DO FOR EACH COURSE ID
                    // --- ... there should only be one; just copied loop
                    // --- ... if course info is coming in, the course exists
                    while( resultSet.next() ){
                        // --- SAVE NAME IN SCHOOL LIST
                        schoolId      = resultSet.getInt( "school_id" );
                        courseId      = resultSet.getInt( "course_id" );
                        courseName    = resultSet.getString( "school_name" );
                        courseInfo    = getCourse( courseName );
        
                        // --- IF COURSE IS IN THE LIST
                        if( courseInfo != null ){
                            // --- GET DATA
                            courseInfo.setId( courseId );
                        }
                         // --- ENDIF COURSE IS IN THE LIST
                    }
                    // --- ENDDO FOR EACH COURSE ID
                break;
                default:
                    // --- DO NOTHING
                break;
            };
            // --- ENDDO CASE OF QUERY TYPE
        }
        catch(Exception e){
            // --- IF THE DATA IS EMPTY
            if( e.getMessage() == "null" ){
                // --- LET IT PASS
                eventProcessed = true;
            }
            // --- ELSE
            else {
                System.out.println( "SQL Problem..." + e.getMessage() );
                eventProcessed = false;
            }
        }
        
        // --- RETURN
        return( eventProcessed );
    }
    // --- END
    

    @Override
    public void handleCmd(String cmd, String cmdData, int cmdKey) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    /**
     * @brief roles supported by schoolhouse
     * 
     * This application will enforce behavior based on known
     * roles
     */
    public enum MarkerTypes { QUIZ(1),     TEST(2),           ATTENDANCE(3), 
                              LAB(4),      HOMEWORK(5),       POP_QUIZ(6), 
                              IN_CLASS(7), OPEN_BOOK_TEST(8), OTHER(9);

        private        final int m_value;
        private static final int enumSize = MarkerTypes.values().length;

        /**
         * @brief constructor
         * 
         * This initializes values
         */
        private MarkerTypes( int value )
        // --- BEGIN
        {
            // --- SET VALUE
            this.m_value = value;
        }
        // --- END

        /**
         * @brief size of enum
         * 
         * returns the size of enum
         */
        static int getSize( ) {
            return( enumSize );
        } 

        /**
         * @brief get int cast of enum
         * 
         * returns the ordinal representation of enum
         */
        public int getValue()
        // --- BEGIN
        {
            // --- RETURN INTEGER REPRESENTATION
            return( m_value );
        }       
        // --- END

        /**
         * @brief allow conversion from ordinal value to enum object
         * 
         * This takes a value and returns an enumeration representation.
         */
        static MarkerTypes fromValue( int value )
        // --- BEGIN
        { 
            // --- DECLARE LOCAL VVARIABLES
            MarkerTypes enumRep = null;

            // --- DO WHILE REPRESENTATION NOT FOUND
            for( MarkerTypes enumVal: MarkerTypes.values() ) { 
                // --- IF ORDINAL VALUE MATCHES
                if( enumVal.m_value == value) { 
                    // --- RETURN ENUM
                    return enumVal;  
                }  
                // --- ENDIF ORDINAL VALUE MATCHES
            }  
            // --- ENDDO WHILE REPRESENTATION NOT FOUND

            // --- RETURN
            return( enumRep );  
        }  
        // --- END
    };
}
