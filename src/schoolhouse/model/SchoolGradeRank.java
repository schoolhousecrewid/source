/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package schoolhouse.model;

import schoolhouse.common.SchoolApp;
import schoolhouse.common.SchoolApp;



/**
 *
 * @author wpeets
 */
public class SchoolGradeRank extends DataObject {
    private String    m_rankName;
    private int       m_rankLevel;
    private float     m_rankPoints;
            SchoolApp m_appHandle;
    
    /**
     * @brief constructor
     * 
     * This initializes values
     */
    public SchoolGradeRank( SchoolApp appHandle )
    // --- BEGIN
    {
        // --- SET VALUE
        m_appHandle = appHandle;
    }
    // --- END


    /**
     * @return the m_rankName
     */
    public String getName() {
        return m_rankName;
    }

    /**
     * @param rankName rank tag
     */
    public void setName( String rankName ) {
        this.m_rankName = rankName;
    }

    /**
     * @return the m_rankLevel
     */
    public int getLevel() {
        return m_rankLevel;
    }

    /**
     * @param rankLevel min grade
     * TBD may not be needed
     */
    public void setLevel( int rankLevel ) {
        this.m_rankLevel = rankLevel;
    }

    /**
     * @return the  rankPoints for grade
     */
    public float getPoints() {
        return m_rankPoints;
    }

    /**
     * @param rankPoints who the grade applies to
     */
    public void setPoints( float rankPoints ) {
        this.m_rankPoints = rankPoints;
    }        
}
